#ifndef __CUDA_CONSTANTS__CUH
#define __CUDA_CONSTANTS__CUH

#define BLOCKSIZE_KERNEL 128
#define BLOCKSIZE_TRANSFER 256
#define MAXIMUM_GRID_DIM 65535
#define TC float
#define NGLLC 5
#define NGLL2DC 25
#define NGLL3DC 125
#define NGLL3DC_PADDED 128

/* Define to a macro mangling the given C identifier (in lower and upper
case), which must not contain underscores, for linking with Fortran. */
#define FC_FUNC(name,NAME) name ## _

/* As FC_FUNC, but for C identifiers containing underscores. */
#define FC_FUNC_(name,NAME) name ## _

#define PI 3.14159265359

#define N_SLS 3

#endif