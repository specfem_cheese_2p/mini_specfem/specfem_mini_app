
#include "specfem_gpu.hpp"

//  ----------------------------------------------
extern "C"
void FC_FUNC_(init_specfemgpu,
	          INIT_SPECFEMGPU)(long* specfemgpu_pointer){
  SpecfemGPU<TC> *sp = new SpecfemGPU<TC>();
  *specfemgpu_pointer = (long)sp;
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(destroy_specfemgpu,
	          DESTROY_SPECFEMGPU)(long* specfemgpu_pointer){
  SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
  //sp->free_memory();
  delete sp;
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_set_size,
SPECFEMGPU_SET_SIZE)(long* specfemgpu_pointer, int* nspec, int* nglob) {
SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
int NSPEC = *nspec;
int NGLOB = *nglob;
sp->set_size(NSPEC, NGLOB);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mesh,
SPECFEMGPU_TRANSFERT_MESH)(long* specfemgpu_pointer, TC* fc_dxi_dx, TC* fc_dxi_dy, TC* fc_dxi_dz,
                                                     TC* fc_deta_dx, TC* fc_deta_dy, TC* fc_deta_dz,
                                                     TC* fc_dgamma_dx, TC* fc_dgamma_dy, TC* fc_dgamma_dz) {
SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
sp->tranfert_mesh(fc_dxi_dx, fc_dxi_dy, fc_dxi_dz,
                  fc_deta_dx, fc_deta_dy, fc_deta_dz,
                  fc_dgamma_dx, fc_dgamma_dy, fc_dgamma_dz);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mass,
SPECFEMGPU_TRANSFERT_MASS)(long* specfemgpu_pointer, TC* fc_inv_mass){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_inv_mass_matrix(fc_inv_mass);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_fields,
SPECFEMGPU_TRANSFERT_FIELDS)(long* specfemgpu_pointer, TC* fc_displ, TC* fc_veloc, TC* fc_accel){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_fields(fc_displ, fc_veloc, fc_accel);   
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_model,
SPECFEMGPU_TRANSFERT_MODEL)(long* specfemgpu_pointer, TC* fc_rho, TC* fc_kappa, TC* fc_mu){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_model(fc_rho, fc_kappa, fc_mu);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_attenuation_parameters,
SPECFEMGPU_TRANSFERT_ATTENUATION_PARAMETERS)(long* specfemgpu_pointer, 
TC* fc_alphaval, TC* fc_betaval, TC* fc_gammaval,
TC* fc_factor_common_mu, TC* fc_factor_common_kappa){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_attenuation_parameters(fc_alphaval, fc_betaval, fc_gammaval,
    fc_factor_common_mu, fc_factor_common_kappa);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_ibool,
SPECFEMGPU_TRANSFERT_IBOOL)(long* specfemgpu_pointer, int* fc_ibool){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_ibool(fc_ibool);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_phase_inner,
SPECFEMGPU_TRANSFERT_PHASE_INNER)(long* specfemgpu_pointer, 
int* fc_phase_ispec_inner_elastic, int* fc_nspec_outer_elastic, int* fc_nspec_inner_elastic)
{
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nspec_outer_elastic = *fc_nspec_outer_elastic;
    int nspec_inner_elastic = *fc_nspec_inner_elastic;
    sp->transfert_phase_inner(fc_phase_ispec_inner_elastic, 
    nspec_outer_elastic, nspec_inner_elastic);   
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_boundary,
SPECFEMGPU_TRANSFERT_BOUNDARY)(long* specfemgpu_pointer, int* fc_index_gll_boundary,
 TC* fc_wstacey, TC* fc_rho_vp, TC* fc_rho_vs, TC* fc_normal, int* fc_ngll_boundary){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int ngll_boundary = * fc_ngll_boundary;
    sp->transfert_boundary(fc_index_gll_boundary, fc_wstacey, 
    fc_rho_vp, fc_rho_vs, fc_normal, ngll_boundary);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_source,
SPECFEMGPU_TRANSFERT_SOURCE)(long* specfemgpu_pointer, int* fc_ispec_source, 
TC* fc_Fx, TC* fc_Fy, TC* fc_Fz, TC* fc_hxis, TC* fc_hetas, TC* fc_hgammas, 
TC* fc_used_stf, int* fc_nsrc, int* fc_nt){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nsrc = *fc_nsrc;
    int nt = *fc_nt; 
    sp->transfert_source(fc_ispec_source, fc_Fx, fc_Fy, fc_Fz, 
    fc_hxis, fc_hetas, fc_hgammas, fc_used_stf, nsrc, nt);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_receiver,
SPECFEMGPU_TRANSFERT_RECEIVER)(long* specfemgpu_pointer, int* fc_ispec_rec, 
TC* fc_hxir, TC* fc_hetar, TC* fc_hgammar, TC* fc_seismogram_d, int* fc_nrec, int* fc_nstep) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nrec= *fc_nrec;
    int nstep=*fc_nstep;
    sp->transfert_receiver(fc_ispec_rec, fc_hxir, fc_hetar, fc_hgammar, fc_seismogram_d, nrec, nstep);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_reference_element,
SPECFEMGPU_TRANSFERT_REFERENCE_ELEMENT)(long* specfemgpu_pointer, 
TC* fc_wgllwgll_xy, TC* fc_hprime_wgll_xx, TC* fc_hprime_xxT){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_reference_element(fc_wgllwgll_xy, fc_hprime_wgll_xx, fc_hprime_xxT);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_deltat,
SPECFEMGPU_TRANSFERT_DELTAT)(long* specfemgpu_pointer, TC* fc_dt){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    TC dt = *fc_dt;
    sp->transfert_deltat(dt);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mpi_boundary,
SPECFEMGPU_TRANSFERT_MPI_BOUNDARY)(long* specfemgpu_pointer, int* fc_ibool_interfaces_ext, 
int* fc_nibool_interfaces_ext, int* fc_nb_interfaces, int* fc_nspec_interfaces_max, 
int* fc_max_nibool_interfaces_ext){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nb_interfaces = *fc_nb_interfaces;
    int max_nibool_interfaces_ext = *fc_max_nibool_interfaces_ext;
    int nspec_interfaces_max = *fc_nspec_interfaces_max;
    //std::cout <<" max_nibool_interfaces_ext "  << max_nibool_interfaces_ext << std::endl; 
    sp->transfert_mpi_boundary(fc_ibool_interfaces_ext, fc_nibool_interfaces_ext, 
    nb_interfaces, max_nibool_interfaces_ext, nspec_interfaces_max);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_print,
SPECFEMGPU_PRINT) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->print_this();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_displ,
SPECFEMGPU_GET_DISPL) (long* specfemgpu_pointer, TC* fc_displ) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_displ(fc_displ);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_veloc,
SPECFEMGPU_GET_VELOC) (long* specfemgpu_pointer, TC* fc_veloc) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_veloc(fc_veloc);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_accel,
SPECFEMGPU_GET_ACCEL) (long* specfemgpu_pointer, TC* fc_accel) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_accel(fc_accel);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_set_accel,
SPECFEMGPU_SET_ACCEL) (long* specfemgpu_pointer, TC* fc_accel) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->set_accel(fc_accel);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_seismogram,
SPECFEMGPU_GET_SEISMOGRAM) (long* specfemgpu_pointer, TC* fc_seismogram) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_seismogram_d(fc_seismogram);
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_apply_inv_mass,
SPECFEMGPU_APPLY_INV_MASS) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->apply_inv_matrix();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_predictor,
SPECFEMGPU_PREDICTOR) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->predictor();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_corrector,
SPECFEMGPU_CORRECTOR) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->corrector();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_rhs,
SPECFEMGPU_RHS) (long* specfemgpu_pointer, int* fc_iphase) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int iphase = *fc_iphase;
    sp->compute_rhs(iphase);
    //sp->test_rhs(iphase);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_add_source_force,
SPECFEMGPU_ADD_SOURCE_FORCE) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->add_source_force();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_store_seismogram,
SPECFEMGPU_STORE_SEISMOGRAM) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->store_seismogram();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_compute_stacey,
SPECFEMGPU_COMPUTE_STAYCEY) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->compute_stacey();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_prepare_mpi_buffer,
SPECFEMGPU_PREPARE_MPI_BUFFER) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->prepare_mpi_buffer();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_store_mpi_buffer,
SPECFEMGPU_STORE_MPI_BUFFER) (long* specfemgpu_pointer, TC* fc_mpi_buffer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_mpi_buffer(fc_mpi_buffer);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_recv_mpi_buffer,
SPECFEMGPU_GET_RECV_MPI_BUFFER) (long* specfemgpu_pointer, TC* fc_mpi_buffer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_recv_mpi_buffer(fc_mpi_buffer);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_assemble_mpi_accel,
SPECFEMGPU_ASSEMBLE_MPI_ACCEL) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->assemble_mpi_accel();
}
