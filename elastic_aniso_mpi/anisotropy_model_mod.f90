module anisotropy_model 
    use config_mod

    implicit none

    
    real(kind=CUSTOM_REAL), private, dimension(6,6) :: rot_TTI, rot_TTI_transpose
    real(kind=CUSTOM_REAL), public, dimension(6,6) :: c_vti, c_tti
    real(kind=CUSTOM_REAL), private, parameter :: PI_cr=3.14159265359

    public :: get_rotation_TTI, get_thomsen2Cij, vti2tti, display_aniso

contains 

    subroutine get_rotation_TTI(azimuth, tilt)

        real(kind=CUSTOM_REAL), intent(in) :: azimuth, tilt
        real(kind=CUSTOM_REAL) :: cos_phi, sin_phi  !! azimuth 
        real(kind=CUSTOM_REAL) :: cos_teta, sin_teta !! tilt
        real(kind=CUSTOM_REAL) :: cos_2phi, sin_2phi  !! azimuth 
        real(kind=CUSTOM_REAL) :: cos_2teta, sin_2teta !! tilt
        real(kind=CUSTOM_REAL), parameter :: alf=0.5_CUSTOM_REAL

        cos_phi  = cos(azimuth * PI_cr / 180._CUSTOM_REAL)
        sin_phi  = sin(azimuth * PI_cr / 180._CUSTOM_REAL)
        cos_teta = cos(tilt * PI_cr / 180._CUSTOM_REAL)
        sin_teta = sin(tilt * PI_cr / 180._CUSTOM_REAL)

        cos_2phi  = cos(2._CUSTOM_REAL * azimuth * PI_cr / 180._CUSTOM_REAL)
        sin_2phi  = sin(2._CUSTOM_REAL * azimuth * PI_cr / 180._CUSTOM_REAL)
        cos_2teta = cos(2._CUSTOM_REAL * tilt * PI_cr / 180._CUSTOM_REAL)
        sin_2teta = sin(2._CUSTOM_REAL * tilt * PI_cr / 180._CUSTOM_REAL)
        
        rot_TTI(1,1) = cos_phi*cos_phi
        rot_TTI(1,2) = sin_phi*sin_phi*cos_teta*cos_teta
        rot_TTI(1,3) = sin_phi*sin_phi*sin_teta*sin_teta
        rot_TTI(1,4) = -sin_phi*sin_phi*sin_2teta
        rot_TTI(1,5) =  sin_2phi*sin_teta
        rot_TTI(1,6) = -sin_2phi*cos_teta

        rot_TTI(2,1) = sin_phi*sin_phi
        rot_TTI(2,2) = cos_phi*cos_phi*cos_teta*cos_teta
        rot_TTI(2,3) = cos_phi*cos_phi*sin_teta*sin_teta
        rot_TTI(2,4) = -cos_phi*cos_phi*sin_2teta
        rot_TTI(2,5) = -sin_2phi*sin_teta
        rot_TTI(2,6) =  sin_2teta*cos_phi

        rot_TTI(3,1) = 0._CUSTOM_REAL
        rot_TTI(3,2) = sin_teta*sin_teta
        rot_TTI(3,3) = cos_teta*cos_teta
        rot_TTI(3,4) = sin_2teta
        rot_TTI(3,5) = 0._CUSTOM_REAL
        rot_TTI(3,6) = 0._CUSTOM_REAL

        rot_TTI(4,1) = 0._CUSTOM_REAL
        rot_TTI(4,2) = alf*cos_phi*sin_2teta
        rot_TTI(4,3) = -alf*cos_phi*sin_2teta
        rot_TTI(4,4) = cos_phi*cos_2teta
        rot_TTI(4,5) = sin_phi*cos_teta
        rot_TTI(4,6) = sin_phi*sin_teta

        rot_TTI(5,1) = 0._CUSTOM_REAL
        rot_TTI(5,2) = -alf*sin_phi*sin_2teta
        rot_TTI(5,3) = alf*sin_phi*sin_2teta
        rot_TTI(5,4) = -sin_phi*cos_2teta
        rot_TTI(5,5) = cos_phi*cos_teta
        rot_TTI(5,6) = cos_phi*sin_phi

        rot_TTI(6,1) = alf*sin_2phi
        rot_TTI(6,2) = -alf*sin_2phi*cos_teta*cos_teta
        rot_TTI(6,3) = -alf*sin_2phi*sin_teta*sin_teta
        rot_TTI(6,4) = alf*sin_2phi*sin_teta*sin_teta
        rot_TTI(6,5) = -cos_2phi*sin_teta
        rot_TTI(6,6) = cos_2phi*cos_teta

        rot_TTI_transpose = transpose(rot_TTI)
    end subroutine get_rotation_TTI


    subroutine get_thomsen2Cij(rho, vp, vs, ep, gm, de)
        real(kind=CUSTOM_REAL), intent(in) :: rho, vp, vs, ep, gm, de
        integer :: i,j
        c_vti(1,1) = (1._CUSTOM_REAL+2._CUSTOM_REAL*ep)* rho * vp * vp
        c_vti(1,2) = -2*(2*gm+1)*vs*vs*rho + (1._CUSTOM_REAL+2._CUSTOM_REAL*ep)* rho * vp * vp
        c_vti(1,3) =  rho * (sqrt( (vp**2-vs**2)*((1+2*de)*vp**2-vs**2)) - vs**2)
        c_vti(1,4) =  0._CUSTOM_REAL
        c_vti(1,5) =  0._CUSTOM_REAL
        c_vti(1,6) =  0._CUSTOM_REAL
        c_vti(2,2) = (1._CUSTOM_REAL+2._CUSTOM_REAL*ep)* rho * vp * vp
        c_vti(2,3) = rho * ( sqrt( (vp**2-vs**2) * (  (1+2*de)*vp**2-vs**2) ) - vs**2 )
        c_vti(2,4) =  0._CUSTOM_REAL
        c_vti(2,5) =  0._CUSTOM_REAL
        c_vti(2,6) =  0._CUSTOM_REAL
        c_vti(3,3) = rho *vp *vp
        c_vti(3,4) =  0._CUSTOM_REAL
        c_vti(3,5) =  0._CUSTOM_REAL
        c_vti(3,6) =  0._CUSTOM_REAL
        c_vti(4,4) = rho*vs*vs
        c_vti(4,5) =  0._CUSTOM_REAL
        c_vti(4,6) =  0._CUSTOM_REAL
        c_vti(5,5) = rho*vs*vs
        c_vti(5,6) =  0._CUSTOM_REAL
        c_vti(6,6) = (2*gm+1)*vs*vs*rho
    
        ! symetisation
        do i=1,6
            do j=i+1,6
                c_vti(j,i) = c_vti(i,j)
            end do
        end do

    end subroutine get_thomsen2Cij

    subroutine vti2tti()
        real(kind=CUSTOM_REAL), dimension(6,6) :: tmp_m
        tmp_m = matmul(c_vti, rot_TTI_transpose)
        c_tti = matmul(rot_TTI, tmp_m)
    end subroutine vti2tti

    subroutine display_aniso()
        integer :: i
        write(*,*)
        do i=1,6
        write(*,'(6f30.15)') rot_TTI(i,:)
        end do
        write(*,*)
        do i=1,6
        write(*,'(6f30.15)') c_tti(i,:)
        end do
        write(*,*)
    end subroutine display_aniso

end module anisotropy_model