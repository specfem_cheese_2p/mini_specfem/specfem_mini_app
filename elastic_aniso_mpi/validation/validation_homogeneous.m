%
% validation test for elastic_aniso_mpi config_0()
% mpirun -np 4 ./xspecfem_mini_app
%
%
%
% 

% current run output
U = load('../displ_00001.txt');

% reference output
Uref= load('displ_00001_ref_config0.txt');

figure; 
subplot(3,1,1)
plot(U(:,2))
hold on
plot(Uref(:,2),'--')


subplot(3,1,2)
plot(U(:,3))
hold on
plot(Uref(:,3),'--')



subplot(3,1,3)
plot(U(:,4))
hold on
plot(Uref(:,4),'--')

legend('actual','reference')
