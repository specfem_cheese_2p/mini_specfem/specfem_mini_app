module specfem_mod

  use config_mod
  use meshfem_mod
  use element_mod
  use addressing_mod
  use domain_decomp_mod
  use acquisition_mod
  use mpi_mod
  use anisotropy_model

  !! optional modules
  use IO_mod
  
  implicit none
  
  integer, public :: nspec_specfem, nglob_specfem
  
  !! GLL(dof) points in mesh  
  double precision, public, dimension(:,:), allocatable :: xdof_specfem

  !!  shape function jacobian matrix
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: dxi_dx_specfem, dxi_dy_specfem, dxi_dz_specfem
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: deta_dx_specfem, deta_dy_specfem, deta_dz_specfem
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: dgamma_dx_specfem, dgamma_dy_specfem, dgamma_dz_specfem

  !! shape function jacobian matrix determinant
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: jacobian_specfem

  !! material properties 
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: rho_specfem , kappa_specfem, mu_specfem
  real(kind=CUSTOM_REAL), public, dimension(:,:,:,:), allocatable :: c11_specfem, c12_specfem, c13_specfem, &
          c14_specfem, c15_specfem, c16_specfem, c22_specfem, c23_specfem, c24_specfem, c25_specfem, &
          c26_specfem, c33_specfem, c34_specfem, c35_specfem, c36_specfem, c44_specfem, &
          c45_specfem, c46_specfem, c55_specfem, c56_specfem, c66_specfem
  !! inverse mass matrix
  real(kind=CUSTOM_REAL), public, dimension(:), allocatable :: inv_massx_specfem, inv_massy_specfem, inv_massz_specfem

  !! physical fields to solve
  real(kind=CUSTOM_REAL), public, dimension(:,:), allocatable :: displ_specfem, veloc_specfem, accel_specfem 

  !! indirect addressing
  integer, public, dimension(:,:,:,:), allocatable :: ibool_specfem

  !! local element quantities
  real(kind=CUSTOM_REAL), public, dimension(NGLLX)        :: wxgll
  real(kind=CUSTOM_REAL), public, dimension(NGLLY)        :: wygll
  real(kind=CUSTOM_REAL), public, dimension(NGLLZ)        :: wzgll
  real(kind=CUSTOM_REAL), public, dimension(NGLLY, NGLLZ) :: wgllwgll_yz
  real(kind=CUSTOM_REAL), public, dimension(NGLLX, NGLLZ) :: wgllwgll_xz
  real(kind=CUSTOM_REAL), public, dimension(NGLLX, NGLLY) :: wgllwgll_xy
  real(kind=CUSTOM_REAL), public, dimension(NGLLX, NGLLX) :: hprimewgll_xx, hprime_xxT
  real(kind=CUSTOM_REAL), public, dimension(NGLLY, NGLLY) :: hprimewgll_yy, hprime_yyT
  real(kind=CUSTOM_REAL), public, dimension(NGLLZ, NGLLZ) :: hprimewgll_zz, hprime_zzT 

  !! boundary condition
  integer, public :: ngll_boundary
  integer, public, dimension(:), allocatable :: index_gll_boundary
  real(kind=CUSTOM_REAL), public, dimension(:), allocatable :: wstacey, rho_vp, rho_vs
  real(kind=CUSTOM_REAL), public, dimension(:,:), allocatable :: normal
  
  !! acquisition

  ! force source
  integer, public :: nsource
  !real(kind=CUSTOM_REAL), dimension(:), allocatable ::  source_array
  !real(kind=CUSTOM_REAL), private ::  source_array
  real(kind=CUSTOM_REAL), public, dimension(:), allocatable :: xs, ys, zs
  real(kind=CUSTOM_REAL), public, dimension(:,:), allocatable :: hxis, hetas, hgammas
  real(kind=CUSTOM_REAL), dimension(:), allocatable :: Fx, Fy, Fz
  real(kind=CUSTOM_REAL), public, dimension(:), allocatable :: used_stf
  integer, public, dimension(:), allocatable :: ispec_src
  
  ! receivers 
  integer :: nrec
  real(kind=CUSTOM_REAL), public, dimension(:), allocatable :: xr, yr, zr
  real(kind=CUSTOM_REAL), public, dimension(:,:), allocatable :: hxir, hetar, hgammar
  real(kind=CUSTOM_REAL), public, dimension(:,:,:), allocatable :: seismogram_d
  integer, public, dimension(:), allocatable :: ispec_rec
  
  !!
  integer, public :: myrank !, ier
  character(len=CHAR_LEN) ::  prname_debug
  !! constants 
  real(kind=CUSTOM_REAL), public, parameter ::  FOUR_THIRDS = real((4.d0 / 3.d0),CUSTOM_REAL)

  !!
  real(kind=CUSTOM_REAL), public :: deltatover2, deltatsqover2
  real(kind=CUSTOM_REAL), public :: min_resolved_period
  
  public  :: specfem_setup, specfem_setup_mass_matrices, write_sismogram
  private :: allocate_specfem_arrays, set_up_stacey, define_stacey_mass_matrix, check_mesh,&
  stf_ricker, compute_and_print_source_time_function
  
contains

  
  
  !!---------------------------------------------------------------------------
  subroutine specfem_setup(myrank_in)
    
    integer, intent(in) :: myrank_in
    integer :: i,j,k,ispec,ip
    double precision :: x0, x1, y0, y1, z0, z1

    double precision, dimension(:,:,:,:), allocatable :: xgll_tmp, ygll_tmp, zgll_tmp
    double precision, dimension(NGLLX, NGLLY, NGLLZ)  :: xgll, ygll, zgll
    double precision, dimension(NGLLX, NGLLY, NGLLZ)  :: jacobian
    double precision, dimension(NGLLX, NGLLY, NGLLZ)  :: dxi_dx, dxi_dy, dxi_dz
    double precision, dimension(NGLLX, NGLLY, NGLLZ)  :: deta_dx, deta_dy, deta_dz
    double precision, dimension(NGLLX, NGLLY, NGLLZ)  :: dgamma_dx, dgamma_dy, dgamma_dz
    double precision, dimension(NGNOD)                :: xelm, yelm, zelm
    double precision :: typical_mesh_size

    myrank = myrank_in

    deltatover2 = 0.5 * deltat_config
    deltatsqover2 = deltat_config*deltat_config *0.5

    !! typical reference size   
    typical_mesh_size =  min(zmax_config-zmin_config, &
         min(xmax_config - xmin_config, ymax_config - ymin_config))


    call allocate_specfem_arrays()
    allocate(xgll_tmp(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             ygll_tmp(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             zgll_tmp(NGLLX, NGLLY, NGLLZ, nspec_specfem))
      
    do ispec = 1, nspec_specfem
       
       x0 = xgrid_meshfem(1,1,1,ispec)
       y0 = ygrid_meshfem(1,1,1,ispec)
       z0 = zgrid_meshfem(1,1,1,ispec)
       
       x1 = xgrid_meshfem(2,1,1,ispec)
       y1 = ygrid_meshfem(1,2,1,ispec)
       z1 = zgrid_meshfem(1,1,2,ispec)

       call define_element_Hex8(xelm, yelm, zelm, x0, x1, y0, y1, z0, z1)
       call compute_solver_data(xelm, yelm, zelm, &
                                xgll, ygll, zgll, &
                                dxi_dx, dxi_dy, dxi_dz,&
                                deta_dx, deta_dy, deta_dz, &
                                dgamma_dx, dgamma_dy, dgamma_dz, &
                                jacobian)


       !! store GLL(dof) coordinates 
       xgll_tmp(:,:,:, ispec) = xgll(:,:,:)
       ygll_tmp(:,:,:, ispec) = ygll(:,:,:)
       zgll_tmp(:,:,:, ispec) = zgll(:,:,:)
       
       !! store arrays for solver  : shape function jacobian matrix determinant
       jacobian_specfem(:,:,:,ispec) = real(jacobian(:,:,:), CUSTOM_REAL)
       
       !!  shape function jacobian matrix
       dxi_dx_specfem(:,:,:,ispec) = real(dxi_dx(:,:,:), CUSTOM_REAL)
       dxi_dy_specfem(:,:,:,ispec) = real(dxi_dy(:,:,:), CUSTOM_REAL)
       dxi_dz_specfem(:,:,:,ispec) = real(dxi_dz(:,:,:), CUSTOM_REAL)
       
       deta_dx_specfem(:,:,:,ispec) = real(deta_dx(:,:,:), CUSTOM_REAL)
       deta_dy_specfem(:,:,:,ispec) = real(deta_dy(:,:,:), CUSTOM_REAL)
       deta_dz_specfem(:,:,:,ispec) = real(deta_dz(:,:,:), CUSTOM_REAL)
       
       dgamma_dx_specfem(:,:,:,ispec) = real(dgamma_dx(:,:,:), CUSTOM_REAL)
       dgamma_dy_specfem(:,:,:,ispec) = real(dgamma_dy(:,:,:), CUSTOM_REAL)
       dgamma_dz_specfem(:,:,:,ispec) = real(dgamma_dz(:,:,:), CUSTOM_REAL)
       
       !! material properties 
       rho_specfem(:,:,:,ispec) = rho_config
       kappa_specfem(:,:,:,ispec) = rho_config*(vp_config*vp_config - FOUR_THIRDS*vs_config*vs_config)
       mu_specfem(:,:,:,ispec) = rho_config*vs_config*vs_config

       !! tti model from vti, azimuth and tilt 
      call get_rotation_TTI(azimuth_config, tilt_config)
      call get_thomsen2Cij(rho_config, vp_config, vs_config, epsilon_config, gamma_config, delta_config)
      call vti2tti()
      
      c11_specfem(:,:,:,ispec) = c_tti(1,1)
      c12_specfem(:,:,:,ispec) = c_tti(1,2)
      c13_specfem(:,:,:,ispec) = c_tti(1,3)
      c14_specfem(:,:,:,ispec) = c_tti(1,4)
      c15_specfem(:,:,:,ispec) = c_tti(1,5)
      c16_specfem(:,:,:,ispec) = c_tti(1,6)

      c22_specfem(:,:,:,ispec) = c_tti(2,2)  
      c23_specfem(:,:,:,ispec) = c_tti(2,3)
      c24_specfem(:,:,:,ispec) = c_tti(2,4)
      c25_specfem(:,:,:,ispec) = c_tti(2,5)
      c26_specfem(:,:,:,ispec) = c_tti(2,6)

      c33_specfem(:,:,:,ispec) = c_tti(3,3)
      c34_specfem(:,:,:,ispec) = c_tti(3,4)
      c35_specfem(:,:,:,ispec) = c_tti(3,5)
      c36_specfem(:,:,:,ispec) = c_tti(3,6)

      c44_specfem(:,:,:,ispec) = c_tti(4,4)
      c45_specfem(:,:,:,ispec) = c_tti(4,5)
      c46_specfem(:,:,:,ispec) = c_tti(4,6)

      c55_specfem(:,:,:,ispec) = c_tti(5,5)
      c56_specfem(:,:,:,ispec) = c_tti(5,6)

      c66_specfem(:,:,:,ispec) = c_tti(6,6)
      if (myrank == 0 .and. ispec == 1) then
         call display_aniso()
      end if
    end do

    !! compute initila global adressing
    call create_adressing(ibool_specfem, xgll_tmp, ygll_tmp, zgll_tmp, &
                          NGLLX, NGLLY, NGLLZ, nspec_specfem, typical_mesh_size)

    nglob_specfem = maxval(ibool_specfem)
    call allocate_specfem_fields()

    ! change global adressing (better to reduce cache misses)
    call get_global_indirect_addressing(nspec_specfem, nglob_specfem, ibool_specfem)

    !! store DOF points
    do ispec = 1, nspec_specfem

       do k = 1, NGLLZ
          do j = 1, NGLLY
             do i = 1, NGLLX
                ip = ibool_specfem(i,j,k,ispec)
                xdof_specfem(1,ip) = xgll_tmp(i,j,k,ispec)
                xdof_specfem(2,ip) = ygll_tmp(i,j,k,ispec)
                xdof_specfem(3,ip) = zgll_tmp(i,j,k,ispec)
             end do
          end do
       end do
    end do

    deallocate(xgll_tmp, ygll_tmp, zgll_tmp)
  
    call define_weight_and_derivation_matrices(hprime_xxT, hprime_yyT, hprime_zzT, &
         hprimewgll_xx,hprimewgll_yy,hprimewgll_zz, &
         wgllwgll_yz, wgllwgll_xz, wgllwgll_xy, wxgll, wygll, wzgll)

    !! setup acquisition --------------------------------
    nsource=1
    allocate(xs(nsource), ys(nsource), zs(nsource))
    allocate(Fx(nsource), Fy(nsource), Fz(nsource))
    allocate(hxis(NGLLX, nsource),  hetas(NGLLY, nsource), hgammas(NGLLZ, nsource))
    allocate(ispec_src(nsource))
    allocate(used_stf(NSTEP))
    call compute_and_print_source_time_function()

    !! define source
    xs(1)=xs_config; ys(1)=ys_config; zs(1)=zs_config
    Fx=0.;Fy=0.;Fz=1.; 
    
    nrec=1
    allocate(xr(nrec), yr(nrec), zr(nrec))
    allocate(hxir(NGLLX, nrec),  hetar(NGLLY, nrec), hgammar(NGLLZ, nrec))
    allocate(ispec_rec(nrec))
    allocate(seismogram_d(NSTEP, 3, nrec))
    seismogram_d(:,:,:)=0._CUSTOM_REAL
    
    !! define receiver 
    xr(1)=xr_config; yr(1)=yr_config; zr(1)=zr_config
    
    call setup_acquisition(xs, ys, zs, xr, yr, zr, ispec_src, ispec_rec, &
         ibool_specfem, xdof_specfem, nspec_specfem, nglob_specfem, nsource, nrec,&
         hxis, hetas, hgammas, hxir, hetar, hgammar)

    call set_up_stacey()

    call check_mesh()
    
  end subroutine specfem_setup
  
  !! ##########################################################################################################
  !!---------------------------------------------------------------------------
  subroutine allocate_specfem_arrays()

    if (myrank==0 .and. verbose) then 
       write(*,*) '*********************************************'
       write(*,*) "             setup specfem  "
       write(*,*)
    end if
    
    nspec_specfem = nspec_meshfem

    allocate(dxi_dx_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
             dxi_dy_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             dxi_dz_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
             deta_dx_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             deta_dy_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             deta_dz_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
             dgamma_dx_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             dgamma_dy_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
             dgamma_dz_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
             jacobian_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem)) 
             
   allocate(rho_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            kappa_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            mu_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            c11_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            c12_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            c13_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            c14_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem), &
            c15_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c16_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c22_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c23_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c24_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c25_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c26_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c33_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c34_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c35_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c36_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),& 
            c44_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c45_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c46_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c55_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c56_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem),&
            c66_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem))

    allocate(ibool_specfem(NGLLX, NGLLY, NGLLZ, nspec_specfem))
    
  end subroutine allocate_specfem_arrays
 !!---------------------------------------------------------------------------
  subroutine allocate_specfem_fields()
   allocate(xdof_specfem(NDIM,nglob_specfem))
   allocate(displ_specfem(3,nglob_specfem))
   allocate(veloc_specfem(3,nglob_specfem))
   allocate(accel_specfem(3,nglob_specfem))
   allocate(inv_massx_specfem(nglob_specfem))
   allocate(inv_massy_specfem(nglob_specfem))
   allocate(inv_massz_specfem(nglob_specfem))
  end subroutine allocate_specfem_fields
  !! ##########################################################################################################
  !!---------------------------------------------------------------------------
  subroutine set_up_stacey()
    
    double precision,  dimension(3,NGLLX, NGLLY) :: normal_xy
    double precision,  dimension(3,NGLLX, NGLLZ) :: normal_xz
    double precision,  dimension(3,NGLLY, NGLLZ) :: normal_yz
    
    double precision,  dimension(NGLLX, NGLLY) :: jacobian2D_xy
    double precision,  dimension(NGLLX, NGLLZ) :: jacobian2D_xz
    double precision,  dimension(NGLLY, NGLLZ) :: jacobian2D_yz

    double precision, dimension(4) :: xelm, yelm, zelm
    integer :: ib, i, j, k, ispec

    
    ib = 0
    ngll_boundary=0
    !! determine dimension for allication
    do ispec = 1, nspec_specfem

       if (iboun_meshfem(1, ispec)) ngll_boundary = ngll_boundary + NGLLY*NGLLZ
       if (iboun_meshfem(2, ispec)) ngll_boundary = ngll_boundary + NGLLY*NGLLZ
       if (iboun_meshfem(3, ispec)) ngll_boundary = ngll_boundary + NGLLX*NGLLZ
       if (iboun_meshfem(4, ispec)) ngll_boundary = ngll_boundary + NGLLX*NGLLZ
       if (iboun_meshfem(5, ispec)) ngll_boundary = ngll_boundary + NGLLX*NGLLY
       if (iboun_meshfem(6, ispec)) ngll_boundary = ngll_boundary + NGLLX*NGLLY
       
    end do
    
    !write(*,*) " boundry ", ngll_boundary
    allocate(index_gll_boundary(ngll_boundary))
    allocate(wstacey(ngll_boundary))
    allocate(normal(3,ngll_boundary))
    allocate(rho_vp(ngll_boundary), rho_vs(ngll_boundary))
    do ispec = 1, nspec_specfem
       
       if (iboun_meshfem(1, ispec)) then

          xelm(1) = xdof_specfem(1,ibool_specfem(1,1,1,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(1,1,1,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(1,1,1,ispec))
          
          xelm(2) = xdof_specfem(1,ibool_specfem(1,NGLLY,1,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(1,NGLLY,1,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(1,NGLLY,1,ispec))

          xelm(3) = xdof_specfem(1,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(1,NGLLY,NGLLZ,ispec))

          xelm(4) = xdof_specfem(1,ibool_specfem(1,1,NGLLZ,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(1,1,NGLLZ,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(1,1,NGLLZ,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_yz, normal_yz, NGLLY, NGLLZ)
          
          do k=1,NGLLZ
             do j=1,NGLLY
                ib = ib + 1
                normal(1,ib) = -normal_yz(1,j,k)
                normal(2,ib) = normal_yz(2,j,k)
                normal(3,ib) = normal_yz(3,j,k)
                index_gll_boundary(ib) = ibool_specfem(1,j,k,ispec)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                wstacey(ib) = jacobian2D_yz(j,k)*wgllwgll_yz(j,k)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(1,j,k,ispec)+kappa_specfem(1,j,k,ispec))*rho_specfem(1,j,k,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(1,j,k,ispec)*rho_specfem(1,j,k,ispec))
             end do
          end do
          
       end if
       
       if (iboun_meshfem(2, ispec)) then

          xelm(1) = xdof_specfem(1,ibool_specfem(NGLLX,1,1,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(NGLLX,1,1,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(NGLLX,1,1,ispec))
          
          xelm(2) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,1,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,1,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,1,ispec))

          xelm(3) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))

          xelm(4) = xdof_specfem(1,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(NGLLX,1,NGLLZ,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_yz, normal_yz, NGLLY, NGLLZ)

          do k=1,NGLLZ
             do j=1,NGLLY
                ib = ib + 1
                normal(1,ib) = normal_yz(1,j,k)
                normal(2,ib) = normal_yz(2,j,k)
                normal(3,ib) = normal_yz(3,j,k)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                index_gll_boundary(ib) = ibool_specfem(NGLLX,j,k,ispec)
                wstacey(ib) = jacobian2D_yz(j,k)*wgllwgll_yz(j,k)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(NGLLX,j,k,ispec)+&
                kappa_specfem(NGLLX,j,k,ispec))*rho_specfem(NGLLX,j,k,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(NGLLX,j,k,ispec)*rho_specfem(NGLLX,j,k,ispec))
             end do
          end do
          
       end if

       if (iboun_meshfem(3, ispec)) then
          
          xelm(1) = xdof_specfem(1,ibool_specfem(1,1,1,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(1,1,1,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(1,1,1,ispec))
          
          xelm(2) = xdof_specfem(1,ibool_specfem(NGLLX,1,1,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(NGLLX,1,1,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(NGLLX,1,1,ispec))
          
          xelm(3) = xdof_specfem(1,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          
          xelm(4) = xdof_specfem(1,ibool_specfem(1,1,NGLLZ,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(1,1,NGLLZ,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(1,1,NGLLZ,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_xz, normal_xz, NGLLX, NGLLZ)
          
           do k=1,NGLLZ
             do i=1,NGLLX
                ib = ib + 1
                normal(1,ib) = normal_xz(1,i,k)
                normal(2,ib) = normal_xz(2,i,k)
                normal(3,ib) = normal_xz(3,i,k)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                index_gll_boundary(ib) = ibool_specfem(i,1,k,ispec)
                wstacey(ib) = jacobian2D_xz(i,k)*wgllwgll_xz(i,k)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(i,1,k,ispec)+&
                kappa_specfem(i,1,k,ispec))*rho_specfem(i,1,k,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(i,1,k,ispec)*rho_specfem(i,1,k,ispec))
             end do
          end do
          
        end if


        if (iboun_meshfem(4, ispec)) then
          
          xelm(1) = xdof_specfem(1,ibool_specfem(1,NGLLY,1,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(1,NGLLY,1,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(1,NGLLY,1,ispec))
          
          xelm(2) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,1,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,1,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,1,ispec))
          
          xelm(3) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          
          xelm(4) = xdof_specfem(1,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(1,NGLLY,NGLLZ,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_xz, normal_xz, NGLLX, NGLLZ)

           do k=1,NGLLZ
             do i=1,NGLLX
                ib = ib + 1
                normal(1,ib) = normal_xz(1,i,k)
                normal(2,ib) = -normal_xz(2,i,k)
                normal(3,ib) = normal_xz(3,i,k)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                index_gll_boundary(ib) = ibool_specfem(i,NGLLY,k,ispec)
                wstacey(ib) = jacobian2D_xz(i,k)*wgllwgll_xz(i,k)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(i,NGLLY,k,ispec)+&
                kappa_specfem(i,NGLLY,k,ispec))*rho_specfem(i,NGLLY,k,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(i,NGLLY,k,ispec)*rho_specfem(i,NGLLY,k,ispec))
             end do
          end do
          
        end if

        if (iboun_meshfem(5, ispec)) then
          
          xelm(1) = xdof_specfem(1,ibool_specfem(1,1,1,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(1,1,1,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(1,1,1,ispec))
          
          xelm(2) = xdof_specfem(1,ibool_specfem(NGLLX,1,1,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(NGLLX,1,1,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(NGLLX,1,1,ispec))
          
          xelm(3) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,1,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,1,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,1,ispec))
          
          xelm(4) = xdof_specfem(1,ibool_specfem(1,NGLLY,1,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(1,NGLLY,1,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(1,NGLLY,1,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_xy, normal_xy, NGLLX, NGLLY)

           do j=1,NGLLY
             do i=1,NGLLX
                ib = ib + 1
                normal(1,ib) = normal_xy(1,i,j)
                normal(2,ib) = normal_xy(2,i,j)
                normal(3,ib) = -normal_xy(3,i,j)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                index_gll_boundary(ib) = ibool_specfem(i,j,1,ispec)
                wstacey(ib) = jacobian2D_xy(i,j)*wgllwgll_xy(i,j)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(i,j,1,ispec)+&
                kappa_specfem(i,j,1,ispec))*rho_specfem(i,j,1,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(i,j,1,ispec)*rho_specfem(i,j,1,ispec))
             end do
          end do
          
       end if
       
       if (iboun_meshfem(6, ispec)) then
          
          xelm(1) = xdof_specfem(1,ibool_specfem(1,1,NGLLZ,ispec))
          yelm(1) = xdof_specfem(2,ibool_specfem(1,1,NGLLZ,ispec))
          zelm(1) = xdof_specfem(3,ibool_specfem(1,1,NGLLZ,ispec))
           
          xelm(2) = xdof_specfem(1,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          yelm(2) = xdof_specfem(2,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          zelm(2) = xdof_specfem(3,ibool_specfem(NGLLX,1,NGLLZ,ispec))
          
          xelm(3) = xdof_specfem(1,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          yelm(3) = xdof_specfem(2,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          zelm(3) = xdof_specfem(3,ibool_specfem(NGLLX,NGLLY,NGLLZ,ispec))
          
          xelm(4) = xdof_specfem(1,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          yelm(4) = xdof_specfem(2,ibool_specfem(1,NGLLY,NGLLZ,ispec))
          zelm(4) = xdof_specfem(3,ibool_specfem(1,NGLLY,NGLLZ,ispec))

          call compute_jacobian_2D(xelm, yelm, zelm, jacobian2D_xy, normal_xy, NGLLX, NGLLY)

           do j=1,NGLLY
             do i=1,NGLLX
                ib = ib + 1
                normal(1,ib) = normal_xy(1,i,j)
                normal(2,ib) = normal_xy(2,i,j)
                normal(3,ib) = normal_xy(3,i,j)
                !write(*,*) normal(1,ib), normal(2,ib), normal(3,ib)
                index_gll_boundary(ib) = ibool_specfem(i,j,NGLLZ,ispec)
                wstacey(ib) = jacobian2D_xy(i,j)*wgllwgll_xy(i,j)
                rho_vp(ib) =  sqrt( (FOUR_THIRDS*mu_specfem(i,j,NGLLZ,ispec)+&
                kappa_specfem(i,j,NGLLZ,ispec))*rho_specfem(i,j,NGLLZ,ispec))
                rho_vs(ib) =  sqrt(mu_specfem(i,j,NGLLZ,ispec)*rho_specfem(i,j,NGLLZ,ispec))
             end do
          end do
          
        end if
     end do
  end subroutine set_up_stacey
  
  
  !!---------------------------------------------------------------------------
  subroutine specfem_setup_mass_matrices()

   integer :: i, j, k, ispec, iglob
   double precision :: weight
   real(kind=CUSTOM_REAL), dimension(:), allocatable :: mass
   
   allocate(mass(nglob_specfem))
   mass(:) = 0._CUSTOM_REAL
   
   do ispec = 1, nspec_specfem
      do k = 1, NGLLZ
         do j = 1, NGLLY
            do i = 1, NGLLX
               iglob = ibool_specfem(i,j,k,ispec)
               weight = wxgll(i)*wygll(j)*wzgll(k) 
               mass(iglob) = mass(iglob) + &
                    real( dble(jacobian_specfem(i,j,k,ispec)) * weight * dble(rho_specfem(i,j,k,ispec)) , kind=CUSTOM_REAL)
            end do
         end do
      end do
   end do

   call define_stacey_mass_matrix()

   inv_massx_specfem(:) = mass(:) + inv_massx_specfem(:)
   inv_massy_specfem(:) = mass(:) + inv_massy_specfem(:)
   inv_massz_specfem(:) = mass(:) + inv_massz_specfem(:)

   call assemble_MPI_scalar_blocking(nglob_specfem, inv_massx_specfem)
   call assemble_MPI_scalar_blocking(nglob_specfem, inv_massy_specfem)
   call assemble_MPI_scalar_blocking(nglob_specfem, inv_massz_specfem)

   where(inv_massx_specfem <= 0._CUSTOM_REAL) inv_massx_specfem = 1._CUSTOM_REAL
   inv_massx_specfem(:) = 1._CUSTOM_REAL / inv_massx_specfem(:)
   where(inv_massy_specfem <= 0._CUSTOM_REAL) inv_massy_specfem = 1._CUSTOM_REAL
   inv_massy_specfem(:) = 1._CUSTOM_REAL / inv_massy_specfem(:)
   where(inv_massz_specfem <= 0._CUSTOM_REAL) inv_massz_specfem = 1._CUSTOM_REAL
   inv_massz_specfem(:) = 1._CUSTOM_REAL / inv_massz_specfem(:)

   deallocate(mass)
 end subroutine specfem_setup_mass_matrices
!!---------------------------------------------------------------------------
 subroutine define_stacey_mass_matrix()
  
  integer :: ib, iglob
  real(kind=CUSTOM_REAL) :: nx, ny, nz, vn
  real(kind=CUSTOM_REAL) :: tx, ty, tz, weight

  ! adds contributions to mass matrix to stabilize Stacey conditions
  inv_massx_specfem(:) = 0._CUSTOM_REAL
  inv_massy_specfem(:) = 0._CUSTOM_REAL
  inv_massz_specfem(:) = 0._CUSTOM_REAL

  do ib = 1, ngll_boundary

     nx = normal(1,ib)
     ny = normal(2,ib)
     nz = normal(3,ib)

     iglob = index_gll_boundary(ib)

     vn = deltatover2*(nx+ny+nz)

     tx = rho_vp(ib) * vn*nx + rho_vs(ib) * (deltatover2-vn*nx)
     ty = rho_vp(ib) * vn*ny + rho_vs(ib) * (deltatover2-vn*ny)
     tz = rho_vp(ib) * vn*nz + rho_vs(ib) * (deltatover2-vn*nz)
     
     weight = wstacey(ib)

     inv_massx_specfem(iglob) = inv_massx_specfem(iglob) + tx*weight
     inv_massy_specfem(iglob) = inv_massy_specfem(iglob) + tx*weight
     inv_massz_specfem(iglob) = inv_massz_specfem(iglob) + tx*weight

  end do
 
 end subroutine define_stacey_mass_matrix

!!---------------------------------------------------------------------------
 subroutine check_mesh()

   integer :: i,j,k, iglob1, iglob2, ispec, npgll, npglob
   integer :: i1,i2,j1,j2,k1,k2
   real(kind=CUSTOM_REAL) :: vp, vs, vel_min, vel_max, vel_min_glob, vel_max_glob
   REAL(KIND=custom_real) :: dt_suggested
   real(kind=CUSTOM_REAL) :: dist, distance_min,  distance_max, distance_min_glob, &
   distance_max_glob, avg_distance_glob
   real(kind=CUSTOM_REAL) :: x1, y1, z1
   real(kind=CUSTOM_REAL) :: x2, y2, z2
   real(kind=CUSTOM_REAL) :: avg_distance, pmax, frq_max , tmp_cr
   real(kind=CUSTOM_REAL) :: elemsize_min,elemsize_max

   !! check mesh resolution and input parameters in order to avoid NaN
   ! initializes
   npglob=0
   avg_distance_glob = 0.
   dt_suggested = HUGEVAL
   frq_max =  HUGEVAL
   vel_min_glob = HUGEVAL
   vel_max_glob = -HUGEVAL
   distance_min_glob=-HUGEVAL
   distance_max_glob=HUGEVAL
   do ispec=1, nspec_specfem  
      ! loops over all GLL points
      vel_max =  -HUGEVAL
      vel_min = HUGEVAL
      distance_min = HUGEVAL
      distance_max = -HUGEVAL
      elemsize_min = HUGEVAL
      elemsize_max = -HUGEVAL
      npgll = 0
      avg_distance = 0.
      do k=1,NGLLZ-1
         do j=1,NGLLY-1
            do i=1,NGLLX-1
              
               !! velocities 
               vs = sqrt(mu_specfem(i,j,k,ispec) / rho_specfem(i,j,k,ispec))
               vp = sqrt(kappa_specfem(i,j,k,ispec) /  rho_specfem(i,j,k,ispec) + FOUR_THIRDS * vs *vs)

               vel_max = max(vp, vel_max)
               vel_min = min(vs, vel_min)
               
               npgll = npgll + 1

            enddo
         enddo
      enddo
  
     ! loops over the four edges that are along X   
     i1 = 1
     i2 = NGLLX
     do k = 1, NGLLZ, NGLLZ-1
        do j = 1, NGLLY, NGLLY-1
          iglob1 = ibool_specfem(i1,j,k,ispec)
          iglob2 = ibool_specfem(i2,j,k,ispec)

          x1 = xdof_specfem(1,iglob1)
          y1 = xdof_specfem(2,iglob1)
          z1 = xdof_specfem(3,iglob1)

          x2 = xdof_specfem(1,iglob2)
          y2 = xdof_specfem(2,iglob2)
          z2 = xdof_specfem(3,iglob2)

          dist = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2)

          if (dist < elemsize_min) elemsize_min = dist
          if (dist > elemsize_max) elemsize_max = dist
        enddo
     enddo

     j1 = 1
     j2 = NGLLY
     do k = 1, NGLLZ, NGLLZ-1
        do i = 1, NGLLX, NGLLX-1
           iglob1 = ibool_specfem(i,j1,k,ispec)
           iglob2 = ibool_specfem(i,j2,k,ispec)
     
           x1 = xdof_specfem(1,iglob1)
           y1 = xdof_specfem(2,iglob1)
           z1 = xdof_specfem(3,iglob1)
     
           x2 = xdof_specfem(1,iglob2)
           y2 = xdof_specfem(2,iglob2)
           z2 = xdof_specfem(3,iglob2)
     
           dist = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2)
     
           if (dist < elemsize_min) elemsize_min = dist
           if (dist > elemsize_max) elemsize_max = dist
        enddo
     enddo

     ! loops over the four edges that are along Z
     k1 = 1
     k2 = NGLLZ
     do j = 1, NGLLY, NGLLY-1
        do i = 1, NGLLX, NGLLX-1
          iglob1 = ibool_specfem(i,j,k1,ispec)
          iglob2 = ibool_specfem(i,j,k2,ispec)

          x1 = xdof_specfem(1,iglob1)
          y1 = xdof_specfem(2,iglob1)
          z1 = xdof_specfem(3,iglob1)

          x2 = xdof_specfem(1,iglob2)
          y2 = xdof_specfem(2,iglob2)
          z2 = xdof_specfem(3,iglob2)

          dist = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2)

          if (dist < elemsize_min) elemsize_min = dist
          if (dist > elemsize_max) elemsize_max = dist
       enddo
     enddo

     elemsize_min = sqrt( elemsize_min )
     elemsize_max = sqrt( elemsize_max )
     
     vel_min_glob = min(vel_min, vel_min_glob)
     vel_max_glob = max(vel_max, vel_max_glob)

     distance_min = elemsize_min 
     distance_max = elemsize_max 
   
     distance_min_glob = min(distance_min_glob, distance_min)
     distance_max_glob = max(distance_max_glob, distance_max)

     dt_suggested = min(dt_suggested, COURANT_SUGGESTED * distance_min / vel_max)
      
     ! average distance between GLL points within this element
     avg_distance =  elemsize_max / ( NGLLX - 1)  ! since NGLLX = NGLLY = NGLLZ
     avg_distance_glob = avg_distance_glob + avg_distance
     npglob = npglob + 1

     pmax = avg_distance / vel_min * NPTS_PER_WAVELENGTH
     frq_max = min(frq_max, 1./pmax)
      
   end do

   avg_distance_glob = avg_distance_glob/npglob
   tmp_cr = avg_distance_glob
   call sum_all_cr(tmp_cr, avg_distance_glob)
   avg_distance_glob = avg_distance_glob / nbproc_mpi
   
   call min_all_cr(vel_min_glob, vel_min)
   call max_all_cr(vel_max_glob, vel_max)

   tmp_cr = dt_suggested
   call min_all_cr(tmp_cr, dt_suggested)
   tmp_cr = frq_max
   call min_all_cr(tmp_cr, frq_max)

   if (myrank == 0 .and. verbose) then
      write(*,*) '*********************************************'
      write(*,*) '*** Verification of simulation parameters ***'
      write(*,*) '*********************************************'
      write(*,*) '*** Minimun velocity = ', vel_min
      write(*,*) '*** Maximum velocity = ', vel_max
      write(*,*) '*** average distance between points = ', avg_distance_glob
      write(*,*) '*** Maximum frquency resolved = ',frq_max, 1./frq_max
      write(*,*) '*** Maximum suggested time step = ',dt_suggested
      write(*,*) '*** for DT : ',deltat_config
      write(*,*) 
   end if
   min_resolved_period = 1./frq_max
   call bcast_all_cr(min_resolved_period)

   if (deltat_config > dt_suggested) then
      write(*,*) " instable simulation reduce time step less than  ",dt_suggested
      stop
   end if
 end subroutine check_mesh

!!---------------------------------------------------------------------------
  subroutine write_xdmf_mesh(myrank)
    integer, intent(in) :: myrank
    integer :: iunit=667
    character(len=CHAR_LEN) :: name_field
    prname_debug = 'mesh.xdmf'
    call open_xdmf_mesh(prname_debug, iunit, myrank)
    name_field = "jacobian"
    call add_xdmf_field(iunit, myrank, name_field, jacobian_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dxi_dx"
    call add_xdmf_field(iunit, myrank, name_field, dxi_dx_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dxi_dy"
    call add_xdmf_field(iunit, myrank, name_field, dxi_dy_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dxi_dz"
    call add_xdmf_field(iunit, myrank, name_field, dxi_dz_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "deta_dx"
    call add_xdmf_field(iunit, myrank, name_field, deta_dx_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "deta_dy"
    call add_xdmf_field(iunit, myrank, name_field, deta_dy_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "deta_dz"
    call add_xdmf_field(iunit, myrank, name_field, deta_dz_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dgamma_dx"
    call add_xdmf_field(iunit, myrank, name_field, dgamma_dx_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dgamma_dy"
    call add_xdmf_field(iunit, myrank, name_field, dgamma_dy_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "dgamma_dz"
    call add_xdmf_field(iunit, myrank, name_field, dgamma_dz_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "rho"
    call add_xdmf_field(iunit, myrank, name_field, rho_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "kappa"
    call add_xdmf_field(iunit, myrank, name_field, kappa_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    name_field = "mu"
    call add_xdmf_field(iunit, myrank, name_field, mu_specfem, ibool_specfem, &
    NGLLX, NGLLY, NGLLZ, nspec_specfem, nglob_specfem)
    call close_xdmf_mesh(iunit, myrank)
  end subroutine write_xdmf_mesh
 
 !!---------------------------------------------------------------------------
  subroutine compute_and_print_source_time_function()
   integer :: it
   real(kind=CUSTOM_REAL) :: t
   character(len=100) :: source_file
   write(source_file, '("source_time_function.txt")')  
   open(10,file=trim(source_file))
   do it=1,NSTEP
      t = (it-1)*deltat_config
      used_stf(it) = real(stf_ricker(t), CUSTOM_REAL)
      write(10, *) (it-1)*deltat_config, used_stf(it)
   end do
   close(10)
  end subroutine 
!---------------------------------------------------------------------------
  double precision function stf_ricker(t)
  real(kind=CUSTOM_REAL) :: t
  double precision  :: a, f0, dt, t0
  f0=f0_config     
  t0 = 1.2/f0
  a = amplitude_source_config
  dt = dble(t) - t0
  dt  = PI *PI *f0 *f0 * dt *dt
  stf_ricker = a*(1.d0 - 2*dt)*exp(-dt)
end function stf_ricker
!!---------------------------------------------------------------------------
subroutine write_sismogram()
   integer :: ispec, irec, it
   character(len=100) :: sismo_file
   do irec =1, nrec
      ispec = ispec_rec(irec)
      if (ispec > 0) then
         write(sismo_file, '("displ_",i5.5,".txt")') irec 
         open(10,file=trim(sismo_file))
         if (CUSTOM_REAL>=8) then 
           do it=1,NSTEP
               write(10,'(4E41.20)') (it-1)*deltat_config, seismogram_d(it,1,irec),seismogram_d(it,2,irec), seismogram_d(it,3,irec)
           end do
         else 
           do it=1,NSTEP
              write(10,*) (it-1)*deltat_config, seismogram_d(it,1,irec),seismogram_d(it,2,irec), seismogram_d(it,3,irec)
           end do
         end if
         close(10)
      end if
   end do
end subroutine write_sismogram
end module specfem_mod
