# specfem_mini_app

Mini apps based on specfem3d : https://gitlab.com/specfem_cheese_2p/full_app/specfem3d
Each version of the mini application tests a particular case of specfem: 

```
  acoustic_iso_mpi : cpu + MPI, acoustic (fluid) case
  elastic_iso_mpi  : cpu + MPI, elastic isotropic case
  elastic_iso_vectorised_mpi : cpu + MPI + cache blocking vectorization, elastic isotropic case  
  elastic_iso_att_mpi : cpu + MPI, visco-elastic isotropic case
  elastic_aniso_mpi : cpu + MPI, elastic anisotropic case 
  
  acoustic_iso_cuda_mpi : GPU + CUDA + MPI, acoustic (fluid) case
  elastic_iso_cuda_mpi : GPU + CUDA + MPI,  elastic isotropic case
  elastic_iso_att_cuda_mpi : GPU + CUDA + MPI, visco-elastic isotropic case
  elastic_aniso_cuda_mpi : GPU + CUDA + MPI, elastic anisotropic case

```

## configuration and compilation 

An example makefile is provided for each mini-application. The configuration is hard-coded in config_mod.f90, 4 configurations are defined and must be chosen before compilation.
