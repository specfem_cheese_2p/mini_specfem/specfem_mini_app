module acquisition_mod

  use config_mod
  use element_mod
  use mpi_mod
  
  implicit none

  integer, private, parameter :: NUM_ITER=10
  
contains
  
  !! ##########################################################################################################
  !!---------------------------------------------------------------------------
  subroutine setup_acquisition(xs, ys, zs, xr, yr, zr,&
       ispec_src, ispec_rec, ibool, xnodes, &
       nspec, nglob, nsource, nrec, &
       hxis, hetas, hgammas, hxir, hetar, hgammar)

    !real(kind=CUSTOM_REAL) :: source_array
    integer, intent(in) :: nspec, nglob, nsource, nrec
    double precision, dimension(NDIM, NGLOB), intent(in) :: xnodes
    integer, dimension(NGLLX, NGLLY, NGLLZ, NSPEC), intent(in) :: ibool
    real(kind=CUSTOM_REAL), dimension(nsource), intent(in)  :: xs, ys, zs
    real(kind=CUSTOM_REAL), dimension(nrec), intent(in)  ::  xr, yr, zr
  
    integer, dimension(nsource), intent(inout) :: ispec_src
    integer, dimension(nrec), intent(inout) :: ispec_rec
    real(kind=CUSTOM_REAL), dimension(NGLLX, nsource), intent(inout) :: hxis
    real(kind=CUSTOM_REAL), dimension(NGLLY, nsource), intent(inout) :: hetas
    real(kind=CUSTOM_REAL), dimension(NGLLZ, nsource), intent(inout) :: hgammas
    real(kind=CUSTOM_REAL), dimension(NGLLX, nrec), intent(inout) :: hxir
    real(kind=CUSTOM_REAL), dimension(NGLLY, nrec), intent(inout) :: hetar
    real(kind=CUSTOM_REAL), dimension(NGLLZ, nrec), intent(inout) :: hgammar
    
    double precision, dimension(:), allocatable :: dist_all
    double precision, dimension(1) :: dist_dummy
    integer :: is, ir, irank
    double precision :: x, y, z, dist
    double precision :: xi, eta, gamma


    integer :: i_local
    
    double precision, dimension(NGLLX) :: xigll, hxi, hpxi
    double precision, dimension(NGLLY) :: yigll, heta, hpeta
    double precision, dimension(NGLLZ) :: zigll, hgamma, hpgamma
    

    call define_gll(xigll, yigll, zigll)
    allocate(dist_all(nbproc_mpi))
    
    do is = 1, nsource
       x=xs(is);y=ys(is);z=zs(is)
       call locate_point_in_mesh(ibool, xnodes, nspec, nglob, x, y, z, xi, eta, gamma, ispec_src(is), dist)
       dist_dummy(1)=dist
       call gather_all_all_dp(dist_dummy, dist_all, 1, nbproc_mpi)
       call get_selected_rank(ispec_src(is))
       
       if (ispec_src(is) > 0) then
          if (verbose) then 
             write(*,*)
             write(*,'(a16, i3, a12, i6)') " Locate source  ", is,"    slice : ", myrank_mpi  
             write(*,'(a25, e15.7)') " Distance form source :  ", dist
             write(*,'(a12,i8)') " Element  : ",  ispec_src(is)
             write(*,'(a14,3f9.4)') " Local coord : ", xi, eta, gamma
             write(*,*)
          end if
          call lagrange_any(xi, NGLLX, xigll, hxi, hpxi)
          call lagrange_any(eta, NGLLY, yigll, heta, hpeta)
          call lagrange_any(gamma, NGLLZ, zigll, hgamma, hpgamma)
          hxis(:,is) = hxi(:)
          hetas(:,is) = heta(:)
          hgammas(:,is) = hgamma(:)
       end if

       
    end do
    
   
    do ir = 1, nrec
       x=xr(ir);y=yr(ir);z=zr(ir)
       call locate_point_in_mesh(ibool, xnodes, nspec, nglob, x, y, z, xi, eta, gamma, ispec_rec(ir), dist)
       dist_dummy(1)=dist
       call gather_all_all_dp(dist_dummy, dist_all, 1, nbproc_mpi)
       call get_selected_rank(ispec_rec(ir))

       i_local=0
       if (ispec_rec(ir) > 0) then
          i_local = i_local + 1
          if (verbose) then 
             write(*,*)
             write(*,'(a17,i3,a12,i6)') " Locate receiver  ", ir,"    slice : ", myrank_mpi  
             write(*,'(a25, e15.7)') " Distance form receiver :  ", dist
             write(*,'(a12,i8)') " Element  : ",  ispec_rec(ir)
             write(*,'(a14,3f9.4)') " Local coord : ", xi, eta, gamma
             write(*,*)
          end if
          call lagrange_any(xi, NGLLX, xigll, hxi, hpxi)
          call lagrange_any(eta, NGLLY, yigll, heta, hpeta)
          call lagrange_any(gamma, NGLLZ, zigll, hgamma, hpgamma)
          hxir(:,ir) = hxi(:)
          hetar(:,ir) = heta(:)
          hgammar(:,ir) = hgamma(:)
       end if
       
    end do

    deallocate(dist_all)

  contains

!!---------------------------------------------------------------------------
    subroutine get_selected_rank(ispec_selected)
      integer, intent(inout) :: ispec_selected
      integer :: i
      double precision :: mind
      
      ! get min distance rank
      mind = HUGEVAL
      do i=1,nbproc_mpi
         
         if ( mind > dist_all(i) ) then
            irank = i-1
            !write(*,*) " irank ", irank 
            mind = dist_all(i)
         end if

      end do
      
      ! get min rank
      if ( myrank_mpi .ne. irank ) then
         ispec_selected = - 1
      end if
      
    end subroutine get_selected_rank

  end subroutine setup_acquisition

!!---------------------------------------------------------------------------
  subroutine locate_point_in_mesh(ibool, xnodes, nspec, nglob, x, y, z, xi, eta, gamma, ispec_selected, dist)

    integer, intent(in) ::  nspec, nglob
    double precision, dimension(NDIM,NGLOB), intent(in) :: xnodes
    integer, dimension(NGLLX, NGLLY, NGLLZ, NSPEC), intent(in) :: ibool
    double precision, intent(in) :: x, y, z
    
    double precision, intent(inout) :: xi, eta, gamma, dist
    integer, intent(inout) :: ispec_selected 

    double precision :: distmin_squared, dist_squared, dist_theshold
    double precision :: x_found, y_found, z_found
    double precision :: x0,x1, y0,y1, z0,z1, xp, yp, zp
    integer :: ix_initial_guess, iy_initial_guess, iz_initial_guess, ispec 
    integer :: MIDX,MIDY,MIDZ
    integer :: i, j, k, iglob, iter_loop, imin, imax, jmin, jmax, kmin, kmax
    double precision ::  xixs,xiys,xizs,etaxs,etays,etazs,gammaxs,gammays,gammazs
    double precision :: dxi, deta, dgamma, dx, dy, dz
    
    double precision, dimension(NGNOD) :: xnode, ynode, znode
    double precision, dimension(NGLLX) :: xigll
    double precision, dimension(NGLLY) :: yigll
    double precision, dimension(NGLLZ) :: zigll


    MIDX=NGLLX/2
    MIDY=NGLLY/2
    MIDZ=NGLLZ/2

    dist_theshold = 5.d0*min(min(dx_config, dy_config), dz_config)
    dist_theshold = dist_theshold*dist_theshold
    distmin_squared = HUGEVAL

    imin=2; imax=NGLLX-1
    jmin=2; jmax=NGLLY-1
    kmin=2; kmax=NGLLZ-1

    ix_initial_guess = 1
    iy_initial_guess = 1
    iz_initial_guess = 1
    ispec_selected  = 1

    iglob = ibool(1, 1, 1, 1)
    x_found = xnodes(1,iglob)
    y_found = xnodes(2,iglob)
    z_found = xnodes(3,iglob)
    
    do ispec = 1, nspec
       
       iglob =  ibool(MIDX,MIDY,MIDZ,ispec)
       dist_squared = (x - xnodes(1,iglob))**2 +&
                      (y - xnodes(2,iglob))**2 +&
                      (z - xnodes(3,iglob))**2

       if (dist_squared > dist_theshold ) cycle

      
       
       ! find closest GLL point form target
       do k = kmin, kmax
          do j = jmin, jmax
             do i = imin, imax

                iglob = ibool(i,j,k,ispec)

                dist_squared = (x - xnodes(1,iglob))**2 +&
                               (y - xnodes(2,iglob))**2 +&
                               (z - xnodes(3,iglob))**2
                
                if (dist_squared < distmin_squared) then

                   distmin_squared = dist_squared

                   ispec_selected  = ispec
                   
                   ix_initial_guess = i
                   iy_initial_guess = j
                   iz_initial_guess = k
                   
                   x_found = xnodes(1,iglob)
                   y_found = xnodes(2,iglob)
                   z_found = xnodes(3,iglob)
                   
                endif
                
             end do
          end do
       end do
       
    end do


    ! define quantiies in reference element
    call define_gll(xigll, yigll, zigll)

    ! general coordinate of initial guess
    xi    = xigll(ix_initial_guess)
    eta   = yigll(iy_initial_guess)
    gamma = zigll(iz_initial_guess)
    
    iglob = ibool(1, 1, 1, ispec_selected)
    x0 = xnodes(1,iglob)
    y0 = xnodes(2,iglob)
    z0 = xnodes(3,iglob)
    
    iglob = ibool(NGLLX, NGLLY, NGLLZ, ispec_selected)
    x1 = xnodes(1,iglob)
    y1 = xnodes(2,iglob)
    z1 = xnodes(3,iglob)
    
    call define_element_Hex8(xnode, ynode, znode, x0,x1, y0,y1, z0,z1)
    
    do iter_loop = 1,  NUM_ITER

       call compute_jacobian(xnode, ynode, znode, xi, eta, gamma,&
            xp, yp, zp, xixs,xiys,xizs,etaxs,etays,etazs,gammaxs,gammays,gammazs)


       ! compute distance to target location
       dx = - (xp - x)
       dy = - (yp - y)
       dz = - (zp - z)

       ! compute increments
       dxi  = xixs*dx + xiys*dy + xizs*dz
       deta = etaxs*dx + etays*dy + etazs*dz
       dgamma = gammaxs*dx + gammays*dy + gammazs*dz
       
       ! update values
       xi = xi + dxi
       eta = eta + deta
       gamma = gamma + dgamma
       
       ! impose that we stay in that element
       ! (useful if user gives a point outside the mesh for instance)
       if (xi > 1.01d0) xi     =  1.01d0
       if (xi < -1.01d0) xi     = -1.01d0
       if (eta > 1.01d0) eta    =  1.01d0
       if (eta < -1.01d0) eta    = -1.01d0
       if (gamma > 1.01d0) gamma  =  1.01d0
       if (gamma < -1.01d0) gamma  = -1.01d0
       
    end do
    
    call compute_jacobian(xnode, ynode, znode, xi, eta, gamma, &
         xp, yp, zp, xixs,xiys,xizs,etaxs,etays,etazs,gammaxs,gammays,gammazs)

    dist = sqrt((x - xp)**2 + (y - yp)**2 + (z - zp)**2)
   
  end subroutine locate_point_in_mesh
  
end module acquisition_mod
