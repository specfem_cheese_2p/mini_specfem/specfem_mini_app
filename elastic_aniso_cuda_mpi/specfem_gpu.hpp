#ifndef SPECFEM_CUDA_HPP
#define SPECFEM_CUDA_HPP

#include <iostream>
#include <cmath>
#include<math.h>
#include "cuda_constants.cuh"
#include <cuda_runtime.h>
#include "kernel_specfem3D_GPU.hpp"
#include "GPUCPUArray.h"

template <typename T>
class SpecfemGPU  
{
public:
  
  
  SpecfemGPU(){
    this->NDIM=3;
    this->size_mpi_buffer=0;
    this->nb_mpi_interfaces=0;
    this->max_nibool_interfaces=0;
    this->nspec_interfaces_max=0;
    cudaStreamCreate(&this->compute_stream);
    cudaStreamCreate(&this->copy_stream);
  }
  
  ~SpecfemGPU(){
    cudaFreeHost(this->h_mpi_send_buf);
    cudaFreeHost(this->h_mpi_recv_buf);
    cudaFree(this->d_mpi_send_buf);
    cudaFree(this->d_mpi_recv_buf);
  }

  void set_size(int nspec, int nglob){
    this->NSPEC = nspec;
    this->NGLOB = nglob;
    this->NCOMP = 3;
    this->it = 0;
  }

  void tranfert_mesh(T* fc_dxi_dx, T* fc_dxi_dy, T* fc_dxi_dz,
                     T* fc_deta_dx, T* fc_deta_dy, T* fc_deta_dz,
                     T* fc_dgamma_dx, T* fc_dgamma_dy, T* fc_dgamma_dz) {
    
    auto location = access_location::host;
    int NP = this->NSPEC*NGLL3DC;
    this->dxi_dx.resize(NP);
    this->dxi_dy.resize(NP);
    this->dxi_dz.resize(NP);
    this->deta_dx.resize(NP);
    this->deta_dy.resize(NP);
    this->deta_dz.resize(NP);
    this->dgamma_dx.resize(NP);
    this->dgamma_dy.resize(NP);
    this->dgamma_dz.resize(NP);
    
    ArrayHandle<T> h_dxi_dx_(this->dxi_dx,location, access_mode::readwrite);
    ArrayHandle<T> h_dxi_dy_(this->dxi_dy,location, access_mode::readwrite);
    ArrayHandle<T> h_dxi_dz_(this->dxi_dz,location, access_mode::readwrite);
    ArrayHandle<T> h_deta_dx_(this->deta_dx,location, access_mode::readwrite);
    ArrayHandle<T> h_deta_dy_(this->deta_dy,location, access_mode::readwrite);
    ArrayHandle<T> h_deta_dz_(this->deta_dz,location, access_mode::readwrite);
    ArrayHandle<T> h_dgamma_dx_(this->dgamma_dx,location, access_mode::readwrite);
    ArrayHandle<T> h_dgamma_dy_(this->dgamma_dy,location, access_mode::readwrite);
    ArrayHandle<T> h_dgamma_dz_(this->dgamma_dz,location, access_mode::readwrite);

    memcpy(h_dxi_dx_.data, fc_dxi_dx, sizeof(T)*NP);
    memcpy(h_dxi_dy_.data, fc_dxi_dy, sizeof(T)*NP);
    memcpy(h_dxi_dz_.data, fc_dxi_dz, sizeof(T)*NP);
    memcpy(h_deta_dx_.data, fc_deta_dx, sizeof(T)*NP);
    memcpy(h_deta_dy_.data, fc_deta_dy, sizeof(T)*NP);
    memcpy(h_deta_dz_.data, fc_deta_dz, sizeof(T)*NP);
    memcpy(h_dgamma_dx_.data, fc_dgamma_dx, sizeof(T)*NP);
    memcpy(h_dgamma_dy_.data, fc_dgamma_dy, sizeof(T)*NP);
    memcpy(h_dgamma_dz_.data, fc_dgamma_dz, sizeof(T)*NP);
  }
  
  void transfert_inv_mass_matrix(T* fc_inv_mass){
    auto location = access_location::host;
    this->inv_mass.resize(this->NGLOB*this->NCOMP);
    ArrayHandle<T> h_inv_mass_(this->inv_mass,location, access_mode::readwrite);
    memcpy(h_inv_mass_.data, fc_inv_mass, sizeof(T)*this->NGLOB*this->NCOMP);
  }

  void transfert_fields(T* fc_displ, T* fc_veloc, T* fc_accel){
    auto location = access_location::host;
    int NP = this->NGLOB*this->NCOMP;
    this->displ.resize(NP);
    this->veloc.resize(NP);
    this->accel.resize(NP);
    ArrayHandle<T> h_displ_(this->displ,location, access_mode::readwrite);
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::readwrite);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    memcpy(h_displ_.data, fc_displ, sizeof(T)*NP);
    memcpy(h_veloc_.data, fc_veloc, sizeof(T)*NP);
    memcpy(h_accel_.data, fc_accel, sizeof(T)*NP);
  }

  // void transfert_model(T* fc_rho, T* fc_kappa, T* fc_mu){
  //   auto location = access_location::host;
  //   int NP = this->NSPEC*NGLL3DC;
  //   this->rho.resize(NP);
  //   this->kappa.resize(NP);
  //   this->mu.resize(NP);
  //   ArrayHandle<T> h_rho_(this->rho,location, access_mode::readwrite);
  //   ArrayHandle<T> h_kappa_(this->kappa,location, access_mode::readwrite);
  //   ArrayHandle<T> h_mu_(this->mu,location, access_mode::readwrite);
  //   memcpy(h_rho_.data, fc_rho, sizeof(T)*NP);
  //   memcpy(h_kappa_.data, fc_kappa, sizeof(T)*NP);
  //   memcpy(h_mu_.data, fc_mu, sizeof(T)*NP);
  // }
   void transfert_model(T* fc_c11, T* fc_c12, T* fc_c13, T* fc_c14, T* fc_c15, T* fc_c16,
   T* fc_c22, T* fc_c23, T* fc_c24, T* fc_c25, T* fc_c26, 
   T* fc_c33, T* fc_c34, T* fc_c35, T* fc_c36, 
   T* fc_c44, T* fc_c45, T* fc_c46, 
   T* fc_c55, T* fc_c56, T* fc_c66){
    auto location = access_location::host;
    int NP = this->NSPEC*NGLL3DC;
    
    this->c11.resize(NP);
    this->c12.resize(NP);
    this->c13.resize(NP);
    this->c14.resize(NP);
    this->c15.resize(NP);
    this->c16.resize(NP);
    
    this->c22.resize(NP);
    this->c23.resize(NP);
    this->c24.resize(NP);
    this->c25.resize(NP);
    this->c26.resize(NP);
    
    this->c33.resize(NP);
    this->c34.resize(NP);
    this->c35.resize(NP);
    this->c36.resize(NP);
    
    this->c44.resize(NP);
    this->c45.resize(NP);
    this->c46.resize(NP);
    
    this->c55.resize(NP);
    this->c56.resize(NP);
    
    this->c66.resize(NP);

    ArrayHandle<T> h_c11_(this->c11,location, access_mode::readwrite);
    ArrayHandle<T> h_c12_(this->c12,location, access_mode::readwrite);
    ArrayHandle<T> h_c13_(this->c13,location, access_mode::readwrite);
    ArrayHandle<T> h_c14_(this->c14,location, access_mode::readwrite);
    ArrayHandle<T> h_c15_(this->c15,location, access_mode::readwrite);
    ArrayHandle<T> h_c16_(this->c16,location, access_mode::readwrite);

    ArrayHandle<T> h_c22_(this->c22,location, access_mode::readwrite);
    ArrayHandle<T> h_c23_(this->c23,location, access_mode::readwrite);
    ArrayHandle<T> h_c24_(this->c24,location, access_mode::readwrite);
    ArrayHandle<T> h_c25_(this->c25,location, access_mode::readwrite);
    ArrayHandle<T> h_c26_(this->c26,location, access_mode::readwrite);

    ArrayHandle<T> h_c33_(this->c33,location, access_mode::readwrite);
    ArrayHandle<T> h_c34_(this->c34,location, access_mode::readwrite);
    ArrayHandle<T> h_c35_(this->c35,location, access_mode::readwrite);
    ArrayHandle<T> h_c36_(this->c36,location, access_mode::readwrite);

    ArrayHandle<T> h_c44_(this->c44,location, access_mode::readwrite);
    ArrayHandle<T> h_c45_(this->c45,location, access_mode::readwrite);
    ArrayHandle<T> h_c46_(this->c46,location, access_mode::readwrite);

    ArrayHandle<T> h_c55_(this->c55,location, access_mode::readwrite);
    ArrayHandle<T> h_c56_(this->c56,location, access_mode::readwrite);
    
    ArrayHandle<T> h_c66_(this->c66,location, access_mode::readwrite);

    memcpy(h_c11_.data, fc_c11, sizeof(T)*NP);
    memcpy(h_c12_.data, fc_c12, sizeof(T)*NP);
    memcpy(h_c13_.data, fc_c13, sizeof(T)*NP);
    memcpy(h_c14_.data, fc_c14, sizeof(T)*NP);
    memcpy(h_c15_.data, fc_c15, sizeof(T)*NP);
    memcpy(h_c16_.data, fc_c16, sizeof(T)*NP);

    memcpy(h_c22_.data, fc_c22, sizeof(T)*NP);
    memcpy(h_c23_.data, fc_c23, sizeof(T)*NP);
    memcpy(h_c24_.data, fc_c24, sizeof(T)*NP);
    memcpy(h_c25_.data, fc_c25, sizeof(T)*NP);
    memcpy(h_c26_.data, fc_c26, sizeof(T)*NP);

    memcpy(h_c33_.data, fc_c33, sizeof(T)*NP);
    memcpy(h_c34_.data, fc_c34, sizeof(T)*NP);
    memcpy(h_c35_.data, fc_c35, sizeof(T)*NP);
    memcpy(h_c36_.data, fc_c36, sizeof(T)*NP);

    memcpy(h_c44_.data, fc_c44, sizeof(T)*NP);
    memcpy(h_c45_.data, fc_c45, sizeof(T)*NP);
    memcpy(h_c46_.data, fc_c46, sizeof(T)*NP);

    memcpy(h_c55_.data, fc_c55, sizeof(T)*NP);
    memcpy(h_c56_.data, fc_c56, sizeof(T)*NP);

    memcpy(h_c66_.data, fc_c66, sizeof(T)*NP);

  }

  void transfert_reference_element(T* fc_wgllwgll_xy, T* fc_hprime_wgll_xx, T* fc_hprime_xxT){
    auto location = access_location::host; 
    this->wgllwgll_xy.resize(NGLL2DC);
    this->hprime_wgll_xx.resize(NGLL2DC);
    this->hprime_xxT.resize(NGLL2DC);
    ArrayHandle<T> h_wgllwgll_xy_(this->wgllwgll_xy, location, access_mode::readwrite);
    ArrayHandle<T> h_hprime_wgll_xx_(this->hprime_wgll_xx, location, access_mode::readwrite);
    ArrayHandle<T> h_hprime_xxT_(this->hprime_xxT, location, access_mode::readwrite);
    memcpy(h_wgllwgll_xy_.data,fc_wgllwgll_xy, sizeof(T)*NGLL2DC);
    memcpy(h_hprime_wgll_xx_.data,fc_hprime_wgll_xx, sizeof(T)*NGLL2DC);
    memcpy(h_hprime_xxT_.data, fc_hprime_xxT, sizeof(T)*NGLL2DC);
  }

  void transfert_ibool(int* fc_ibool){
    auto location = access_location::host; 
    int NP = this->NSPEC*NGLL3DC;
    this->ibool.resize(NP);
    ArrayHandle<int> h_ibool_(this->ibool, location, access_mode::readwrite);
    memcpy(h_ibool_.data, fc_ibool, sizeof(int)*NP);
  }

  void transfert_phase_inner(int* fc_phase_ispec_inner, 
  int nspec_outer, int nspec_inner)
  {
    auto location = access_location::host; 
    this->NSPEC_INNER = nspec_inner;
    this->NSPEC_OUTER = nspec_outer;
    int NP = std::max(this->NSPEC_INNER, this->NSPEC_OUTER);
    this->phase_ispec_inner.resize(2*NP);
    ArrayHandle<int> h_phase_ispec_inner_(this->phase_ispec_inner, location, access_mode::readwrite);
    memcpy(h_phase_ispec_inner_.data, fc_phase_ispec_inner, sizeof(int)*2*NP);
  }

  void transfert_mpi_boundary(int* fc_ibool_interfaces_ext, int* fc_nibool_interfaces_ext, 
  int nb_interfaces, int max_nibool_interfaces_ext, int nspec_interfaces_max){
    //std::cout << " transfert mpi boundary " << max_nibool_interfaces_ext << std::endl;
    auto location = access_location::host;
    this->nb_mpi_interfaces = nb_interfaces;
    this->max_nibool_interfaces = max_nibool_interfaces_ext;
    this->nspec_interfaces_max = nspec_interfaces_max;
    this->ibool_interfaces_ext_mesh.resize(NGLL2DC*nspec_interfaces_max*nb_interfaces);
    this->nibool_interfaces_ext_mesh.resize(nb_interfaces);
    this->offset_ibool_interf = NGLL2DC*nspec_interfaces_max;
    // this->mpi_send_buf.resize(this->NDIM*max_nibool_interfaces_ext*nb_interfaces);
    // this->mpi_recv_buf.resize(this->NDIM*max_nibool_interfaces_ext*nb_interfaces);
    ArrayHandle<int> h_nibool_interfaces_ext_mesh_(this->nibool_interfaces_ext_mesh, location, access_mode::readwrite);
    ArrayHandle<int> h_ibool_interfaces_ext_mesh_(this->ibool_interfaces_ext_mesh, location, access_mode::readwrite);
    memcpy(h_ibool_interfaces_ext_mesh_.data, fc_ibool_interfaces_ext, sizeof(int)*NGLL2DC*nspec_interfaces_max*nb_interfaces);
    memcpy(h_nibool_interfaces_ext_mesh_.data, fc_nibool_interfaces_ext, sizeof(int)*nb_interfaces);
    // prepare buffer for GPU2GPU MPI communication
    //printf(h_ibool_interfaces_ext_mesh_.data[999])
    this->size_mpi_buffer =  this->NDIM * nb_interfaces * max_nibool_interfaces_ext;
    cudaMallocHost((void**)&(this->h_mpi_send_buf),sizeof(T)*(this->size_mpi_buffer));
    cudaMallocHost((void**)&(this->h_mpi_recv_buf),sizeof(T)*(this->size_mpi_buffer));
    cudaMalloc((void**)&(this->d_mpi_send_buf),sizeof(T)*(this->size_mpi_buffer));
    cudaMalloc((void**)&(this->d_mpi_recv_buf),sizeof(T)*(this->size_mpi_buffer));
  }

  void transfert_boundary(int* fc_index_gll_boundary, T* fc_wstacey, 
  T* fc_rho_vp, T* fc_rho_vs, T* fc_normal, int ngll_boundary){
    auto location = access_location::host;
    this->NGLL_BOUNDARY=ngll_boundary;

    this->index_gll_boundary.resize(this->NGLL_BOUNDARY);
    this->wstacey.resize(this->NGLL_BOUNDARY);
    this->rho_vp.resize(this->NGLL_BOUNDARY);
    this->rho_vs.resize(this->NGLL_BOUNDARY);
    this->normal.resize(this->NGLL_BOUNDARY*this->NCOMP);
    
    //std::cout << " Transfert Boundary " << this->NGLL_BOUNDARY << " " << this->NCOMP  << std::endl;
    
    ArrayHandle<int> h_index_gll_boundary_(this->index_gll_boundary, location, access_mode::readwrite);
    ArrayHandle<T> h_wstacey_(this->wstacey, location, access_mode::readwrite);
    ArrayHandle<T> h_rho_vp_(this->rho_vp, location, access_mode::readwrite);
    ArrayHandle<T> h_rho_vs_(this->rho_vs, location, access_mode::readwrite);
    ArrayHandle<T> h_normal_(this->normal, location, access_mode::readwrite);
    
    memcpy(h_index_gll_boundary_.data, fc_index_gll_boundary, sizeof(int)*this->NGLL_BOUNDARY);
    memcpy(h_wstacey_.data, fc_wstacey, sizeof(T)*this->NGLL_BOUNDARY);
    memcpy(h_rho_vp_.data, fc_rho_vp, sizeof(T)*this->NGLL_BOUNDARY);
    memcpy(h_rho_vs_.data, fc_rho_vs, sizeof(T)*this->NGLL_BOUNDARY);
    memcpy(h_normal_.data, fc_normal, sizeof(T)*this->NGLL_BOUNDARY*this->NCOMP);
  }

  void transfert_source(int* fc_ispec_source, T* fc_Fx, T* fc_Fy, T* fc_Fz, 
  T* fc_hxis, T* fc_hetas, T* fc_hgammas, T* fc_used_stf, int nsrc, int nt){
    auto location = access_location::host;
    this->NSOURCES=nsrc;
    int n1=this->NSOURCES;
    int n2=this->NSOURCES*NGLLC; // assume NGLLX=NGLLY=NGLLZ==NGLLC
    this->ispec_src.resize(n1);
    this->Fx.resize(n1);
    this->Fy.resize(n1);
    this->Fz.resize(n1);
    this->hxis.resize(n2);
    this->hetas.resize(n2);
    this->hgammas.resize(n2);
    this->used_stf.resize(nt);
    ArrayHandle<int> h_ispec_src_(this->ispec_src, location, access_mode::readwrite);
    ArrayHandle<T> h_Fx_(this->Fx, location, access_mode::readwrite);
    ArrayHandle<T> h_Fy_(this->Fy, location, access_mode::readwrite);
    ArrayHandle<T> h_Fz_(this->Fz, location, access_mode::readwrite);
    ArrayHandle<T> h_hxis_(this->hxis, location, access_mode::readwrite);
    ArrayHandle<T> h_hetas_(this->hetas, location, access_mode::readwrite);
    ArrayHandle<T> h_hgammas_(this->hgammas, location, access_mode::readwrite);
    ArrayHandle<T> h_used_stf_(this->used_stf, location, access_mode::readwrite);
    memcpy(h_ispec_src_.data, fc_ispec_source, sizeof(int)*n1);
    memcpy(h_Fx_.data, fc_Fx, sizeof(T)*n1);
    memcpy(h_Fy_.data, fc_Fy, sizeof(T)*n1);
    memcpy(h_Fz_.data, fc_Fz, sizeof(T)*n1);
    memcpy(h_hxis_.data, fc_hxis, sizeof(T)*n2);
    memcpy(h_hetas_.data, fc_hetas, sizeof(T)*n2);
    memcpy(h_hgammas_.data, fc_hgammas, sizeof(T)*n2);
    memcpy(h_used_stf_.data, fc_used_stf, sizeof(T)*nt);
  }

  void transfert_receiver(int* fc_ispec_rec, T* fc_hxir, T* fc_hetar, T* fc_hgammar, 
  T* fc_seismogram_d, int nrec, int nstep){
    auto location = access_location::host;
    this->NREC=nrec;
    this->NSTEP=nstep;
    int n1=this->NREC;
    int n2=this->NREC*NGLLC; // assume NGLLX=NGLLY=NGLLZ==NGLLC
    int n3=this->NREC*this->NCOMP*this->NSTEP;
    this->ispec_rec.resize(n1);
    this->hxir.resize(n2);
    this->hetar.resize(n2);
    this->hgammar.resize(n2);
    this->seismogram_d.resize(n3);
    ArrayHandle<int> h_ispec_rec_(this->ispec_rec, location, access_mode::readwrite);
    ArrayHandle<T> h_hxir_(this->hxir, location, access_mode::readwrite);
    ArrayHandle<T> h_hetar_(this->hetar, location, access_mode::readwrite);
    ArrayHandle<T> h_hgammar_(this->hgammar, location, access_mode::readwrite);
    ArrayHandle<T> h_seismogram_d_(this->seismogram_d, location, access_mode::readwrite);
    memcpy(h_ispec_rec_.data, fc_ispec_rec, sizeof(int)*n1);
    memcpy(h_hxir_.data, fc_hxir, sizeof(T)*n2);
    memcpy(h_hetar_.data, fc_hetar, sizeof(T)*n2);
    memcpy(h_hgammar_.data, fc_hgammar, sizeof(T)*n2);
    memcpy(h_seismogram_d_.data, fc_seismogram_d, sizeof(T)*n3);
  }
  
  void transfert_deltat(T dt){
    this->DT = dt;
  }

  void get_displ(T* fc_displ){
    auto location = access_location::host;
    int NP = this->NGLOB*this->NCOMP;
    ArrayHandle<T> h_displ_(this->displ,location, access_mode::readwrite);
    memcpy(fc_displ, h_displ_.data, sizeof(T)*NP);
  }

  void get_veloc(T* fc_veloc){
    auto location = access_location::host;
    int NP = this->NGLOB*this->NCOMP;
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::readwrite);
    memcpy(fc_veloc, h_veloc_.data, sizeof(T)*NP);
    //this->check_veloc();
  }

  void get_accel(T* fc_accel){
    auto location = access_location::host;
    int NP = this->NGLOB*this->NCOMP;
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    memcpy(fc_accel, h_accel_.data, sizeof(T)*NP);
  }

  void set_accel(T* fc_accel){
    auto location = access_location::host;
    int NP = this->NGLOB*this->NCOMP;
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    memcpy(h_accel_.data, fc_accel, sizeof(T)*NP);
    //std::cout << " set accel to it : " << h_accel_.data[3*35888 +2] << std::endl;
  }

  void get_seismogram_d(T* fc_seismogram_d){
    auto location = access_location::host;
    int n=this->NREC*this->NCOMP*this->NSTEP;
    ArrayHandle<T> h_seismogram_d_(this->seismogram_d, location, access_mode::readwrite);
    memcpy(fc_seismogram_d, h_seismogram_d_.data, sizeof(T)*n);
  }

  void predictor(){
    auto location = access_location::device;
    ArrayHandle<T> h_displ_(this->displ,location, access_mode::readwrite);
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::readwrite);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    T* p_displ = h_displ_.data;
    T* p_veloc = h_veloc_.data;
    T* p_accel = h_accel_.data;
    T dt = this->DT; 
    T dt_2 = 0.5*dt;
    T dts2 = dt*dt*0.5;
    int n = this->NGLOB*this->NCOMP;
    predictor_CUDA(p_displ, p_veloc, p_accel, dt, dt_2, dts2, n, this->compute_stream);
  }

  void corrector(){
    auto location = access_location::device;
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::readwrite);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::read);
    T* p_veloc = h_veloc_.data;
    const T* p_accel = h_accel_.data; 
    T dt_2 = 0.5*this->DT;
    int n = this->NGLOB*this->NCOMP;
    corrector_CUDA(p_veloc, p_accel, dt_2, n, this->compute_stream);
  }

  void apply_inv_matrix(){
    auto location = access_location::device;
    int n = this->NGLOB*this->NCOMP;
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    T* p_accel = h_accel_.data; 
    ArrayHandle<T> h_inv_mass_(this->inv_mass,location, access_mode::read);
    const T* p_invmass = h_inv_mass_.data;
    apply_invmass_CUDA(p_accel, p_invmass,  n, this->compute_stream);
  }

  void compute_rhs(int iphase){
    auto location = access_location::device;
    ArrayHandle<T> h_dxi_dx_(this->dxi_dx,location, access_mode::read);
    ArrayHandle<T> h_dxi_dy_(this->dxi_dy,location, access_mode::read);
    ArrayHandle<T> h_dxi_dz_(this->dxi_dz,location, access_mode::read);
    ArrayHandle<T> h_deta_dx_(this->deta_dx,location, access_mode::read);
    ArrayHandle<T> h_deta_dy_(this->deta_dy,location, access_mode::read);
    ArrayHandle<T> h_deta_dz_(this->deta_dz,location, access_mode::read);
    ArrayHandle<T> h_dgamma_dx_(this->dgamma_dx,location, access_mode::read);
    ArrayHandle<T> h_dgamma_dy_(this->dgamma_dy,location, access_mode::read);
    ArrayHandle<T> h_dgamma_dz_(this->dgamma_dz,location, access_mode::read);
    const T* __restrict__ p_dxi_dx = h_dxi_dx_.data;
    const T* __restrict__ p_dxi_dy = h_dxi_dy_.data;
    const T* __restrict__ p_dxi_dz = h_dxi_dz_.data;
    const T* __restrict__ p_deta_dx = h_deta_dx_.data;
    const T* __restrict__ p_deta_dy = h_deta_dy_.data;
    const T* __restrict__ p_deta_dz = h_deta_dz_.data;
    const T* __restrict__ p_dgamma_dx = h_dgamma_dx_.data;
    const T* __restrict__ p_dgamma_dy = h_dgamma_dy_.data;
    const T* __restrict__ p_dgamma_dz = h_dgamma_dz_.data;


    ArrayHandle<T> h_c11_(this->c11,location, access_mode::read);
    ArrayHandle<T> h_c12_(this->c12,location, access_mode::read);
    ArrayHandle<T> h_c13_(this->c13,location, access_mode::read);
    ArrayHandle<T> h_c14_(this->c14,location, access_mode::read);
    ArrayHandle<T> h_c15_(this->c15,location, access_mode::read);
    ArrayHandle<T> h_c16_(this->c16,location, access_mode::read);

    ArrayHandle<T> h_c22_(this->c22,location, access_mode::read);
    ArrayHandle<T> h_c23_(this->c23,location, access_mode::read);
    ArrayHandle<T> h_c24_(this->c24,location, access_mode::read);
    ArrayHandle<T> h_c25_(this->c25,location, access_mode::read);
    ArrayHandle<T> h_c26_(this->c26,location, access_mode::read);

    ArrayHandle<T> h_c33_(this->c33,location, access_mode::read);
    ArrayHandle<T> h_c34_(this->c34,location, access_mode::read);
    ArrayHandle<T> h_c35_(this->c35,location, access_mode::read);
    ArrayHandle<T> h_c36_(this->c36,location, access_mode::read);

    ArrayHandle<T> h_c44_(this->c44,location, access_mode::read);
    ArrayHandle<T> h_c45_(this->c45,location, access_mode::read);
    ArrayHandle<T> h_c46_(this->c46,location, access_mode::read);

    ArrayHandle<T> h_c55_(this->c55,location, access_mode::read);
    ArrayHandle<T> h_c56_(this->c56,location, access_mode::read);
    
    ArrayHandle<T> h_c66_(this->c66,location, access_mode::read);
    
    const T* __restrict__ p_c11 = h_c11_.data;   
    const T* __restrict__ p_c12 = h_c12_.data; 
    const T* __restrict__ p_c13 = h_c13_.data;  
    const T* __restrict__ p_c14 = h_c14_.data;   
    const T* __restrict__ p_c15 = h_c15_.data; 
    const T* __restrict__ p_c16 = h_c16_.data; 

    const T* __restrict__ p_c22 = h_c22_.data; 
    const T* __restrict__ p_c23 = h_c23_.data;  
    const T* __restrict__ p_c24 = h_c24_.data;   
    const T* __restrict__ p_c25 = h_c25_.data; 
    const T* __restrict__ p_c26 = h_c26_.data; 

    const T* __restrict__ p_c33 = h_c33_.data;  
    const T* __restrict__ p_c34 = h_c34_.data;   
    const T* __restrict__ p_c35 = h_c35_.data; 
    const T* __restrict__ p_c36 = h_c36_.data; 

    const T* __restrict__ p_c44 = h_c44_.data;   
    const T* __restrict__ p_c45 = h_c45_.data; 
    const T* __restrict__ p_c46 = h_c46_.data; 

    const T* __restrict__ p_c55 = h_c55_.data; 
    const T* __restrict__ p_c56 = h_c56_.data; 
    
    const T* __restrict__ p_c66 = h_c66_.data;


    ArrayHandle<int> h_ibool_(this->ibool, location, access_mode::read);
    const int* __restrict__ p_ibool = h_ibool_.data;

    ArrayHandle<int> h_phase_ispec_inner_(this->phase_ispec_inner, location, access_mode::read);
    const int* __restrict__ p_phase_ispec_inner=h_phase_ispec_inner_.data;

    ArrayHandle<T> h_wgllwgll_xy_(this->wgllwgll_xy, location, access_mode::read);
    ArrayHandle<T> h_hprime_wgll_xx_(this->hprime_wgll_xx, location, access_mode::read);
    ArrayHandle<T> h_hprime_xxT_(this->hprime_xxT, location, access_mode::read);
    const T* __restrict__ p_wgllwgll_xy = h_wgllwgll_xy_.data;
    const T* __restrict__ p_hprime_wgll_xx = h_hprime_wgll_xx_.data;
    const T* __restrict__ p_hprime_xxT = h_hprime_xxT_.data;

    ArrayHandle<T> h_displ_(this->displ,location, access_mode::read);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    const T* __restrict__ p_displ = h_displ_.data;
    T* __restrict__ p_accel = h_accel_.data;

    const int nspec=this->NSPEC;
    const int nspec_inner=this->NSPEC_INNER;
    const int nspec_outer=this->NSPEC_OUTER;
    const int nb_phase_ispec = std::max(nspec_outer, nspec_inner);
    
    compute_internal_forces_elastic_aniso_3D_CUDA(p_accel, p_displ,
    p_dxi_dx, p_dxi_dy,  p_dxi_dz, 
    p_deta_dx, p_deta_dy, p_deta_dz, 
    p_dgamma_dx, p_dgamma_dy, p_dgamma_dz,
    p_c11, p_c12, p_c13, p_c14, p_c15, p_c16,
    p_c22, p_c23, p_c24, p_c25, p_c26,
    p_c33, p_c34, p_c35, p_c36,
    p_c44, p_c45, p_c46,
    p_c55, p_c56,
    p_c66,
    p_wgllwgll_xy, p_hprime_wgll_xx, p_hprime_xxT,
    p_ibool, p_phase_ispec_inner, 
    nspec, nspec_inner, nspec_outer, nb_phase_ispec, iphase, this->compute_stream);
    
    //this->check_accel();

  }

  void add_source_force(){
    auto location = access_location::device;
    ArrayHandle<int> h_ispec_src_(this->ispec_src, location, access_mode::read);
    ArrayHandle<T> h_Fx_(this->Fx, location, access_mode::read);
    ArrayHandle<T> h_Fy_(this->Fy, location, access_mode::read);
    ArrayHandle<T> h_Fz_(this->Fz, location, access_mode::read);
    ArrayHandle<T> h_hxis_(this->hxis, location, access_mode::read);
    ArrayHandle<T> h_hetas_(this->hetas, location, access_mode::read);
    ArrayHandle<T> h_hgammas_(this->hgammas, location, access_mode::read);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    ArrayHandle<int> h_ibool_(this->ibool, location, access_mode::read);
    ArrayHandle<T> h_used_stf_(this->used_stf, location, access_mode::readwrite);
    const int* __restrict__ p_ibool = h_ibool_.data;
    T* __restrict__ p_accel = h_accel_.data;
    const T* __restrict__ p_Fx = h_Fx_.data;
    const T* __restrict__ p_Fy = h_Fy_.data;
    const T* __restrict__ p_Fz = h_Fz_.data;
    const T* __restrict__ p_hxis = h_hxis_.data;
    const T* __restrict__ p_hetas = h_hetas_.data;
    const T* __restrict__ p_hgammas = h_hgammas_.data;
    const T* __restrict__ p_used_stf = h_used_stf_.data;
    const int* __restrict__ p_ispec_src = h_ispec_src_.data;
    int NSRC = this->NSOURCES;
    int NT = this->NSTEP; 
    int IT = this->it;
    add_source_force_CUDA(p_accel, p_Fx, p_Fy, p_Fz, 
    p_hxis, p_hetas, p_hgammas, p_used_stf, p_ibool, p_ispec_src, 
    NSRC, NT, IT, this->compute_stream);
    this->it++;
  }

  void store_seismogram(){
    auto location = access_location::device;  
    ArrayHandle<int> h_ispec_rec_(this->ispec_rec, location, access_mode::read);
    ArrayHandle<T> h_hxir_(this->hxir, location, access_mode::read);
    ArrayHandle<T> h_hetar_(this->hetar, location, access_mode::read);
    ArrayHandle<T> h_hgammar_(this->hgammar, location, access_mode::read);
    ArrayHandle<T> h_displ_(this->displ,location, access_mode::read);
    ArrayHandle<int> h_ibool_(this->ibool, location, access_mode::read);
    ArrayHandle<T> h_seismogram_d_(this->seismogram_d, location, access_mode::readwrite);
    const int* __restrict__ p_ispec_rec = h_ispec_rec_.data;
    const int* __restrict__ p_ibool = h_ibool_.data;
    const T* __restrict__ p_displ = h_displ_.data;
    const T* __restrict__ p_hxir = h_hxir_.data;
    const T* __restrict__ p_hetar = h_hetar_.data;
    const T* __restrict__ p_hgammar = h_hgammar_.data;
    T* __restrict__ p_seismogram_d = h_seismogram_d_.data; 
    int IT = this->it-1;
    int NREC =this->NREC;
    store_seismogram_CUDA(p_seismogram_d, p_displ, p_hxir, p_hetar, p_hgammar, 
    p_ispec_rec, p_ibool, IT, NREC, this->compute_stream);
  }
 
  void compute_stacey(){
    auto location = access_location::device;
    ArrayHandle<int> h_index_gll_boundary_(this->index_gll_boundary, location, access_mode::read);
    ArrayHandle<T> h_wstacey_(this->wstacey, location, access_mode::read);
    ArrayHandle<T> h_rho_vp_(this->rho_vp, location, access_mode::read);
    ArrayHandle<T> h_rho_vs_(this->rho_vs, location, access_mode::read);
    ArrayHandle<T> h_normal_(this->normal, location, access_mode::read);
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::read);
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    const int* __restrict__ p_index_gll_boundary = h_index_gll_boundary_.data;
    const T* __restrict__ p_wstacey = h_wstacey_.data;
    const T* __restrict__ p_rho_vp = h_rho_vp_.data;
    const T* __restrict__ p_rho_vs = h_rho_vs_.data;
    const T* __restrict__ p_normal = h_normal_.data;
    const T* __restrict__ p_veloc = h_veloc_.data;
    T* __restrict__ p_accel = h_accel_.data;
    stacey_elastic_boundary_condition3D_CUDA(p_accel, p_veloc, 
    p_rho_vp, p_rho_vs, p_wstacey, p_normal, p_index_gll_boundary, 
    this->NGLL_BOUNDARY, this->compute_stream);
  } 

  void prepare_mpi_buffer(){
    if (this->size_mpi_buffer > 0) {
      auto location = access_location::device;
      ArrayHandle<T> h_accel_(this->accel,location, access_mode::read);
      ArrayHandle<int> h_nibool_interfaces_ext_mesh_(this->nibool_interfaces_ext_mesh, location, access_mode::read);
      ArrayHandle<int> h_ibool_interfaces_ext_mesh_(this->ibool_interfaces_ext_mesh, location, access_mode::read);
      const T* __restrict__ p_accel = h_accel_.data;
      const int* __restrict__ p_nibool_interfaces_ext_mesh = h_nibool_interfaces_ext_mesh_.data;
      const int* __restrict__ p_ibool_interfaces_ext_mesh = h_ibool_interfaces_ext_mesh_.data;
      prepare_MPI_buffer_CUDA(this->d_mpi_send_buf, this->h_mpi_send_buf, p_accel,
      p_nibool_interfaces_ext_mesh, p_ibool_interfaces_ext_mesh, this->offset_ibool_interf,
      this->nb_mpi_interfaces, this->max_nibool_interfaces, this->size_mpi_buffer,
      this->compute_stream, this->copy_stream);
    }
  }

  void get_mpi_buffer(T* fc_mpi_buffer){
    if (this->size_mpi_buffer > 0) {
      cudaStreamSynchronize(this->copy_stream);
      memcpy(fc_mpi_buffer,  this->h_mpi_send_buf, sizeof(T)*(this->size_mpi_buffer));
    }
  }

  void get_recv_mpi_buffer(T* fc_mpi_buffer){
    if (this->size_mpi_buffer > 0) {

      memcpy(this->h_mpi_recv_buf, fc_mpi_buffer, sizeof(T)*(this->size_mpi_buffer));
      cudaMemcpyAsync(this->d_mpi_recv_buf, this->h_mpi_recv_buf,
      size_mpi_buffer*sizeof(T),cudaMemcpyHostToDevice,this->copy_stream);
      //cudaStreamSynchronize(this->compute_stream);
    }
  }

  void assemble_mpi_accel(){
    if (this->size_mpi_buffer > 0) {
    auto location = access_location::device;
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::readwrite);
    ArrayHandle<int> h_nibool_interfaces_ext_mesh_(this->nibool_interfaces_ext_mesh, location, access_mode::read);
    ArrayHandle<int> h_ibool_interfaces_ext_mesh_(this->ibool_interfaces_ext_mesh, location, access_mode::read);
    T* __restrict__ p_accel = h_accel_.data;
    const int* __restrict__ p_nibool_interfaces_ext_mesh = h_nibool_interfaces_ext_mesh_.data;
    const int* __restrict__ p_ibool_interfaces_ext_mesh = h_ibool_interfaces_ext_mesh_.data;
    assemble_MPI_vector_CUDA(p_accel, this->d_mpi_recv_buf, p_nibool_interfaces_ext_mesh, p_ibool_interfaces_ext_mesh,
      this->offset_ibool_interf,this->nb_mpi_interfaces, this->max_nibool_interfaces, this->size_mpi_buffer,
      this->compute_stream, this->copy_stream);
    }
  }

  void check_accel(){
    auto location = access_location::host;
    ArrayHandle<T> h_accel_(this->accel,location, access_mode::read);
    for (int iglob=0;iglob<this->NGLOB;iglob++) {
      if (isnan(h_accel_.data[3*iglob])) {
        std::cout << "NaN 1 : " << iglob << std::endl;
      }
      if (isnan(h_accel_.data[3*iglob+1])) {
        std::cout << "NaN 2 : " << iglob << std::endl;
      }
      if (isnan(h_accel_.data[3*iglob+2])) {
        std::cout << "NaN 3 : " << iglob << std::endl;
      }
    }
  }
 void check_veloc(){
    auto location = access_location::host;
    ArrayHandle<T> h_veloc_(this->veloc,location, access_mode::read);
    for (int iglob=0;iglob<this->NGLOB;iglob++) {
      if (isnan(h_veloc_.data[3*iglob])) {
        std::cout << "NaN 1 : " << iglob << std::endl;
      }
      if (isnan(h_veloc_.data[3*iglob+1])) {
        std::cout << "NaN 2 : " << iglob << std::endl;
      }
      if (isnan(h_veloc_.data[3*iglob+2])) {
        std::cout << "NaN 3 : " << iglob << std::endl;
      }
    }
  }
  void print_this(){
    std::cout<<std::endl; 
    std::cout << " SPECFEM CONSTANTS ON GPU " << std::endl;
    std::cout << "NSPEC " << this->NSPEC << std::endl;
    std::cout << "NGLOB " << this->NGLOB << std::endl;
    std::cout << "DT " << this->DT << std::endl;
    std::cout << " NGLL BOUNDAY " << this->NGLL_BOUNDARY << std::endl; 
    std::cout << " NCOMP " << this->NCOMP << std::endl;
    std::cout << " NSOURCES " << this->NSOURCES << std::endl;
    std::cout << " NREC " << this->NREC << std::endl;
    std::cout << " nb mpi interf  " << this->nb_mpi_interfaces << std::endl;
    std::cout << " max nibool interf " << this->max_nibool_interfaces << std::endl;
    std::cout << " nspec interf max " << this->nspec_interfaces_max << std::endl;
    // auto location = access_location::host;
    // ArrayHandle<int> h_ibool_(this->ibool,location, access_mode::read);
    // std::cout << " ibool[0]=" <<  h_ibool_.data[0] << std::endl; 
    // std::cout << " Shape ibool interface " << this->offset_ibool_interf << " " << this->nb_mpi_interfaces << std::endl;
    // ArrayHandle<int> h_ibool_interfaces_ext_mesh_(this->ibool_interfaces_ext_mesh, location, access_mode::read);
    // ArrayHandle<int> h_nibool_interfaces_ext_mesh_(this->nibool_interfaces_ext_mesh, location, access_mode::read);
    // for (int interf = 0; interf < this->nb_mpi_interfaces; interf++) {
    //   std::cout << std::endl << " interface " << interf << " " << h_nibool_interfaces_ext_mesh_.data[interf] << std::endl << std::endl;
    //   for (int ipoint=0; ipoint < h_nibool_interfaces_ext_mesh_.data[interf]; ipoint++) {
    //     std::cout << " " << h_ibool_interfaces_ext_mesh_.data[ipoint + this->offset_ibool_interf*interf]; 
    //   }
    //   std::cout << std::endl;
    // }
    // std::cout << std::endl;
    // auto location = access_location::host;
    // int NP = this->NSPEC*NGLL3DC;
    // ArrayHandle<T> h_dxi_dx_(this->dxi_dx,location, access_mode::readwrite);
    // for (int i;i<NGLL3DC; i++) {
    //   std::cout << 'i '<<  h_dxi_dx_.data[i] << " ";
    // }
    //std::cout<<std::endl; 
  }

public:
  
  int NDIM;
  int NSTEP;
  int NSPEC; 
  int NGLOB; 
  int NCOMP;
  int NGLL_BOUNDARY;
  int NSOURCES;
  int NREC;
  int NSPEC_INNER; 
  int NSPEC_OUTER;  
  T DT;
  
  // current time step
  int it;
  
  // dof indexation 
  GPUCPUArray<int> ibool;

  // computation phase (outer element in MPI slice / inner element in MPI slice)
  GPUCPUArray<int> phase_ispec_inner;
 
  // MPI interfaces
  int size_mpi_buffer, offset_ibool_interf; 
  T* d_mpi_send_buf; 
  T* d_mpi_recv_buf; 
  T* h_mpi_send_buf;
  T* h_mpi_recv_buf; // pinned memory
  GPUCPUArray<int> nibool_interfaces_ext_mesh, ibool_interfaces_ext_mesh;
  int nb_mpi_interfaces, max_nibool_interfaces, nspec_interfaces_max;

  // mesh and model 
  // geometric factors
  GPUCPUArray<T>  dxi_dx, dxi_dy, dxi_dz, 
    deta_dx, deta_dy, deta_dz,
    dgamma_dx, dgamma_dy, dgamma_dz;
 
  // material properties
  //GPUCPUArray<T> rho, kappa, mu;
  GPUCPUArray<T> c11, c12, c13, c14, c15, c16;
  GPUCPUArray<T> c22, c23, c24, c25, c26;
  GPUCPUArray<T> c33, c34, c35, c36;
  GPUCPUArray<T> c44, c45, c46;
  GPUCPUArray<T> c55, c56;
  GPUCPUArray<T> c66;
  
  // inverse mass matrix 
  GPUCPUArray<T> inv_mass;
 
  // element local quantities 
  // GPUCPUArray<T> wxgll, wygll, wzgll, 
  // wgllwgll_yz, wgllwgll_xz, wgllwgll_xy,
  // hprimewgll_xx, hprime_xxT, 
  // hprimewgll_yy, hprime_yyT, 
  // hprimewgll_zz, hprime_zzT;

  // fileds
  GPUCPUArray<T>  displ, veloc, accel; 

  // reference element : assume NGLLX=NGLLY=NGLLZ,  thus use only those 3 arrays 
  GPUCPUArray<T> wgllwgll_xy, hprime_wgll_xx, hprime_xxT;

  // boundary condition 
  GPUCPUArray<int> index_gll_boundary;
  GPUCPUArray<T> wstacey, rho_vp, rho_vs, normal;

  // force sources
  GPUCPUArray<T> Fx, Fy, Fz;
  GPUCPUArray<T> hxis, hetas, hgammas;
  GPUCPUArray<T> used_stf;
  GPUCPUArray<int> ispec_src;

  // stations
  GPUCPUArray<int> ispec_rec;
  GPUCPUArray<T> hxir, hetar, hgammar;
  GPUCPUArray<T> seismogram_d;

  // GPU configuration
   cudaStream_t compute_stream, copy_stream;
};
#endif 
