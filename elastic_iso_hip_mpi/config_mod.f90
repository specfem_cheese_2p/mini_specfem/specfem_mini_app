module config_mod

  implicit none

  !! ------------------------------ global parameters -------------------
  !! specfem precision computation 
  integer, parameter :: CUSTOM_REAL=4

  !! discretization in element 
  integer, parameter :: NGLLX=5, NGLLY=5, NGLLZ=5 
 
  ! very large and very small values
  double precision, parameter :: HUGEVAL = 1.d+30,TINYVAL = 1.d-9

  ! Gauss-Lobatto point definition
  double precision, parameter :: GAUSSALPHA = 0.d0, GAUSSBETA = 0.d0

  ! dimensions 
  integer, parameter :: NGNOD = 8, NDIM = 3

  ! character len 
  integer, parameter :: CHAR_LEN = 512

  ! small negligible initial value to avoid very slow underflow trapping
  ! but not too small to avoid trapping on velocity and acceleration in Newmark
  real(kind=CUSTOM_REAL), parameter :: VERYSMALLVAL = 1.E-24_CUSTOM_REAL
  
  ! constants
  double precision, parameter :: PI = 3.141592653589793d0

  !! verbose outputs 
  logical, parameter :: verbose=.true.

  !! check stablity of computation
  logical, parameter :: check_stability=.false.

  !! for verbose mode inforamtion 
  integer, parameter :: NTSTEP_BETWEEN_OUTPUT_INFO=1000

  !! for movies
  logical, parameter :: movie=.false.
  
  logical, parameter :: debug=.false.
  
  !!---------------------------------------------------------------------------

  ! empirical choice to estimate time step and period resolved:
  ! Courant number for time step estimate
  real(kind=CUSTOM_REAL),parameter :: COURANT_SUGGESTED = 0.25
  ! number of points per minimum wavelength for minimum period estimate
  real(kind=CUSTOM_REAL),parameter :: NPTS_PER_WAVELENGTH = 5

  
  !! ---------------------- user case  parameters --------------------
  !! domain boundary  
  double precision :: xmin_config, xmax_config, ymin_config, ymax_config, zmin_config, zmax_config

  !! domain discretization (size of elements)
  double precision :: dx_config, dy_config, dz_config

  !! number of element in cartesian mesh
  integer                :: NX_config, NY_config, NZ_config
  
  !! cartesian domain decomposition (NPX is multiple of NX_element and NPY is multiple of NY_element)
  integer                :: NPX_config, NPY_config

  !! model parameter
  real(kind=CUSTOM_REAL) :: rho_config, vp_config, vs_config

  !! source position
  real(kind=CUSTOM_REAL) :: xs_config, ys_config, zs_config

  !! receiver position
  real(kind=CUSTOM_REAL) :: xr_config, yr_config, zr_config

  !! time step
  real(kind=CUSTOM_REAL) :: deltat_config
  integer :: NSTEP

  !! source time function (ricker)
  real(kind=CUSTOM_REAL) :: f0_config, amplitude_source_config

contains

  !! todo if need to read config file 
  subroutine read_config()
  end subroutine read_config

  !! hardcoded configuration 
  subroutine default_config()
    call benchmark_homogeneous()
    !call config_0()
    !call config_1()
    !call config_2()
  end subroutine default_config

  subroutine config_0()
    
    ! define domain size
    xmin_config = -1500.
    xmax_config = 1700.
    ymin_config = -1500.
    ymax_config = 1700.
    zmin_config = -1500.
    zmax_config = 1700.

    !! number of element 
    NX_config = 16
    NY_config = 16
    NZ_config = 16

    !! define domain decomposition 
    NPX_config = 1
    NPY_config = 1

    !! compute size of elements 
    dx_config = (xmax_config -xmin_config) / real(NX_config, kind=CUSTOM_REAL)  
    dy_config = (ymax_config -ymin_config) / real(NY_config, kind=CUSTOM_REAL)
    dz_config = (zmax_config -zmin_config) / real(NZ_config, kind=CUSTOM_REAL)

    !! define model
    rho_config = 2700.
    vp_config  = 6000.
    vs_config  = 3500.

    !! source position
    xs_config = -1.
    ys_config = -1.
    zs_config = -1.

    !! receiver position
    xr_config = 0.
    yr_config = 0.
    zr_config = -100.

    !!
    deltat_config  = 0.001
    NSTEP   = 3000

    f0_config=4.
    amplitude_source_config=1e10
  end subroutine config_0


  subroutine config_1()

    !! define domain size
    xmin_config = 0.
    xmax_config = 10000.
    ymin_config = 0.
    ymax_config = 10000.
    zmin_config = -5500.
    zmax_config = 0.

    !! number of element 
    NX_config = 48
    NY_config = 48
    NZ_config = 24

    !! define domain decomposition 
    NPX_config=1
    NPY_config=1

    !! use mpirun -np NPX_config*NPY_config 

    !! compute size of elements 
    dx_config = (xmax_config -xmin_config) / real(NX_config, kind=CUSTOM_REAL)
    dy_config = (ymax_config -ymin_config) / real(NY_config, kind=CUSTOM_REAL)
    dz_config = (zmax_config -zmin_config) / real(NZ_config, kind=CUSTOM_REAL)

    !! define model
    rho_config = 1200.
    vp_config  = 1500.
    vs_config  = 920.

    !! source position
    xs_config = 5000.
    ys_config = 5000.
    zs_config = -3000.

    !! receiver position
    xr_config = 2500.
    yr_config = 5000.
    zr_config = 0.

    !!
    deltat_config  = 0.005
    NSTEP   = 2000

    ! !!
    f0_config=1.
    amplitude_source_config=1e10

  end subroutine config_1

  subroutine config_2()
    ! define domain size
    xmin_config = 0.
    xmax_config = 10000.
    ymin_config = 0.
    ymax_config = 10000.
    zmin_config = -5500.
    zmax_config = 0.

    !! number of element 
    NX_config = 96
    NY_config = 96
    NZ_config = 48

    !! define domain decomposition 
    NPX_config=1
    NPY_config=1

    !! use mpirun -np NPX_config*NPY_config 

    !! compute size of elements 
    dx_config = (xmax_config -xmin_config) / real(NX_config, kind=CUSTOM_REAL)
    dy_config = (ymax_config -ymin_config) / real(NY_config, kind=CUSTOM_REAL)
    dz_config = (zmax_config -zmin_config) / real(NZ_config, kind=CUSTOM_REAL)

    !! define model
    rho_config = 1200.
    vp_config  = 1500.
    vs_config  = 920.

    !! source position
    xs_config = 5000.
    ys_config = 5000.
    zs_config = -3000.

    !! receiver position
    xr_config = 2500.
    yr_config = 5000.
    zr_config = 0.

    !!
    deltat_config  = 0.0025
    NSTEP   = 4000

    !!
    f0_config=2.
    amplitude_source_config=1e10

  end subroutine config_2

  subroutine benchmark_homogeneous()
    
    ! this benchmark is the same that in 
    ! https://github.com/SPECFEM/specfem3d/tree/devel/EXAMPLES/benchmarks/attenuation/viscoelastic
    
    ! define domain size
    xmin_config = -1500.
    xmax_config =  1500.
    ymin_config = -1500.
    ymax_config =  1500.
    zmin_config = -1500.
    zmax_config =  1500.

    !! number of element 
    NX_config = 80
    NY_config = 80
    NZ_config = 80
   
    !! define domain decomposition 
    NPX_config = 1
    NPY_config = 1
    
    !! compute size of elements 
    dx_config = (xmax_config -xmin_config) / real(NX_config, kind=CUSTOM_REAL)  
    dy_config = (ymax_config -ymin_config) / real(NY_config, kind=CUSTOM_REAL)
    dz_config = (zmax_config -zmin_config) / real(NZ_config, kind=CUSTOM_REAL)

    !! define model
    rho_config = 2000.
    vp_config  = 3297.849
    vs_config  = 2222.536
    
    !! source position
    xs_config = 10.
    ys_config = 10.
    zs_config = 10.

    !! receiver position
    xr_config = 510.
    yr_config = 510.
    zr_config = 510.

    !deltat_config  = 0.0005
    deltat_config = 0.0005
    NSTEP  = 1300

    f0_config=18.
    amplitude_source_config=1.

  end subroutine benchmark_homogeneous


end module config_mod
