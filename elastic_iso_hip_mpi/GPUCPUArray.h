// Copyright (c) 2009-2016 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.

/*! \file GPUCPUArray.h
    \brief Defines the GPUCPUArray class
*/

#ifdef NVCC
#error This header cannot be compiled by nvcc
#endif

#ifndef __GPUARRAY_H__
#define __GPUARRAY_H__

// for vector types
#ifdef ENABLE_CUDA
#include <cuda_runtime.h>
#endif

#include <string.h>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <stdlib.h>

#ifdef ENABLE_CUDA
// clean error checking
void checkForErrors(const cudaError_t status, const char *file, const int line)
{
  if (status != cudaSuccess)
    {
      std::cerr << "CUDA ERROR ";
      std::cerr << "at line " << line << " of file " << file << ":" << std::endl;
      std::cerr << cudaGetErrorString(status) << ". Exiting..." << std::endl;
      // throw an error exception
      throw(std::runtime_error("CUDA Error"));
    }
}


#define CHECK_CUDA_ERROR() { \
    cudaError_t err_sync = cudaGetLastError(); \
    checkForErrors(err_sync, __FILE__, __LINE__); \
    cudaError_t err_async = cudaDeviceSynchronize(); \
    checkForErrors(err_async, __FILE__, __LINE__); \
    }
#else
#define CHECK_CUDA_ERROR()
#endif

//! Specifies where to acquire the data
struct access_location
    {
    //! The enum
    enum Enum
        {
        host,   //!< Ask to acquire the data on the host
#ifdef ENABLE_CUDA
        device  //!< Ask to acquire the data on the device
#endif
        };
    };

//! Defines where the data is currently stored
struct data_location
    {
    //! The enum
    enum Enum
        {
        host,       //!< Data was last updated on the host
#ifdef ENABLE_CUDA
        device,     //!< Data was last updated on the device
        hostdevice  //!< Data is up to date on both the host and device
#endif
        };
    };

//! Sepcify how the data is to be accessed
struct access_mode
    {
    //! The enum
    enum Enum
        {
        read,       //!< Data will be accessed read only
        readwrite,  //!< Data will be accessed for read and write
        overwrite   //!< The data is to be completely overwritten during this aquire
        };
    };

template<class T> class GPUCPUArray;

//! Handle to access the data pointer handled by GPUCPUArray
/*! The data in GPUCPUArray is only accessible via ArrayHandle. The pointer is accessible for the lifetime of the
    ArrayHandle. When the ArrayHandle is destroyed, the GPUCPUArray is notified that the data has been released. This
    tracking mechanism provides for error checking that will cause code assertions to fail if the data is aquired
    more than once.

    ArrayHandle is intended to be used within a scope limiting its use. For example:
    \code
     GPUCPUArray<int> gpu_array(100);

    {
    ArrayHandle<int> h_handle(gpu_array, access_location::host, access_mode::readwrite);
    ... use h_handle.data ...
    }
    \endcode

    The actual raw pointer \a data should \b NOT be assumed to be the same after the handle is released.
    The pointer may in fact be re-allocated somewhere else after the handle is released and before the next handle
    is acquired.

    \ingroup data_structs
*/
template<class T> class ArrayHandle
    {
    public:

      // VM VM  added default constructor : not allowed because of const member that are not defined with this defaul constructor
      //ArrayHandle() : data(NULL) {}

	//! Aquires the data and sets \a m_data
        inline ArrayHandle(const GPUCPUArray<T>& gpu_array, const access_location::Enum location = access_location::host,
                           const access_mode::Enum mode = access_mode::readwrite);
        //! Notifies the containing GPUCPUArray that the handle has been released
        inline ~ArrayHandle();

	// VM VM added setup method 
        //void setup(const GPUCPUArray<T>& gpu_array, const access_location::Enum location = access_location::host,
	//	   const access_mode::Enum mode = access_mode::readwrite);
	
        T* const data;          //!< Pointer to data

    private:
        const GPUCPUArray<T>& m_gpu_array; //!< Reference to the GPUCPUArray that owns \a data
    };



//! Class for managing an array of elements on the GPU mirrored to the CPU
/*!
GPUCPUArray provides a template class for managing the majority of the GPU<->CPU memory usage patterns in
HOOSPH. It represents a single array of elements which is present both on the CPU and GPU. Via
ArrayHandle, classes can access the array pointers through a handle for a short time. All needed
memory transfers from the host <-> device are handled by the class based on the access mode and
location specified when acquiring an ArrayHandle.

GPUCPUArray is fairly advanced, C++ wise. It is a template class, so GPUCPUArray's of floats, float4's,
uint2's, etc.. can be made. It comes with a copy constructor and = operator so you can (expensively)
pass GPUCPUArray's around in arguments or overwite one with another via assignment (inexpensive swaps can be
performed with swap()). The ArrayHandle acquisition method guarantees that every aquired handle will be
released. About the only thing it \b doesn't do is prevent the user from writing to a pointer acquired
with a read only mode.

At a high level, GPUCPUArray encapsulates a single flat data pointer \a T* \a data with \a num_elements
elements, and keeps a copy of this data on both the host and device. When accessing this data through
the construction of an ArrayHandle instance, the \a location (host or device) you wish to access the data
must be specified along with an access \a mode (read, readwrite, overwrite).

When the data is accessed in the same location it was last written to, the pointer is simply returned.
If the data is accessed in a different location, it will be copied before the pointer is returned.

When the data is accessed in the \a read mode, it is assumed that the data will not be written to and
thus there is no need to copy memory the next time the data is aquired somewhere else. Using the readwrite
mode specifies that the data is to be read and written to, necessitating possible copies to the desired location
before the data can be accessed and again before the next access. If the data is to be completely overwritten
\b without reading it first, then an expensive memory copy can be avoided by using the \a overwrite mode.

Data with both 1-D and 2-D representations can be allocated by using the appropriate constructor.
2-D allocated data is still just a flat pointer, but the row width is rounded up to a multiple of
16 elements to facilitate coalescing. The actual allocated width is accessible with getPitch(). Here
is an example of addressing element i,j in a 2-D allocated GPUCPUArray.
\code
GPUCPUArray<int> gpu_array(100, 200, m_exec_conf);
unsigned int pitch = gpu_array.getPitch();

ArrayHandle<int> h_handle(gpu_array, access_location::host, access_mode::readwrite);
h_handle.data[i*pitch + j] = 5;
\endcode

A future modification of GPUCPUArray will allow mirroring or splitting the data across multiple GPUs.

\ingroup data_structs
*/
template<class T> class GPUCPUArray
    {
    public:
        //! Constructs a NULL GPUCPUArray
        GPUCPUArray();
        //! Constructs a 1-D GPUCPUArray
        GPUCPUArray(unsigned int num_elements);
        //! Constructs a 2-D GPUCPUArray
        // GPUCPUArray(unsigned int width, unsigned int height);
        //! Frees memory
        virtual ~GPUCPUArray();

#ifdef ENABLE_CUDA
        //! Constructs a 1-D GPUCPUArray
        GPUCPUArray(unsigned int num_elements, bool mapped);
        //! Constructs a 2-D GPUCPUArray
        // GPUCPUArray(unsigned int width, unsigned int height, bool mapped);
#endif

        //! Copy constructor
        GPUCPUArray(const GPUCPUArray& from);
        //! = operator
        GPUCPUArray& operator=(const GPUCPUArray& rhs);

        //! Swap the pointers in two GPUCPUArrays
        inline void swap(GPUCPUArray& from);

        //! Swap the pointers of two equally sized GPUCPUArrays
        inline void swap(GPUCPUArray& from) const;

        //! Get the number of elements
        /*!
         - For 1-D allocated GPUCPUArrays, this is the number of elements allocated.
         - For 2-D allocated GPUCPUArrays, this is the \b total number of elements (\a pitch * \a height) allocated
        */
        unsigned int getNumElements() const
            {
            return m_num_elements;
            }

        //! Test if the GPUCPUArray is NULL
        bool isNull() const
            {
            return (h_data == NULL);
            }

        //! Get the width of the allocated rows in elements
        /*!
         - For 2-D allocated GPUCPUArrays, this is the total width of a row in memory (including the padding added for coalescing)
         - For 1-D allocated GPUCPUArrays, this is the simply the number of elements allocated.
        */
        unsigned int getPitch() const
            {
            return m_pitch;
            }

        //! Get the number of rows allocated
        /*!
         - For 2-D allocated GPUCPUArrays, this is the height given to the constructor
         - For 1-D allocated GPUCPUArrays, this is the simply 1.
        */
        unsigned int getHeight() const
            {
            return m_height;
            }

        virtual void resize(unsigned int num_elements);

    protected:
        //! Clear memory starting from a given element
        /*! \param first The first element to clear
         */
        inline void memclear(unsigned int first=0);

        //! Acquires the data pointer for use
        inline T* aquire(const access_location::Enum location, const access_mode::Enum mode
        #ifdef ENABLE_CUDA
                         , bool async = false
        #endif
                        ) const;

        //! Release the data pointer
        inline void release() const
            {
            m_acquired = false;
            }

    private:
        mutable unsigned int m_num_elements;            //!< Number of elements
        mutable unsigned int m_pitch;                   //!< Pitch of the rows in elements
        mutable unsigned int m_height;                  //!< Number of allocated rows

        mutable bool m_acquired;                //!< Tracks whether the data has been aquired
        mutable data_location::Enum m_data_location;    //!< Tracks the current location of the data
#ifdef ENABLE_CUDA
        mutable bool m_mapped;                          //!< True if we are using mapped memory
#endif

    // ok, this looks weird, but I want m_exec_conf to be protected and not have to go reorder all of the initializers
    protected:
#ifdef ENABLE_CUDA
        mutable T* d_data;      //!< Pointer to allocated device memory
#endif

        mutable T* h_data;      //!< Pointer to allocated host memory

    private:
        //! Helper function to allocate memory
        inline void allocate();
        //! Helper function to free memory
        inline void deallocate();

#ifdef ENABLE_CUDA
        //! Helper function to copy memory from the device to host
        inline void memcpyDeviceToHost(bool async) const;
        //! Helper function to copy memory from the host to device
        inline void memcpyHostToDevice(bool async) const;
#endif

        //! Helper function to resize host array
        inline T* resizeHostArray(unsigned int num_elements);

        //! Helper function to resize device array
        inline T* resizeDeviceArray(unsigned int num_elements);

        // need to be friends of all the implementations of ArrayHandle and ArrayHandleAsync
        friend class ArrayHandle<T>;

    };

//******************************************
// ArrayHandle implementation
// *****************************************

/*! \param gpu_array GPUCPUArray host to the pointer data
    \param location Desired location to access the data
    \param mode Mode to access the data with
*/
template<class T> ArrayHandle<T>::ArrayHandle(const GPUCPUArray<T>& gpu_array, const access_location::Enum location,
                                              const access_mode::Enum mode) :
        data(gpu_array.aquire(location, mode)), m_gpu_array(gpu_array)
    {
    }
/*
template<class T> void ArrayHandle<T>::setup(const GPUCPUArray<T>& gpu_array, const access_location::Enum location,
					     const access_mode::Enum mode){
  data = gpu_array.aquire(location, mode);
  m_gpu_array = gpu_array;
}
*/
template<class T> ArrayHandle<T>::~ArrayHandle()
    {
    // assert(m_gpu_array.m_acquired);
    m_gpu_array.m_acquired = false;
    }


//******************************************
// GPUCPUArray implementation
// *****************************************

template<class T> GPUCPUArray<T>::GPUCPUArray() :
        m_num_elements(0), m_pitch(0), m_height(0), m_acquired(false), m_data_location(data_location::host),
#ifdef ENABLE_CUDA
        m_mapped(false),
        d_data(NULL),
#endif
        h_data(NULL)
    {
    }

/*! \param num_elements Number of elements to allocate in the array
    \param exec_conf Shared pointer to the execution configuration for managing CUDA initialization and shutdown
*/
template<class T> GPUCPUArray<T>::GPUCPUArray(unsigned int num_elements) :
        m_num_elements(num_elements), m_pitch(num_elements), m_height(1), m_acquired(false), m_data_location(data_location::host),
#ifdef ENABLE_CUDA
        m_mapped(false),
        d_data(NULL),
#endif
        h_data(NULL)
    {
    // allocate and clear memory
    allocate();
    memclear();
    }

/*! \param width Width of the 2-D array to allocate (in elements)
    \param height Number of rows to allocate in the 2D array
    \param exec_conf Shared pointer to the execution configuration for managing CUDA initialization and shutdown
*/
// template<class T> GPUCPUArray<T>::GPUCPUArray(unsigned int width, unsigned int height) :
//         m_height(height), m_acquired(false), m_data_location(data_location::host),
// #ifdef ENABLE_CUDA
//         m_mapped(false),
//         d_data(NULL),
// #endif
//         h_data(NULL)
//     {
//     // make m_pitch the next multiple of 16 larger or equal to the given width
//     m_pitch = (width + (16 - (width & 15)));
//
//     // setup the number of elements
//     m_num_elements = m_pitch * m_height;
//
//     // allocate and clear memory
//     allocate();
//     memclear();
//     }

#ifdef ENABLE_CUDA
/*! \param num_elements Number of elements to allocate in the array
    \param exec_conf Shared pointer to the execution configuration for managing CUDA initialization and shutdown
    \param mapped True if we are using mapped-pinned memory
*/
template<class T> GPUCPUArray<T>::GPUCPUArray(unsigned int num_elements, bool mapped) :
        m_num_elements(num_elements), m_pitch(num_elements), m_height(1), m_acquired(false), m_data_location(data_location::host),
        m_mapped(mapped),
        d_data(NULL),
        h_data(NULL)
    {
    // allocate and clear memory
    allocate();
    memclear();
    }

/*! \param width Width of the 2-D array to allocate (in elements)
    \param height Number of rows to allocate in the 2D array
    \param exec_conf Shared pointer to the execution configuration for managing CUDA initialization and shutdown
    \param mapped True if we are using mapped-pinned memory
*/
// template<class T> GPUCPUArray<T>::GPUCPUArray(unsigned int width, unsigned int height, bool mapped) :
//         m_height(height), m_acquired(false), m_data_location(data_location::host),
//         m_mapped(mapped),
//         d_data(NULL),
//         h_data(NULL)
//     {
//     // make m_pitch the next multiple of 16 larger or equal to the given width
//     m_pitch = (width + (16 - (width & 15)));
//
//     // setup the number of elements
//     m_num_elements = m_pitch * m_height;
//
//     // allocate and clear memory
//     allocate();
//     memclear();
//     }
#endif

template<class T> GPUCPUArray<T>::~GPUCPUArray()
    {
    deallocate();
    }

template<class T> GPUCPUArray<T>::GPUCPUArray(const GPUCPUArray& from) : m_num_elements(from.m_num_elements), m_pitch(from.m_pitch),
        m_height(from.m_height), m_acquired(false), m_data_location(data_location::host),
#ifdef ENABLE_CUDA
        m_mapped(from.m_mapped),
        d_data(NULL),
#endif
        h_data(NULL)
    {
    // allocate and clear new memory the same size as the data in from
    allocate();
    memclear();

    // copy over the data to the new GPUCPUArray
    if (m_num_elements > 0)
        {
        ArrayHandle<T> h_handle(from, access_location::host, access_mode::read);
        memcpy(h_data, h_handle.data, sizeof(T)*m_num_elements);
        }
    }

template<class T> GPUCPUArray<T>& GPUCPUArray<T>::operator=(const GPUCPUArray& rhs)
    {
    if (this != &rhs) // protect against invalid self-assignment
        {
        // sanity check
        // assert(!m_acquired && !rhs.m_acquired);

        // free current memory
        deallocate();

        // copy over basic elements
        m_num_elements = rhs.m_num_elements;
        m_pitch = rhs.m_pitch;
        m_height = rhs.m_height;
#ifdef ENABLE_CUDA
        m_mapped = rhs.m_mapped;
#endif
        // initialize state variables
        m_data_location = data_location::host;

        // allocate and clear new memory the same size as the data in rhs
        allocate();
        memclear();

        // copy over the data to the new GPUCPUArray
        if (m_num_elements > 0)
            {
            ArrayHandle<T> h_handle(rhs, access_location::host, access_mode::read);
            memcpy(h_data, h_handle.data, sizeof(T)*m_num_elements);
            }
        }

    return *this;
    }

/*! \param from GPUCPUArray to swap \a this with

    a.swap(b) will result in the equivalent of:
    \code
GPUCPUArray c(a);
a = b;
b = c;
    \endcode

    But it will be done in a super-efficent way by just swapping the internal pointers, thus avoiding all the expensive
    memory deallocations/allocations and copies using the copy constructor and assignment operator.
*/
template<class T> void GPUCPUArray<T>::swap(GPUCPUArray& from)
    {
    // this may work, but really shouldn't be done when aquired
    // assert(!m_acquired && !from.m_acquired);
    // assert(&from != this);

    std::swap(m_num_elements, from.m_num_elements);
    std::swap(m_pitch, from.m_pitch);
    std::swap(m_height, from.m_height);
    std::swap(m_acquired, from.m_acquired);
    std::swap(m_data_location, from.m_data_location);
#ifdef ENABLE_CUDA
    std::swap(d_data, from.d_data);
    std::swap(m_mapped, from.m_mapped);
#endif
    std::swap(h_data, from.h_data);
    }

//! Swap the pointers of two GPUCPUArrays (const version)
template<class T> void GPUCPUArray<T>::swap(GPUCPUArray& from) const
    {
    // assert(!m_acquired && !from.m_acquired);
    // assert(&from != this);

    std::swap(m_num_elements, from.m_num_elements);
    std::swap(m_pitch, from.m_pitch);
    std::swap(m_height, from.m_height);
    std::swap(m_acquired, from.m_acquired);
    std::swap(m_data_location, from.m_data_location);
#ifdef ENABLE_CUDA
    std::swap(d_data, from.d_data);
    std::swap(m_mapped, from.m_mapped);
#endif
    std::swap(h_data, from.h_data);
    }

/*! \pre m_num_elements is set
    \pre pointers are not allocated
    \post All memory pointers needed for GPUCPUArray are allocated
*/
template<class T> void GPUCPUArray<T>::allocate()
    {
    // don't allocate anything if there are zero elements
    if (m_num_elements == 0)
        return;

    // sanity check
    // assert(h_data == NULL);

    // allocate host memory
    // at minimum, alignment needs to be 32 bytes for AVX
    h_data = new T[m_num_elements];

#ifdef ENABLE_CUDA
    // assert(d_data == NULL);

        // register pointer for DMA
        cudaHostRegister(h_data,m_num_elements*sizeof(T), m_mapped ? cudaHostRegisterMapped : cudaHostRegisterDefault);

        // allocate and/or map host memory
        if (m_mapped)
            {
            cudaHostGetDevicePointer(&d_data, h_data, 0);
            CHECK_CUDA_ERROR();
            }
        else
            {
            cudaMalloc(&d_data, m_num_elements*sizeof(T));
	    //printf("m_num_elements : %d %d \n", m_num_elements, sizeof(T));
            CHECK_CUDA_ERROR();
            }
#endif
    }

/*! \pre allocate() has been called
    \post All allocated memory is freed
*/
template<class T> void GPUCPUArray<T>::deallocate()
     {
       //printf("DEALLOCATE : m_num_elements : %d %d \n", m_num_elements, sizeof(T));
    // don't do anything if there are no elements
    if (m_num_elements == 0) {
        return;
    }

    // sanity check
    // assert(!m_acquired);
    // assert(h_data);

    // free memory

#ifdef ENABLE_CUDA

        {
        // assert(d_data);
        cudaHostUnregister(h_data);
        CHECK_CUDA_ERROR();

        if (! m_mapped)
            {
            cudaFree(d_data);
            CHECK_CUDA_ERROR();
            }
        }
#endif

    delete[] h_data;

    // set pointers to NULL
    h_data = NULL;
#ifdef ENABLE_CUDA
    d_data = NULL;
#endif
    }

/*! \pre allocate() has been called
    \post All allocated memory is set to 0
*/
template<class T> void GPUCPUArray<T>::memclear(unsigned int first)
    {
    // don't do anything if there are no elements
    if (m_num_elements == 0)
        return;

    // assert(h_data);
    // assert(first < m_num_elements);

    // clear memory
    memset(h_data+first, 0, sizeof(T)*(m_num_elements-first));

#ifdef ENABLE_CUDA

        {
        // assert(d_data);
        if (! m_mapped) cudaMemset(d_data+first, 0, (m_num_elements-first)*sizeof(T));
        }
#endif
    }


#ifdef ENABLE_CUDA
/*! \post All memory on the device is copied to the host array
*/
template<class T> void GPUCPUArray<T>::memcpyDeviceToHost(bool async) const
    {
    // don't do anything if there are no elements
    if (m_num_elements == 0)
        return;

    if (m_mapped)
        {
        // if we are using mapped pinned memory, no need to copy, only synchronize
        if (!async) cudaDeviceSynchronize();
        return;
        }


    if (async)
        cudaMemcpyAsync(h_data, d_data, sizeof(T)*m_num_elements, cudaMemcpyDeviceToHost);
    else
        cudaMemcpy(h_data, d_data, sizeof(T)*m_num_elements, cudaMemcpyDeviceToHost);

    }

/*! \post All memory on the host is copied to the device array
*/
template<class T> void GPUCPUArray<T>::memcpyHostToDevice(bool async) const
    {
    // don't do anything if there are no elements
    if (m_num_elements == 0)
        return;

    if (m_mapped)
        {
        // if we are using mapped pinned memory, no need to copy
        // rely on CUDA's implicit synchronization
        return;
        }

    if (async)
        cudaMemcpyAsync(d_data, h_data, sizeof(T)*m_num_elements, cudaMemcpyHostToDevice);
    else
        cudaMemcpy(d_data, h_data, sizeof(T)*m_num_elements, cudaMemcpyHostToDevice);

    }
#endif

/*! \param location Desired location to access the data
    \param mode Mode to access the data with
    \param async True if array copying should be done async

    aquire() is the workhorse of GPUCPUArray. It tracks the internal state variable \a data_location and
    performs all host<->device memory copies as needed during the state changes given the
    specified access mode and location where the data is to be acquired.

    aquire() cannot be directly called by the user class. Data must be accessed through ArrayHandle.
*/
template<class T> T* GPUCPUArray<T>::aquire(const access_location::Enum location, const access_mode::Enum mode
#ifdef ENABLE_CUDA
                                         , bool async
#endif
                                        ) const
    {
    // sanity check
    // assert(!m_acquired);
    m_acquired = true;

    // base case - handle acquiring a NULL GPUCPUArray by simply returning NULL to prevent any memcpys from being attempted
    if (isNull())
        return NULL;

    // first, break down based on where the data is to be acquired
    if (location == access_location::host)
        {
        // then break down based on the current location of the data
        if (m_data_location == data_location::host)
            {
            // the state stays on the host regardles of the access mode
            return h_data;
            }
#ifdef ENABLE_CUDA
        else if (m_data_location == data_location::hostdevice)
            {
            // finally perform the action baed on the access mode requested
            if (mode == access_mode::read)  // state stays on hostdevice
                m_data_location = data_location::hostdevice;
            else if (mode == access_mode::readwrite)    // state goes to host
                m_data_location = data_location::host;
            else if (mode == access_mode::overwrite)    // state goes to host
                m_data_location = data_location::host;
            else
                {

                throw std::runtime_error("Error acquiring data");
                }

            return h_data;
            }
        else if (m_data_location == data_location::device)
            {
            // finally perform the action baed on the access mode requested
            if (mode == access_mode::read)
                {
                // need to copy data from the device to the host
                memcpyDeviceToHost(async);
                // state goes to hostdevice
                m_data_location = data_location::hostdevice;
                }
            else if (mode == access_mode::readwrite)
                {
                // need to copy data from the device to the host
                memcpyDeviceToHost(async);
                // state goes to host
                m_data_location = data_location::host;
                }
            else if (mode == access_mode::overwrite)
                {
                // no need to copy data, it will be overwritten
                // state goes to host
                m_data_location = data_location::host;
                }
            else
                {

                throw std::runtime_error("Error acquiring data");
                }

            return h_data;
            }
#endif
        else
            {

            throw std::runtime_error("Error acquiring data");
            return NULL;
            }
        }
#ifdef ENABLE_CUDA
    else if (location == access_location::device)
        {
        // check that a GPU is actually specified


        // then break down based on the current location of the data
        if (m_data_location == data_location::host)
            {
            // finally perform the action baed on the access mode requested
            if (mode == access_mode::read)
                {
                // need to copy data to the device
                memcpyHostToDevice(async);
                // state goes to hostdevice
                m_data_location = data_location::hostdevice;
                }
            else if (mode == access_mode::readwrite)
                {
                // need to copy data to the device
                memcpyHostToDevice(async);
                // state goes to device
                m_data_location = data_location::device;
                }
            else if (mode == access_mode::overwrite)
                {
                // no need to copy data to the device, it is to be overwritten
                // state goes to device
                m_data_location = data_location::device;
                }
            else
                {

                throw std::runtime_error("Error acquiring data");
                }

            return d_data;
            }
        else if (m_data_location == data_location::hostdevice)
            {
            // finally perform the action baed on the access mode requested
            if (mode == access_mode::read)  // state stays on hostdevice
                m_data_location = data_location::hostdevice;
            else if (mode == access_mode::readwrite)    // state goes to device
                m_data_location = data_location::device;
            else if (mode == access_mode::overwrite)    // state goes to device
                m_data_location = data_location::device;
            else
                {

                throw std::runtime_error("Error acquiring data");
                }
            return d_data;
            }
        else if (m_data_location == data_location::device)
            {
            // the stat stays on the device regardless of the access mode
            return d_data;
            }
        else
            {

            throw std::runtime_error("Error acquiring data");
            return NULL;
            }
        }
#endif
    else
        {

        throw std::runtime_error("Error acquiring data");
        return NULL;
        }
    }

/*! \post Memory on the host is resized, the newly allocated part of the array
 *        is reset to zero
 *! \returns a pointer to the newly allocated memory area
*/
template<class T> T* GPUCPUArray<T>::resizeHostArray(unsigned int num_elements)
    {
    // if not allocated, do nothing
    if (isNull()) return NULL;

    // allocate resized array
    T *h_tmp = NULL;

    // allocate host memory
    // at minimum, alignment needs to be 32 bytes for AVX
    h_tmp = new T[num_elements];

#ifdef ENABLE_CUDA

        {
        cudaHostRegister(h_tmp, num_elements*sizeof(T), m_mapped ? cudaHostRegisterMapped : cudaHostRegisterDefault);
        }
#endif
    // clear memory
    memset(h_tmp, 0, sizeof(T)*num_elements);

    // copy over data
    unsigned int num_copy_elements = m_num_elements > num_elements ? num_elements : m_num_elements;
    memcpy(h_tmp, h_data, sizeof(T)*num_copy_elements);

    // free old memory location
#ifdef ENABLE_CUDA

        {
        cudaHostUnregister(h_data);
        CHECK_CUDA_ERROR();
        }
#endif

    delete[] h_data;
    h_data = h_tmp;

#ifdef ENABLE_CUDA
    // update device pointer
    if (m_mapped)
        cudaHostGetDevicePointer(&d_data, h_data, 0);
#endif

    return h_data;
    }

// /*! \post Memory on the host is resized, the newly allocated part of the array
//  *        is reset to zero
//  *! \returns a pointer to the newly allocated memory area
// */
// template<class T> T* GPUCPUArray<T>::resize2DHostArray(unsigned int pitch, unsigned int new_pitch, unsigned int height, unsigned int new_height )
//     {
//     // allocate resized array
//     T *h_tmp = NULL;
//
//     // allocate host memory
//     // at minimum, alignment needs to be 32 bytes for AVX
//     unsigned int size = new_pitch*new_height*sizeof(T);
//     h_tmp = new T*[size];
//     if (retval != 0)
//         {
//
//         throw std::runtime_error("Error allocating GPUCPUArray.");
//         }
//
// #ifdef ENABLE_CUDA
//
//         {
//         cudaHostRegister(h_tmp, size, cudaHostRegisterDefault);
//         }
// #endif
//
//     // clear memory
//     memset(h_tmp, 0, sizeof(T)*new_pitch*new_height);
//
//     // copy over data
//     // every column is copied separately such as to align with the new pitch
//     unsigned int num_copy_rows = height > new_height ? new_height : height;
//     unsigned int num_copy_columns = pitch > new_pitch ? new_pitch : pitch;
//     for (unsigned int i = 0; i < num_copy_rows; i++)
//         memcpy(h_tmp + i * new_pitch, h_data + i*pitch, sizeof(T)*num_copy_columns);
//
//     // free old memory location
// #ifdef ENABLE_CUDA
//
//         {
//         cudaHostUnregister(h_data);
//         CHECK_CUDA_ERROR();
//         }
// #endif
//
//     delete[] h_data;
//     h_data = h_tmp;
//
// #ifdef ENABLE_CUDA
//     // update device pointer
//     if (m_mapped)
//         cudaHostGetDevicePointer(&d_data, h_data, 0);
// #endif
//
//     return h_data;
//     }
//
/*! \post Memory on the device is resized, the newly allocated part of the array
 *        is reset to zero
 *! \returns a device pointer to the newly allocated memory area
*/
template<class T> T* GPUCPUArray<T>::resizeDeviceArray(unsigned int num_elements)
    {
#ifdef ENABLE_CUDA
    if (m_mapped) return NULL;

    // allocate resized array
    T *d_tmp;
    cudaMalloc(&d_tmp, num_elements*sizeof(T));
    CHECK_CUDA_ERROR();

    // assert(d_tmp);

    // clear memory
    cudaMemset(d_tmp, 0, num_elements*sizeof(T));
    CHECK_CUDA_ERROR();

    // copy over data
    unsigned int num_copy_elements = m_num_elements > num_elements ? num_elements : m_num_elements;
    cudaMemcpy(d_tmp, d_data, sizeof(T)*num_copy_elements,cudaMemcpyDeviceToDevice);
    CHECK_CUDA_ERROR();

    // free old memory location
    cudaFree(d_data);
    CHECK_CUDA_ERROR();

    d_data = d_tmp;
    return d_data;
#else
    return NULL;
#endif
    }

// /*! \post Memory on the device is resized, the newly allocated part of the array
//  *        is reset to zero
//  *! \returns a device pointer to the newly allocated memory area
// */
// template<class T> T* GPUCPUArray<T>::resize2DDeviceArray(unsigned int pitch, unsigned int new_pitch, unsigned int height, unsigned int new_height)
//     {
// #ifdef ENABLE_CUDA
//     if (m_mapped) return NULL;
//
//     // allocate resized array
//     T *d_tmp;
//     cudaMalloc(&d_tmp, new_pitch*new_height*sizeof(T));
//     CHECK_CUDA_ERROR();
//
//     assert(d_tmp);
//
//     // clear memory
//     cudaMemset(d_tmp, 0, new_pitch*new_height*sizeof(T));
//     CHECK_CUDA_ERROR();
//
//     // copy over data
//     // every column is copied separately such as to align with the new pitch
//     unsigned int num_copy_rows = height > new_height ? new_height : height;
//     unsigned int num_copy_columns = pitch > new_pitch ? new_pitch : pitch;
//
//     for (unsigned int i = 0; i < num_copy_rows; i++)
//         {
//         cudaMemcpy(d_tmp + i * new_pitch, d_data + i * pitch, sizeof(T)*num_copy_columns,cudaMemcpyDeviceToDevice);
//         CHECK_CUDA_ERROR();
//         }
//
//     // free old memory location
//     cudaFree(d_data);
//     CHECK_CUDA_ERROR();
//
//     d_data = d_tmp;
//     return d_data;
// #else
//     return NULL;
// #endif
//     }

/*! \param num_elements new size of array
 *
 * \warning An array can be expanded or shrunk, depending on the parameters supplied.
 *          It is the responsibility of the caller to ensure that no data is inadvertently lost when
 *          reducing the size of the array.
*/
template<class T> void GPUCPUArray<T>::resize(unsigned int num_elements)
    {
    // assert(! m_acquired);
    // assert(num_elements > 0);
    if(num_elements==m_num_elements) return;
    // if not allocated, simply allocate
    if (isNull())
        {
        m_num_elements = num_elements;
        allocate();
        return;
        };

    resizeHostArray(num_elements);
#ifdef ENABLE_CUDA
        resizeDeviceArray(num_elements);
#endif
    m_num_elements = num_elements;
    m_pitch = num_elements;
    }

// /*! \param width new width of array
// *   \param height new height of array
// *
// *   \warning An array can be expanded or shrunk, depending on the parameters supplied.
// *   It is the responsibility of the caller to ensure that no data is inadvertently lost when
// *   reducing the size of the array.
// */
// template<class T> void GPUCPUArray<T>::resize(unsigned int width, unsigned int height)
//     {
//     assert(! m_acquired);
//
//     // make m_pitch the next multiple of 16 larger or equal to the given width
//     unsigned int new_pitch = (width + (16 - (width & 15)));
//
//     unsigned int num_elements = new_pitch * height;
//     assert(num_elements > 0);
//
//     // if not allocated, simply allocate
//     if (isNull())
//         {
//         m_num_elements = num_elements;
//         allocate();
//         m_pitch = new_pitch;
//         m_height = height;
//         return;
//         };
//
//
//     resize2DHostArray(m_pitch, new_pitch, m_height, height);
// #ifdef ENABLE_CUDA
//         resize2DDeviceArray(m_pitch, new_pitch, m_height, height);
// #endif
//     m_num_elements = num_elements;
//
//     m_height = height;
//     m_pitch  = new_pitch;
//     m_num_elements = m_pitch * m_height;
//     }
#endif
