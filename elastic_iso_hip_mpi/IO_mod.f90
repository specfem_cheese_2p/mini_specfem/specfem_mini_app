module IO_mod

  use config_mod
  use mpi_mod
  implicit none
  
  real, private, dimension(:,:), allocatable :: nodes_coords
  integer, private, dimension(:,:), allocatable :: grid_connectivity
  
  !integer, private, dimension(:,:,:,:), allocatable :: ibool
  !integer, private :: nx, ny, nz, nspec, nglob
  integer, private :: nb_points, nb_cells 
  integer, private :: total_nb_points, total_nb_cells

  
  integer, private, dimension(:), allocatable :: n_by_rank, np_by_rank
  integer, private :: my_offset_pts, my_offset_cells


contains
!!---------------------------------------------------------------------------
subroutine open_xdmf_mesh(fname, iunit, myrank)
   integer, intent(in) :: iunit, myrank
   character(len=CHAR_LEN), intent(in) :: fname
   if (myrank == 0) then  
      open(iunit, file=trim(fname))
   end if
  call write_geometry_xdmf(iunit, myrank)
end subroutine open_xdmf_mesh
!!---------------------------------------------------------------------------
subroutine close_xdmf_mesh(iunit, myrank)
   integer, intent(in) :: iunit, myrank
   if (myrank == 0) then   
      call end_xdmf_scalar(iunit)
      close(iunit)
   end if
end subroutine close_xdmf_mesh
!!---------------------------------------------------------------------------
subroutine add_xdmf_field(iunit, myrank, name_field, field, ibool_in, &
   nx_in, ny_in, nz_in, nel_in, nglob_in)
   integer, intent(in) :: iunit, myrank, nx_in,ny_in,nz_in,nel_in, nglob_in
   real(kind=CUSTOM_REAL), dimension(nx_in,ny_in,nz_in,nel_in), intent(in) :: field
   integer,  dimension(nx_in,ny_in,nz_in,nel_in), intent(in) :: ibool_in
   character(len=CHAR_LEN), intent(in) :: name_field
   real, dimension(:), allocatable :: data_to_dump
   character(len=CHAR_LEN) :: name_file
   integer :: i, j, k, ispec, iglob
   allocate(data_to_dump(nglob_in))
   do ispec =1, nel_in
      do k=1, nz_in
         do j=1, ny_in
            do i=1, nx_in
               iglob = ibool_in(i,j,k, ispec)
               data_to_dump(iglob) = field(i,j,k,ispec)
           end do
        end do
      end do
   end do
   name_file='output/'//trim(name_field)//'.bin' 
   call dump_binary_field_r4_mpi(name_file, data_to_dump, nglob_in)
   if (myrank == 0 ) then 
      call add_xdmf_scalar_field(iunit, myrank, name_file, name_field, total_nb_points)
   end if
   deallocate(data_to_dump)
end subroutine add_xdmf_field
!!---------------------------------------------------------------------------
subroutine write_xdmf_movie(myrank, nt)
   integer, intent(in) :: myrank, nt
   integer :: iunit=666, it, nnt!, irank
   character(len=CHAR_LEN) :: prname, name_file

103     format(&
   '      <Grid Name="T',i6.6, i4,'" GridType="Collection" CollectionType="Spatial">')
104     format(&
   '        <Grid Name="T',i6.6,i4,i4,'">')
     
   if (myrank == 0) then  
      write(prname,"('gll_veloc.xdmf')") 
      open(iunit, file=trim(prname))
   end if
   
   call write_geometry_xdmf(iunit, myrank)

   if (myrank==0) then
      !! count number of time step in xdmf file
      !irank = 0
      nnt=0
      do it=1,nt
         if  (mod(it,100) == 0 ) nnt=nnt+1
      end do
      
      call add_time_serie(iunit, nnt)
      do it =1, nt
         if (mod(it,NTSTEP_BETWEEN_OUTPUT_INFO) == 0 ) then

           !! begin grid collection spatial
           write(iunit,103) it,0

           write(iunit,104) it,0,0
           write(name_file,"('output/veloc_x_it',i6.6,'.bin')") it
           call add_xdmf_scalar_field_v1(iunit, myrank, name_file, "Vx", total_nb_points)
           write(iunit,'(a)') "    </Grid>"
            
           write(iunit,104) it,1,0
           write(name_file,"('output/veloc_y_it',i6.6,'.bin')") it
           call add_xdmf_scalar_field_v1(iunit, myrank, name_file, "Vy", total_nb_points)
           write(iunit,'(a)') "    </Grid>"
            
           write(iunit,104) it,2,0
           write(name_file,"('output/veloc_z_it',i6.6,'.bin')") it
           call add_xdmf_scalar_field_v1(iunit, myrank, name_file, "Vz", total_nb_points)
           write(iunit,'(a)') "    </Grid>"
             
           write(iunit,'(a)') "    </Grid>"
            
         end if
      end do
      write(iunit,'(a)') "    </Grid>"
      call end_xdmf_scalar(iunit)
      close(iunit)
   end if
   
 end subroutine write_xdmf_movie
!!---------------------------------------------------------------------------
 subroutine io_setup_connectivity(point_coords, ibool_in, nx, ny, nz, nel, np, myrank)
   integer, intent(in) :: nx, ny, nz, nel, np, myrank
   double precision, dimension(3,np), intent(in) :: point_coords
   integer,  dimension(nx,ny,nz,nel), intent(in) :: ibool_in
   integer, dimension(1) :: nn
   
   integer :: ispec, i,j,k, npoints, ncells, ns, ig !, irank
   !character(len=CHAR_LEN) :: fname 
   !integer :: file_handle
   !integer(KIND=MPI_OFFSET_KIND) :: my_offset

   allocate(n_by_rank(nbproc_mpi))
   allocate(np_by_rank(nbproc_mpi))
   nb_points = np
   !! count number of cells
   ns=0
   do ispec=1,nel
      do k=1,nz-1
         do j=1,ny-1
            do i=1,nx-1
               ns=ns+1
            end do
         end do
      end do
   end do
   nb_cells = ns

   !! store node coordinates 
   allocate(nodes_coords(3, nb_points))
   nodes_coords(:,:) = point_coords(:,:)

   !! grid connectivity for visualization
   allocate(grid_connectivity(8,ns))
   
   !! number of cells by mpi rank
   nn(1) = ns
   call gather_all_all_i(nn, n_by_rank, 1, NPX_config*NPY_config)
   my_offset_cells = sum(n_by_rank(1:myrank)) !! for mpi-io
   ncells = sum(n_by_rank(:)) !! total number of cells 

   !! number of points by mpi rank 
   nn(1) = np
   call gather_all_all_i(nn, np_by_rank, 1, NPX_config*NPY_config)
   my_offset_pts = sum(np_by_rank(1:myrank)) !! for mpi-io
   npoints = sum(np_by_rank(:))  !! total number of points 
   

   !! define connectivity for visualization (8 edges per hexahedra)
   ig = 0
   do ispec=1,nel
      do k=1,nz-1
         do j=1,ny-1
            do i=1,nx-1
               !! count element
               ig = ig + 1
               !! add offset for mpi-io 
               grid_connectivity(1, ig) = ibool_in(i,j,k,ispec)-1 + my_offset_pts
               grid_connectivity(2, ig) = ibool_in(i+1,j,k,ispec)-1 + my_offset_pts
               grid_connectivity(3, ig) = ibool_in(i+1,j+1,k,ispec)-1 + my_offset_pts
               grid_connectivity(4, ig) = ibool_in(i,j+1,k,ispec)-1  + my_offset_pts
               grid_connectivity(5, ig) = ibool_in(i,j,k+1,ispec)-1 + my_offset_pts
               grid_connectivity(6, ig) = ibool_in(i+1,j,k+1,ispec)-1 + my_offset_pts
               grid_connectivity(7, ig) = ibool_in(i+1,j+1,k+1,ispec)-1 + my_offset_pts
               grid_connectivity(8, ig) = ibool_in(i,j+1,k+1,ispec)-1 + my_offset_pts
            end do
         end do
      end do
   end do

   total_nb_cells = sum(n_by_rank(:))
   total_nb_points = sum(np_by_rank(:))

   call io_dump_grid()

  end subroutine io_setup_connectivity

!!---------------------------------------------------------------------------
  subroutine io_dump_grid()
   integer :: file_handle
   character(len=CHAR_LEN) :: fname 
   integer(KIND=MPI_OFFSET_KIND) :: my_offset
   !! write connectivity file 
   write(fname,"('output/grid.bin')") 
   call delete_and_open_mpi_file(fname, file_handle)
   my_offset = int(my_offset_cells*8*4, KIND=MPI_OFFSET_KIND)
   call write_int4_bin_mpi(file_handle, grid_connectivity(1,1), 8*nb_cells, my_offset)
   call close_mpi_file(file_handle)
   !! write points 
   write(fname,"('output/points.bin')") 
   call delete_and_open_mpi_file(fname, file_handle)
   my_offset = int(my_offset_pts*3*4, KIND=MPI_OFFSET_KIND)
   call write_r4_bin_mpi(file_handle, nodes_coords(1,1), 3*nb_points, my_offset)
   call close_mpi_file(file_handle)
  end subroutine io_dump_grid

!!---------------------------------------------------------------------------
  subroutine write_geometry_xdmf(iunit, myrank)
   integer, intent(in) :: iunit, myrank
   character(len=CHAR_LEN) :: fname 
   
100 format(&
   '<?xml version="1.0" ?>',/&
   '<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>',/&
   '<Xdmf xmlns:xi="http://www.w3.org/2001/XInclude" Version="2.0">',/&
   '   <Domain>')

203 format(&
   '     <Topology TopologyType="Hexahedron" NumberOfElements="', i10 , '">" ',/&      
   '         <DataItem Dimensions="',i10,' 8" NumberType="Int" Format="binary" Endian="Litte">')
104 format(&
   '         </DataItem> ',/&
   '     </Topology>') 
   
205 format(&
   '     <Geometry GeometryType="XYZ">',/&
   '         <DataItem Dimensions="',i10,' 3" NumberType="Float" Format="binary" Endian="Litte">')
106 format(&
   '         </DataItem> ',/&
   '     </Geometry>')
   
   !! mesh xdmf file for visualization
   if (myrank==0) then
      write(iunit,100)
      write(fname,"('output/grid.bin')") 
      write(iunit,203) total_nb_cells, total_nb_cells !n_by_rank(irank+1), n_by_rank(irank+1)
      write(iunit,'(10x,a200)') trim(fname)
      write(iunit,104)
      write(iunit,205) total_nb_points !np_by_rank(irank+1)
      write(fname,"('output/points.bin')") 
      write(iunit,'(10x,a200)') trim(fname)
      write(iunit,106)
   end if

 end subroutine write_geometry_xdmf

!!---------------------------------------------------------------------------
  subroutine add_time_serie(iunit, nt)

    integer, intent(in) :: iunit, nt
    integer :: i
101 format(&
         '     <Grid Name="TimeSeries" GridType="Collection" CollectionType="Temporal">',/&
         '         <Time TimeType="HyperSlab">' , /&
         '               <DataItem Format="XML" NumberType="Float" Dimensions="', i10,'">')
    
102 format(&
         '               </DataItem>', / &
         '         </Time>')

    write(iunit,101) nt
    do i=1,nt
       write(iunit, '(10x, f10.3)') real(i)
    end do
    write(iunit,102)
    
  end subroutine add_time_serie 
 !!---------------------------------------------------------------------------
  subroutine add_xdmf_scalar_field(iunit, irank, name_file, field_name, np)

   integer, intent(in) :: iunit, np, irank
   character(len=CHAR_LEN), intent(in) :: name_file
   character(len=CHAR_LEN), intent(in) :: field_name
100 format('   <Grid>')    
101 format(&
'     <Topology Reference="/Xdmf/Domain/Topology[',i2,']"/>',/&
'     <Geometry Reference="/Xdmf/Domain/Geometry[',i2,']"/>',/&
'         <Attribute Name="',A10,'" AttributeType="Scalar" Center="Node">', /&
'            <DataItem  NumberType="Float" Format="binary"  Endian="Little" Dimensions="',i20,' 1">' )
102 format(&
'          </DataItem>',/&
'     </Attribute>')
   write(iunit, 100)
   write(iunit, 101) irank+1,irank+1, trim(field_name), np
   write(iunit, '(10x,a200)') adjustl(trim(name_file))
   write(iunit, 102)
   call end_time(iunit)
 end subroutine add_xdmf_scalar_field 
!!---------------------------------------------------------------------------
  subroutine add_xdmf_scalar_field_v1(iunit, irank, name_file, field_name, np)

    integer, intent(in) :: iunit, np, irank
    character(len=CHAR_LEN), intent(in) :: name_file
    character(len=2), intent(in) :: field_name
    
101 format(&
'     <Topology Reference="/Xdmf/Domain/Topology[',i2,']"/>',/&
'     <Geometry Reference="/Xdmf/Domain/Geometry[',i2,']"/>',/&
'         <Attribute Name="',A2,'" AttributeType="Scalar" Center="Node">', /&
'            <DataItem  NumberType="Float" Format="binary"  Endian="Little" Dimensions="',i20,' 1">' )
    
102 format(&
'          </DataItem>',/&
'     </Attribute>')

    write(iunit, 101) irank+1,irank+1, trim(field_name), np
    write(iunit, '(10x,a200)') adjustl(trim(name_file))
    write(iunit, 102)
    
  end subroutine add_xdmf_scalar_field_v1
!!--------------------------------------
  subroutine begin_time(iunit, it)
    integer, intent(in) :: it,iunit
    character(len=9) :: Tname
101 format(&
         '     <Grid Name="',a9,'">')
    
    write(Tname,"('T',i8.8)") it
    write(iunit, 101) Tname
  end subroutine begin_time
!!-------------------------------------
  subroutine end_time(iunit)
    integer, intent(in) :: iunit
101 format('   </Grid>')
    write(iunit, 101)
  end subroutine end_time
!!-------------------------------------
  subroutine end_xdmf_scalar(iunit)
    integer, intent(in) :: iunit
101 format(& 
         '   </Domain>',/&
         '</Xdmf>')
    write(iunit,101)
  end subroutine end_xdmf_scalar
!!---------------------------------------------------------------------------
  subroutine dump_binary_mpi(name_file, field, icomp, np)
   implicit none
   integer, intent(in) :: icomp, np
   character(len=CHAR_LEN), intent(in) :: name_file
   real(kind=CUSTOM_REAL), dimension(3,np), intent(in) :: field
   integer(KIND=MPI_OFFSET_KIND) :: my_offset
   integer :: file_handle
   integer :: in_offset
   call delete_and_open_mpi_file(name_file, file_handle)
   in_offset = 4*np   ! in byte
   call compute_mpi_io_offset(in_offset, my_offset)
   call write_r4_bin_mpi(file_handle, field(icomp,:), np, my_offset)
   call close_mpi_file(file_handle)
  end subroutine dump_binary_mpi
!!---------------------------------------------------------------------------
  subroutine dump_binary_field_r4_mpi(name_file, field, np)
   implicit none
   integer, intent(in) :: np
   character(len=CHAR_LEN), intent(in) :: name_file
   real, dimension(np), intent(in) :: field
   integer(KIND=MPI_OFFSET_KIND) :: my_offset
   integer :: file_handle
   integer :: in_offset
   call delete_and_open_mpi_file(name_file, file_handle)
   in_offset = 4*np   ! in byte
   call compute_mpi_io_offset(in_offset, my_offset)
   call write_r4_bin_mpi(file_handle, field, np, my_offset)
   call close_mpi_file(file_handle)
  end subroutine dump_binary_field_r4_mpi
end module IO_mod