module mpi_mod
  
  !
  ! module to manage mpi calls
  !
  
  use config_mod
  use mpi
  implicit none
  
  integer, public  :: myrank_mpi
  integer, public  :: nbproc_mpi
  integer, public, parameter  :: itag = 0
  integer, private :: ier
  
  integer, parameter :: CUSTOM_MPI_TYPE = MPI_REAL

  
contains

  !!---------------------------------------------------------------------------
  subroutine mpi_begin()

    call MPI_INIT(ier)
    if (ier /= 0 ) stop 'Error initializing MPI'
    call MPI_COMM_RANK(MPI_COMM_WORLD,myrank_mpi,ier)
    if (ier /= 0 ) stop 'Error MPI_COMM_RANK MPI'
    call MPI_COMM_SIZE(MPI_COMM_WORLD, nbproc_mpi, ier)
    if (ier /= 0 ) stop 'Error MPI_COMM_SIZE MPI'
    
  end subroutine mpi_begin
  
  !!----------------------------------------------------------------------------
  subroutine mpi_end()

    call MPI_BARRIER(MPI_COMM_WORLD,ier)
    if (ier /= 0) stop 'Error finalizing MPI'


    ! stop all the MPI processes, and exit
    call MPI_FINALIZE(ier)
    if (ier /= 0) stop 'Error finalizing MPI'

  end subroutine mpi_end

  
  !!----------------------------------------------------------------------------
  subroutine synchronize_all()

    integer :: ier
  
    ! synchronizes MPI processes
    call MPI_BARRIER(MPI_COMM_WORLD,ier)
    if (ier /= 0 ) stop 'Error synchronize MPI processes'

  end subroutine synchronize_all
!
!-------------------------------------------------------------------------------------------------
!
  subroutine sum_all_all_i(sendbuf, recvbuf)

    integer :: sendbuf, recvbuf
    integer :: ier
    
    call MPI_ALLREDUCE(sendbuf,recvbuf,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ier)
    if (ier /= 0 ) stop 'Error  MPI_ALLREDUCE'
    
  end subroutine sum_all_all_i
!
!-------------------------------------------------------------------------------------------------
!
   subroutine sum_all_i(sendbuf, recvbuf)

     integer :: sendbuf, recvbuf
     integer :: ier

     call MPI_REDUCE(sendbuf,recvbuf,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ier)
     if (ier /= 0 ) stop 'Error  MPI_REDUCE'
     
   end subroutine sum_all_i
!
!-------------------------------------------------------------------------------------------------
!
   subroutine sum_all_cr(sendbuf, recvbuf)


    real(kind=CUSTOM_REAL) :: sendbuf, recvbuf
    integer :: ier

    call MPI_REDUCE(sendbuf,recvbuf,1,CUSTOM_MPI_TYPE,MPI_SUM,0,MPI_COMM_WORLD,ier)
    if (ier /= 0 ) stop 'Error  MPI_REDUCE'
    
  end subroutine sum_all_cr  
!
!-------------------------------------------------------------------------------------------------
!
   subroutine min_all_cr(sendbuf, recvbuf)
  
    implicit none
  
    real(kind=CUSTOM_REAL) :: sendbuf, recvbuf
    integer :: ier
  
    call MPI_REDUCE(sendbuf,recvbuf,1,CUSTOM_MPI_TYPE,MPI_MIN,0,MPI_COMM_WORLD,ier)
  
    end subroutine min_all_cr
!
!-------------------------------------------------------------------------------------------------
!
    subroutine max_all_cr(sendbuf, recvbuf)
    
      implicit none
        
      real(kind=CUSTOM_REAL) :: sendbuf, recvbuf
      integer :: ier
    
      call MPI_REDUCE(sendbuf,recvbuf,1,CUSTOM_MPI_TYPE,MPI_MAX,0,MPI_COMM_WORLD,ier)
    
      end subroutine max_all_cr   

!-------------------------------------------------------------------------------------------------
!
    subroutine min_all_dp(sendbuf, recvbuf)
  
        implicit none
      
        double precision :: sendbuf, recvbuf
        integer :: ier
      
        call MPI_REDUCE(sendbuf,recvbuf,1,MPI_DOUBLE_PRECISION,MPI_MIN,0,MPI_COMM_WORLD,ier)
      
    end subroutine min_all_dp
!
!-------------------------------------------------------------------------------------------------
!
    subroutine max_all_dp(sendbuf, recvbuf)
        
          implicit none
            
          double precision:: sendbuf, recvbuf
          integer :: ier
        
          call MPI_REDUCE(sendbuf,recvbuf,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,MPI_COMM_WORLD,ier)
        
    end subroutine max_all_dp         
!
!-------------------------------------------------------------------------------------------------
!

      subroutine bcast_all_cr(buffer)

        implicit none

        real(kind=CUSTOM_REAL) :: buffer
      
        integer :: ier
      
        call MPI_BCAST(buffer,1,CUSTOM_MPI_TYPE,0,MPI_COMM_WORLD,ier)
      
        end subroutine bcast_all_cr     
!
!-------------------------------------------------------------------------------------------------
!
  subroutine gather_all_all_i(sendbuf, recvbuf, counts, NPROC)

 
    implicit none

    integer NPROC,counts
    integer, dimension(counts) :: sendbuf
    integer, dimension(counts,0:NPROC-1) :: recvbuf

    integer :: ier

    call MPI_ALLGATHER(sendbuf,counts,MPI_INTEGER,recvbuf,counts,MPI_INTEGER,MPI_COMM_WORLD,ier)

  end subroutine gather_all_all_i

!
!-------------------------------------------------------------------------------------------------
!
  subroutine gather_all_all_cr(sendbuf, recvbuf, counts, NPROC)

 
    implicit none

    integer NPROC,counts
    real(kind=CUSTOM_REAL), dimension(counts) :: sendbuf
    real(kind=CUSTOM_REAL), dimension(counts,0:NPROC-1) :: recvbuf

    integer :: ier

    call MPI_ALLGATHER(sendbuf,counts,CUSTOM_MPI_TYPE,recvbuf,counts,CUSTOM_MPI_TYPE,MPI_COMM_WORLD,ier)

  end subroutine gather_all_all_cr
!
!-------------------------------------------------------------------------------------------------
!
  subroutine gather_all_all_dp(sendbuf, recvbuf, counts, NPROC)

 
    implicit none
    
    integer NPROC,counts
    double precision, dimension(counts) :: sendbuf
    double precision, dimension(counts,0:NPROC-1) :: recvbuf
    
    integer :: ier
    
    call MPI_ALLGATHER(sendbuf,counts,MPI_DOUBLE_PRECISION,recvbuf,counts,MPI_DOUBLE_PRECISION,MPI_COMM_WORLD,ier)
    
  end subroutine gather_all_all_dp
!-------------------------------------------------------------------------------------------------
!
! Send/Receive MPI
!
!-------------------------------------------------------------------------------------------------

! asynchronuous send/receive

  subroutine isend_cr(sendbuf, sendcount, dest, sendtag, req)


    integer :: sendcount, dest, sendtag, req
    real(kind=CUSTOM_REAL), dimension(sendcount) :: sendbuf
    
    integer :: ier
    
    call MPI_ISEND(sendbuf,sendcount,CUSTOM_MPI_TYPE,dest,sendtag,MPI_COMM_WORLD,req,ier)
    if (ier /= 0 ) stop 'Error isend_cr'
    
  end subroutine isend_cr

!
!-------------------------------------------------------------------------------------------------
!

  subroutine irecv_cr(recvbuf, recvcount, dest, recvtag, req)

 
    integer :: recvcount, dest, recvtag, req
    real(kind=CUSTOM_REAL), dimension(recvcount) :: recvbuf

    integer :: ier

    call MPI_IRECV(recvbuf,recvcount,CUSTOM_MPI_TYPE,dest,recvtag,MPI_COMM_WORLD,req,ier)
    if (ier /= 0 ) stop 'Error irecv_cr'
    
  end subroutine irecv_cr


!-------------------------------------------------------------------------------------------------
!

  subroutine isend_i(sendbuf, sendcount, dest, sendtag, req)


    integer :: sendcount, dest, sendtag, req
    integer, dimension(sendcount) :: sendbuf

    integer :: ier

    call MPI_ISEND(sendbuf,sendcount,MPI_INTEGER,dest,sendtag,MPI_COMM_WORLD,req,ier)

  end subroutine isend_i
  
!
!-------------------------------------------------------------------------------------------------
!

  subroutine irecv_i(recvbuf, recvcount, dest, recvtag, req)
    
    integer :: recvcount, dest, recvtag, req
    integer, dimension(recvcount) :: recvbuf
    integer :: ier

    call MPI_IRECV(recvbuf,recvcount,MPI_INTEGER,dest,recvtag,MPI_COMM_WORLD,req,ier)

  end subroutine irecv_i

!
!-------------------------------------------------------------------------------------------------
!

  subroutine wait_req(req)

    integer :: req

    integer :: ier

    call mpi_wait(req,MPI_STATUS_IGNORE,ier)
    if (ier /= 0 ) stop 'Error wait_req'
    
  end subroutine wait_req


!
!-------------------------------------------------------------------------------------------------
!
  subroutine compute_mpi_io_offset(in_offset, my_offset)
    integer, intent(in) :: in_offset
    integer(KIND=MPI_OFFSET_KIND), intent(inout) :: my_offset
    integer, dimension(:), allocatable :: offsets
    integer :: ierr
    allocate(offsets(nbproc_mpi))
    call MPI_Allgather(in_offset, 1, MPI_INTEGER, offsets, 1, &
    MPI_INTEGER, MPI_COMM_WORLD, ierr)
    my_offset = int(SUM(offsets(1:myrank_mpi)), kind=MPI_OFFSET_KIND)
    deallocate(offsets)
  end subroutine

!
!-------------------------------------------------------------------------------------------------
!
  subroutine delete_and_open_mpi_file(name_file, file_handle)
    character(len=CHAR_LEN), intent(in) :: name_file
    integer, intent(inout) :: file_handle
    integer :: ierr
    call MPI_File_delete(trim(name_file), MPI_INFO_NULL, ierr)  
    call MPI_File_open(MPI_COMM_WORLD, trim(name_file), &
     MPI_MODE_WRONLY + MPI_MODE_CREATE, MPI_INFO_NULL, file_handle, ierr)
  end subroutine delete_and_open_mpi_file

!
!-------------------------------------------------------------------------------------------------
!
  subroutine write_r4_bin_mpi(file_handle, data_in, np, my_offset)
    integer, intent(in) :: file_handle
    integer, intent(in) :: np
    integer(KIND=MPI_OFFSET_KIND), intent(in) :: my_offset
    real(kind=CUSTOM_REAL), dimension(np), intent(in) :: data_in
    real, dimension(:), allocatable :: data_sngl
    integer :: ierr
    call MPI_File_seek(file_handle, my_offset, MPI_SEEK_SET, ierr)
    allocate(data_sngl(np))
    data_sngl(:) = data_in(:)
    call MPI_File_write(file_handle, data_sngl, 4*np, &
    MPI_CHARACTER, MPI_STATUS_IGNORE, ierr)
    deallocate(data_sngl)
  end subroutine write_r4_bin_mpi
!
!-------------------------------------------------------------------------------------------------
!
  subroutine write_dr4_bin_mpi(file_handle, data, np, my_offset)
    integer, intent(in) :: file_handle
    integer, intent(in) :: np
    integer(KIND=MPI_OFFSET_KIND), intent(in) :: my_offset
    double precision, dimension(np), intent(in) :: data
    integer :: ierr
    call MPI_File_seek(file_handle, my_offset, MPI_SEEK_SET, ierr)
    call MPI_File_write(file_handle, sngl(data(:)), 4*np, &
    MPI_CHARACTER, MPI_STATUS_IGNORE, ierr)
  end subroutine write_dr4_bin_mpi
!
!-------------------------------------------------------------------------------------------------
!
  subroutine write_int4_bin_mpi(file_handle, data, np, my_offset)
    integer, intent(in) :: file_handle
    integer, intent(in) :: np
    integer(KIND=MPI_OFFSET_KIND), intent(in) :: my_offset
    integer, dimension(np), intent(in) :: data
    integer :: ierr
    call MPI_File_seek(file_handle, my_offset, MPI_SEEK_SET, ierr)
    call MPI_File_write(file_handle, data, 4*np, &
    MPI_CHARACTER, MPI_STATUS_IGNORE, ierr)
  end subroutine write_int4_bin_mpi
!
!-------------------------------------------------------------------------------------------------
!
  subroutine close_mpi_file(file_handle)
    integer, intent(inout) :: file_handle
    integer :: ierr
    CALL MPI_File_close(file_handle, ierr)
  end subroutine close_mpi_file

 end module mpi_mod
