module addressing_mod

  use config_mod, only : NDIM, NGLLX, NGLLY, NGLLZ
  
  implicit none
  public  :: create_adressing, get_global_indirect_addressing, sort_array_coordinates
  private :: get_global, heap_sort_multi
  
contains

  !!---------------------------------------------------------------------------------------
  subroutine create_adressing(ibool, xstore, ystore, zstore, NX, NY, NZ, NSP,  typical_mesh_size)

    integer, intent(in) :: NX, NY,NZ, NSP
    double precision, intent(in) :: typical_mesh_size
    double precision, dimension(NX,NY,NZ,NSP), intent(in) :: xstore, ystore, zstore
    integer, dimension(NX,NY,NZ,NSP), intent(inout) :: ibool

    integer :: i,j,k,ispec, ilocnum, ieoff 
    integer :: npointot, nglob
    double precision, dimension(:), allocatable :: xp, yp, zp
    integer, dimension(:), allocatable  :: iglob, locval 
    logical, dimension(:), allocatable  :: ifseg

    npointot = NX*NY*NZ*NSP
    allocate(xp(npointot))
    allocate(yp(npointot))
    allocate(zp(npointot))
    allocate(iglob(npointot))
    allocate(locval(npointot))
    allocate(ifseg(npointot))
    
    ! puts x,y,z locations into 1D arrays
    do ispec = 1,NSP
       ieoff = NX*NY*NZ*(ispec-1)
       ilocnum = 0
       do k = 1,NZ
          do j = 1,NY
             do i = 1,NX
                ilocnum = ilocnum + 1
                xp(ilocnum+ieoff) = xstore(i,j,k,ispec)
                yp(ilocnum+ieoff) = ystore(i,j,k,ispec)
                zp(ilocnum+ieoff) = zstore(i,j,k,ispec)
             enddo
          enddo
       enddo
    enddo
    
    ! sorts xp,yp,zp in lexicographical order (increasing values)
    call get_global(npointot, xp, yp, zp, iglob, locval, ifseg, nglob, typical_mesh_size)
    
    ibool(:,:,:,:) = 0
    do ispec = 1,NSP
      ieoff = NX*NY*NZ*(ispec-1)
      ilocnum = 0
      do k = 1,NZ
        do j = 1,NY
          do i = 1,NX
            ilocnum = ilocnum + 1
            ibool(i,j,k,ispec) = iglob(ilocnum+ieoff)
          enddo
        enddo
      enddo
   enddo

   
    deallocate(xp, yp, zp)
    deallocate(iglob, locval)
    deallocate(ifseg)
    
  end subroutine create_adressing

  
!!---------------------------------------------------------------------------------------
  subroutine get_global(npointot, xp, yp, zp, iglob, locval, ifseg, nglob, typical_mesh_size)

    integer, intent(in)    ::  npointot
    integer, intent(inout) ::  nglob
    integer, dimension(npointot), intent(inout) :: iglob, locval
    logical, dimension(npointot), intent(inout) ::  ifseg
    double precision, dimension(npointot) ::  xp, yp, zp
    double precision, intent(in) :: typical_mesh_size
    
    double precision :: SMALLVALTOL
    integer, dimension(:), allocatable :: ninseg,idummy
    !define geometrical tolerance based upon typical size of the model
    SMALLVALTOL = 1.d-10 * typical_mesh_size

    allocate(ninseg(npointot))
    allocate(idummy(npointot))
    
    call sort_array_coordinates(npointot, xp, yp, zp, idummy, iglob, locval, ifseg, &
         nglob, ninseg, SMALLVALTOL)
    
    deallocate(ninseg, idummy)
    
  end subroutine get_global

!! ------------------------------------------------------------------

  subroutine get_global_indirect_addressing(nspec, nglob, ibool)

    implicit none
    
    integer, intent(in) :: nspec,nglob
    integer, dimension(NGLLX,NGLLY,NGLLZ,nspec), intent(inout) :: ibool

    ! mask to sort ibool
    integer, dimension(:), allocatable :: mask_ibool
    integer, dimension(:,:,:,:), allocatable :: copy_ibool_ori
    integer :: inumber
    integer:: i,j,k,ispec,ier

    ! copies original array
    allocate(copy_ibool_ori(NGLLX,NGLLY,NGLLZ,nspec),stat=ier)
    if (ier /= 0) stop 'error allocating array copy_ibool_ori module adressing mod'
    allocate(mask_ibool(nglob),stat=ier)
    if (ier /= 0) stop 'error allocating array mask_ibool module adressing mod'
  
    
    mask_ibool(:) = -1
    copy_ibool_ori(:,:,:,:) = ibool(:,:,:,:)

    ! reduces misses
    inumber = 0
    do ispec=1,nspec
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
                if (mask_ibool(copy_ibool_ori(i,j,k,ispec)) == -1) then
                   ! create a new point
                   inumber = inumber + 1
                   ibool(i,j,k,ispec) = inumber
                   mask_ibool(copy_ibool_ori(i,j,k,ispec)) = inumber
                else
                   ! use an existing point created previously
                   ibool(i,j,k,ispec) = mask_ibool(copy_ibool_ori(i,j,k,ispec))
                endif
             enddo
          enddo
       enddo
    enddo
    
    ! cleanup
    deallocate(copy_ibool_ori,stat=ier); if (ier /= 0) stop 'error in deallocate get_global_indirect_addressing 1'
    deallocate(mask_ibool,stat=ier); if (ier /= 0) stop 'error in deallocate get_global_indirect_addressing 2'

  end subroutine get_global_indirect_addressing
  
!!---------------------------------------------------------------------------------------
  subroutine sort_array_coordinates(npointot, x, y, z, ibool, iglob, locval, ifseg, nglob, ninseg, xtol)

    ! this routine MUST be in double precision to avoid sensitivity
    ! to roundoff errors in the coordinates of the points
    !
    ! returns: sorted indexing array (ibool),  reordering array (iglob) & number of global points (nglob)

    

    implicit none

    integer, intent(in) :: npointot
    double precision, dimension(npointot), intent(inout) :: x, y, z
    integer, dimension(npointot), intent(inout) :: ibool

    integer, dimension(npointot), intent(out) :: iglob, locval, ninseg
    logical, dimension(npointot), intent(out) :: ifseg
    integer, intent(out) :: nglob
    double precision, intent(in) :: xtol

    ! local parameters
    integer :: i, j
    integer :: nseg, ioff, iseg, ig

    ! establish initial pointers
    do i = 1,npointot
       locval(i) = i
    enddo

    ifseg(:) = .false.

    nseg = 1
    ifseg(1) = .true.
    ninseg(1) = npointot

    do j = 1,NDIM
       
       ! sort within each segment
       ioff = 1
       do iseg = 1,nseg
          if (j == 1) then
             ! sort on X
             call heap_sort_multi(ninseg(iseg), x(ioff), y(ioff), z(ioff), ibool(ioff), locval(ioff))
          else if (j == 2) then
             ! then sort on Y for a sublist of given constant X
             call heap_sort_multi(ninseg(iseg), y(ioff), x(ioff), z(ioff), ibool(ioff), locval(ioff))
          else
             ! then sort on Z for a sublist of given constant X and Y
             call heap_sort_multi(ninseg(iseg), z(ioff), x(ioff), y(ioff), ibool(ioff), locval(ioff))
          endif
          ioff = ioff + ninseg(iseg)
       enddo
       
       ! check for jumps in current coordinate
       ! define a tolerance, normalized radius is 1., so let's use a small value
       if (j == 1) then
          do i = 2,npointot
             if (dabs(x(i) - x(i-1)) > xtol) ifseg(i) = .true.
          enddo
       else if (j == 2) then
          do i = 2,npointot
             if (dabs(y(i) - y(i-1)) > xtol) ifseg(i) = .true.
          enddo
       else
          do i = 2,npointot
             if (dabs(z(i) - z(i-1)) > xtol) ifseg(i) = .true.
          enddo
       endif

       ! count up number of different segments
       nseg = 0
       do i = 1,npointot
          if (ifseg(i)) then
             nseg = nseg + 1
             ninseg(nseg) = 1
          else
             ninseg(nseg) = ninseg(nseg) + 1
          endif
       enddo
       
    enddo
    
    ! assign global node numbers (now sorted lexicographically)
    ig = 0
    do i = 1,npointot
       ! eliminate the multiples by using a single (new) point number for all the points that have the same X Y Z after sorting
       if (ifseg(i)) ig = ig + 1
       iglob(locval(i)) = ig
    enddo
    
    nglob = ig
    
  end subroutine sort_array_coordinates

  !!---------------------------------------------------------------------------
  ! sorting routine left here for inlining
  
  ! -------------------- library for sorting routine ------------------
  
  ! sorting routines put here in same file to allow for inlining
  
  ! this directive avoids triggering a random bug in Intel ifort v13 (in the compiler, not in SPECFEM),
  ! fixed in later versions of Intel ifort, which also ignore this directive because it was discontinued
  !$DIR NOOPTIMIZE
  subroutine heap_sort_multi(N, dx, dy, dz, ia, ib)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(N), intent(inout) :: dx
    double precision, dimension(N), intent(inout) :: dy
    double precision, dimension(N), intent(inout) :: dz
    integer, dimension(N), intent(inout) :: ia
    integer, dimension(N), intent(inout) :: ib
    
    integer :: i
    
    ! checks if anything to do
    if (N < 2) return
    
    ! builds heap
    do i = N/2, 1, -1
       call heap_sort_siftdown(i, n)
    enddo
    
    ! sorts array
    do i = N, 2, -1
       ! swaps last and first entry in this section
       call dswap(dx, 1, i)
       call dswap(dy, 1, i)
       call dswap(dz, 1, i)
       call iswap(ia, 1, i)
       call iswap(ib, 1, i)
       call heap_sort_siftdown(1, i - 1)
    enddo
    
  contains
    
    ! this directive avoids triggering a random bug in Intel ifort v13 (in the compiler, not in SPECFEM),
    ! fixed in later versions of Intel ifort, which also ignore this directive because it was discontinued
    !$DIR NOOPTIMIZE
    subroutine dswap(A, i, j)
      
      double precision, dimension(:), intent(inout) :: A
      integer, intent(in) :: i
      integer, intent(in) :: j
      
      double precision :: tmp
      
      tmp = A(i)
      A(i) = A(j)
      A(j) = tmp
      
    end subroutine dswap
    
    subroutine iswap(A, i, j)
      
      integer, dimension(:), intent(inout) :: A
      integer, intent(in) :: i
      integer, intent(in) :: j
      
      integer :: tmp
      
      tmp = A(i)
      A(i) = A(j)
      A(j) = tmp
      
    end subroutine iswap
    
    ! this directive avoids triggering a random bug in Intel ifort v13 (in the compiler, not in SPECFEM),
    ! fixed in later versions of Intel ifort, which also ignore this directive because it was discontinued
    !$DIR NOOPTIMIZE
    subroutine heap_sort_siftdown(start, bottom)
      
      integer, intent(in) :: start
      integer, intent(in) :: bottom
      
      integer :: i, j
      double precision :: xtmp, ytmp, ztmp
      integer :: atmp, btmp
      
      i = start
      xtmp = dx(i)
      ytmp = dy(i)
      ztmp = dz(i)
      atmp = ia(i)
      btmp = ib(i)
      
      j = 2 * i
      do while (j <= bottom)
         ! chooses larger value first in this section
         if (j < bottom) then
            if (dx(j) <= dx(j+1)) j = j + 1
         endif
         
         ! checks if section already smaller than initial value
         if (dx(j) < xtmp) exit
         
         dx(i) = dx(j)
         dy(i) = dy(j)
         dz(i) = dz(j)
         ia(i) = ia(j)
         ib(i) = ib(j)
         i = j
         j = 2 * i
      enddo
      
      dx(i) = xtmp
      dy(i) = ytmp
      dz(i) = ztmp
      ia(i) = atmp
      ib(i) = btmp
      
    end subroutine heap_sort_siftdown
    
  end subroutine heap_sort_multi
  

  
end module addressing_mod
