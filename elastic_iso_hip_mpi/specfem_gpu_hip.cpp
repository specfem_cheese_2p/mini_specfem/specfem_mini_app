
#include "specfem_gpu_hip.hpp"

//  ----------------------------------------------
void get_free_memory(double* free_db, double* used_db, double* total_db) {

  //TRACE("get_free_memory");

  // gets memory usage in byte
  size_t free_byte = 0;
  size_t total_byte = 0;

//#ifdef USE_CUDA
//if (run_cuda){
    hipError_t status = hipMemGetInfo( &free_byte, &total_byte ) ;
    if (hipSuccess != status){
      printf("Error: hipMemGetInfo fails, %s \n", hipGetErrorString(status) );
      exit(EXIT_FAILURE);
    }
//  }
//#endif
// #ifdef USE_HIP
//   if (run_hip){
//     hipError_t status = hipMemGetInfo( &free_byte, &total_byte ) ;
//     if (hipSuccess != status){
//       printf("Error: hipMemGetInfo fails, %s \n", hipGetErrorString(status) );
//       exit(EXIT_FAILURE);
//     }
//   }
// #endif

  *free_db = (double)free_byte ;
  *total_db = (double)total_byte ;
  *used_db = *total_db - *free_db ;
  return;
}


//  ----------------------------------------------
extern "C"
void FC_FUNC_(get_free_device_memory,
              get_FREE_DEVICE_MEMORY)(TC* free, TC* used, TC* total) {
  //TRACE("get_free_device_memory");

  double free_db,used_db,total_db;

  get_free_memory(&free_db,&used_db,&total_db);

  // converts to MB
  *free = (TC) free_db/1024.0/1024.0;
  *used = (TC) used_db/1024.0/1024.0;
  *total = (TC) total_db/1024.0/1024.0;
  return;
}


//  ----------------------------------------------
extern "C"
void FC_FUNC_(init_gpu,
              INIT_GPU)(int* myrank_f, int* ncuda_devices){
    int device = 0;
    int device_count = 0;
    hipGetDeviceCount(&device_count);
    if (device_count == 0) {
        printf("No CUDA devices found\n");
        exit(1);
    }
  // Gets rank number of MPI process
  int myrank = *myrank_f;

  // Gets number of GPU devices
  device_count = 0;
  hipGetDeviceCount(&device_count);

  // being verbose and catches error from first call to CUDA runtime function, without synchronize call
  hipError_t err = hipGetLastError();

  // adds quick check on versions
  int driverVersion = 0, runtimeVersion = 0;
  hipDriverGetVersion(&driverVersion);
  hipRuntimeGetVersion(&runtimeVersion);

  // exit in case first cuda call failed
  if (err != hipSuccess){
    fprintf(stderr,"Error after hipGetDeviceCount: %s\n", hipGetErrorString(err));
    fprintf(stderr,"CUDA Device count: %d\n",device_count);
    fprintf(stderr,"CUDA Driver Version / Runtime Version: %d.%d / %d.%d\n",
            driverVersion / 1000, (driverVersion % 100) / 10,
            runtimeVersion / 1000, (runtimeVersion % 100) / 10);
    exit(1);
    //exit_on_error("CUDA runtime error: hipGetDeviceCount failed\n\nplease check if driver and runtime libraries work together\nor on cluster environments enable MPS (Multi-Process Service) to use single GPU with multiple MPI processes\n\nexiting...\n");
  }
  
  // checks if CUDA devices available
  //if (device_count == 0) exit_on_error("CUDA runtime error: there is no device supporting CUDA\n");

  // returns device count to fortran
  *ncuda_devices = device_count;
  
  device = myrank % device_count;

  hipSetDevice( device );
 //exit_on_gpu_error("hipSetDevice has invalid device");

  // double check that device was  properly selected
  hipGetDevice(&device);
  if (device != (myrank % device_count) ){
       printf("Error rank: %d devices: %d \n",myrank,device_count);
       printf("  hipSetDevice()=%d\n  hipGetDevice()=%d\n",myrank%device_count,device);
       //exit_on_error("CUDA set/get device error: device id conflict \n");
       exit(1);
  }
  // returns a handle to the active device
  //hipGetDevice(&device);

  // get device properties
  struct hipDeviceProp_t deviceProp;
  hipGetDeviceProperties(&deviceProp,device);

  // exit if the machine has no CUDA-enabled device
  if (deviceProp.major == 9999 && deviceProp.minor == 9999){
    fprintf(stderr,"No CUDA-enabled device found, exiting...\n\n");
    exit(1);
    //exit_on_error("CUDA runtime error: there is no CUDA-enabled device found\n");
  }

  // print device properties
  // memory usage
  double free_db,used_db,total_db;
  get_free_memory(&free_db,&used_db,&total_db);

  int do_output_info = 0;
  if (myrank==0) { do_output_info = 1; }

  // output to file
  if (do_output_info ){
    //fp = fopen(filename,"w");
    //if (fp != NULL){
      // display device properties
      printf("Device Name = %s\n",deviceProp.name);
      printf("memory:\n");
      printf("  totalGlobalMem (in MB): %f\n",(unsigned long) deviceProp.totalGlobalMem / (1024.f * 1024.f));
      printf("  totalGlobalMem (in GB): %f\n",(unsigned long) deviceProp.totalGlobalMem / (1024.f * 1024.f * 1024.f));
      printf("  totalConstMem (in bytes): %lu\n",(unsigned long) deviceProp.totalConstMem);
      printf("  Maximum 1D texture size (in bytes): %lu\n",(unsigned long) deviceProp.maxTexture1D);
      printf("  sharedMemPerBlock (in bytes): %lu\n",(unsigned long) deviceProp.sharedMemPerBlock);
      printf("  regsPerBlock (in bytes): %lu\n",(unsigned long) deviceProp.regsPerBlock);
      printf("blocks:\n");
      printf("  Maximum number of threads per block: %d\n",deviceProp.maxThreadsPerBlock);
      printf("  Maximum size of each dimension of a block: %d x %d x %d\n",
              deviceProp.maxThreadsDim[0],deviceProp.maxThreadsDim[1],deviceProp.maxThreadsDim[2]);
      printf("  Maximum sizes of each dimension of a grid: %d x %d x %d\n",
              deviceProp.maxGridSize[0],deviceProp.maxGridSize[1],deviceProp.maxGridSize[2]);
      printf("features:\n");
      printf("  Compute capability of the device = %d.%d\n", deviceProp.major, deviceProp.minor);
      printf("  multiProcessorCount: %d\n",deviceProp.multiProcessorCount);
      if (deviceProp.canMapHostMemory){
        printf("  canMapHostMemory: TRUE\n");
      }else{
        printf("  canMapHostMemory: FALSE\n");
      }
      //if (deviceProp.asyncEngineCount){
      //  printf("  asyncEngineCount: TRUE\n");
      //}else{
      //  printf("  deviceOverlap: FALSE\n");
      //}
      if (deviceProp.concurrentKernels){
        printf("  concurrentKernels: TRUE\n");
      }else{
        printf("  concurrentKernels: FALSE\n");
      }
      printf("HIP Device count: %d\n",device_count);
      printf("HIP Driver Version / Runtime Version          %d.%d / %d.%d\n",
              driverVersion / 1000, (driverVersion % 100) / 10,
              runtimeVersion / 1000, (runtimeVersion % 100) / 10);

      // outputs initial memory infos via hipMemGetInfo()
      printf("memory usage:\n");
      printf("  rank %d: GPU memory usage: used = %f MB, free = %f MB, total = %f MB\n",myrank,
              used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);

      // closes output file
      //fclose(fp);
    //}
  }
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(gpu_synchronize,
	      GPU_SYNCHRONIZE)(){
    hipDeviceSynchronize();
}


//  ----------------------------------------------
extern "C"
void FC_FUNC_(init_specfemgpu,
	          INIT_SPECFEMGPU)(long* specfemgpu_pointer){
  SpecfemGPU<TC> *sp = new SpecfemGPU<TC>();
  *specfemgpu_pointer = (long)sp;
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(destroy_specfemgpu,
	          DESTROY_SPECFEMGPU)(long* specfemgpu_pointer){
  SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
  //sp->free_memory();
  delete sp;
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_set_size,
SPECFEMGPU_SET_SIZE)(long* specfemgpu_pointer, int* nspec, int* nglob) {
SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
int NSPEC = *nspec;
int NGLOB = *nglob;
sp->set_size(NSPEC, NGLOB);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mesh,
SPECFEMGPU_TRANSFERT_MESH)(long* specfemgpu_pointer, TC* fc_dxi_dx, TC* fc_dxi_dy, TC* fc_dxi_dz,
                                                     TC* fc_deta_dx, TC* fc_deta_dy, TC* fc_deta_dz,
                                                     TC* fc_dgamma_dx, TC* fc_dgamma_dy, TC* fc_dgamma_dz) {
SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
sp->tranfert_mesh(fc_dxi_dx, fc_dxi_dy, fc_dxi_dz,
                  fc_deta_dx, fc_deta_dy, fc_deta_dz,
                  fc_dgamma_dx, fc_dgamma_dy, fc_dgamma_dz);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mass,
SPECFEMGPU_TRANSFERT_MASS)(long* specfemgpu_pointer, TC* fc_inv_mass){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_inv_mass_matrix(fc_inv_mass);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_fields,
SPECFEMGPU_TRANSFERT_FIELDS)(long* specfemgpu_pointer, TC* fc_displ, TC* fc_veloc, TC* fc_accel){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_fields(fc_displ, fc_veloc, fc_accel);   
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_model,
SPECFEMGPU_TRANSFERT_MODEL)(long* specfemgpu_pointer, TC* fc_rho, TC* fc_kappa, TC* fc_mu){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_model(fc_rho, fc_kappa, fc_mu);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_ibool,
SPECFEMGPU_TRANSFERT_IBOOL)(long* specfemgpu_pointer, int* fc_ibool){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_ibool(fc_ibool);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_phase_inner,
SPECFEMGPU_TRANSFERT_PHASE_INNER)(long* specfemgpu_pointer, 
int* fc_phase_ispec_inner_elastic, int* fc_nspec_outer_elastic, int* fc_nspec_inner_elastic)
{
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nspec_outer_elastic = *fc_nspec_outer_elastic;
    int nspec_inner_elastic = *fc_nspec_inner_elastic;
    sp->transfert_phase_inner(fc_phase_ispec_inner_elastic, 
    nspec_outer_elastic, nspec_inner_elastic);   
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_boundary,
SPECFEMGPU_TRANSFERT_BOUNDARY)(long* specfemgpu_pointer, int* fc_index_gll_boundary,
 TC* fc_wstacey, TC* fc_rho_vp, TC* fc_rho_vs, TC* fc_normal, int* fc_ngll_boundary){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int ngll_boundary = * fc_ngll_boundary;
    sp->transfert_boundary(fc_index_gll_boundary, fc_wstacey, 
    fc_rho_vp, fc_rho_vs, fc_normal, ngll_boundary);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_source,
SPECFEMGPU_TRANSFERT_SOURCE)(long* specfemgpu_pointer, int* fc_ispec_source, 
TC* fc_Fx, TC* fc_Fy, TC* fc_Fz, TC* fc_hxis, TC* fc_hetas, TC* fc_hgammas, 
TC* fc_used_stf, int* fc_nsrc, int* fc_nt){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nsrc = *fc_nsrc;
    int nt = *fc_nt; 
    sp->transfert_source(fc_ispec_source, fc_Fx, fc_Fy, fc_Fz, 
    fc_hxis, fc_hetas, fc_hgammas, fc_used_stf, nsrc, nt);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_receiver,
SPECFEMGPU_TRANSFERT_RECEIVER)(long* specfemgpu_pointer, int* fc_ispec_rec, 
TC* fc_hxir, TC* fc_hetar, TC* fc_hgammar, TC* fc_seismogram_d, int* fc_nrec, int* fc_nstep) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nrec= *fc_nrec;
    int nstep=*fc_nstep;
    sp->transfert_receiver(fc_ispec_rec, fc_hxir, fc_hetar, fc_hgammar, fc_seismogram_d, nrec, nstep);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_reference_element,
SPECFEMGPU_TRANSFERT_REFERENCE_ELEMENT)(long* specfemgpu_pointer, 
TC* fc_wgllwgll_xy, TC* fc_hprime_wgll_xx, TC* fc_hprime_xxT){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->transfert_reference_element(fc_wgllwgll_xy, fc_hprime_wgll_xx, fc_hprime_xxT);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_deltat,
SPECFEMGPU_TRANSFERT_DELTAT)(long* specfemgpu_pointer, TC* fc_dt){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    TC dt = *fc_dt;
    sp->transfert_deltat(dt);   
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_transfert_mpi_boundary,
SPECFEMGPU_TRANSFERT_MPI_BOUNDARY)(long* specfemgpu_pointer, int* fc_ibool_interfaces_ext, 
int* fc_nibool_interfaces_ext, int* fc_nb_interfaces, int* fc_nspec_interfaces_max, 
int* fc_max_nibool_interfaces_ext){
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int nb_interfaces = *fc_nb_interfaces;
    int max_nibool_interfaces_ext = *fc_max_nibool_interfaces_ext;
    int nspec_interfaces_max = *fc_nspec_interfaces_max;
    //std::cout <<" max_nibool_interfaces_ext "  << max_nibool_interfaces_ext << std::endl; 
    sp->transfert_mpi_boundary(fc_ibool_interfaces_ext, fc_nibool_interfaces_ext, 
    nb_interfaces, max_nibool_interfaces_ext, nspec_interfaces_max);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_print,
SPECFEMGPU_PRINT) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->print_this();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_displ,
SPECFEMGPU_GET_DISPL) (long* specfemgpu_pointer, TC* fc_displ) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_displ(fc_displ);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_veloc,
SPECFEMGPU_GET_VELOC) (long* specfemgpu_pointer, TC* fc_veloc) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_veloc(fc_veloc);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_accel,
SPECFEMGPU_GET_ACCEL) (long* specfemgpu_pointer, TC* fc_accel) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_accel(fc_accel);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_set_accel,
SPECFEMGPU_SET_ACCEL) (long* specfemgpu_pointer, TC* fc_accel) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->set_accel(fc_accel);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_seismogram,
SPECFEMGPU_GET_SEISMOGRAM) (long* specfemgpu_pointer, TC* fc_seismogram) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_seismogram_d(fc_seismogram);
}
//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_apply_inv_mass,
SPECFEMGPU_APPLY_INV_MASS) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->apply_inv_matrix();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_predictor,
SPECFEMGPU_PREDICTOR) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->predictor();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_corrector,
SPECFEMGPU_CORRECTOR) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->corrector();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_rhs,
SPECFEMGPU_RHS) (long* specfemgpu_pointer, int* fc_iphase) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    int iphase = *fc_iphase;
    sp->compute_rhs(iphase);
    //sp->test_rhs(iphase);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_add_source_force,
SPECFEMGPU_ADD_SOURCE_FORCE) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->add_source_force();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_store_seismogram,
SPECFEMGPU_STORE_SEISMOGRAM) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->store_seismogram();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_compute_stacey,
SPECFEMGPU_COMPUTE_STAYCEY) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->compute_stacey();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_prepare_mpi_buffer,
SPECFEMGPU_PREPARE_MPI_BUFFER) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->prepare_mpi_buffer();
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_store_mpi_buffer,
SPECFEMGPU_STORE_MPI_BUFFER) (long* specfemgpu_pointer, TC* fc_mpi_buffer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_mpi_buffer(fc_mpi_buffer);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_get_recv_mpi_buffer,
SPECFEMGPU_GET_RECV_MPI_BUFFER) (long* specfemgpu_pointer, TC* fc_mpi_buffer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->get_recv_mpi_buffer(fc_mpi_buffer);
}

//  ----------------------------------------------
extern "C"
void FC_FUNC_(specfemgpu_assemble_mpi_accel,
SPECFEMGPU_ASSEMBLE_MPI_ACCEL) (long* specfemgpu_pointer) {
    SpecfemGPU<TC> *sp = (SpecfemGPU<TC>*) (*specfemgpu_pointer);
    sp->assemble_mpi_accel();
}
