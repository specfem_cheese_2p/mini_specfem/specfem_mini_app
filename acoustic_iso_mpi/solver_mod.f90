module solver_mod

    !
    !
    !  module that manage the main computation 
    !
    !

    use specfem_mod

    implicit none 

    private    
    public :: iterate_time

contains

!!---------------------------------------------------------------------------
subroutine iterate_time()

    integer :: it, iphase
    real(kind=CUSTOM_REAL) :: t
    character(len=CHAR_LEN) :: name_file
    real(kind=CUSTOM_REAL) :: max_displ
    real :: time_start, time_end
    
    !! initlialize fields 
    call initialize_solver()
    
    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'Starting time iteration loop...'
       write(*,*)
    end if

    !! prepare for volume visualization
    call io_setup_connectivity(xdof_specfem, ibool_specfem, NGLLX, NGLLY, NGLLZ,&
    nspec_specfem, nglob_specfem, myrank)
    !! complete 3D mesh visualization 
    call write_xdmf_mesh(myrank)

    call cpu_time(time_start)

    do it = 1, NSTEP
     
      t = (it-1)*deltat_config

       if ( mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. check_stability) then
          max_displ = maxval(pot_ac_specfem(:))
          if (max_displ /= max_displ) then
             stop 'forward simulation became unstable in elastic domain and blew up'
          end if
       end if
       
       if (myrank == 0 .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. verbose) then
          write(*,*) " time step : ",  it, ' / ', NSTEP
       end if
       
       if (movie .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0) then

          write(name_file,"('output/pot_ac_it',i6.6,'.bin')") it
          call dump_scalar_cr_binary_mpi(name_file, pot_ac_specfem, nglob_specfem)

       end if

       call newmark_predictor()

       !! compute rhs --------------------------------------------------------------
       do iphase = 1, 2
          
          call compute_forces_iso_elastic(iphase)
          
          if (iphase == 1) then
             
             call add_source_term(it)          !! source term
             call store_seismogram(it)         !! store seismograms 
             call compute_stacey()             !! boundary conditions

             !! send accelareation to neighbours mpi-slice
             call assemble_MPI_vector_async_send(nglob_specfem, pot_ac_dot_dot_specfem)
          else
             !! receive accelareation from neighbours mpi-slice
             call assemble_MPI_vector_async_w_ord(nglob_specfem, pot_ac_dot_dot_specfem)
          end if

       end do
       !! -------------------------------------------------------------------------
       
       call  newmark_corector()
       
    end do

    call cpu_time(time_end)
    
    call write_sismogram()

    if (movie) then
       call write_xdmf_movie(myrank, NSTEP)
    end if

    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'End of time iteration loop...', time_end - time_start,' s'
       write(*,*)
    end if
    
     
end subroutine iterate_time
!!---------------------------------------------------------------------------
subroutine initialize_solver()
    pot_ac_specfem(:) = 0._CUSTOM_REAL
    pot_ac_dot_specfem(:) = 0._CUSTOM_REAL
    pot_ac_dot_dot_specfem(:) = 0._CUSTOM_REAL
end subroutine initialize_solver
!!---------------------------------------------------------------------------
subroutine newmark_predictor()
    integer :: i
    do i=1, nglob_specfem
       pot_ac_specfem(i) = pot_ac_specfem(i) + deltat_config*pot_ac_dot_specfem(i) + deltatsqover2*pot_ac_dot_dot_specfem(i)
       pot_ac_dot_specfem(i) = pot_ac_dot_specfem(i) + deltatover2*pot_ac_dot_dot_specfem(i)
       pot_ac_dot_dot_specfem(i) = 0._CUSTOM_REAL
    end do
end subroutine newmark_predictor
!!---------------------------------------------------------------------------
subroutine newmark_corector()
    integer :: i
    do  i=1, nglob_specfem
       pot_ac_dot_dot_specfem(i) = pot_ac_dot_dot_specfem(i)*inv_mass_specfem(i)
       pot_ac_dot_specfem(i) = pot_ac_dot_specfem(i) + deltatover2*pot_ac_dot_dot_specfem(i)
    end do
end subroutine newmark_corector
!!---------------------------------------------------------------------------
subroutine add_source_term(it)
    integer, intent(in) :: it
    integer :: ispec, isrc, i,j,k, iglob
    real(kind=CUSTOM_REAL) :: hl
    do isrc =1, nsource
       ispec = ispec_src(isrc)
       if (ispec > 0) then
          
          do k=1,NGLLZ
             do j=1,NGLLY
                do i=1,NGLLX
                   iglob = ibool_specfem(i,j,k,ispec)
                   hl = hxis(i,isrc)*hetas(j,isrc)*hgammas(k,isrc) * used_stf(it) 
                  !  accel_specfem(1,iglob) = accel_specfem(1,iglob) + Fx(isrc) * hl
                  !  accel_specfem(2,iglob) = accel_specfem(2,iglob) + Fy(isrc) * hl
                   pot_ac_dot_dot_specfem(iglob) = pot_ac_dot_dot_specfem(iglob) + Fz(isrc) * hl
                end do
             end do
          end do
                
       end if
    end do
end subroutine add_source_term
!!---------------------------------------------------------------------------
subroutine store_seismogram(it)
    integer, intent(in) :: it
    integer :: ispec, irec, i,j,k, iglob
    real(kind=CUSTOM_REAL) :: hl
    do irec =1, nrec
       ispec = ispec_rec(irec)
       if (ispec > 0) then
          do k=1,NGLLZ
             do j=1,NGLLY
                do i=1,NGLLX
                   iglob = ibool_specfem(i,j,k,ispec)
                   hl = hxir(i,irec)*hetar(j,irec)*hgammar(k,irec)
                   seismogram_p(it, irec) = seismogram_p(it, irec) + hl * pot_ac_specfem(iglob)
                  !  seismogram_d(it, 2, irec) = seismogram_d(it, 2, irec) + hl * displ_specfem(2,iglob)
                  !  seismogram_d(it, 3, irec) = seismogram_d(it, 3, irec) + hl * displ_specfem(3,iglob)
                end do
             end do
          end do
       end if
    end do
 end subroutine store_seismogram
 !!---------------------------------------------------------------------------
subroutine compute_stacey()
    integer :: ib, iglob
    real(kind=CUSTOM_REAL) ::  vz, vn
    real(kind=CUSTOM_REAL) ::  tz, weight
    do ib = 1, ngll_boundary
      iglob = index_gll_boundary(ib)
      vz = pot_ac_dot_specfem(iglob)      
      vn = 1._CUSTOM_REAL / rho_vp(ib)
      tz =  vn*vz 
      weight = wstacey(ib)
      pot_ac_dot_dot_specfem(iglob) = pot_ac_dot_dot_specfem(iglob) - tz*weight
    end do
end subroutine compute_stacey
 !!---------------------------------------------------------------------------
subroutine compute_forces_iso_elastic(iphase)


    integer, intent(in) :: iphase

    integer :: i, j, k, l, ispec_p, ispec, iglob
    integer :: num_elements

    real(kind=CUSTOM_REAL) :: xixl,xiyl,xizl,etaxl,etayl,etazl,gammaxl,gammayl,gammazl,jacobianl
    real(kind=CUSTOM_REAL) :: dpotdxl,dpotdyl,dpotdzl
    real(kind=CUSTOM_REAL) :: tempx1l,tempx2l,tempx3l
    
    real(kind=CUSTOM_REAL) :: fac1,fac2,fac3
    real(kind=CUSTOM_REAL) :: hp1, hp2, hp3
    
    real(kind=CUSTOM_REAL) :: rho_invl
   
    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: pot_ac_loc
    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: tempx1,tempx2,tempx3
   

    ! choses inner/outer elements
    if (iphase == 1) then
       num_elements = nspec_outer_elastic
    else
       num_elements = nspec_inner_elastic
    endif

    do ispec_p = 1,num_elements

       ! returns element id from stored element list
       ispec = phase_ispec_inner_elastic(ispec_p,iphase)

       ! store element displacement in local array 
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
                iglob = ibool_specfem(i,j,k,ispec)
                pot_ac_loc(i,j,k) = pot_ac_specfem(iglob)
             enddo
          enddo
       enddo

       
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
                
                tempx1l = 0._CUSTOM_REAL
                tempx2l = 0._CUSTOM_REAL
                tempx3l = 0._CUSTOM_REAL
               
                
                ! we can merge these loops because NGLLX = NGLLY = NGLLZ
                do l=1,NGLLX
                   hp1 = hprime_xxT(l,i)
                   tempx1l = tempx1l + pot_ac_loc(l,j,k) * hp1

                   hp2 = hprime_yyT(l,j)
                   tempx2l = tempx2l + pot_ac_loc(i,l,k) * hp2
                  
                   hp3 = hprime_zzT(l,k)
                   tempx3l = tempx3l + pot_ac_loc(i,j,l) * hp3
                  
                enddo
                
                xixl = dxi_dx_specfem(i,j,k,ispec)
                xiyl = dxi_dy_specfem(i,j,k,ispec)
                xizl = dxi_dz_specfem(i,j,k,ispec)
                etaxl = deta_dx_specfem(i,j,k,ispec)
                etayl = deta_dy_specfem(i,j,k,ispec)
                etazl = deta_dz_specfem(i,j,k,ispec)
                gammaxl = dgamma_dx_specfem(i,j,k,ispec)
                gammayl = dgamma_dy_specfem(i,j,k,ispec)
                gammazl = dgamma_dz_specfem(i,j,k,ispec)
                jacobianl = jacobian_specfem(i,j,k,ispec)
                
                
                dpotdxl = xixl*tempx1l + etaxl*tempx2l + gammaxl*tempx3l
                dpotdyl = xiyl*tempx1l + etayl*tempx2l + gammayl*tempx3l
                dpotdzl = xizl*tempx1l + etazl*tempx2l + gammazl*tempx3l
                 
                ! isotropic case
                rho_invl = rho_inv_specfem(i,j,k,ispec)
                
                ! form dot product with test vector, non-symmetric form 
                tempx1(i,j,k) = jacobianl * rho_invl * (dpotdxl * xixl + dpotdyl * xiyl + dpotdzl * xizl) 
                tempx2(i,j,k) = jacobianl * rho_invl * (dpotdxl * etaxl + dpotdyl * etayl + dpotdzl * etazl)  
                tempx3(i,j,k) = jacobianl * rho_invl * (dpotdxl * gammaxl + dpotdyl * gammayl + dpotdzl * gammazl) 
              
             end do
          end do
       end do


       ! second double-loop over GLL to compute all the terms
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
           
                tempx1l = 0._CUSTOM_REAL
                tempx2l = 0._CUSTOM_REAL
                tempx3l = 0._CUSTOM_REAL
            
                ! we can merge these loops because NGLLX = NGLLY = NGLLZ
                do l=1,NGLLX
                   fac1 = hprimewgll_xx(l,i)
                   tempx1l = tempx1l + tempx1(l,j,k) * fac1
               
                   fac2 = hprimewgll_yy(l,j)
                   tempx2l = tempx2l + tempx2(i,l,k) * fac2

                   fac3 = hprimewgll_zz(l,k)
                   tempx3l = tempx3l + tempx3(i,j,l) * fac3
                
                enddo
                
                fac1 = wgllwgll_yz(j,k)
                fac2 = wgllwgll_xz(i,k)
                fac3 = wgllwgll_xy(i,j)

                ! sum contributions from each element to the global mesh using indirect addressing
                iglob = ibool_specfem(i,j,k,ispec)
                pot_ac_dot_dot_specfem(iglob) = pot_ac_dot_dot_specfem(iglob) - (fac1 * tempx1l + fac2 * tempx2l + fac3 * tempx3l)
               

             end do
          end do
       end do
       
    end do

  end subroutine compute_forces_iso_elastic

end module solver_mod