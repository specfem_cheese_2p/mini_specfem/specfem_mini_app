% current run output
U = load('../displ_00001.txt');

% reference output
Uref= load('displ_00001_ref_hommogeneous.txt');

figure; 
plot(U(:,2))
hold on
plot(Uref(:,2),'--')

