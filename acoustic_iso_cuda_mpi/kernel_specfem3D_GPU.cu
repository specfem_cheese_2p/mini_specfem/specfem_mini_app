#ifndef __KERNEL_SPECFEM3D__CU
#define __KERNEL_SPECFEM3D__CU

#include <iostream>
#include "cuda_constants.cuh"

 void get_blocks_xy(int num_blocks,int* num_blocks_x,int* num_blocks_y) {

    // Initially sets the blocks_x to be the num_blocks, and adds rows as needed (block size limit of 65535).
    // If an additional row is added, the row length is cut in
    // half. If the block count is odd, there will be 1 too many blocks,
    // which must be managed at runtime with an if statement.
  
    *num_blocks_x = num_blocks;
    *num_blocks_y = 1;
  
    while (*num_blocks_x > MAXIMUM_GRID_DIM) {
      *num_blocks_x = (int) ceil(*num_blocks_x * 0.5f);
      *num_blocks_y = *num_blocks_y * 2;
    }
  
    return;
  }
  
//###############################################################################################################
__global__ void kernel_acoustic_iso(
  TC* __restrict__ p_pot_ac_dot_dot,
  const TC* __restrict__ p_pot_ac,
  const TC* __restrict__ p_dxi_dx,
  const TC* __restrict__ p_dxi_dy,
  const TC* __restrict__ p_dxi_dz,
  const TC* __restrict__ p_deta_dx,
  const TC* __restrict__ p_deta_dy,
  const TC* __restrict__ p_deta_dz,
  const TC* __restrict__ p_dgamma_dx,
  const TC* __restrict__ p_dgamma_dy,
  const TC* __restrict__ p_dgamma_dz,
  const TC* __restrict__ p_inv_rho,
  const TC* __restrict__ p_wgllwgll_xy,
  const TC* __restrict__ p_hprime_wgll_xx,
  const TC* __restrict__ p_hprime_xxT,
  const int* __restrict__ p_ibool,
  const int* __restrict__ p_phase_ispec_inner, 
  const int nb_blocks_to_compute,
  const int nb_phase_ispec, 
  const int IPHASE)
  {
    // block number -> element (one block per element)
    int bx = blockIdx.y*gridDim.x+blockIdx.x;
    // thread -> gll (one thread per gll)
    int tx = threadIdx.x;
    int th_dummy = tx; // thread that have no work to do
    // limits thread ids to range [0,NGLL^3-1] because tx is in range [0, NGLL3_PADDED - 1]
    if (tx >= NGLL3DC) th_dummy = NGLL3DC - 1;
    // checks if anything to do 
    if (bx >= nb_blocks_to_compute) return;
    
    // spectral-element id
    // iphase-1 and working_element-1 for Fortran->C array conventions
    const int working_element = p_phase_ispec_inner[bx + nb_phase_ispec*(IPHASE-1)] - 1;
    
    // local padded index
    //const int offset = working_element*NGLL3DC_PADDED + tx;  
    // local non-padded index
    const int index = working_element*NGLL3DC + th_dummy;
    const int offset = index; // in this version non padded

    // global index
    const int iglob = p_ibool[offset] - 1;

   // shared memory
   __shared__ TC sh_tempx[NGLL3DC_PADDED];
   //__shared__ TC sh_tempy[NGLL3DC_PADDED];
   //__shared__ TC sh_tempz[NGLL3DC_PADDED];
   __shared__ TC sh_hprime_xx[NGLL2DC];
   __shared__ TC sh_hprimewgll_xx[NGLL2DC];
   
   // load shared memory 
   if (tx < NGLL2DC) {
    sh_hprime_xx[tx] = p_hprime_xxT[tx];
    sh_hprimewgll_xx[tx] = p_hprime_wgll_xx[tx];
   }

   if (threadIdx.x < NGLL3DC ){
    sh_tempx[tx] = p_pot_ac[iglob];
    //sh_tempy[tx] = p_displ[iglob*3 + 1];
    //sh_tempz[tx] = p_displ[iglob*3 + 2];
   } else {
    sh_tempx[tx] = 0.;
    //sh_tempy[tx] = 0.;
    //sh_tempz[tx] = 0.;
   }

   const TC inv_rhol = p_inv_rho[offset];

   const TC xixl = p_dxi_dx[offset];
   const TC xiyl = p_dxi_dy[offset];
   const TC xizl = p_dxi_dz[offset];
 
   const TC etaxl = p_deta_dx[offset];
   const TC etayl = p_deta_dy[offset];
   const TC etazl = p_deta_dz[offset];
 
   const TC gammaxl = p_dgamma_dx[offset];
   const TC gammayl = p_dgamma_dy[offset];
   const TC gammazl = p_dgamma_dz[offset];
 
   const TC jacobianl = inv_rhol / (xixl*(etayl*gammazl-etazl*gammayl)
          -xiyl*(etaxl*gammazl-etazl*gammaxl)
          +xizl*(etaxl*gammayl-etayl*gammaxl));
   
   // local index in element   
   const int K = (th_dummy/NGLL2DC);
   const int J = ((th_dummy-K*NGLL2DC)/NGLLC);
   const int I = (th_dummy-K*NGLL2DC-J*NGLLC);
  
   TC tempx1l=0.f;
   //TC tempy1l=0.f;
   //TC tempz1l=0.f;
 
   TC tempx2l=0.f;
   //TC tempy2l=0.f;
   //TC tempz2l=0.f;
 
   TC tempx3l=0.f;
   //TC tempy3l=0.f;
   //TC tempz3l=0.f;
   
   __syncthreads();
   
   #pragma unroll
   for (int l=0;l<NGLLC;l++) {
 
     TC fac1 = sh_hprime_xx[l + I*NGLLC];
     TC fac2 = sh_hprime_xx[l + J*NGLLC];
     TC fac3 = sh_hprime_xx[l + K*NGLLC];
 
     tempx1l += sh_tempx[K*NGLL2DC+J*NGLLC+l] * fac1;
     //tempy1l += sh_tempy[K*NGLL2DC+J*NGLLC+l] * fac1;
     //tempz1l += sh_tempz[K*NGLL2DC+J*NGLLC+l] * fac1;
     
     tempx2l += sh_tempx[K*NGLL2DC+l*NGLLC+I] * fac2;
     //tempy2l += sh_tempy[K*NGLL2DC+l*NGLLC+I] * fac2;
     //tempz2l += sh_tempz[K*NGLL2DC+l*NGLLC+I] * fac2;
     
     tempx3l += sh_tempx[l*NGLL2DC+J*NGLLC+I] * fac3;
     //tempy3l += sh_tempy[l*NGLL2DC+J*NGLLC+I] * fac3;
     //tempz3l += sh_tempz[l*NGLL2DC+J*NGLLC+I] * fac3;
   }
  
   // compute derivatives of ux, uy and uz with respect to x, y and z
  TC dpdxl = xixl*tempx1l + etaxl*tempx2l + gammaxl*tempx3l;
  TC dpdyl = xiyl*tempx1l + etayl*tempx2l + gammayl*tempx3l;
  TC dpdzl = xizl*tempx1l + etazl*tempx2l + gammazl*tempx3l;

  // TC duydxl = xixl*tempy1l + etaxl*tempy2l + gammaxl*tempy3l;
  // TC duydyl = xiyl*tempy1l + etayl*tempy2l + gammayl*tempy3l;
  // TC duydzl = xizl*tempy1l + etazl*tempy2l + gammazl*tempy3l;

  // TC duzdxl = xixl*tempz1l + etaxl*tempz2l + gammaxl*tempz3l;
  // TC duzdyl = xiyl*tempz1l + etayl*tempz2l + gammayl*tempz3l;
  // TC duzdzl = xizl*tempz1l + etazl*tempz2l + gammazl*tempz3l;

  // // precompute to save operations
  // TC duxdxl_plus_duydyl = duxdxl + duydyl;
  // TC duxdxl_plus_duzdzl = duxdxl + duzdzl;
  // TC duydyl_plus_duzdzl = duydyl + duzdzl;
  // TC duxdyl_plus_duydxl = duxdyl + duydxl;
  // TC duzdxl_plus_duxdzl = duzdxl + duxdzl;
  // TC duzdyl_plus_duydzl = duzdyl + duydzl;
 
  // compute elastic tensor coefficient (unrelaxed) -------------------------------------
  // TC lambdalplus2mul = kappal + 1.33333333333333333333 * mul;  // 4./3. = 1.3333333
  // TC lambdal = lambdalplus2mul - 2.0 * mul;
  // -------------------------------------------------------------------------------------

  // compute stress -----------------------------------------------------------------------
  // TC sigma_xx = lambdalplus2mul*duxdxl + lambdal*duydyl_plus_duzdzl;
  // TC sigma_yy = lambdalplus2mul*duydyl + lambdal*duxdxl_plus_duzdzl;
  // TC sigma_zz = lambdalplus2mul*duzdzl + lambdal*duxdxl_plus_duydyl;
  // TC sigma_xy = mul*duxdyl_plus_duydxl;
  // TC sigma_xz = mul*duzdxl_plus_duxdzl;
  // TC sigma_yz = mul*duzdyl_plus_duydzl;

  // 1. cut-plane xi ----------------------------------------------
  __syncthreads();
  // fills shared memory arrays
  if (threadIdx.x < NGLL3DC) {
    sh_tempx[tx] = jacobianl * (dpdxl*xixl + dpdyl*xiyl + dpdzl*xizl); 
    // sh_tempy[tx] = jacobianl * (sigma_xy*xixl + sigma_yy*xiyl + sigma_yz*xizl); 
    // sh_tempz[tx] = jacobianl * (sigma_xz*xixl + sigma_yz*xiyl + sigma_zz*xizl); 
    }
  __syncthreads();
  //
  tempx1l=0.f;
  // tempy1l=0.f;
  // tempz1l=0.f;
 
  #pragma unroll
  for (int l=0;l<NGLLC;l++) {
    TC fac = sh_hprimewgll_xx[I*NGLLC+l];
    tempx1l += sh_tempx[K*NGLL2DC+J*NGLLC+l] * fac;
    // tempy1l += sh_tempy[K*NGLL2DC+J*NGLLC+l] * fac;
    // tempz1l += sh_tempz[K*NGLL2DC+J*NGLLC+l] * fac;
  }
  
  // 2. cut-plane eta ----------------------------------------------
  __syncthreads();
  // fills shared memory arrays
  if (threadIdx.x < NGLL3DC) {
    sh_tempx[tx] = jacobianl * (dpdxl*etaxl + dpdyl*etayl + dpdzl*etazl); 
    // sh_tempy[tx] = jacobianl * (sigma_xy*etaxl + sigma_yy*etayl + sigma_yz*etazl); 
    // sh_tempz[tx] = jacobianl * (sigma_xz*etaxl + sigma_yz*etayl + sigma_zz*etazl); 
    }
  __syncthreads();

  tempx2l=0.f;
  // tempy2l=0.f;
  // tempz2l=0.f;
 
  #pragma unroll
  for (int l=0;l<NGLLC;l++) {
    TC fac = sh_hprimewgll_xx[J*NGLLC+l];
    tempx2l += sh_tempx[K*NGLL2DC+l*NGLLC+I] * fac;
    // tempy2l += sh_tempy[K*NGLL2DC+l*NGLLC+I] * fac;
    // tempz2l += sh_tempz[K*NGLL2DC+l*NGLLC+I] * fac;
  }
  
  // 3. cut-plane gamma ----------------------------------------------
  __syncthreads();
  // fills shared memory arrays
  if (threadIdx.x < NGLL3DC) {
    sh_tempx[tx] = jacobianl * (dpdxl*gammaxl + dpdyl*gammayl + dpdzl*gammazl); 
    // sh_tempy[tx] = jacobianl * (sigma_xy*gammaxl + sigma_yy*gammayl + sigma_yz*gammazl); 
    // sh_tempz[tx] = jacobianl * (sigma_xz*gammaxl + sigma_yz*gammayl + sigma_zz*gammazl);
    }
  __syncthreads();

  tempx3l=0.f;
  // tempy3l=0.f;
  // tempz3l=0.f;
  

  #pragma unroll
  for (int l=0;l<NGLLC;l++) {
    TC fac = sh_hprimewgll_xx[K*NGLLC+l];
    tempx3l += sh_tempx[l*NGLL2DC+J*NGLLC+I] * fac;
    // tempy3l += sh_tempy[l*NGLL2DC+J*NGLLC+I] * fac;
    // tempz3l += sh_tempz[l*NGLL2DC+J*NGLLC+I] * fac;
  }

  // assembles acceleration array
  if (threadIdx.x < NGLL3DC) {

    // load double weights
    TC fac1 = p_wgllwgll_xy[K*NGLLC+J];
    TC fac2 = p_wgllwgll_xy[K*NGLLC+I];
    TC fac3 = p_wgllwgll_xy[J*NGLLC+I];
  
    TC sum_terms1 = - (fac1*tempx1l + fac2*tempx2l + fac3*tempx3l);
    // TC sum_terms2 = - (fac1*tempy1l + fac2*tempy2l + fac3*tempy3l);
    // TC sum_terms3 = - (fac1*tempz1l + fac2*tempz2l + fac3*tempz3l);

    atomicAdd(&p_pot_ac_dot_dot[iglob],   sum_terms1);
    // atomicAdd(&p_accel[iglob*3+1], sum_terms2);
    // atomicAdd(&p_accel[iglob*3+2], sum_terms3);
  }
}

//------------------------------------------------------------------------
  void compute_internal_forces_acoustic_iso_3D_CUDA(
    TC* __restrict__ p_pot_ac_dot_dot,
    const TC* __restrict__ p_pot_ac,
    const TC* __restrict__ p_dxi_dx,
    const TC* __restrict__ p_dxi_dy,
    const TC* __restrict__ p_dxi_dz,
    const TC* __restrict__ p_deta_dx,
    const TC* __restrict__ p_deta_dy,
    const TC* __restrict__ p_deta_dz,
    const TC* __restrict__ p_dgamma_dx,
    const TC* __restrict__ p_dgamma_dy,
    const TC* __restrict__ p_dgamma_dz,
    const TC* __restrict__ p_inv_rho,
    const TC* __restrict__ p_wgllwgll_xy,
    const TC* __restrict__ p_hprime_wgll_xx,
    const TC* __restrict__ p_hprime_xxT,
    const int* __restrict__ p_ibool,
    const int* __restrict__ p_phase_ispec_inner,
    const int NSPEC, 
    const int NSPEC_INNER, 
    const int NSPEC_OUTER, 
    const int NB_PHASE_ISPEC,
    const int IPHASE,
    cudaStream_t compute_stream) {
    
    int nb_blocks_to_compute;
    if (IPHASE==1) {
      nb_blocks_to_compute = NSPEC_OUTER;
    } else  {
      nb_blocks_to_compute = NSPEC_INNER;
    }
    int blocksize = NGLL3DC_PADDED; 
    int num_blocks_x, num_blocks_y;
    get_blocks_xy(nb_blocks_to_compute, &num_blocks_x, &num_blocks_y);
    dim3 grid(num_blocks_x,num_blocks_y);
    dim3 threads(blocksize,1,1);
    
    //printf(" compute rhs call cuda : nb_compute=%d iphase=%d \n", nb_blocks_to_compute, IPHASE);
    if (nb_blocks_to_compute > 0) {
      kernel_acoustic_iso<<<grid,threads,0,compute_stream>>>(p_pot_ac_dot_dot, p_pot_ac,
        p_dxi_dx, p_dxi_dy, p_dxi_dz,
        p_deta_dx, p_deta_dy, p_deta_dz,
        p_dgamma_dx, p_dgamma_dy, p_dgamma_dz, 
        p_inv_rho, 
        p_wgllwgll_xy, p_hprime_wgll_xx, p_hprime_xxT, 
        p_ibool, p_phase_ispec_inner, 
        nb_blocks_to_compute, NB_PHASE_ISPEC, IPHASE);
      }
    }

//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void kernel_add_sources_acoustic(
  TC* __restrict__ p_pot_ac_dot_dot,
  const int* __restrict__ p_ibool,
  const TC* __restrict__ p_Fx,
  const TC* __restrict__ p_Fy,
  const TC* __restrict__ p_Fz,
  const TC* __restrict__ p_hxis,
  const TC* __restrict__ p_heats,
  const TC* __restrict__ p_hgammas,
  const TC* __restrict__  p_stf,
  const int* __restrict__ p_ispec_src,
  const int it,
  const int NSTEP)
  {
    // 3D grid for thread in 3D element
    int i = threadIdx.x;
    int j = threadIdx.y;
    int k = threadIdx.z;

    // source is block id
    int isource = blockIdx.x + gridDim.x*blockIdx.y; 
    // element index 
    int ispec = p_ispec_src[isource]-1;
    if (ispec >= 0) {
        // dof index
        int iglob=p_ibool[NGLL3DC*ispec+NGLL2DC*k + NGLLC*j + i]-1;

        const TC stfA = p_hxis[i+isource*NGLLC] * 
                        p_heats[j+isource*NGLLC] *
                        p_hgammas[k+isource*NGLLC] * 
                        p_stf[it+NSTEP*isource];

        // const TC stf0 = stfA * p_Fx[isource];
        // const TC stf1 = stfA * p_Fy[isource];
        //const TC stf2 = stfA * p_Fz[isource];
    
        atomicAdd(&p_pot_ac_dot_dot[iglob], stfA);
        // atomicAdd(&p_accel[iglob*3+1],stf1);
        // atomicAdd(&p_accel[iglob*3+2],stf2);
    }
}

//------------------------------------------------------------------------     
void add_source_acoustic_CUDA(
  TC* __restrict__ p_pot_ac_dot_dot,
  const TC* __restrict__ p_Fx,
  const TC* __restrict__ p_Fy,
  const TC* __restrict__ p_Fz,
  const TC* __restrict__ p_hxis,
  const TC* __restrict__ p_hetas,
  const TC* __restrict__ p_hgammas,
  const TC* __restrict__ p_stf,
  const int* __restrict__ p_ibool,
  const int* p_ispec_src,
  const int NSOURCES,
  const int NSTEP, 
  const int it,
  cudaStream_t compute_stream){

  int num_blocks_x, num_blocks_y;
  get_blocks_xy(NSOURCES,&num_blocks_x,&num_blocks_y);

  dim3 grid(num_blocks_x,num_blocks_y);
  dim3 threads(NGLLC,NGLLC,NGLLC);
  if (NSOURCES>0) {
    kernel_add_sources_acoustic<<<grid,threads,0,compute_stream>>>(p_pot_ac_dot_dot, p_ibool, p_Fx, p_Fy, p_Fz, 
      p_hxis, p_hetas, p_hgammas, p_stf, p_ispec_src, it, NSTEP);
  }
}

//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void  cuda_kernel_predictor(TC* __restrict__ p_pot_ac, 
  TC* __restrict__ p_pot_ac_dot, TC* __restrict__ p_pot_ac_dot_dot,
  const TC dt, const TC dt_2, const TC dts2, const int n) {
    int id = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
    if (id < n){
      p_pot_ac[id] += dt * p_pot_ac_dot[id] + dts2*p_pot_ac_dot_dot[id];
      p_pot_ac_dot[id] += dt_2 * p_pot_ac_dot_dot[id];
      p_pot_ac_dot_dot[id] = 0.;
    }
  }


//---------------------------------------------------------------------------------
void predictor_CUDA(TC* __restrict__ p_pot_ac, TC* __restrict__ p_pot_ac_dot, TC* __restrict__ p_pot_ac_dot_dot,
const TC dt, const TC dt_2, const TC dts2, const int n, cudaStream_t compute_stream){

int size = n;

int blocksize = BLOCKSIZE_KERNEL;
int size_padded = ((int)ceil(((double)size)/((double)blocksize)))*blocksize;

int num_blocks_x, num_blocks_y;
get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);

dim3 grid(num_blocks_x,num_blocks_y);
dim3 threads(blocksize,1,1);
cuda_kernel_predictor<<<grid,threads,0,compute_stream>>>(p_pot_ac, p_pot_ac_dot, p_pot_ac_dot_dot, dt, dt_2, dts2, n);
}

//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void  cuda_kernel_invmass(TC* __restrict__ p_pot_ac_dot_dot, const TC* __restrict__ p_invmass, const int n) {
  int id = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
  if (id < n){
    p_pot_ac_dot_dot[id] *= p_invmass[id];
  }
}

//---------------------------------------------------------------------------------
void apply_invmass_CUDA(TC* __restrict__ p_pot_ac_dot_dot, const TC* __restrict__ p_invmass, const int n,
  cudaStream_t compute_stream) {

 int size = n;

 int blocksize = BLOCKSIZE_KERNEL;
 int size_padded = ((int)ceil(((double)size)/((double)blocksize)))*blocksize;

 int num_blocks_x, num_blocks_y;
 get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);

 dim3 grid(num_blocks_x,num_blocks_y);
 dim3 threads(blocksize,1,1);
 cuda_kernel_invmass<<<grid,threads,0,compute_stream>>>(p_pot_ac_dot_dot, p_invmass, n);
}

//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void cuda_kernel_corrector(TC* __restrict__ p_pot_ac_dot, const TC* __restrict__ p_pot_ac_dot_dot, const TC dt_2, const int n) {

  int id = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
  if (id < n){
    p_pot_ac_dot[id] += dt_2 * p_pot_ac_dot_dot[id];
  }
}

//---------------------------------------------------------------------------------
void corrector_CUDA( TC* __restrict__ p_pot_ac_dot, const TC* __restrict__ p_pot_ac_dot_dot, const TC dt_2, const int n, 
  cudaStream_t compute_stream){

 int size = n;

 int blocksize = BLOCKSIZE_KERNEL;
 int size_padded = ((int)ceil(((double)size)/((double)blocksize)))*blocksize;

 int num_blocks_x, num_blocks_y;
 get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);

 dim3 grid(num_blocks_x,num_blocks_y);
 dim3 threads(blocksize,1,1);
 cuda_kernel_corrector<<<grid,threads,0,compute_stream>>>(p_pot_ac_dot, p_pot_ac_dot_dot, dt_2, n);
}

//#################################################################################################################
__global__ void cuda_kernel_stacey_acosutic_boundary_condition3D(
  TC* __restrict__ p_pot_ac_dot_dot,
  const TC* __restrict__ p_pot_ac_dot,
  const TC* __restrict__ p_rho_vp,
  const TC* __restrict__ p_wstacey,
  const int* __restrict__  p_index_gll,
  const int n) {

    int ib = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
   
    if (ib < n) {
      int iglob = p_index_gll[ib] - 1;
      TC weight =  p_pot_ac_dot[iglob] * p_wstacey[ib] / p_rho_vp[ib];
      atomicAdd(&p_pot_ac_dot_dot[iglob],-weight);
    }
}


//---------------------------------------------------------------------------------
void stacey_acoustic_boundary_condition3D_CUDA(
   TC* __restrict__ p_pot_ac_dot_dot,
   const TC* __restrict__ p_pot_ac_dot,
   const TC* __restrict__ p_rho_vp,
   const TC* __restrict__ p_wstacey,
   const int* __restrict__  p_index_gll,
   const int n,
   cudaStream_t compute_stream) {
    
    // block of BLOCKSIZE_KERNEL thread 
    int size = n;
    
    int blocksize = BLOCKSIZE_KERNEL;
    int size_padded = ((int)ceil(((double)size)/((double)blocksize)))*blocksize;

    int num_blocks_x, num_blocks_y;
    get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);

    dim3 grid(num_blocks_x,num_blocks_y);
    dim3 threads(blocksize,1,1);

    cuda_kernel_stacey_acosutic_boundary_condition3D<<<grid,threads,0,compute_stream>>>(
      p_pot_ac_dot_dot, p_pot_ac_dot, p_rho_vp,
      p_wstacey, p_index_gll, n);    
  }

//#################################################################################################################
__global__ void cuda_kernel_store_seismogram(
  TC* __restrict__ p_seismogram_p,
  const TC* __restrict__ p_pot_ac,
  const TC* __restrict__ p_hxir,
  const TC* __restrict__ p_hetar,
  const TC* __restrict__ p_hgammar,
  const int* __restrict__ p_ispec_rec,
  const int* __restrict__ p_ibool, 
  const int it, const int nrec_local){
    
  int irec_local = blockIdx.x + blockIdx.y*gridDim.x;
  if (irec_local >= nrec_local) {return;}

  int tx = threadIdx.x;
    
  // local index
  int K = (tx/NGLL2DC);
  int J = ((tx-K*NGLL2DC)/NGLLC);
  int I = (tx-K*NGLL2DC-J*NGLLC);

  __shared__ TC sh_dxd[NGLL3DC_PADDED];
  
  sh_dxd[tx] = 0.;

  int ispec = p_ispec_rec[irec_local]-1;
  if (ispec >=0) {
    int iglob = p_ibool[NGLL3DC*ispec+NGLL2DC*K + NGLLC*J + I]-1;

    if (tx < NGLL3DC) {
      TC hlagrange = p_hxir[irec_local + nrec_local*I]*p_hetar[irec_local + nrec_local*J]*p_hgammar[irec_local + nrec_local*K];
      sh_dxd[tx] = hlagrange * p_pot_ac[iglob];
    }

    __syncthreads();
    for (unsigned int s=1; s<NGLL3DC_PADDED ; s *= 2) {
      if (tx % (2*s) == 0){ sh_dxd[tx] += sh_dxd[tx + s];}
      __syncthreads();
    }
    if (tx == 0) {p_seismogram_p[irec_local+nrec_local*it] = sh_dxd[0];}
  }
}
//---------------------------------------------------------------------------------
void store_seismogram_CUDA(
  TC* __restrict__ p_seismogram_p,
  const TC* __restrict__ p_pot_ac,
  const TC* __restrict__ p_hxir,
  const TC* __restrict__ p_hetar,
  const TC* __restrict__ p_hgammar,
  const int* __restrict__ p_ispec_rec,
  const int* __restrict__ p_ibool,
  const int it,
  const int NREC,
  cudaStream_t compute_stream){
    if (NREC <= 0) {return;}
    int num_blocks_x, num_blocks_y;
    get_blocks_xy(NREC,&num_blocks_x,&num_blocks_y);
    dim3 grid(num_blocks_x,num_blocks_y);
    dim3 threads(NGLL3DC_PADDED,1,1);
    cuda_kernel_store_seismogram<<<grid,threads,0,compute_stream>>>(p_seismogram_p, p_pot_ac,
      p_hxir, p_hetar, p_hgammar, p_ispec_rec, p_ibool, it, NREC);
}


//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void cuda_kernel_prepare_MPI_buffer(
  TC* __restrict__ d_mpi_send_buf,
  const TC* __restrict__ pt_pot_ac_dot_dot,
  const int* __restrict__ p_nibool_interfaces_ext_mesh,
  const int* __restrict__ p_ibool_interfaces_ext_mesh, 
  const int offset_ibool_interf, 
  const int nb_mpi_interfaces,
  const int max_nibool_interfaces) {
    int id = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
    int ientry,ientry1,iglob;
    for( int iinterface=0; iinterface < nb_mpi_interfaces; iinterface++) {
      if (id < p_nibool_interfaces_ext_mesh[iinterface]) {
       // entry in interface array
       ientry = id + offset_ibool_interf*iinterface;
       ientry1 = id + max_nibool_interfaces*iinterface;
       // global index in wavefield
       iglob = p_ibool_interfaces_ext_mesh[ientry] - 1;
       d_mpi_send_buf[ientry1] = pt_pot_ac_dot_dot[iglob];
      //  d_mpi_send_buf[3*ientry1 + 1 ] = pt_accel[3*iglob + 1];
      //  d_mpi_send_buf[3*ientry1 + 2 ] = pt_accel[3*iglob + 2];
      }
    }
}
//---------------------------------------------------------------------------------
void prepare_MPI_buffer_CUDA(TC* __restrict__ d_mpi_send_buf,
  TC* __restrict__ h_mpi_send_buf,
  const TC* __restrict__ p_pot_ac_dot_dot,
  const int* __restrict__ p_nibool_interfaces_ext_mesh,
  const int* __restrict__ p_ibool_interfaces_ext_mesh, 
  const int offset_ibool_interf,
  const int nb_mpi_interfaces,
  const int max_nibool_interfaces, 
  const int size_mpi_buffer, 
  cudaStream_t compute_stream,
  cudaStream_t copy_stream) {
  if (size_mpi_buffer <= 0) {return;}
  int blocksize = BLOCKSIZE_TRANSFER;
  int size_padded = ((int)ceil(((double)max_nibool_interfaces)/((double)blocksize)))*blocksize;
  int num_blocks_x, num_blocks_y;
  get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);
  dim3 grid(num_blocks_x,num_blocks_y);
  dim3 threads(blocksize,1,1);
  cuda_kernel_prepare_MPI_buffer<<<grid,threads,0,compute_stream>>>(
    d_mpi_send_buf,p_pot_ac_dot_dot,
    p_nibool_interfaces_ext_mesh,
    p_ibool_interfaces_ext_mesh,
    offset_ibool_interf,
    nb_mpi_interfaces,
    max_nibool_interfaces);
  cudaStreamSynchronize(compute_stream);
  cudaMemcpyAsync(h_mpi_send_buf, d_mpi_send_buf,
                  size_mpi_buffer*sizeof(TC),
                  cudaMemcpyDeviceToHost,
                  copy_stream);
  }

//#################################################################################################################
//---------------------------------------------------------------------------------
__global__ void cuda_kernel_assemble_MPI_vector(TC* __restrict__ p_pot_ac_dot_dot,
    const TC* __restrict__  d_mpi_recv_buf,
    const int* __restrict__ p_nibool_interfaces_ext_mesh, 
    const int* __restrict__ p_ibool_interfaces_ext_mesh,
    const int offset_ibool_interf,
    const int  nb_mpi_interfaces, 
    const int  max_nibool_interfaces){
      int id = threadIdx.x + blockIdx.x*blockDim.x + blockIdx.y*gridDim.x*blockDim.x;
      int ientry,ientry1, iglob;
      for( int iinterface=0; iinterface < nb_mpi_interfaces; iinterface++) {
        if (id < p_nibool_interfaces_ext_mesh[iinterface]) {
         // entry in interface array
         ientry = id + offset_ibool_interf*iinterface;
         ientry1 = id + max_nibool_interfaces*iinterface;
         // global index in wavefield
         iglob = p_ibool_interfaces_ext_mesh[ientry] - 1;
         atomicAdd(&p_pot_ac_dot_dot[iglob],d_mpi_recv_buf[ientry1]);
        }
      }
    }
//---------------------------------------------------------------------------------
  void assemble_MPI_vector_CUDA(TC* __restrict__ p_pot_ac_dot_dot, 
    const TC* __restrict__  d_mpi_recv_buf, 
    const int* __restrict__ p_nibool_interfaces_ext_mesh, 
    const int* __restrict__ p_ibool_interfaces_ext_mesh,
    const int offset_ibool_interf,
    const int  nb_mpi_interfaces, 
    const int  max_nibool_interfaces,
    const int  size_mpi_buffer,
    cudaStream_t compute_stream, 
    cudaStream_t copy_stream){
      if (size_mpi_buffer <= 0) {return;}
      // Wait until previous copy stream finishes. We assemble while other compute kernels execute.
      cudaStreamSynchronize(copy_stream);
      int blocksize = BLOCKSIZE_TRANSFER;
      int size_padded = ((int)ceil(((double)max_nibool_interfaces)/((double)blocksize)))*blocksize;
      int num_blocks_x, num_blocks_y;
      get_blocks_xy(size_padded/blocksize,&num_blocks_x,&num_blocks_y);
      dim3 grid(num_blocks_x,num_blocks_y);
      dim3 threads(blocksize,1,1);
      cuda_kernel_assemble_MPI_vector<<<grid,threads,0,compute_stream>>>(
        p_pot_ac_dot_dot, d_mpi_recv_buf, p_nibool_interfaces_ext_mesh, p_ibool_interfaces_ext_mesh,
        offset_ibool_interf, nb_mpi_interfaces, max_nibool_interfaces);
    }
      
#endif