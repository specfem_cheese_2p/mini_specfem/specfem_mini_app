module element_mod

  use config_mod
  implicit none 

  
  ! a few useful constants
  !double precision, private, parameter :: ZERO = 0.d0, ONE = 1.d0, TWO = 2.d0, &
  !THREE=3.d0, FOUR=4.d0,  HALF = 0.5d0, ONE_EIGHTH=1.d0/8.d0

  public  :: compute_solver_data, define_element_Hex8, define_weight_and_derivation_matrices, lagrange_any
  private :: get_shape3D, endw1, endw2, gammaf, jacg, jacobf, PNDLEG, PNLEG, pnormj, zwgjd, zwgljd, pnglj, pndglj, &
       lagrange_deriv_GLL

contains



  !! ##########################################################################################################
  !! store edges for current element
  subroutine define_element_Hex8(xnode, ynode, znode, x0,x1, y0,y1, z0,z1)

    double precision, intent(in) ::  x0,x1, y0,y1, z0,z1
    double precision, dimension(NGNOD), intent(inout)  :: xnode, ynode, znode
    
    xnode(1) = x0
    ynode(1) = y0
    znode(1) = z0
    
    xnode(2) = x1
    ynode(2) = y0
    znode(2) = z0
    
    xnode(3) = x1
    ynode(3) = y1
    znode(3) = z0
    
    xnode(4) = x0
    ynode(4) = y1
    znode(4) = z0
    
    xnode(5) = x0
    ynode(5) = y0
    znode(5) = z1
    
    xnode(6) = x1
    ynode(6) = y0
    znode(6) = z1
    
    xnode(7) = x1
    ynode(7) = y1
    znode(7) = z1
    
    xnode(8) = x0
    ynode(8) = y1
    znode(8) = z1
    
  end subroutine define_element_Hex8

  !!---------------------------------------------------------------------
  subroutine define_gll(xigll, yigll, zigll)
    
    double precision, dimension(NGLLX), intent(inout) :: xigll
    double precision, dimension(NGLLY), intent(inout) :: yigll
    double precision, dimension(NGLLZ) ,intent(inout) :: zigll
    
    double precision, dimension(NGLLX) :: wxgll
    double precision, dimension(NGLLY) :: wygll
    double precision, dimension(NGLLZ) :: wzgll
    
    call zwgljd(xigll, wxgll, NGLLX, GAUSSALPHA, GAUSSBETA)
    call zwgljd(yigll, wygll, NGLLY, GAUSSALPHA, GAUSSBETA)
    call zwgljd(zigll, wzgll, NGLLZ, GAUSSALPHA, GAUSSBETA)
    
  end subroutine define_gll

  !!---------------------------------------------------------------------
  subroutine define_shape(shape3D, dershape3D, xi, eta, gamma)

    double precision, intent(in) :: xi, eta, gamma
    double precision, dimension(8), intent(inout) :: shape3D
    double precision, dimension(3,NGNOD), intent(inout) :: dershape3D
    double precision :: ra1, ra2, rb1, rb2, rc1, rc2
    
    ra1 = one + xi
    ra2 = one - xi

    rb1 = one + eta
    rb2 = one - eta

    rc1 = one + gamma
    rc2 = one - gamma

    shape3D(1) = ONE_EIGHTH*ra2*rb2*rc2
    shape3D(2) = ONE_EIGHTH*ra1*rb2*rc2
    shape3D(3) = ONE_EIGHTH*ra1*rb1*rc2
    shape3D(4) = ONE_EIGHTH*ra2*rb1*rc2
    shape3D(5) = ONE_EIGHTH*ra2*rb2*rc1
    shape3D(6) = ONE_EIGHTH*ra1*rb2*rc1
    shape3D(7) = ONE_EIGHTH*ra1*rb1*rc1
    shape3D(8) = ONE_EIGHTH*ra2*rb1*rc1

    dershape3D(1,1) = - ONE_EIGHTH*rb2*rc2
    dershape3D(1,2) = ONE_EIGHTH*rb2*rc2
    dershape3D(1,3) = ONE_EIGHTH*rb1*rc2
    dershape3D(1,4) = - ONE_EIGHTH*rb1*rc2
    dershape3D(1,5) = - ONE_EIGHTH*rb2*rc1
    dershape3D(1,6) = ONE_EIGHTH*rb2*rc1
    dershape3D(1,7) = ONE_EIGHTH*rb1*rc1
    dershape3D(1,8) = - ONE_EIGHTH*rb1*rc1

    dershape3D(2,1) = - ONE_EIGHTH*ra2*rc2
    dershape3D(2,2) = - ONE_EIGHTH*ra1*rc2
    dershape3D(2,3) = ONE_EIGHTH*ra1*rc2
    dershape3D(2,4) = ONE_EIGHTH*ra2*rc2
    dershape3D(2,5) = - ONE_EIGHTH*ra2*rc1
    dershape3D(2,6) = - ONE_EIGHTH*ra1*rc1
    dershape3D(2,7) = ONE_EIGHTH*ra1*rc1
    dershape3D(2,8) = ONE_EIGHTH*ra2*rc1

    dershape3D(3,1) = - ONE_EIGHTH*ra2*rb2
    dershape3D(3,2) = - ONE_EIGHTH*ra1*rb2
    dershape3D(3,3) = - ONE_EIGHTH*ra1*rb1
    dershape3D(3,4) = - ONE_EIGHTH*ra2*rb1
    dershape3D(3,5) = ONE_EIGHTH*ra2*rb2
    dershape3D(3,6) = ONE_EIGHTH*ra1*rb2
    dershape3D(3,7) = ONE_EIGHTH*ra1*rb1
    dershape3D(3,8) = ONE_EIGHTH*ra2*rb1
    
    
  end subroutine define_shape
  !
  !! ##########################################################################################################   
  !! compute geometric factor for current element
  subroutine compute_solver_data(xelm, yelm, zelm, &
                                 xgll_elem, ygll_elem, zgll_elem,&
                                 xix_elem,  xiy_elem, xiz_elem, &
                                 etax_elem,  etay_elem, etaz_elem, &
                                 gammax_elem,  gammay_elem, gammaz_elem, &
                                 jacobian_elem)

    ! element arrays for solver  
    double precision, dimension(NGNOD), intent(in)  :: xelm, yelm, zelm
    double precision, dimension(NGLLX, NGLLY, NGLLZ),intent(inout)  :: xgll_elem, ygll_elem, zgll_elem, jacobian_elem
    double precision, dimension(NGLLX, NGLLY, NGLLZ),intent(inout)  :: xix_elem,  xiy_elem, xiz_elem
    double precision, dimension(NGLLX, NGLLY, NGLLZ),intent(inout)  :: etax_elem,  etay_elem, etaz_elem
    double precision, dimension(NGLLX, NGLLY, NGLLZ),intent(inout)  :: gammax_elem,  gammay_elem, gammaz_elem

    double precision, dimension(NGLLX) :: xigll, wxgll
    double precision, dimension(NGLLY) :: yigll, wygll
    double precision, dimension(NGLLZ) :: zigll, wzgll
  
    ! 3D shape functions and their derivatives
    double precision, dimension(NGNOD ,NGLLX, NGLLY, NGLLZ)       :: shape3D
    double precision, dimension(NDIM, NGNOD, NGLLX, NGLLY, NGLLZ) :: dershape3D
    
    integer :: i,j,k,ia
    double precision :: xmesh,ymesh,zmesh
    double precision :: xxi, xeta, xgamma, yxi, yeta, ygamma, zxi, zeta, zgamma
    double precision :: xix, xiy, xiz, etax, etay, etaz, gammax, gammay, gammaz
    double precision :: jacobian
   
    call zwgljd(xigll, wxgll, NGLLX, GAUSSALPHA, GAUSSBETA)
    call zwgljd(yigll, wygll, NGLLY, GAUSSALPHA, GAUSSBETA)
    call zwgljd(zigll, wzgll, NGLLZ, GAUSSALPHA, GAUSSBETA)
    call get_shape3D(shape3D, dershape3D, xigll, yigll, zigll)

    do k=1,NGLLZ
       do j=1,NGLLY
          do i=1,NGLLX

             xmesh = ZERO
             ymesh = ZERO
             zmesh = ZERO
             
             xxi = ZERO
             xeta = ZERO
             xgamma = ZERO

             yxi = ZERO
             yeta = ZERO
             ygamma = ZERO

             zxi = ZERO
             zeta = ZERO
             zgamma = ZERO


             do ia=1,NGNOD

                xmesh = xmesh + shape3D(ia,i,j,k)*xelm(ia)
                ymesh = ymesh + shape3D(ia,i,j,k)*yelm(ia)
                zmesh = zmesh + shape3D(ia,i,j,k)*zelm(ia)

                xxi = xxi + dershape3D(1,ia,i,j,k)*xelm(ia)
                xeta = xeta + dershape3D(2,ia,i,j,k)*xelm(ia)
                xgamma = xgamma + dershape3D(3,ia,i,j,k)*xelm(ia)
                
                yxi = yxi + dershape3D(1,ia,i,j,k)*yelm(ia)
                yeta = yeta + dershape3D(2,ia,i,j,k)*yelm(ia)
                ygamma = ygamma + dershape3D(3,ia,i,j,k)*yelm(ia)
                
                zxi = zxi + dershape3D(1,ia,i,j,k)*zelm(ia)
                zeta = zeta + dershape3D(2,ia,i,j,k)*zelm(ia)
                zgamma = zgamma + dershape3D(3,ia,i,j,k)*zelm(ia)

             end do

             
             jacobian = xxi*(yeta*zgamma-ygamma*zeta) - &
                         xeta*(yxi*zgamma-ygamma*zxi) + &
                         xgamma*(yxi*zeta-yeta*zxi)

            
             xix = (yeta*zgamma-ygamma*zeta) / jacobian
             xiy = (xgamma*zeta-xeta*zgamma) / jacobian
             xiz = (xeta*ygamma-xgamma*yeta) / jacobian

             etax = (ygamma*zxi-yxi*zgamma) / jacobian
             etay = (xxi*zgamma-xgamma*zxi) / jacobian
             etaz = (xgamma*yxi-xxi*ygamma) / jacobian

             gammax = (yxi*zeta-yeta*zxi) / jacobian
             gammay = (xeta*zxi-xxi*zeta) / jacobian
             gammaz = (xxi*yeta-xeta*yxi) / jacobian


             xgll_elem(i,j,k) = real(xmesh, kind=CUSTOM_REAL)
             ygll_elem(i,j,k) = real(ymesh, kind=CUSTOM_REAL)
             zgll_elem(i,j,k) = real(zmesh, kind=CUSTOM_REAL)
             
             xix_elem(i,j,k) = real(xix,kind=CUSTOM_REAL)
             xiy_elem(i,j,k) = real(xiy,kind=CUSTOM_REAL)
             xiz_elem(i,j,k) = real(xiz,kind=CUSTOM_REAL)
             
             etax_elem(i,j,k) = real(etax,kind=CUSTOM_REAL)
             etay_elem(i,j,k) = real(etay,kind=CUSTOM_REAL)
             etaz_elem(i,j,k) = real(etaz,kind=CUSTOM_REAL)

             gammax_elem(i,j,k) = real(gammax,kind=CUSTOM_REAL)
             gammay_elem(i,j,k) = real(gammay,kind=CUSTOM_REAL)
             gammaz_elem(i,j,k) = real(gammaz,kind=CUSTOM_REAL)

             jacobian_elem(i,j,k) =1. / (xix*(etay*gammaz-etaz*gammay) &
                      -xiy*(etax*gammaz-etaz*gammax) &
                      +xiz*(etax*gammay-etay*gammax))
            
             
             
          end do
       end do
    end do
             
    
  end subroutine compute_solver_data

  !
  !! ##########################################################################################################   
  subroutine compute_jacobian(xelm, yelm, zelm, xi,eta, gamma,  & 
       x, y, z, xix, xiy, xiz, etax, etay, etaz, gammax, gammay, gammaz)
    double precision, dimension(NGNOD), intent(in)  :: xelm, yelm, zelm
    double precision, intent(in) :: xi,eta, gamma
  
    ! 3D shape functions and their derivatives
    double precision, dimension(NGNOD)       :: shape3D
    double precision, dimension(NDIM, NGNOD) :: dershape3D
    double precision, intent(inout) :: x, y, z, xix,xiy,xiz,etax,etay,etaz,gammax,gammay,gammaz
    double precision :: xxi, xeta, xgamma, yxi, yeta, ygamma, zxi, zeta, zgamma, jacobian
    integer :: ia
    
    call define_shape(shape3D, dershape3D, xi, eta, gamma)
    
    ! compute coordinates and jacobian matrix
    x=ZERO
    y=ZERO
    z=ZERO
    xxi=ZERO
    xeta=ZERO
    xgamma=ZERO
    yxi=ZERO
    yeta=ZERO
    ygamma=ZERO
    zxi=ZERO
    zeta=ZERO
    zgamma=ZERO
    
    do ia=1,NGNOD
       x=x+shape3D(ia)*xelm(ia)
       y=y+shape3D(ia)*yelm(ia)
       z=z+shape3D(ia)*zelm(ia)
       
       xxi=xxi+dershape3D(1,ia)*xelm(ia)
       xeta=xeta+dershape3D(2,ia)*xelm(ia)
       xgamma=xgamma+dershape3D(3,ia)*xelm(ia)
       yxi=yxi+dershape3D(1,ia)*yelm(ia)
       yeta=yeta+dershape3D(2,ia)*yelm(ia)
       ygamma=ygamma+dershape3D(3,ia)*yelm(ia)
       zxi=zxi+dershape3D(1,ia)*zelm(ia)
       zeta=zeta+dershape3D(2,ia)*zelm(ia)
       zgamma=zgamma+dershape3D(3,ia)*zelm(ia)
    enddo
    
    jacobian = xxi*(yeta*zgamma-ygamma*zeta) - xeta*(yxi*zgamma-ygamma*zxi) + xgamma*(yxi*zeta-yeta*zxi)

    if (jacobian <= ZERO) stop '3D Jacobian undefined'

    ! invert the relation (Fletcher p. 50 vol. 2)
    xix=(yeta*zgamma-ygamma*zeta)/jacobian
    xiy=(xgamma*zeta-xeta*zgamma)/jacobian
    xiz=(xeta*ygamma-xgamma*yeta)/jacobian
    etax=(ygamma*zxi-yxi*zgamma)/jacobian
    etay=(xxi*zgamma-xgamma*zxi)/jacobian
    etaz=(xgamma*yxi-xxi*ygamma)/jacobian
    gammax=(yxi*zeta-yeta*zxi)/jacobian
    gammay=(xeta*zxi-xxi*zeta)/jacobian
    gammaz=(xxi*yeta-xeta*yxi)/jacobian

  end subroutine compute_jacobian
  
  !
  !! ##########################################################################################################   
  subroutine compute_jacobian_2D(xelm, yelm, zelm, jacobian2D, normal, NGLLA, NGLLB)

    implicit none
    
    ! generic routine that accepts any polynomial degree in each direction

    integer, intent(in)  :: NGLLA, NGLLB
    double precision, dimension(4), intent(in) ::  xelm, yelm, zelm
    double precision, dimension(3,NGLLA,NGLLB), intent(inout) ::  normal
    double precision, dimension(NGLLA,NGLLB), intent(inout) ::jacobian2D


    
    double precision, dimension(2,4,NGLLA,NGLLB) :: dershape2D
    double precision, dimension(4,NGLLA,NGLLB)   :: shape2D
    
    integer  :: i,j,ia
    double precision  :: xxi,xeta,yxi,yeta,zxi,zeta
    double precision  :: unx,uny,unz,jacobian

    double precision, dimension(NGLLA) :: xigll, wxgll
    double precision, dimension(NGLLB) :: yigll, wygll
    
    call zwgljd(xigll, wxgll, NGLLA, GAUSSALPHA, GAUSSBETA)
    call zwgljd(yigll, wygll, NGLLB, GAUSSALPHA, GAUSSBETA)
    call get_shape2D(shape2D, dershape2D, xigll, yigll, NGLLA, NGLLB)

    do j=1,NGLLB
       do i=1,NGLLA

          xxi=ZERO
          xeta=ZERO
          yxi=ZERO
          yeta=ZERO
          zxi=ZERO
          zeta=ZERO
          do ia=1,4
             xxi = xxi + dershape2D(1,ia,i,j) * xelm(ia)
             xeta = xeta + dershape2D(2,ia,i,j) * xelm(ia)
             yxi = yxi + dershape2D(1,ia,i,j) * yelm(ia)
             yeta = yeta +dershape2D(2,ia,i,j) * yelm(ia)
             zxi = zxi + dershape2D(1,ia,i,j) * zelm(ia)
             zeta =zeta + dershape2D(2,ia,i,j) * zelm(ia)
          enddo
          
          !   calculate the unnormalized normal to the boundary
          unx = yxi*zeta - yeta*zxi
          uny = zxi*xeta - zeta*xxi
          unz = xxi*yeta - xeta*yxi
          jacobian = dsqrt(unx**2+uny**2+unz**2)
          
          !   normalize normal vector and store surface jacobian
          jacobian2D(i,j) = jacobian
          normal(1,i,j) = unx / jacobian
          normal(2,i,j) = uny / jacobian
          normal(3,i,j) = unz / jacobian
        
          
       enddo
    enddo

  end subroutine compute_jacobian_2D
  
  !! ##########################################################################################################   
  ! 3D shape functions for 8-node

  subroutine get_shape3D(shape3D,dershape3D,xigll,yigll,zigll)

    implicit none
    
  ! Gauss-Lobatto-Legendre points of integration
    double precision xigll(NGLLX)
    double precision yigll(NGLLY)
    double precision zigll(NGLLZ)

    ! 3D shape functions and their derivatives
    double precision shape3D(8,NGLLX,NGLLY,NGLLZ)
    double precision dershape3D(3,NGNOD,NGLLX,NGLLY,NGLLZ)

    integer i,j,k

    ! location of the nodes of the 3D hexahedra elements
    double precision xi,eta,gamma
    double precision ra1,ra2,rb1,rb2,rc1,rc2

    ! ***
    ! *** create 3D shape functions and jacobian
    ! ***
    
    do i=1,NGLLX
       do j=1,NGLLY
          do k=1,NGLLZ
             
             xi = xigll(i)
             eta = yigll(j)
             gamma = zigll(k)
             
             !--- case of a 3D 8-node element (Dhatt-Touzot p. 115)
             
             
             ra1 = one + xi
             ra2 = one - xi
             
             rb1 = one + eta
             rb2 = one - eta
             
             rc1 = one + gamma
             rc2 = one - gamma
             
             shape3D(1,i,j,k) = ONE_EIGHTH*ra2*rb2*rc2
             shape3D(2,i,j,k) = ONE_EIGHTH*ra1*rb2*rc2
             shape3D(3,i,j,k) = ONE_EIGHTH*ra1*rb1*rc2
             shape3D(4,i,j,k) = ONE_EIGHTH*ra2*rb1*rc2
             shape3D(5,i,j,k) = ONE_EIGHTH*ra2*rb2*rc1
             shape3D(6,i,j,k) = ONE_EIGHTH*ra1*rb2*rc1
             shape3D(7,i,j,k) = ONE_EIGHTH*ra1*rb1*rc1
             shape3D(8,i,j,k) = ONE_EIGHTH*ra2*rb1*rc1
             
             dershape3D(1,1,i,j,k) = - ONE_EIGHTH*rb2*rc2
             dershape3D(1,2,i,j,k) = ONE_EIGHTH*rb2*rc2
             dershape3D(1,3,i,j,k) = ONE_EIGHTH*rb1*rc2
             dershape3D(1,4,i,j,k) = - ONE_EIGHTH*rb1*rc2
             dershape3D(1,5,i,j,k) = - ONE_EIGHTH*rb2*rc1
             dershape3D(1,6,i,j,k) = ONE_EIGHTH*rb2*rc1
             dershape3D(1,7,i,j,k) = ONE_EIGHTH*rb1*rc1
             dershape3D(1,8,i,j,k) = - ONE_EIGHTH*rb1*rc1
             
             dershape3D(2,1,i,j,k) = - ONE_EIGHTH*ra2*rc2
             dershape3D(2,2,i,j,k) = - ONE_EIGHTH*ra1*rc2
             dershape3D(2,3,i,j,k) = ONE_EIGHTH*ra1*rc2
             dershape3D(2,4,i,j,k) = ONE_EIGHTH*ra2*rc2
             dershape3D(2,5,i,j,k) = - ONE_EIGHTH*ra2*rc1
             dershape3D(2,6,i,j,k) = - ONE_EIGHTH*ra1*rc1
             dershape3D(2,7,i,j,k) = ONE_EIGHTH*ra1*rc1
             dershape3D(2,8,i,j,k) = ONE_EIGHTH*ra2*rc1
             
             dershape3D(3,1,i,j,k) = - ONE_EIGHTH*ra2*rb2
             dershape3D(3,2,i,j,k) = - ONE_EIGHTH*ra1*rb2
             dershape3D(3,3,i,j,k) = - ONE_EIGHTH*ra1*rb1
             dershape3D(3,4,i,j,k) = - ONE_EIGHTH*ra2*rb1
             dershape3D(3,5,i,j,k) = ONE_EIGHTH*ra2*rb2
             dershape3D(3,6,i,j,k) = ONE_EIGHTH*ra1*rb2
             dershape3D(3,7,i,j,k) = ONE_EIGHTH*ra1*rb1
             dershape3D(3,8,i,j,k) = ONE_EIGHTH*ra2*rb1
             
             
             
          enddo
       enddo
    enddo
    
  end subroutine get_shape3D

  !!---------------------------------------------------------------------------
  
  subroutine get_shape2D(shape2D, dershape2D, xigll, yigll, NGLLA, NGLLB)

    implicit none
    integer, intent(in) ::  NGLLA,NGLLB
    double precision, dimension(NGLLA), intent(in) :: xigll
    double precision, dimension(NGLLB), intent(in) :: yigll
    
    ! 2D shape functions and their derivatives
    double precision, dimension(4,NGLLA,NGLLB),   intent(inout) :: shape2D
    double precision, dimension(2,4,NGLLA,NGLLB), intent(inout) :: dershape2D
    
    integer  :: i,j
    
    ! location of the nodes of the 2D quadrilateral elements
    double precision  :: xi,eta
    double precision  :: xi_map,eta_map
    
    ! generate the 2D shape functions and their derivatives (4 nodes)
    do i=1,NGLLA
       xi=xigll(i)
       do j=1,NGLLB
          eta=yigll(j)
          
          ! map coordinates to [0,1]
          xi_map = (xi + 1.) / 2.
          eta_map = (eta + 1.) / 2.
          
          ! corner nodes
          shape2D(1,i,j) = (1 - xi_map)*(1 - eta_map)
          shape2D(2,i,j) = xi_map*(1 - eta_map)
          shape2D(3,i,j) = xi_map*eta_map
          shape2D(4,i,j) = (1 - xi_map)*eta_map
          
          dershape2D(1,1,i,j) = (eta - 1.) / 4.
          dershape2D(2,1,i,j) = (xi - 1.) / 4.
          
          dershape2D(1,2,i,j) = (1. - eta) / 4.
          dershape2D(2,2,i,j) = (-1. - xi) / 4.
          
          dershape2D(1,3,i,j) = (1. + eta) / 4.
          dershape2D(2,3,i,j) = (1. + xi) / 4.
          
          dershape2D(1,4,i,j) = (- 1. - eta) / 4.
          dershape2D(2,4,i,j) = (1. - xi) / 4.
          
       enddo
    enddo
  end subroutine get_shape2D
  
  !!---------------------------------------------------------------------------
  subroutine define_weight_and_derivation_matrices(hprime_xxT, hprime_yyT, hprime_zzT, hprimewgll_xx,hprimewgll_yy,hprimewgll_zz, &
       wgllwgll_yz,  wgllwgll_xz, wgllwgll_xy, wxgll1, wygll1, wzgll1)

    real(kind=CUSTOM_REAL), dimension(NGLLX),        intent(inout) :: wxgll1
    real(kind=CUSTOM_REAL), dimension(NGLLY),        intent(inout) :: wygll1
    real(kind=CUSTOM_REAL), dimension(NGLLZ),        intent(inout) :: wzgll1
    real(kind=CUSTOM_REAL), dimension(NGLLX, NGLLX), intent(inout) :: hprimewgll_xx, hprime_xxT
    real(kind=CUSTOM_REAL), dimension(NGLLY, NGLLY), intent(inout) :: hprimewgll_yy, hprime_yyT
    real(kind=CUSTOM_REAL), dimension(NGLLZ, NGLLZ), intent(inout) :: hprimewgll_zz, hprime_zzT
    real(kind=CUSTOM_REAL), dimension(NGLLY, NGLLZ), intent(inout) :: wgllwgll_yz
    real(kind=CUSTOM_REAL), dimension(NGLLX, NGLLZ), intent(inout) :: wgllwgll_xz
    real(kind=CUSTOM_REAL), dimension(NGLLX, NGLLY), intent(inout) :: wgllwgll_xy
    
    integer  :: i,j,k,i1,i2,j1,j2,k1,k2
    double precision, dimension(NGLLX) :: xigll, wxgll
    double precision, dimension(NGLLY) :: yigll, wygll
    double precision, dimension(NGLLZ) :: zigll, wzgll


  call zwgljd(xigll, wxgll, NGLLX, GAUSSALPHA, GAUSSBETA)
  call zwgljd(yigll, wygll, NGLLY, GAUSSALPHA, GAUSSBETA)
  call zwgljd(zigll, wzgll, NGLLZ, GAUSSALPHA, GAUSSBETA)

  !! output weight
  wxgll1(:) = real(wxgll(:),kind=CUSTOM_REAL)
  wygll1(:) = real(wygll(:),kind=CUSTOM_REAL)
  wzgll1(:) = real(wzgll(:),kind=CUSTOM_REAL)

  !! precompute matrices 
  do i1=1,NGLLX
    do i2=1,NGLLX
      hprime_xxT(i1,i2) = real(lagrange_deriv_GLL(i1-1,i2-1,xigll,NGLLX),kind=CUSTOM_REAL)
      hprimewgll_xx(i2,i1) = real(lagrange_deriv_GLL(i1-1,i2-1,xigll,NGLLX)*wxgll(i2),kind=CUSTOM_REAL)
    enddo
  enddo

  do j1=1,NGLLY
    do j2=1,NGLLY
      hprime_yyT(j1,j2) = real(lagrange_deriv_GLL(j1-1,j2-1,yigll,NGLLY),kind=CUSTOM_REAL)
      hprimewgll_yy(j2,j1) = real(lagrange_deriv_GLL(j1-1,j2-1,yigll,NGLLY)*wygll(j2),kind=CUSTOM_REAL)
    enddo
  enddo

  do k1=1,NGLLZ
    do k2=1,NGLLZ
      hprime_zzT(k1,k2) = real(lagrange_deriv_GLL(k1-1,k2-1,zigll,NGLLZ),kind=CUSTOM_REAL)
      hprimewgll_zz(k2,k1) = real(lagrange_deriv_GLL(k1-1,k2-1,zigll,NGLLZ)*wzgll(k2),kind=CUSTOM_REAL)
    enddo
  enddo

  do i=1,NGLLX
    do j=1,NGLLY
      wgllwgll_xy(i,j) = real(wxgll(i)*wygll(j),kind=CUSTOM_REAL)
    enddo
  enddo

  do i=1,NGLLX
    do k=1,NGLLZ
      wgllwgll_xz(i,k) = real(wxgll(i)*wzgll(k),kind=CUSTOM_REAL)
    enddo
  enddo

  do j=1,NGLLY
    do k=1,NGLLZ
      wgllwgll_yz(j,k) = real(wygll(j)*wzgll(k),kind=CUSTOM_REAL)
    enddo
  enddo
  
  
end subroutine define_weight_and_derivation_matrices

!
!
!! ########################################################################################################## 
!=======================================================================
!
!  Library to compute the Gauss-Lobatto-Legendre points and weights
!  Based on Gauss-Lobatto routines from M.I.T.
!  Department of Mechanical Engineering
!
!=======================================================================

! note: this version uses zwgljd() with double precision arguments

  double precision function endw1(n,alpha,beta)

  implicit none

  integer n
  double precision alpha,beta
  double precision apb,f1,fint1,fint2,f2,di,abn,abnn,a1,a2,a3,f3
  integer i

  f3 = zero
  apb = alpha+beta
  if (n == 0) then
    endw1 = zero
    return
  endif
  f1   = gammaf(alpha+two)*gammaf(beta+one)/gammaf(apb+three)
  f1   = f1*(apb+two)*two**(apb+two)/two
  if (n == 1) then
    endw1 = f1
    return
  endif
  fint1 = gammaf(alpha+two)*gammaf(beta+one)/gammaf(apb+three)
  fint1 = fint1*two**(apb+two)
  fint2 = gammaf(alpha+two)*gammaf(beta+two)/gammaf(apb+four)
  fint2 = fint2*two**(apb+three)
  f2    = (-two*(beta+two)*fint1 + (apb+four)*fint2) * (apb+three)/four
  if (n == 2) then
    endw1 = f2
    return
  endif
  do i=3,n
    di   = dble(i-1)
    abn  = alpha+beta+di
    abnn = abn+di
    a1   = -(two*(di+alpha)*(di+beta))/(abn*abnn*(abnn+one))
    a2   =  (two*(alpha-beta))/(abnn*(abnn+two))
    a3   =  (two*(abn+one))/((abnn+two)*(abnn+one))
    f3   =  -(a2*f2+a1*f1)/a3
    f1   = f2
    f2   = f3
  enddo
  endw1  = f3

  end function endw1

!
!=======================================================================
!

  double precision function endw2(n,alpha,beta)

  implicit none

  integer n
  double precision alpha,beta

  double precision apb,f1,fint1,fint2,f2,di,abn,abnn,a1,a2,a3,f3
  integer i

  apb = alpha+beta
  f3 = zero
  if (n == 0) then
    endw2 = zero
    return
  endif
  f1   = gammaf(alpha+one)*gammaf(beta+two)/gammaf(apb+three)
  f1   = f1*(apb+two)*two**(apb+two)/two
  if (n == 1) then
    endw2 = f1
    return
  endif
  fint1 = gammaf(alpha+one)*gammaf(beta+two)/gammaf(apb+three)
  fint1 = fint1*two**(apb+two)
  fint2 = gammaf(alpha+two)*gammaf(beta+two)/gammaf(apb+four)
  fint2 = fint2*two**(apb+three)
  f2    = (two*(alpha+two)*fint1 - (apb+four)*fint2) * (apb+three)/four
  if (n == 2) then
    endw2 = f2
    return
  endif
  do i=3,n
    di   = dble(i-1)
    abn  = alpha+beta+di
    abnn = abn+di
    a1   =  -(two*(di+alpha)*(di+beta))/(abn*abnn*(abnn+one))
    a2   =  (two*(alpha-beta))/(abnn*(abnn+two))
    a3   =  (two*(abn+one))/((abnn+two)*(abnn+one))
    f3   =  -(a2*f2+a1*f1)/a3
    f1   = f2
    f2   = f3
  enddo
  endw2  = f3

  end function endw2

!
!=======================================================================
!

  double precision function gammaf (x)

  implicit none

  double precision, parameter :: pi = 3.141592653589793d0

  double precision x

  double precision, parameter :: half=0.5d0,one=1.d0,two=2.d0

  gammaf = one

  if (x == -half) gammaf = -two*dsqrt(pi)
  if (x == half) gammaf =  dsqrt(pi)
  if (x == one) gammaf =  one
  if (x == two) gammaf =  one
  if (x == 1.5d0) gammaf =  dsqrt(pi)/2.d0
  if (x == 2.5d0) gammaf =  1.5d0*dsqrt(pi)/2.d0
  if (x == 3.5d0) gammaf =  2.5d0*1.5d0*dsqrt(pi)/2.d0
  if (x == 3.d0 ) gammaf =  2.d0
  if (x == 4.d0 ) gammaf = 6.d0
  if (x == 5.d0 ) gammaf = 24.d0
  if (x == 6.d0 ) gammaf = 120.d0

  end function gammaf

!
!=====================================================================
!

  subroutine jacg (xjac,np,alpha,beta)

!=======================================================================
!
! computes np Gauss points, which are the zeros of the
! Jacobi polynomial with parameters alpha and beta
!
!                  .alpha = beta =  0.0  ->  Legendre points
!                  .alpha = beta = -0.5  ->  Chebyshev points
!
!=======================================================================

  implicit none

  integer np
  double precision alpha,beta
  double precision xjac(np)

  ! local parameters
  integer k,j,i,jmin,jm,n
  double precision xlast,dth,x,x1,x2,recsum,delx,xmin,swap
  double precision p,pd,pm1,pdm1,pm2,pdm2

  integer, parameter :: K_MAX_ITER = 10
  double precision, parameter :: zero = 0.d0, eps = 1.0d-12

  pm1 = zero
  pm2 = zero
  pdm1 = zero
  pdm2 = zero

  xlast = 0.d0
  n   = np-1
  dth = 4.d0*datan(1.d0)/(2.d0*dble(n)+2.d0)
  p = 0.d0
  pd = 0.d0

  do j=1,np
    if (j == 1) then
      x = dcos((2.d0*(dble(j)-1.d0)+1.d0)*dth)
    else
      x1 = dcos((2.d0*(dble(j)-1.d0)+1.d0)*dth)
      x2 = xlast
      x  = (x1+x2)/2.d0
    endif

    do k=1,K_MAX_ITER
      call jacobf (p,pd,pm1,pdm1,pm2,pdm2,np,alpha,beta,x)
      recsum = 0.d0
      jm = j-1
      do i=1,jm
        recsum = recsum+1.d0/(x-xjac(np-i+1))
      enddo
      delx = -p/(pd-recsum*p)
      x    = x+delx

      ! exits loop if increment too small
      if (abs(delx) < eps) exit

    enddo

    ! checks bounds
    if (np-j+1 < 1 .or. np-j+1 > np) stop 'error np-j+1-index in jacg'

    xjac(np-j+1) = x
    xlast        = x
  enddo

  jmin = 0

  ! orders xjac array in increasing values
  do i=1,np
    xmin = 2.d0
    jmin = i

    ! looks for index with minimum value
    do j=i,np
      ! note: some compilers (cray) might be too aggressive in optimizing this loop,
      !       thus we need this temporary array value x to store and compare values
      x = xjac(j)

      if (x < xmin) then
        xmin = x
        jmin = j
      endif
    enddo

    ! checks bounds
    if (jmin < 1 .or. jmin > np) stop 'error j-index in jacg'

    if (jmin /= i) then
      swap = xjac(i)
      xjac(i) = xjac(jmin)
      xjac(jmin) = swap
    endif

  enddo

  end subroutine jacg

!
!=====================================================================
!

  subroutine jacobf (poly,pder,polym1,pderm1,polym2,pderm2,n,alp,bet,x)

!=======================================================================
!
! Computes the Jacobi polynomial of degree n and its derivative at x
!
!=======================================================================

  implicit none

  double precision poly,pder,polym1,pderm1,polym2,pderm2,alp,bet,x
  integer n

  double precision apb,polyl,pderl,dk,a1,a2,b3,a3,a4,polyn,pdern,psave,pdsave
  integer k

  apb  = alp+bet
  poly = 1.d0
  pder = 0.d0
  psave = 0.d0
  pdsave = 0.d0

  if (n == 0) return

  polyl = poly
  pderl = pder
  poly  = (alp-bet+(apb+2.d0)*x)/2.d0
  pder  = (apb+2.d0)/2.d0
  if (n == 1) return

  do k=2,n
    dk = dble(k)
    a1 = 2.d0*dk*(dk+apb)*(2.d0*dk+apb-2.d0)
    a2 = (2.d0*dk+apb-1.d0)*(alp**2-bet**2)
    b3 = (2.d0*dk+apb-2.d0)
    a3 = b3*(b3+1.d0)*(b3+2.d0)
    a4 = 2.d0*(dk+alp-1.d0)*(dk+bet-1.d0)*(2.d0*dk+apb)
    polyn  = ((a2+a3*x)*poly-a4*polyl)/a1
    pdern  = ((a2+a3*x)*pder-a4*pderl+a3*poly)/a1
    psave  = polyl
    pdsave = pderl
    polyl  = poly
    poly   = polyn
    pderl  = pder
    pder   = pdern
  enddo

  polym1 = polyl
  pderm1 = pderl
  polym2 = psave
  pderm2 = pdsave

  end subroutine jacobf

!
!------------------------------------------------------------------------
!

  double precision function PNDLEG (Z,N)

!------------------------------------------------------------------------
!
!     Compute the derivative of the Nth order Legendre polynomial at Z.
!     Based on the recursion formula for the Legendre polynomials.
!
!------------------------------------------------------------------------
  implicit none

  double precision z
  integer n

  double precision P1,P2,P1D,P2D,P3D,DBLE_K,P3
  integer k

  P1   = 1.d0
  P2   = Z
  P1D  = 0.d0
  P2D  = 1.d0
  P3D  = 1.d0

  do K = 1, N-1
    DBLE_K  = dble(K)
    P3  = ((2.d0*DBLE_K+1.d0)*Z*P2 - DBLE_K*P1)/(DBLE_K+1.d0)
    P3D = ((2.d0*DBLE_K+1.d0)*P2 + (2.d0*DBLE_K+1.d0)*Z*P2D - DBLE_K*P1D) / (DBLE_K+1.d0)
    P1  = P2
    P2  = P3
    P1D = P2D
    P2D = P3D
  enddo

  PNDLEG = P3D

  end function pndleg

!
!------------------------------------------------------------------------
!

  double precision function PNLEG (Z,N)

!------------------------------------------------------------------------
!
!     Compute the value of the Nth order Legendre polynomial at Z.
!     Based on the recursion formula for the Legendre polynomials.
!
!------------------------------------------------------------------------
  implicit none

  double precision z
  integer n

  double precision P1,P2,P3,DBLE_K
  integer k

  P1   = 1.d0
  P2   = Z
  P3   = P2

  do K = 1, N-1
    DBLE_K  = dble(K)
    P3  = ((2.d0*DBLE_K+1.d0)*Z*P2 - DBLE_K*P1)/(DBLE_K+1.d0)
    P1  = P2
    P2  = P3
  enddo

  PNLEG = P3

  end function pnleg

!
!------------------------------------------------------------------------
!

  double precision function pnormj (n,alpha,beta)

  implicit none

  double precision alpha,beta
  integer n

  double precision one,two,dn,const,prod,dindx,frac
  integer i

  one   = 1.d0
  two   = 2.d0
  dn    = dble(n)
  const = alpha+beta+one

  if (n <= 1) then
    prod   = gammaf(dn+alpha)*gammaf(dn+beta)
    prod   = prod/(gammaf(dn)*gammaf(dn+alpha+beta))
    pnormj = prod * two**const/(two*dn+const)
    return
  endif

  prod  = gammaf(alpha+one)*gammaf(beta+one)
  prod  = prod/(two*(one+const)*gammaf(const+one))
  prod  = prod*(one+alpha)*(two+alpha)
  prod  = prod*(one+beta)*(two+beta)

  do i=3,n
    dindx = dble(i)
    frac  = (dindx+alpha)*(dindx+beta)/(dindx*(dindx+alpha+beta))
    prod  = prod*frac
  enddo

  pnormj = prod * two**const/(two*dn+const)

  end function pnormj

!
!------------------------------------------------------------------------
!

  subroutine zwgjd(z,w,np,alpha,beta)

!=======================================================================
!
!     Z w g j d : Generate np Gauss-Jacobi points and weights
!                 associated with Jacobi polynomial of degree n = np-1
!
!     Note : Coefficients alpha and beta must be greater than -1.
!     ----
!=======================================================================

  implicit none


  integer np
  double precision z(np),w(np)
  double precision alpha,beta

  ! local parameters
  integer n,np1,np2,i
  double precision p,pd,pm1,pdm1,pm2,pdm2
  double precision apb,dnp1,dnp2,fac1,fac2,fac3,fnorm,rcoef

  pd = zero
  pm1 = zero
  pm2 = zero
  pdm1 = zero
  pdm2 = zero

  n    = np-1
  apb  = alpha+beta
  p    = zero
  pdm1 = zero

  if (np <= 0) stop 'minimum number of Gauss points is 1'

  if ((alpha <= -one) .or. (beta <= -one)) stop 'alpha and beta must be greater than -1'

  if (np == 1) then
    z(1) = (beta-alpha)/(apb+two)
    w(1) = gammaf(alpha+one)*gammaf(beta+one)/gammaf(apb+two) * two**(apb+one)
    return
  endif

  call jacg(z,np,alpha,beta)

  np1   = n+1
  np2   = n+2
  dnp1  = dble(np1)
  dnp2  = dble(np2)
  fac1  = dnp1+alpha+beta+one
  fac2  = fac1+dnp1
  fac3  = fac2+one
  fnorm = pnormj(np1,alpha,beta)
  rcoef = (fnorm*fac2*fac3)/(two*fac1*dnp2)
  do i=1,np
    call jacobf(p,pd,pm1,pdm1,pm2,pdm2,np2,alpha,beta,z(i))
    w(i) = -rcoef/(p*pdm1)
  enddo

  end subroutine zwgjd

!
!------------------------------------------------------------------------
!

  subroutine zwgljd(z,w,np,alpha,beta)

!=======================================================================
!
!     Z w g l j d : Generate np Gauss-Lobatto-Jacobi points and the
!     -----------   weights associated with Jacobi polynomials of degree
!                   n = np-1.
!
!     Note : alpha and beta coefficients must be greater than -1.
!            Legendre polynomials are special case of Jacobi polynomials
!            just by setting alpha and beta to 0.
!
!=======================================================================

  implicit none

  double precision, parameter :: tol_zero=1.d-30

  integer np
  double precision alpha,beta
  double precision z(np), w(np)

  ! local parameters
  integer n,nm1,i
  double precision p,pd,pm1,pdm1,pm2,pdm2
  double precision alpg,betg

  p = zero
  pm1 = zero
  pm2 = zero
  pdm1 = zero
  pdm2 = zero

  n   = np-1
  nm1 = n-1
  pd  = zero

  if (np <= 1) stop 'minimum number of Gauss-Lobatto points is 2'

! with spectral elements, use at least 3 points
  if (np <= 2) stop 'minimum number of Gauss-Lobatto points for the SEM is 3'

  if ((alpha <= -one) .or. (beta <= -one)) stop 'alpha and beta must be greater than -1'

  if (nm1 > 0) then
    alpg  = alpha+one
    betg  = beta+one
    call zwgjd(z(2:n),w(2:n),nm1,alpg,betg)
  endif

! start and end point at exactly -1 and 1
  z(1)  = - one
  z(np) =  one

! note: Jacobi polynomials with (alpha,beta) equal to zero become Legendre polynomials.
!       for Legendre polynomials, if number of points is odd, the middle abscissa is exactly zero
  if (abs(alpha) < tol_zero .and. abs(beta) < tol_zero) then
    if (mod(np,2) /= 0) z((np-1)/2+1) = zero
  endif

! weights
  do i=2,np-1
    w(i) = w(i)/(one-z(i)**2)
  enddo

  call jacobf(p,pd,pm1,pdm1,pm2,pdm2,n,alpha,beta,z(1))
  w(1)  = endw1(n,alpha,beta)/(two*pd)

  call jacobf(p,pd,pm1,pdm1,pm2,pdm2,n,alpha,beta,z(np))
  w(np) = endw2(n,alpha,beta)/(two*pd)

  end subroutine zwgljd


!
!------------------------------------------------------------------------
!

  double precision function pnglj(z,n)

!------------------------------------------------------------------------
!
!     Compute the value of the Nth order polynomial of the
!     Gauss-Lobatto-Jacobi (0,1) at Z. from Legendre polynomials.
!
!------------------------------------------------------------------------

  implicit none

  double precision z
  integer n
  !double precision :: pnleg

  if (abs(z+1.d0) > TINYVAL) then  ! if (z /= -1.d0)
    pnglj = (pnleg(z,n)+pnleg(z,n+1))/(ONE+z)
  else
    pnglj = (dble(n)+ONE)*(-1)**n
  endif

  end function pnglj

!
!------------------------------------------------------------------------
!

  double precision function pndglj(z,n)

!------------------------------------------------------------------------
!
!     Compute the value of the derivative of Nth order polynomial of the
!     Gauss-Lobatto-Jacobi (0,1) at Z. from Legendre polynomials.
!
!------------------------------------------------------------------------

  implicit none

  double precision z
  integer n

  if (abs(z+1.d0) > TINYVAL) then  ! if (z /= -1.d0)
    pndglj = (pndleg(z,n)+pndleg(z,n+1))/(ONE+z) - (pnleg(z,n)+pnleg(z,n+1))/((ONE+z)**2)
  else
    pndglj = pnleg(-1.d0,n)+pnleg(-1.d0,n+1)
  endif

  end function pndglj

!
!=====================================================================
!

! function to compute the derivative of the Lagrange interpolants
! at any given GLL point

  double precision function lagrange_deriv_GLL(i,j,ZGLL,NZ)

!------------------------------------------------------------------------
!
!     Compute the value of the derivative of the I-th
!     Lagrange interpolant through the
!     NZ Gauss-Lobatto Legendre points ZGLL at point ZGLL(j)
!
!------------------------------------------------------------------------

    implicit none
    
    integer :: i,j,nz
    double precision :: zgll(0:nz-1)

    ! local parameters
    integer :: degpoly
    
    degpoly = nz - 1
    if (i == 0 .and. j == 0) then
       lagrange_deriv_GLL = - dble(degpoly)*(dble(degpoly)+1.d0) * 0.25d0  ! / 4.d0
    else if (i == degpoly .and. j == degpoly) then
       lagrange_deriv_GLL = dble(degpoly)*(dble(degpoly)+1.d0) * 0.25d0  ! / 4.d0
    else if (i == j) then
       lagrange_deriv_GLL = 0.d0
    else
       lagrange_deriv_GLL = pnleg(zgll(j),degpoly) / &
            (pnleg(zgll(i),degpoly)*(zgll(j)-zgll(i))) &
            + (1.d0-zgll(j)*zgll(j))*pndleg(zgll(j),degpoly) / (dble(degpoly)* &
            (dble(degpoly)+1.d0)*pnleg(zgll(i),degpoly)*(zgll(j)-zgll(i))*(zgll(j)-zgll(i)))
    endif
    
  end function lagrange_deriv_GLL

!
!=====================================================================
! 
  subroutine lagrange_any(xi,NGLL,xigll,h,hprime)

! subroutine to compute the Lagrange interpolants based upon the interpolation points
! and their first derivatives at any point xi in [-1,1]

    implicit none

    double precision,intent(in) :: xi

    integer,intent(in) :: NGLL
    double precision,dimension(NGLL),intent(in) :: xigll
    double precision,dimension(NGLL),intent(out) :: h,hprime

    ! local parameters
    integer :: dgr,i,j
    double precision :: prod1,prod2,prod3
    double precision :: prod2_inv
    double precision :: sum
    double precision :: x0,x
    
    do dgr = 1,NGLL
       
       prod1 = 1.0d0
       prod2 = 1.0d0

       ! lagrangian interpolants
       x0 = xigll(dgr)
       do i = 1,NGLL
          if (i /= dgr) then
             x = xigll(i)
             prod1 = prod1*(xi-x)
             prod2 = prod2*(x0-x)
          endif
       enddo
       
       ! takes inverse to avoid additional divisions
       ! (multiplications are cheaper than divisions)
       prod2_inv = 1.d0/prod2
       
       h(dgr) = prod1 * prod2_inv
       
       ! first derivatives
       sum = 0.0d0
       do i = 1,NGLL
          if (i /= dgr) then
             prod3 = 1.0d0
             do j = 1,NGLL
                if (j /= dgr .and. j /= i) prod3 = prod3*(xi-xigll(j))
             enddo
             sum = sum + prod3
          endif
       enddo
       
       hprime(dgr) = sum * prod2_inv
       
    enddo
    
  end subroutine lagrange_any

!

end module element_mod
