program specfem_mini_app
  
  !
  ! main program 
  !


  use config_mod
  use domain_decomp_mod
  use meshfem_mod
  use specfem_mod
  use solver_mod

  implicit none 

  !! initialize mpi communicator
  call mpi_begin()

  !! FTZ for gnu compilers 
  call force_ftz()
  
  !! define configuration and user input for run
  call default_config()
  
  !! cartesian domain decomposition in X and Y direction 
  call split_domain(NPX_config, NPY_config, myrank_mpi, nbproc_mpi)

  !! meshing regular decomposed cartesian domain
  call meshfem(ipx_dd, ipy_dd)

  !! detect element in mpi intrfaces
  call get_mpi_interfaces(mpi_x_meshfem, mpi_y_meshfem, ibool_meshfem, nspec_meshfem)

  !! initialize specfem solver
  call specfem_setup(myrank_mpi)

  !! setup mpi communtication arrays 
  call setup_mpi_comm(xdof_specfem, ibool_specfem, ibool_meshfem, nspec_specfem, nglob_specfem)

  !! setup diagonal mass matrix
  call specfem_setup_mass_matrices()

  !! run specfem solver time loop....
  call iterate_time()

  !! finalize mpi communicator 
  call mpi_end()
  
end program specfem_mini_app
