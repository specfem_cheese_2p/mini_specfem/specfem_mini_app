module solver_mod

    !
    !
    !  module that manage the main computation 
    !
    !

    use specfem_mod

    implicit none 

    private    
    public :: iterate_time

contains

!!---------------------------------------------------------------------------
subroutine iterate_time()

    integer :: it, iphase
    real(kind=CUSTOM_REAL) :: t
    character(len=CHAR_LEN) :: name_file
    real(kind=CUSTOM_REAL) :: max_displ
    real :: time_start, time_end
    
    !! initlialize fields 
    call initialize_solver()
    
    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'Starting time iteration loop...'
       write(*,*)
    end if

    !! prepare for volume visualization
    call io_setup_connectivity(xdof_specfem, ibool_specfem, NGLLX, NGLLY, NGLLZ,&
    nspec_specfem, nglob_specfem, myrank)
    !! complete 3D mesh visualization 
    call write_xdmf_mesh(myrank)

    call cpu_time(time_start)

    do it = 1, NSTEP
     
      t = (it-1)*deltat_config

       if ( mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. check_stability) then
          max_displ = maxval(displ_specfem(:,:))
          if (max_displ /= max_displ) then
             stop 'forward simulation became unstable in elastic domain and blew up'
          end if
       end if
       
       if (myrank == 0 .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. verbose) then
          write(*,*) " time step : ",  it, ' / ', NSTEP
       end if
       
       if (movie .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0) then

          write(name_file,"('output/veloc_x_it',i6.6,'.bin')") it
          call dump_binary_mpi(name_file, veloc_specfem, 1, nglob_specfem)
         
          write(name_file,"('output/veloc_y_it',i6.6,'.bin')") it
          call dump_binary_mpi(name_file, veloc_specfem, 2, nglob_specfem)
          
          write(name_file,"('output/veloc_z_it',i6.6,'.bin')")  it
          call dump_binary_mpi(name_file, veloc_specfem, 3, nglob_specfem)

       end if

       call newmark_predictor()

       !! compute rhs --------------------------------------------------------------
       do iphase = 1, 2
          
          call compute_forces_iso_visco_elastic(iphase)
          
          if (iphase == 1) then
             
             call add_source_term(it)          !! source term
             call store_seismogram(it)         !! store seismograms 
             call compute_stacey()             !! boundary conditions

             !! send accelareation to neighbours mpi-slice
             call assemble_MPI_vector_async_send(nglob_specfem, accel_specfem)
          else
             !! receive accelareation from neighbours mpi-slice
             call assemble_MPI_vector_async_w_ord(nglob_specfem, accel_specfem)
          end if

       end do
       !! -------------------------------------------------------------------------
       
       call  newmark_corector()
       
    end do

    call cpu_time(time_end)
    
    call write_sismogram()

    if (movie) then
       call write_xdmf_movie(myrank, NSTEP)
    end if

    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'End of time iteration loop...', time_end - time_start,' s'
       write(*,*)
    end if
    
     
end subroutine iterate_time
!!---------------------------------------------------------------------------
subroutine initialize_solver()
    displ_specfem(:,:) = 0._CUSTOM_REAL
    veloc_specfem(:,:) = 0._CUSTOM_REAL
    accel_specfem(:,:) = 0._CUSTOM_REAL
    R_trace(:,:,:,:,:) = 0._CUSTOM_REAL
    R_xx(:,:,:,:,:) = 0._CUSTOM_REAL
    R_yy(:,:,:,:,:) = 0._CUSTOM_REAL
    R_xy(:,:,:,:,:) = 0._CUSTOM_REAL
    R_xz(:,:,:,:,:) = 0._CUSTOM_REAL
    R_yz(:,:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_trace(:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_xx(:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_yy(:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_xy(:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_xz(:,:,:,:) = 0._CUSTOM_REAL
    epsilondev_yz(:,:,:,:) = 0._CUSTOM_REAL
end subroutine initialize_solver
!!---------------------------------------------------------------------------
subroutine newmark_predictor()
    integer :: i
    do i=1, nglob_specfem
       displ_specfem(:,i) = displ_specfem(:,i) + deltat_config*veloc_specfem(:,i) + deltatsqover2*accel_specfem(:,i)
       veloc_specfem(:,i) = veloc_specfem(:,i) + deltatover2*accel_specfem(:,i)
       accel_specfem(:,i) = 0._CUSTOM_REAL
    end do
end subroutine newmark_predictor
!!---------------------------------------------------------------------------
subroutine newmark_corector()
    integer :: i
    do  i=1, nglob_specfem
       accel_specfem(1,i) = accel_specfem(1,i)*inv_massx_specfem(i)
       accel_specfem(2,i) = accel_specfem(2,i)*inv_massy_specfem(i)
       accel_specfem(3,i) = accel_specfem(3,i)*inv_massz_specfem(i)
       veloc_specfem(:,i) = veloc_specfem(:,i) + deltatover2*accel_specfem(:,i)
    end do
end subroutine newmark_corector
!!---------------------------------------------------------------------------
subroutine add_source_term(it)
    integer, intent(in) :: it
    integer :: ispec, isrc, i,j,k, iglob
    real(kind=CUSTOM_REAL) :: hl
    do isrc =1, nsource
       ispec = ispec_src(isrc)
       if (ispec > 0) then
          
          do k=1,NGLLZ
             do j=1,NGLLY
                do i=1,NGLLX
                   iglob = ibool_specfem(i,j,k,ispec)
                   hl = hxis(i,isrc)*hetas(j,isrc)*hgammas(k,isrc) * used_stf(it) 
                   accel_specfem(1,iglob) = accel_specfem(1,iglob) + Fx(isrc) * hl
                   accel_specfem(2,iglob) = accel_specfem(2,iglob) + Fy(isrc) * hl
                   accel_specfem(3,iglob) = accel_specfem(3,iglob) + Fz(isrc) * hl
                end do
             end do
          end do
                
       end if
    end do
end subroutine add_source_term
!!---------------------------------------------------------------------------
subroutine store_seismogram(it)
   integer, intent(in) :: it
   integer :: ispec, irec, i,j,k, iglob
   real(kind=CUSTOM_REAL) :: hl
   do irec =1, nrec
      ispec = ispec_rec(irec)
      if (ispec > 0) then
         do k=1,NGLLZ
            do j=1,NGLLY
               do i=1,NGLLX
                  iglob = ibool_specfem(i,j,k,ispec)
                  hl = hxir(i,irec)*hetar(j,irec)*hgammar(k,irec)
                  seismogram_d(it, 1, irec) = seismogram_d(it, 1, irec) + hl * displ_specfem(1,iglob)
                  seismogram_d(it, 2, irec) = seismogram_d(it, 2, irec) + hl * displ_specfem(2,iglob)
                  seismogram_d(it, 3, irec) = seismogram_d(it, 3, irec) + hl * displ_specfem(3,iglob)
               end do
            end do
         end do
      end if
   end do
end subroutine store_seismogram
!!---------------------------------------------------------------------------
subroutine write_sismogram()
   integer :: ispec, irec, it
   character(len=100) :: sismo_file
   do irec =1, nrec
      ispec = ispec_rec(irec)
      if (ispec > 0) then
         write(sismo_file, '("displ_",i5.5,".txt")') irec 
         open(10,file=trim(sismo_file))
         if (CUSTOM_REAL>=8) then 
           do it=1,NSTEP
               write(10,'(4E41.20)') (it-1)*deltat_config, seismogram_d(it,1,irec),seismogram_d(it,2,irec), seismogram_d(it,3,irec)
           end do
         else 
           do it=1,NSTEP
              write(10,*) (it-1)*deltat_config, seismogram_d(it,1,irec),seismogram_d(it,2,irec), seismogram_d(it,3,irec)
           end do
         end if
         close(10)
      end if
   end do
end subroutine write_sismogram
!!---------------------------------------------------------------------------
subroutine compute_stacey()
    integer :: ib, iglob
    real(kind=CUSTOM_REAL) :: nx, ny, nz, vx, vy, vz, vn
    real(kind=CUSTOM_REAL) :: tx, ty, tz, weight
    do ib = 1, ngll_boundary
       nx = normal(1,ib)
       ny = normal(2,ib)
       nz = normal(3,ib)
       iglob = index_gll_boundary(ib)
       vx = veloc_specfem(1,iglob)
       vy = veloc_specfem(2,iglob)
       vz = veloc_specfem(3,iglob)
       vn = vx*nx + vy*ny + vz*nz
       tx = rho_vp(ib) * vn*nx + rho_vs(ib) * (vx-vn*nx)
       ty = rho_vp(ib) * vn*ny + rho_vs(ib) * (vy-vn*ny)
       tz = rho_vp(ib) * vn*nz + rho_vs(ib) * (vz-vn*nz)
       weight = wstacey(ib)
       accel_specfem(1,iglob) = accel_specfem(1,iglob) - tx*weight
       accel_specfem(2,iglob) = accel_specfem(2,iglob) - ty*weight
       accel_specfem(3,iglob) = accel_specfem(3,iglob) - tz*weight
    end do
end subroutine compute_stacey
!!---------------------------------------------------------------------------
subroutine compute_forces_iso_visco_elastic(iphase)

    integer, intent(in) :: iphase

    integer :: i_sls, i, j, k, l, ispec_p, ispec, iglob
    integer :: num_elements

    real(kind=CUSTOM_REAL) :: xixl,xiyl,xizl,etaxl,etayl,etazl,gammaxl,gammayl,gammazl,jacobianl
    real(kind=CUSTOM_REAL) :: duxdxl,duxdyl,duxdzl,duydxl,duydyl,duydzl,duzdxl,duzdyl,duzdzl
    real(kind=CUSTOM_REAL) :: dvxdxl,dvxdyl,dvxdzl,dvydxl,dvydyl,dvydzl,dvzdxl,dvzdyl,dvzdzl

    real(kind=CUSTOM_REAL) :: duxdxl_plus_duydyl,duxdxl_plus_duzdzl,duydyl_plus_duzdzl
    real(kind=CUSTOM_REAL) :: duxdyl_plus_duydxl,duzdxl_plus_duxdzl,duzdyl_plus_duydzl

    real(kind=CUSTOM_REAL) :: sigma_xx,sigma_yy,sigma_zz,sigma_xy,sigma_xz,sigma_yz,sigma_yx,sigma_zx,sigma_zy
    
    real(kind=CUSTOM_REAL) :: fac1,fac2,fac3
    real(kind=CUSTOM_REAL) :: hp1, hp2, hp3
    
    real(kind=CUSTOM_REAL) :: lambdal,mul,lambdalplus2mul
    real(kind=CUSTOM_REAL) :: kappal


    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: epsilondev_trace_loc, epsilondev_xx_loc, &
            epsilondev_yy_loc, epsilondev_xy_loc, epsilondev_xz_loc, epsilondev_yz_loc
    
    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: dummyx_loc,dummyy_loc,dummyz_loc
    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: dummyx_loc_vel,dummyy_loc_vel,dummyz_loc_vel 
    real(kind=CUSTOM_REAL), dimension(NGLLX,NGLLY,NGLLZ) :: tempx1,tempx2,tempx3, tempy1,tempy2,tempy3,tempz1,tempz2,tempz3
    real(kind=CUSTOM_REAL) :: tempx1l,tempx2l,tempx3l, tempy1l,tempy2l,tempy3l,tempz1l,tempz2l,tempz3l, templ
    real(kind=CUSTOM_REAL) :: tempx1lv,tempx2lv,tempx3lv, tempy1lv,tempy2lv,tempy3lv,tempz1lv,tempz2lv,tempz3lv

    real(kind=CUSTOM_REAL) :: R_trace_val, R_xx_val, R_yy_val
    real(kind=CUSTOM_REAL) :: factor_loc, alphaval_loc, betaval_loc, gammaval_loc, Sn, Snp1

    ! choses inner/outer elements
    if (iphase == 1) then
       num_elements = nspec_outer_elastic
    else
       num_elements = nspec_inner_elastic
    endif

    do ispec_p = 1,num_elements

       ! returns element id from stored element list
       ispec = phase_ispec_inner_elastic(ispec_p,iphase)

       ! store element displacement in local array 
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
                iglob = ibool_specfem(i,j,k,ispec)
                dummyx_loc(i,j,k) = displ_specfem(1,iglob)
                dummyy_loc(i,j,k) = displ_specfem(2,iglob)
                dummyz_loc(i,j,k) = displ_specfem(3,iglob)
             enddo
          enddo
       enddo

       ! store element velocity in local array 
       do k=1,NGLLZ
         do j=1,NGLLY
            do i=1,NGLLX
               iglob = ibool_specfem(i,j,k,ispec)
               dummyx_loc_vel(i,j,k) = deltat_config * veloc_specfem(1,iglob)
               dummyy_loc_vel(i,j,k) = deltat_config * veloc_specfem(2,iglob)
               dummyz_loc_vel(i,j,k) = deltat_config * veloc_specfem(3,iglob)
            enddo
         enddo
      enddo

       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
                
                tempx1l = 0._CUSTOM_REAL
                tempy1l = 0._CUSTOM_REAL
                tempz1l = 0._CUSTOM_REAL
                
                tempx2l = 0._CUSTOM_REAL
                tempy2l = 0._CUSTOM_REAL
                tempz2l = 0._CUSTOM_REAL
                
                tempx3l = 0._CUSTOM_REAL
                tempy3l = 0._CUSTOM_REAL
                tempz3l = 0._CUSTOM_REAL

                tempx1lv = 0._CUSTOM_REAL
                tempy1lv = 0._CUSTOM_REAL
                tempz1lv = 0._CUSTOM_REAL
                
                tempx2lv = 0._CUSTOM_REAL
                tempy2lv = 0._CUSTOM_REAL
                tempz2lv = 0._CUSTOM_REAL
                
                tempx3lv = 0._CUSTOM_REAL
                tempy3lv = 0._CUSTOM_REAL
                tempz3lv = 0._CUSTOM_REAL
                
                ! we can merge these loops because NGLLX = NGLLY = NGLLZ
                do l=1,NGLLX

                   hp1 = hprime_xxT(l,i)
                   tempx1l = tempx1l + dummyx_loc(l,j,k) * hp1
                   tempy1l = tempy1l + dummyy_loc(l,j,k) * hp1
                   tempz1l = tempz1l + dummyz_loc(l,j,k) * hp1
              
                   tempx1lv = tempx1lv + dummyx_loc_vel(l,j,k) * hp1
                   tempy1lv = tempy1lv + dummyy_loc_vel(l,j,k) * hp1
                   tempz1lv = tempz1lv + dummyz_loc_vel(l,j,k) * hp1

                   hp2 = hprime_yyT(l,j)
                   tempx2l = tempx2l + dummyx_loc(i,l,k) * hp2
                   tempy2l = tempy2l + dummyy_loc(i,l,k) * hp2
                   tempz2l = tempz2l + dummyz_loc(i,l,k) * hp2
                   
                   tempx2lv = tempx2lv + dummyx_loc_vel(i,l,k) * hp2
                   tempy2lv = tempy2lv + dummyy_loc_vel(i,l,k) * hp2
                   tempz2lv = tempz2lv + dummyz_loc_vel(i,l,k) * hp2

                   hp3 = hprime_zzT(l,k)
                   tempx3l = tempx3l + dummyx_loc(i,j,l) * hp3
                   tempy3l = tempy3l + dummyy_loc(i,j,l) * hp3
                   tempz3l = tempz3l + dummyz_loc(i,j,l) * hp3

                   tempx3lv = tempx3lv + dummyx_loc_vel(i,j,l) * hp3
                   tempy3lv = tempy3lv + dummyy_loc_vel(i,j,l) * hp3
                   tempz3lv = tempz3lv + dummyz_loc_vel(i,j,l) * hp3

                enddo
                
                tempx1lv = tempx1lv + tempx1l
                tempy1lv = tempy1lv + tempy1l
                tempz1lv = tempz1lv + tempz1l

                tempx2lv = tempx2lv + tempx2l
                tempy2lv = tempy2lv + tempy2l
                tempz2lv = tempz2lv + tempz2l

                tempx3lv = tempx3lv + tempx3l
                tempy3lv = tempy3lv + tempy3l
                tempz3lv = tempz3lv + tempz3l

                xixl = dxi_dx_specfem(i,j,k,ispec)
                xiyl = dxi_dy_specfem(i,j,k,ispec)
                xizl = dxi_dz_specfem(i,j,k,ispec)
                etaxl = deta_dx_specfem(i,j,k,ispec)
                etayl = deta_dy_specfem(i,j,k,ispec)
                etazl = deta_dz_specfem(i,j,k,ispec)
                gammaxl = dgamma_dx_specfem(i,j,k,ispec)
                gammayl = dgamma_dy_specfem(i,j,k,ispec)
                gammazl = dgamma_dz_specfem(i,j,k,ispec)
                jacobianl = jacobian_specfem(i,j,k,ispec)
                
                
                duxdxl = xixl*tempx1l + etaxl*tempx2l + gammaxl*tempx3l
                duxdyl = xiyl*tempx1l + etayl*tempx2l + gammayl*tempx3l
                duxdzl = xizl*tempx1l + etazl*tempx2l + gammazl*tempx3l
                 
                duydxl = xixl*tempy1l + etaxl*tempy2l + gammaxl*tempy3l
                duydyl = xiyl*tempy1l + etayl*tempy2l + gammayl*tempy3l
                duydzl = xizl*tempy1l + etazl*tempy2l + gammazl*tempy3l
                 
                duzdxl = xixl*tempz1l + etaxl*tempz2l + gammaxl*tempz3l
                duzdyl = xiyl*tempz1l + etayl*tempz2l + gammayl*tempz3l
                duzdzl = xizl*tempz1l + etazl*tempz2l + gammazl*tempz3l

                ! precompute some sums to save CPU time
                duxdxl_plus_duydyl = duxdxl + duydyl
                duxdxl_plus_duzdzl = duxdxl + duzdzl
                duydyl_plus_duzdzl = duydyl + duzdzl
                duxdyl_plus_duydxl = duxdyl + duydxl
                duzdxl_plus_duxdzl = duzdxl + duxdzl
                duzdyl_plus_duydzl = duzdyl + duydzl

                ! compute strain for attenuation (based on velocity)
                dvxdxl = xixl*tempx1lv + etaxl*tempx2lv + gammaxl*tempx3lv
                dvxdyl = xiyl*tempx1lv + etayl*tempx2lv + gammayl*tempx3lv
                dvxdzl = xizl*tempx1lv + etazl*tempx2lv + gammazl*tempx3lv
                 
                dvydxl = xixl*tempy1lv + etaxl*tempy2lv + gammaxl*tempy3lv
                dvydyl = xiyl*tempy1lv + etayl*tempy2lv + gammayl*tempy3lv
                dvydzl = xizl*tempy1lv + etazl*tempy2lv + gammazl*tempy3lv
                 
                dvzdxl = xixl*tempz1lv + etaxl*tempz2lv + gammaxl*tempz3lv
                dvzdyl = xiyl*tempz1lv + etayl*tempz2lv + gammayl*tempz3lv
                dvzdzl = xizl*tempz1lv + etazl*tempz2lv + gammazl*tempz3lv

                ! deviatoric strain at 0.5(deltat + delata) based on taylor approximation
                templ = ONE_THIRD * (dvxdxl + dvydyl + dvzdzl)
                epsilondev_trace_loc(i,j,k) =  3._CUSTOM_REAL * templ
                epsilondev_xx_loc(i,j,k) = dvxdxl - templ
                epsilondev_yy_loc(i,j,k) = dvydyl - templ
                epsilondev_xy_loc(i,j,k) = 0.5_CUSTOM_REAL * (dvxdyl + dvydxl)
                epsilondev_xz_loc(i,j,k) = 0.5_CUSTOM_REAL * (dvzdxl + dvxdzl)
                epsilondev_yz_loc(i,j,k) = 0.5_CUSTOM_REAL * (dvzdyl + dvydzl)
               
                ! isotropic case
                kappal = kappa_specfem(i,j,k,ispec)
                mul = mu_specfem(i,j,k,ispec)
                 
                 
                lambdalplus2mul = kappal + FOUR_THIRDS * mul
                lambdal = lambdalplus2mul - 2._CUSTOM_REAL * mul
                
                ! compute stress sigma
                sigma_xx = lambdalplus2mul * duxdxl + lambdal * duydyl_plus_duzdzl
                sigma_yy = lambdalplus2mul * duydyl + lambdal * duxdxl_plus_duzdzl
                sigma_zz = lambdalplus2mul * duzdzl + lambdal * duxdxl_plus_duydyl
                 
                sigma_xy = mul * duxdyl_plus_duydxl
                sigma_xz = mul * duzdxl_plus_duxdzl
                sigma_yz = mul * duzdyl_plus_duydzl
               
                ! attenuation contribution : corretion of stress and
                ! memory variable update using strain 
                do i_sls = 1,N_SLS
                  
                  alphaval_loc = alphaval(i_sls)
                  betaval_loc = betaval(i_sls)
                  gammaval_loc = gammaval(i_sls)
                  
                  R_trace_val = R_trace(i_sls,i,j,k,ispec)
                  R_xx_val = R_xx(i_sls,i,j,k,ispec)
                  R_yy_val = R_yy(i_sls,i,j,k,ispec)
                  
                  sigma_xx = sigma_xx - R_xx_val - R_trace_val
                  sigma_yy = sigma_yy - R_yy_val - R_trace_val
                  sigma_zz = sigma_zz + R_xx_val + R_yy_val - R_trace_val
                  sigma_xy = sigma_xy - R_xy(i_sls,i,j,k,ispec)
                  sigma_xz = sigma_xz - R_xz(i_sls,i,j,k,ispec)
                  sigma_yz = sigma_yz - R_yz(i_sls,i,j,k,ispec)

                  factor_loc = kappal * factor_common_kappa_specfem(i_sls,i,j,k,ispec)
                  
                  !! trace
                  Sn   = factor_loc *epsilondev_trace(i,j,k,ispec)
                  Snp1   = factor_loc *epsilondev_trace_loc(i,j,k)
                  R_trace(i_sls,i,j,k,ispec) = alphaval_loc * R_trace(i_sls,i,j,k,ispec) + &
                                               (betaval_loc * Sn + gammaval_loc * Snp1)

                  factor_loc = mul * factor_common_mu_specfem(i_sls,i,j,k,ispec)

                  ! term in xx
                  Sn   = factor_loc * epsilondev_xx(i,j,k,ispec)
                  Snp1   = factor_loc * epsilondev_xx_loc(i,j,k)
                  R_xx(i_sls,i,j,k,ispec) = alphaval_loc * R_xx(i_sls,i,j,k,ispec) + &
                                            betaval_loc * Sn + gammaval_loc * Snp1
                  ! term in yy
                  Sn   = factor_loc * epsilondev_yy(i,j,k,ispec)
                  Snp1   = factor_loc * epsilondev_yy_loc(i,j,k)
                  R_yy(i_sls,i,j,k,ispec) = alphaval_loc * R_yy(i_sls,i,j,k,ispec) + &
                                            betaval_loc * Sn + gammaval_loc * Snp1

                  ! term in xy
                  Sn   = factor_loc * epsilondev_xy(i,j,k,ispec)
                  Snp1   = factor_loc * epsilondev_xy_loc(i,j,k)
                  R_xy(i_sls,i,j,k,ispec) = alphaval_loc * R_xy(i_sls,i,j,k,ispec) + &
                                            betaval_loc * Sn + gammaval_loc * Snp1
                  
                  ! term in xz
                  Sn   = factor_loc * epsilondev_xz(i,j,k,ispec)
                  Snp1   = factor_loc * epsilondev_xz_loc(i,j,k)
                  R_xz(i_sls,i,j,k,ispec) = alphaval_loc * R_xz(i_sls,i,j,k,ispec) + &
                                            betaval_loc * Sn + gammaval_loc * Snp1
                  
                  ! term in yz
                  Sn   = factor_loc * epsilondev_yz(i,j,k,ispec)
                  Snp1   = factor_loc * epsilondev_yz_loc(i,j,k)
                  R_yz(i_sls,i,j,k,ispec) = alphaval_loc * R_yz(i_sls,i,j,k,ispec) + &
                                            betaval_loc * Sn + gammaval_loc * Snp1

               enddo

               ! define symmetric components of sigma
               sigma_yx = sigma_xy
               sigma_zx = sigma_xz
               sigma_zy = sigma_yz
               
                ! form dot product with test vector, non-symmetric form 
                tempx1(i,j,k) = jacobianl * (sigma_xx * xixl + sigma_yx * xiyl + sigma_zx * xizl) 
                tempy1(i,j,k) = jacobianl * (sigma_xy * xixl + sigma_yy * xiyl + sigma_zy * xizl) 
                tempz1(i,j,k) = jacobianl * (sigma_xz * xixl + sigma_yz * xiyl + sigma_zz * xizl) 

                tempx2(i,j,k) = jacobianl * (sigma_xx * etaxl + sigma_yx * etayl + sigma_zx * etazl) 
                tempy2(i,j,k) = jacobianl * (sigma_xy * etaxl + sigma_yy * etayl + sigma_zy * etazl) 
                tempz2(i,j,k) = jacobianl * (sigma_xz * etaxl + sigma_yz * etayl + sigma_zz * etazl) 
                
                tempx3(i,j,k) = jacobianl * (sigma_xx * gammaxl + sigma_yx * gammayl + sigma_zx * gammazl) 
                tempy3(i,j,k) = jacobianl * (sigma_xy * gammaxl + sigma_yy * gammayl + sigma_zy * gammazl) 
                tempz3(i,j,k) = jacobianl * (sigma_xz * gammaxl + sigma_yz * gammayl + sigma_zz * gammazl)

             end do
          end do
       end do


       ! second double-loop over GLL to compute all the terms
       do k=1,NGLLZ
          do j=1,NGLLY
             do i=1,NGLLX
           
                tempx1l = 0._CUSTOM_REAL
                tempy1l = 0._CUSTOM_REAL
                tempz1l = 0._CUSTOM_REAL
                
                tempx2l = 0._CUSTOM_REAL
                tempy2l = 0._CUSTOM_REAL
                tempz2l = 0._CUSTOM_REAL

                tempx3l = 0._CUSTOM_REAL
                tempy3l = 0._CUSTOM_REAL
                tempz3l = 0._CUSTOM_REAL

                ! we can merge these loops because NGLLX = NGLLY = NGLLZ
                do l=1,NGLLX
                   fac1 = hprimewgll_xx(l,i)
                   tempx1l = tempx1l + tempx1(l,j,k) * fac1
                   tempy1l = tempy1l + tempy1(l,j,k) * fac1
                   tempz1l = tempz1l + tempz1(l,j,k) * fac1

                   fac2 = hprimewgll_yy(l,j)
                   tempx2l = tempx2l + tempx2(i,l,k) * fac2
                   tempy2l = tempy2l + tempy2(i,l,k) * fac2
                   tempz2l = tempz2l + tempz2(i,l,k) * fac2

                   fac3 = hprimewgll_zz(l,k)
                   tempx3l = tempx3l + tempx3(i,j,l) * fac3
                   tempy3l = tempy3l + tempy3(i,j,l) * fac3
                   tempz3l = tempz3l + tempz3(i,j,l) * fac3
                enddo
                
                fac1 = wgllwgll_yz(j,k)
                fac2 = wgllwgll_xz(i,k)
                fac3 = wgllwgll_xy(i,j)
 
                ! sum contributions from each element to the global mesh using indirect addressing
                iglob = ibool_specfem(i,j,k,ispec)
                accel_specfem(1,iglob) = accel_specfem(1,iglob) - (fac1 * tempx1l + fac2 * tempx2l + fac3 * tempx3l)
                accel_specfem(2,iglob) = accel_specfem(2,iglob) - (fac1 * tempy1l + fac2 * tempy2l + fac3 * tempy3l)
                accel_specfem(3,iglob) = accel_specfem(3,iglob) - (fac1 * tempz1l + fac2 * tempz2l + fac3 * tempz3l)
               
             end do
          end do
       end do

       ! strore strain for updating memory variable
       epsilondev_trace(:,:,:,ispec) =  epsilondev_trace_loc(:,:,:)
       epsilondev_xx(:,:,:,ispec) =  epsilondev_xx_loc(:,:,:)
       epsilondev_yy(:,:,:,ispec) = epsilondev_yy_loc(:,:,:)
       epsilondev_xy(:,:,:,ispec) =  epsilondev_xy_loc(:,:,:)
       epsilondev_xz(:,:,:,ispec) =  epsilondev_xz_loc(:,:,:)
       epsilondev_yz(:,:,:,ispec) =  epsilondev_yz_loc(:,:,:)

    end do
    
  end subroutine compute_forces_iso_visco_elastic


end module solver_mod