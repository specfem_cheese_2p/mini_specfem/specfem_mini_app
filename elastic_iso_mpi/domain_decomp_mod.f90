module domain_decomp_mod

  use config_mod
  use mpi_mod
  use addressing_mod
 
  implicit none 
  
  integer, public  :: npx_dd, npy_dd
  integer, public  :: ipx_dd, ipy_dd
  integer, public  :: nbproc_dd
  integer, public, dimension(:),   allocatable   :: iproc_x_dd, iproc_y_dd
  integer, public, dimension(:,:), allocatable   :: addressing_dd

  integer, private :: myrank
  integer, private :: nb_interfaces_dd, nspec_interfaces_max_dd
  integer, private, allocatable, dimension(:)      :: my_neighbors_ext_dd, my_nelmnts_neighbors_ext_dd 
  integer, private, allocatable, dimension(:,:,:)  :: my_interfaces_ext_dd
  
  integer, private ::  max_nibool_interfaces_ext_dd
  integer, private, allocatable, dimension(:,:) :: ibool_interfaces_ext_dd
  integer, private, allocatable, dimension(:)   :: nibool_interfaces_ext_dd

  real(kind=CUSTOM_REAL), private, allocatable, dimension(:,:,:) :: mybuffer 
  real(kind=CUSTOM_REAL), private, allocatable, dimension(:,:,:) :: buffer_recv_vector_ext_dd, buffer_send_vector_ext_dd
  integer, private, allocatable, dimension(:) :: request_send_vector_ext_dd, request_recv_vector_ext_dd

  !! for non-blocking communications 
  integer, public, allocatable, dimension(:,:) :: phase_ispec_inner_elastic
  logical, public, allocatable, dimension(:)   :: ispec_is_inner
  integer, public :: num_phase_ispec_elastic, nspec_inner_elastic, nspec_outer_elastic
  
contains
  
  !!-----------------------------------------------------------------------------
  subroutine split_domain(NPX, NPY, myrank_mpi, nbproc_mpi)
    
    integer, intent(in) :: NPX, NPY, myrank_mpi, nbproc_mpi
    integer :: iproc_x, iproc_y, iprocnum

    if (myrank_mpi==0 .and. verbose) then
       write(*,*) '******************************************************************'
       write(*,*) "                      Cartesian Domain deomposition "
       write(*,*) 
       write(*,'( "   Nb domain in  X  direction  : ", i5,  "  Nb domain in Y direction  : ", i5    )')  NPX, NPY
       write(*,*) 
    end if
    
    npx_dd = NPX
    npy_dd = NPY
    nbproc_dd = nbproc_mpi
    ! create global slice addressing for mesher and solver
    allocate(iproc_x_dd(0:nbproc_dd-1), iproc_y_dd(0:nbproc_dd-1))
    allocate(addressing_dd(0:NPX-1,0:NPY-1))
    do iproc_y = 0, NPY - 1
       do iproc_x = 0, NPX - 1
          !write(*,*) iproc_x, iproc_y
          
          iprocnum = iproc_y * NPX + iproc_x
          !write(*,*) iprocnum
          
          iproc_x_dd(iprocnum) = iproc_x
          iproc_y_dd(iprocnum) = iproc_y

          addressing_dd(iproc_x,iproc_y) = iprocnum

       enddo
    enddo

    ipx_dd = iproc_x_dd(myrank_mpi)
    ipy_dd = iproc_y_dd(myrank_mpi)
    myrank = myrank_mpi
    
  end subroutine split_domain


!!---------------------------------------------------------------------------------------
  subroutine get_mpi_interfaces(iMPIcut_xi, iMPIcut_eta, ibool, nspec)

    integer, intent(in) :: nspec
    integer, dimension(2, 2, 2, nspec), intent(in) :: ibool
    logical, dimension(2, nspec), intent(in) :: iMPIcut_xi, iMPIcut_eta
    integer :: ie, ispec, nb_interfaces,  num_interface
    logical, dimension(8) ::  interfaces_dd
    integer, dimension(8) :: nspec_interface
    integer, parameter   :: W=1,E=2,S=3,N=4,NW=5,NE=6,SE=7,SW=8
    
    
    if ( npx_dd >= 2 .or. npy_dd  >= 2) then

       ! determines number of MPI interfaces for each slice
       nb_interfaces = 4
       interfaces_dd(W:N) = .true.
       interfaces_dd(NW:SW) = .false.

       ! slices at model boundaries
       if (ipx_dd == 0) then
          nb_interfaces =  nb_interfaces -1
          interfaces_dd(W) = .false.
       endif
       if (ipx_dd == npx_dd-1) then
          nb_interfaces =  nb_interfaces -1
          interfaces_dd(E) = .false.
       endif
       if (ipy_dd == 0) then
          nb_interfaces =  nb_interfaces -1
          interfaces_dd(S) = .false.
       endif
       if (ipy_dd == npy_dd-1) then
          nb_interfaces =  nb_interfaces -1
          interfaces_dd(N) = .false.
       endif
       
       ! slices in middle of model
       if ((interfaces_dd(W) .eqv. .true.) .and. (interfaces_dd(N) .eqv. .true.)) then
          interfaces_dd(NW) = .true.
          nb_interfaces =  nb_interfaces +1
       endif
       if ((interfaces_dd(N) .eqv. .true.) .and. (interfaces_dd(E) .eqv. .true.)) then
          interfaces_dd(NE) = .true.
          nb_interfaces =  nb_interfaces +1
       endif
       if ((interfaces_dd(E) .eqv. .true.) .and. (interfaces_dd(S) .eqv. .true.)) then
          interfaces_dd(SE) = .true.
          nb_interfaces =  nb_interfaces +1
       endif
       if ((interfaces_dd(W) .eqv. .true.) .and. (interfaces_dd(S) .eqv. .true.)) then
          interfaces_dd(SW) = .true.
          nb_interfaces =  nb_interfaces +1
       endif
       
       nspec_interface(:) = 0
       if (interfaces_dd(W))  nspec_interface(W) = count(iMPIcut_xi(1,:) .eqv. .true.)
       if (interfaces_dd(E))  nspec_interface(E) = count(iMPIcut_xi(2,:) .eqv. .true.)
       if (interfaces_dd(S))  nspec_interface(S) = count(iMPIcut_eta(1,:) .eqv. .true.)
       if (interfaces_dd(N))  nspec_interface(N) = count(iMPIcut_eta(2,:) .eqv. .true.)
       if (interfaces_dd(NW))  nspec_interface(NW) = count((iMPIcut_xi(1,:) .eqv. .true.) .and. (iMPIcut_eta(2,:) .eqv. .true.))
       if (interfaces_dd(NE))  nspec_interface(NE) = count((iMPIcut_xi(2,:) .eqv. .true.) .and. (iMPIcut_eta(2,:) .eqv. .true.))
       if (interfaces_dd(SE))  nspec_interface(SE) = count((iMPIcut_xi(2,:) .eqv. .true.) .and. (iMPIcut_eta(1,:) .eqv. .true.))
       if (interfaces_dd(SW))  nspec_interface(SW) = count((iMPIcut_xi(1,:) .eqv. .true.) .and. (iMPIcut_eta(1,:) .eqv. .true.))

       
       nb_interfaces_dd = nb_interfaces
       nspec_interfaces_max_dd = maxval(nspec_interface)

       allocate(my_neighbors_ext_dd(nb_interfaces_dd))
       allocate(my_nelmnts_neighbors_ext_dd(nb_interfaces_dd))
       allocate(my_interfaces_ext_dd(6, nspec_interfaces_max_dd,nb_interfaces_dd)) 

       num_interface = 0
       
       if (interfaces_dd(W)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd-1,ipy_dd)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(W)
          ie=0
          do ispec = 1, nspec
             if (iMPIcut_xi(1,ispec)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 4
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(1,1,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(1,2,1,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = ibool(1,1,2,ispec)
                my_interfaces_ext_dd(6,ie,num_interface) = ibool(1,2,2,ispec)
                !flag_element_debug(ispec) = W
             end if
          end do
       endif
       
       if (interfaces_dd(E)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd+1,ipy_dd)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(E)
          ie=0
          do ispec = 1, nspec
             if (iMPIcut_xi(2,ispec)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 4
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(2,1,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(2,2,1,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = ibool(2,1,2,ispec)
                my_interfaces_ext_dd(6,ie,num_interface) = ibool(2,2,2,ispec)
                !flag_element_debug(ispec) = E
             end if
          end do
       endif

       if (interfaces_dd(S)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd,ipy_dd-1)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(S)
          ie=0
          do ispec = 1,nspec
             if (iMPIcut_eta(1,ispec)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 4
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(1,1,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(2,1,1,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = ibool(1,1,2,ispec)
                my_interfaces_ext_dd(6,ie,num_interface) = ibool(2,1,2,ispec)
                !flag_element_debug(ispec) = S
             end if
          end do
       endif

       if (interfaces_dd(N)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd,ipy_dd+1)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(N)
          ie=0
          do ispec = 1,nspec
             if (iMPIcut_eta(2,ispec)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 4
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(2,2,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(1,2,1,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = ibool(2,2,2,ispec)
                my_interfaces_ext_dd(6,ie,num_interface) = ibool(1,2,2,ispec)
                !flag_element_debug(ispec) = N
             end if
          end do
       end if

       if (interfaces_dd(NW)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd-1,ipy_dd+1)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(NW)
          ie=0
          do ispec = 1,nspec
             if ((iMPIcut_xi(1,ispec) .eqv. .true.) .and. (iMPIcut_eta(2,ispec) .eqv. .true.)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 2
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(1,2,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(1,2,2,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = -1
                my_interfaces_ext_dd(6,ie,num_interface) = -1
                !flag_element_debug(ispec) = NW
             end if
          end do
       end if

       if (interfaces_dd(NE)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd+1,ipy_dd+1)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(NE)
          ie=0
          do ispec = 1,nspec
             if ((iMPIcut_xi(2,ispec) .eqv. .true.) .and. (iMPIcut_eta(2,ispec) .eqv. .true.)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 2
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(2,2,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(2,2,2,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = -1
                my_interfaces_ext_dd(6,ie,num_interface) = -1
                !flag_element_debug(ispec) = NE
             end if
          end do
       end if

       if (interfaces_dd(SE)) then
          num_interface = num_interface + 1
          my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd+1,ipy_dd-1)
          my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(SE)
          ie=0
          do ispec = 1,nspec
             if ((iMPIcut_xi(2,ispec) .eqv. .true.) .and. (iMPIcut_eta(1,ispec) .eqv. .true.)) then
                ie=ie+1
                my_interfaces_ext_dd(1,ie,num_interface) = ispec
                my_interfaces_ext_dd(2,ie,num_interface) = 2
                my_interfaces_ext_dd(3,ie,num_interface) = ibool(2,1,1,ispec)
                my_interfaces_ext_dd(4,ie,num_interface) = ibool(2,1,2,ispec)
                my_interfaces_ext_dd(5,ie,num_interface) = -1
                my_interfaces_ext_dd(6,ie,num_interface) = -1
                !flag_element_debug(ispec) = SE
             end if
          end do
       end if

       if (interfaces_dd(SW)) then
           num_interface = num_interface + 1
           my_neighbors_ext_dd(num_interface) = addressing_dd(ipx_dd-1,ipy_dd-1)
           my_nelmnts_neighbors_ext_dd(num_interface) = nspec_interface(SW)
           ie=0
           do ispec = 1,nspec
              if ((iMPIcut_xi(1,ispec) .eqv. .true.) .and. (iMPIcut_eta(1,ispec) .eqv. .true.)) then
                 ie=ie+1
                 my_interfaces_ext_dd(1,ie,num_interface) = ispec
                 my_interfaces_ext_dd(2,ie,num_interface) = 2
                 my_interfaces_ext_dd(3,ie,num_interface) = ibool(1,1,1,ispec)
                 my_interfaces_ext_dd(4,ie,num_interface) = ibool(1,1,2,ispec)
                 my_interfaces_ext_dd(5,ie,num_interface) = -1
                 my_interfaces_ext_dd(6,ie,num_interface) = -1
                 !flag_element_debug(ispec) = SW
              endif
           enddo
        endif
        
    else
       
       nb_interfaces_dd = 0
       nspec_interfaces_max_dd = 0
       interfaces_dd(:) = .false.
       allocate(my_neighbors_ext_dd(nb_interfaces_dd))
       allocate(my_nelmnts_neighbors_ext_dd(nb_interfaces_dd))
       allocate(my_interfaces_ext_dd(6, nspec_interfaces_max_dd,nb_interfaces_dd))
       
    end if
    
  end subroutine get_mpi_interfaces
  
  !!---------------------------------------------------------------------------------------  
  subroutine setup_mpi_comm(xdof_specfem, ibool_specfem, ibool_meshfem, nspec_specfem, nglob_specfem)

    integer, intent(in) :: nspec_specfem, nglob_specfem
    integer, dimension(NGLLX, NGLLY, NGLLZ, nspec_specfem), intent(in) :: ibool_specfem
    integer, dimension(2, 2, 2, nspec_specfem),   intent(in) :: ibool_meshfem
    double precision, dimension(3,nglob_specfem), intent(in) :: xdof_specfem
    
    ! for MPI buffers
    integer, dimension(:), allocatable :: locval
    integer, dimension(:), allocatable :: reorder_interface_ext_mesh, ninseg_ext_mesh
    logical, dimension(:), allocatable :: ifseg
    double precision, dimension(:), allocatable :: xp,yp,zp
    integer :: num_points1, num_points2, iinterface, nsize, ilocnum
    integer :: nibool_interfaces_ext_mesh_true, index
    double precision ::  typical_mesh_size, SMALLVALTOL
    integer :: countval,inum,ier, ispec, i,j,k, iglob
    integer,dimension(:),allocatable :: test_flag
    real(kind=CUSTOM_REAL), dimension(:),allocatable :: test_flag_cr
    integer, dimension(:,:), allocatable :: ibool_interfaces_dummy
    !character(len=CHAR_LEN) :: prname_debug
    
    !! typical reference size   
    typical_mesh_size =  min(zmax_config-zmin_config, &
         min(xmax_config - xmin_config, ymax_config - ymin_config))
    !define geometrical tolerance based upon typical size of the model
    SMALLVALTOL = 1.d-10 * typical_mesh_size

    
    !write(*,*) " setup mpi comm ", SMALLVALTOL
    call prepare_assemble_MPI(ibool_specfem, ibool_meshfem, nspec_specfem, nglob_specfem)
    
    ! sorts ibool comm buffers lexicographically for all MPI interfaces
    num_points1 = 0
    num_points2 = 0
    do iinterface = 1, nb_interfaces_dd

       nsize = nibool_interfaces_ext_dd(iinterface)
       allocate(xp(nsize), yp(nsize), zp(nsize))
       allocate(locval(nsize))
       allocate(ifseg(nibool_interfaces_ext_dd(iinterface)))
       allocate(reorder_interface_ext_mesh(nsize))
       allocate(ninseg_ext_mesh(nsize))
      
       ! gets x,y,z coordinates of global points on MPI interface
       do ilocnum = 1, nsize
          index = ibool_interfaces_ext_dd(ilocnum,iinterface)
          xp(ilocnum) = xdof_specfem(1,index)
          yp(ilocnum) = xdof_specfem(2,index)
          zp(ilocnum) = xdof_specfem(3,index)
       end do

       call sort_array_coordinates(nsize, xp,yp,zp,  &
            ibool_interfaces_ext_dd(1:nsize,iinterface),&
            reorder_interface_ext_mesh,locval,ifseg, &
            nibool_interfaces_ext_mesh_true, &
            ninseg_ext_mesh, SMALLVALTOL)
       
        deallocate(xp)
        deallocate(yp)
        deallocate(zp)
        deallocate(locval)
        deallocate(ifseg)
        deallocate(reorder_interface_ext_mesh)
        deallocate(ninseg_ext_mesh)

        ! checks that number of MPI points are still the same
        num_points1 = num_points1 + nsize
        num_points2 = num_points2 + nibool_interfaces_ext_mesh_true
        if (num_points1 /= num_points2) then
           write(*,*) ' error sorting MPI interface points:'
           write(*,*) '   interface:',iinterface,num_points1,num_points2
           stop
        endif

     end do

     
     ! outputs total number of MPI interface points
     call sum_all_i(num_points2,ilocnum)
     if (myrank_mpi == 0 .and. verbose) then
        write(*,*)
        write(*,*) '*********************************************'
        write(*,*) '*** Verification of of MPI communication ***'
        write(*,*) '     total MPI interface points: ',ilocnum
     endif

     ! checks with assembly of test fields
     allocate(test_flag(nglob_specfem),test_flag_cr(nglob_specfem),stat=ier)
     if (ier /= 0) stop 'error allocating array test_flag etc.'
     
     test_flag(:) = 0
     test_flag_cr(:) = 0._CUSTOM_REAL
     countval = 0
     do ispec = 1, nspec_specfem
        ! sets flags on global points
        do k = 1, NGLLZ
           do j = 1, NGLLY
              do i = 1, NGLLX
                 ! global index
                 iglob = ibool_specfem(i,j,k,ispec)
                 
                 ! counts number of unique global points to set
                 if (test_flag(iglob) == 0) countval = countval + 1
                 
                 ! sets identifier
                 test_flag(iglob) = myrank + 1
                 test_flag_cr(iglob) = myrank + 1.0
              enddo
           enddo
        enddo
     enddo

     call synchronize_all()
     
     ! collects contributions from different MPI partitions
     ! sets up MPI communications
     max_nibool_interfaces_ext_dd = maxval(  nibool_interfaces_ext_dd(:) )
     allocate(ibool_interfaces_dummy(max_nibool_interfaces_ext_dd,nb_interfaces_dd),stat=ier)
     if (ier /= 0) stop 'error allocating array ibool_interfaces_dummy'
     
     countval = 0
     do iinterface = 1, nb_interfaces_dd
        ibool_interfaces_dummy(:,iinterface) = &
             ibool_interfaces_ext_dd(1:max_nibool_interfaces_ext_dd,iinterface)
        countval = countval + nibool_interfaces_ext_dd(iinterface)
     enddo
     call synchronize_all()

     call sum_all_i(countval,iglob)
     if (myrank_mpi == 0) then
        if (iglob /= ilocnum) stop 'error total global MPI interface points'
     endif
     
     allocate(buffer_recv_vector_ext_dd(NDIM,max_nibool_interfaces_ext_dd,nb_interfaces_dd))
     allocate(buffer_send_vector_ext_dd(NDIM,max_nibool_interfaces_ext_dd,nb_interfaces_dd))
     allocate(mybuffer(NDIM,max_nibool_interfaces_ext_dd,nb_interfaces_dd))
     allocate(request_send_vector_ext_dd(nb_interfaces_dd))
     allocate(request_recv_vector_ext_dd(nb_interfaces_dd))

     ! adds contributions from different partitions to flag arrays
     ! integer arrays
     call assemble_MPI_scalar_i_blocking(nbproc_dd,nglob_specfem,test_flag, &
          nb_interfaces_dd,max_nibool_interfaces_ext_dd, &
          nibool_interfaces_ext_dd,ibool_interfaces_dummy, &
          my_neighbors_ext_dd)

     ! CUSTOM_REAL arrays
!!$     call assemble_MPI_scalar_blocking(nbproc_dd, nglob_specfem, test_flag_cr, &
!!$          nb_interfaces_dd, max_nibool_interfaces_ext_dd, &
!!$          nibool_interfaces_ext_dd, ibool_interfaces_dummy, &
!!$          my_neighbors_ext_dd)

     call assemble_MPI_scalar_blocking(nglob_specfem, test_flag_cr)
     
     ! checks number of interface points
     i = 0
     j = 0
     do iglob=1,nglob_specfem
        ! only counts flags with MPI contributions
        if (test_flag(iglob) > myrank+1) i = i + 1
        if (test_flag_cr(iglob) > myrank+1.0) j = j + 1
     enddo
     call sum_all_i(i,inum)
     call sum_all_i(j,iglob)
     if (myrank == 0  .and. verbose) then
        write(*,*) '     total assembled MPI interface points:',inum
        if (inum /= iglob .or. inum > ilocnum) stop 'error MPI assembly'
        write(*,*) '*********************************************'
        write(*,*)
     endif
     
   

     deallocate(test_flag)
     deallocate(test_flag_cr)
     deallocate(ibool_interfaces_dummy)

     call setup_inner_outer_element(ibool_specfem, nspec_specfem, nglob_specfem)
     
   end subroutine setup_mpi_comm

  
  !!---------------------------------------------------------------------------------------
  subroutine prepare_assemble_MPI(ibool_specfem, ibool_meshfem, nspec_specfem, nglob_specfem)

    integer, intent(in) :: nspec_specfem, nglob_specfem
    integer, dimension(NGLLX, NGLLY, NGLLZ, nspec_specfem), intent(in) :: ibool_specfem
    integer, dimension(2, 2, 2, nspec_specfem),   intent(in) :: ibool_meshfem
    integer, dimension(:,:), allocatable :: knods
    logical, dimension(:),   allocatable :: mask_ibool_ext_mesh
    integer :: k, ispec, iglob, num_interface, ispec_interface
    integer :: e1, e2, e3, e4, itype, npoin_interface_ext_mesh
    integer  :: ixmin, ixmax, iymin, iymax, izmin, izmax
    integer :: ix, iy, iz
    integer, dimension(8)  :: n
    
    ! initializes
    allocate(knods(8, nspec_specfem))
    allocate(mask_ibool_ext_mesh(nglob_specfem))
    
    do ispec = 1, nspec_specfem
       knods(1,ispec) = ibool_meshfem(1,1,1,ispec)
       knods(2,ispec) = ibool_meshfem(2,1,1,ispec)
       knods(3,ispec) = ibool_meshfem(2,2,1,ispec)
       knods(4,ispec) = ibool_meshfem(1,2,1,ispec)
       knods(5,ispec) = ibool_meshfem(1,1,2,ispec)
       knods(6,ispec) = ibool_meshfem(2,1,2,ispec)
       knods(7,ispec) = ibool_meshfem(2,2,2,ispec)
       knods(8,ispec) = ibool_meshfem(1,2,2,ispec)
    end do
    
    allocate(ibool_interfaces_ext_dd(NGLLX*NGLLY*nspec_interfaces_max_dd,nb_interfaces_dd))
    allocate(nibool_interfaces_ext_dd(nb_interfaces_dd))
    
    ibool_interfaces_ext_dd(:,:) = 0
    nibool_interfaces_ext_dd(:) = 0


    ! loops over MPI interfaces
    do num_interface = 1, nb_interfaces_dd
       npoin_interface_ext_mesh = 0
       mask_ibool_ext_mesh(:) = .false.

       ! loops over number of elements on interface
       do ispec_interface = 1, my_nelmnts_neighbors_ext_dd(num_interface)
          ! spectral element on interface
          ispec = my_interfaces_ext_dd(1,ispec_interface,num_interface)
          ! type of interface: (1) corner point, (2) edge, (4) face
          itype = my_interfaces_ext_dd(2,ispec_interface,num_interface)
          ! gets spectral element corner indices  (defines all nodes of face/edge)
          do k = 1, 8
             n(k) = knods(k,ispec)
          enddo
          
          ! interface node ids
          e1 = my_interfaces_ext_dd(3,ispec_interface,num_interface)
          e2 = my_interfaces_ext_dd(4,ispec_interface,num_interface)
          e3 = my_interfaces_ext_dd(5,ispec_interface,num_interface)
          e4 = my_interfaces_ext_dd(6,ispec_interface,num_interface)

          ! gets i,j,k ranges for interface type
          call get_edge(n, itype, e1, e2, e3, e4, &
               ixmin, ixmax, iymin, iymax, izmin, izmax)
          
          ! counts number and stores indices of (global) points on MPI interface
          do iz = min(izmin,izmax), max(izmin,izmax)
             do iy = min(iymin,iymax), max(iymin,iymax)
                do ix = min(ixmin,ixmax), max(ixmin,ixmax)
                   ! global index
                   iglob = ibool_specfem(ix,iy,iz,ispec)
                   
                   ! stores global index of point on interface
                   if (.not. mask_ibool_ext_mesh(iglob)) then
                      ! masks point as being accounted for
                      mask_ibool_ext_mesh(iglob) = .true.
                      ! adds point to interface
                      npoin_interface_ext_mesh = npoin_interface_ext_mesh + 1
                      ibool_interfaces_ext_dd(npoin_interface_ext_mesh ,num_interface) = iglob
                   endif
                enddo
             enddo
          enddo
          
       enddo

       ! stores total number of (global) points on this MPI interface
       nibool_interfaces_ext_dd(num_interface) = npoin_interface_ext_mesh
       
    enddo

    deallocate( mask_ibool_ext_mesh )
    deallocate(knods)

    
  end subroutine prepare_assemble_MPI


  !
  !----
  !

  subroutine get_edge ( n, itype, e1, e2, e3, e4, &
       ixmin, ixmax, iymin, iymax, izmin, izmax )

    ! returns range of local (GLL) point indices i,j,k depending on given type
    ! for corner point (1), edge (2) or face (4)

    ! corner node indices per spectral element (8)
    integer, dimension(8), intent(in)  :: n
    
    ! interface type & nodes
    integer, intent(in)  :: itype, e1, e2, e3, e4

    ! local (GLL) i,j,k index ranges
    integer, intent(out)  :: ixmin, ixmax, iymin, iymax, izmin, izmax

    ! local parameters
    integer, dimension(4) :: en
    integer :: valence, i

    ! determines local indexes for corners/edges/faces
    if (itype == 1) then
       
       ! corner point
       
       if (e1 == n(1)) then
          ixmin = 1
          ixmax = 1
          iymin = 1
          iymax = 1
          izmin = 1
          izmax = 1
       endif
       if (e1 == n(2)) then
          ixmin = NGLLX
          ixmax = NGLLX
          iymin = 1
          iymax = 1
          izmin = 1
          izmax = 1
       endif
       if (e1 == n(3)) then
          ixmin = NGLLX
          ixmax = NGLLX
          iymin = NGLLY
          iymax = NGLLY
          izmin = 1
          izmax = 1
       endif
       if (e1 == n(4)) then
          ixmin = 1
          ixmax = 1
          iymin = NGLLY
          iymax = NGLLY
          izmin = 1
          izmax = 1
       endif
       if (e1 == n(5)) then
          ixmin = 1
          ixmax = 1
          iymin = 1
          iymax = 1
          izmin = NGLLZ
          izmax = NGLLZ
       endif
       if (e1 == n(6)) then
          ixmin = NGLLX
          ixmax = NGLLX
          iymin = 1
          iymax = 1
          izmin = NGLLZ
          izmax = NGLLZ
       endif
       if (e1 == n(7)) then
          ixmin = NGLLX
          ixmax = NGLLX
          iymin = NGLLY
          iymax = NGLLY
          izmin = NGLLZ
          izmax = NGLLZ
       endif
       if (e1 == n(8)) then
          ixmin = 1
          ixmax = 1
          iymin = NGLLY
          iymax = NGLLY
          izmin = NGLLZ
          izmax = NGLLZ
       endif
       
    else if (itype == 2) then
       
       ! edges
       
       if (e1 == n(1)) then
          ixmin = 1
          iymin = 1
          izmin = 1
          if (e2 == n(2)) then
             ixmax = NGLLX
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(4)) then
             ixmax = 1
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(5)) then
             ixmax = 1
             iymax = 1
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(2)) then
          ixmin = NGLLX
          iymin = 1
          izmin = 1
          if (e2 == n(3)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(1)) then
             ixmax = 1
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(6)) then
             ixmax = NGLLX
             iymax = 1
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(3)) then
          ixmin = NGLLX
          iymin = NGLLY
          izmin = 1
          if (e2 == n(4)) then
             ixmax = 1
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(2)) then
             ixmax = NGLLX
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(7)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(4)) then
          ixmin = 1
          iymin = NGLLY
          izmin = 1
          if (e2 == n(1)) then
             ixmax = 1
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(3)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(8)) then
             ixmax = 1
             iymax = NGLLY
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(5)) then
          ixmin = 1
          iymin = 1
          izmin = NGLLZ
          if (e2 == n(1)) then
             ixmax = 1
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(6)) then
             ixmax = NGLLX
             iymax = 1
             izmax = NGLLZ
          endif
          if (e2 == n(8)) then
             ixmax = 1
             iymax = NGLLY
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(6)) then
          ixmin = NGLLX
          iymin = 1
          izmin = NGLLZ
          if (e2 == n(2)) then
             ixmax = NGLLX
             iymax = 1
             izmax = 1
          endif
          if (e2 == n(7)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = NGLLZ
          endif
          if (e2 == n(5)) then
             ixmax = 1
             iymax = 1
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(7)) then
          ixmin = NGLLX
          iymin = NGLLY
          izmin = NGLLZ
          if (e2 == n(3)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(8)) then
             ixmax = 1
             iymax = NGLLY
             izmax = NGLLZ
          endif
          if (e2 == n(6)) then
             ixmax = NGLLX
             iymax = 1
             izmax = NGLLZ
          endif
       endif
       if (e1 == n(8)) then
          ixmin = 1
          iymin = NGLLY
          izmin = NGLLZ
          if (e2 == n(4)) then
             ixmax = 1
             iymax = NGLLY
             izmax = 1
          endif
          if (e2 == n(5)) then
             ixmax = 1
             iymax = 1
             izmax = NGLLZ
          endif
          if (e2 == n(7)) then
             ixmax = NGLLX
             iymax = NGLLY
             izmax = NGLLZ
          endif
       endif
       
    else if (itype == 4) then
       
       ! face corners
       
       en(1) = e1
       en(2) = e2
       en(3) = e3
       en(4) = e4
       
       ! zmin face
       valence = 0
       do i = 1, 4
          if (en(i) == n(1)) then
             valence = valence+1
          endif
          if (en(i) == n(2)) then
             valence = valence+1
          endif
          if (en(i) == n(3)) then
             valence = valence+1
          endif
          if (en(i) == n(4)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = 1
          iymin = 1
          izmin = 1
          ixmax = NGLLX
          iymax = NGLLY
          izmax = 1
       endif
       
       ! ymin face
       valence = 0
       do i = 1, 4
          if (en(i) == n(1)) then
             valence = valence+1
          endif
          if (en(i) == n(2)) then
             valence = valence+1
          endif
          if (en(i) == n(5)) then
             valence = valence+1
          endif
          if (en(i) == n(6)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = 1
          iymin = 1
          izmin = 1
          ixmax = NGLLX
          iymax = 1
          izmax = NGLLZ
       endif
       
       ! xmax face
       valence = 0
       do i = 1, 4
          if (en(i) == n(2)) then
             valence = valence+1
          endif
          if (en(i) == n(3)) then
             valence = valence+1
          endif
          if (en(i) == n(6)) then
             valence = valence+1
          endif
          if (en(i) == n(7)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = NGLLX
          iymin = 1
          izmin = 1
          ixmax = NGLLX
          iymax = NGLLZ
          izmax = NGLLZ
       endif
       
       ! ymax face
       valence = 0
       do i = 1, 4
          if (en(i) == n(3)) then
             valence = valence+1
          endif
          if (en(i) == n(4)) then
             valence = valence+1
          endif
          if (en(i) == n(7)) then
             valence = valence+1
          endif
          if (en(i) == n(8)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = 1
          iymin = NGLLY
          izmin = 1
          ixmax = NGLLX
          iymax = NGLLY
          izmax = NGLLZ
       endif
       
       ! xmin face
       valence = 0
       do i = 1, 4
          if (en(i) == n(1)) then
             valence = valence+1
          endif
          if (en(i) == n(4)) then
             valence = valence+1
          endif
          if (en(i) == n(5)) then
             valence = valence+1
          endif
          if (en(i) == n(8)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = 1
          iymin = 1
          izmin = 1
          ixmax = 1
          iymax = NGLLY
          izmax = NGLLZ
       endif
       
       ! zmax face
       valence = 0
       do i = 1, 4
          if (en(i) == n(5)) then
             valence = valence+1
          endif
          if (en(i) == n(6)) then
             valence = valence+1
          endif
          if (en(i) == n(7)) then
             valence = valence+1
          endif
          if (en(i) == n(8)) then
             valence = valence+1
          endif
       enddo
       if (valence == 4) then
          ixmin = 1
          iymin = 1
          izmin = NGLLZ
          ixmax = NGLLX
          iymax = NGLLY
          izmax = NGLLZ
       endif
       
    else
       stop 'ERROR get_edge'
    endif
    
    
  end subroutine get_edge


  !
  !------------------------------------------------------------------------------------------------
  !

  subroutine setup_inner_outer_element(ibool, nspec, nglob)

    integer, intent(in) :: nspec, nglob
    integer, dimension(NGLLX, NGLLY, NGLLZ, nspec), intent(in) :: ibool

    integer :: ier, iinterface, i, j,k, ispec, iglob, ispec_inner, ispec_outer
    logical, dimension(:), allocatable :: iglob_is_inner
    
    allocate(ispec_is_inner(nspec))
    allocate(iglob_is_inner(nglob))
    
    ! initialize flags
    ispec_is_inner(:) = .true.
    iglob_is_inner(:) = .true.
    do iinterface = 1, nb_interfaces_dd
       do i = 1, nibool_interfaces_ext_dd(iinterface)
          iglob = ibool_interfaces_ext_dd(i,iinterface)
          iglob_is_inner(iglob) = .false.
       enddo
    enddo
    
    ! determines flags for inner elements (purely inside the partition)
    do ispec = 1, nspec
       do k = 1, NGLLZ
          do j = 1, NGLLY
             do i = 1, NGLLX
                iglob = ibool(i,j,k,ispec)
                ispec_is_inner(ispec) = ( iglob_is_inner(iglob) .and. ispec_is_inner(ispec) )
             enddo
          enddo
       enddo
    enddo
   
    
    ! counts inner and outer elements
    do ispec = 1, nspec

       if (ispec_is_inner(ispec) .eqv. .true.) then
          nspec_inner_elastic = nspec_inner_elastic + 1
       else
          nspec_outer_elastic = nspec_outer_elastic + 1
       endif
       
    enddo


    ! stores indices of inner and outer elements
    num_phase_ispec_elastic = max(nspec_inner_elastic,nspec_outer_elastic)
    if (num_phase_ispec_elastic < 0) stop 'error elastic simulation: num_phase_ispec_elastic is < zero'

    allocate( phase_ispec_inner_elastic(num_phase_ispec_elastic,2),stat=ier)
    phase_ispec_inner_elastic(:,:) = 0

    ispec_inner = 0
    ispec_outer = 0
    do ispec = 1, nspec
       if (ispec_is_inner(ispec) .eqv. .true.) then
          ispec_inner = ispec_inner + 1
          phase_ispec_inner_elastic(ispec_inner,2) = ispec
       else
          ispec_outer = ispec_outer + 1
          phase_ispec_inner_elastic(ispec_outer,1) = ispec
       endif
    enddo
    
    deallocate( iglob_is_inner)

    
  end subroutine setup_inner_outer_element
  
  !!
  !!###############################################################################################
  !!
  subroutine assemble_MPI_scalar_blocking(NGLOB_AB,array_val)
   
   ! assembles scalar field in a blocking way, returns only after values have been assembled

    implicit none

    !integer :: NPROC
    integer :: NGLOB_AB

    ! array to assemble
    real(kind=CUSTOM_REAL), dimension(NGLOB_AB) :: array_val
    real(kind=CUSTOM_REAL), dimension(:,:), allocatable :: buffer_send_scalar
    real(kind=CUSTOM_REAL), dimension(:,:), allocatable :: buffer_recv_scalar
    integer, dimension(:), allocatable :: request_send_scalar
    integer, dimension(:), allocatable :: request_recv_scalar

    
    integer ipoin,iinterface,ier

    ! here we have to assemble all the contributions between partitions using MPI
    
    ! assemble only if more than one partition
    if (nbproc_dd > 1) then
       
       allocate(buffer_send_scalar(max_nibool_interfaces_ext_dd,nb_interfaces_dd),stat=ier)
       if (ier /= 0) stop 'error allocating array buffer_send_scalar'
       allocate(buffer_recv_scalar(max_nibool_interfaces_ext_dd,nb_interfaces_dd),stat=ier)
       if (ier /= 0) stop 'error allocating array buffer_recv_scalar'
       allocate(request_send_scalar(nb_interfaces_dd),stat=ier)
       if (ier /= 0) stop 'error allocating array request_send_scalar'
       allocate(request_recv_scalar(nb_interfaces_dd),stat=ier)
       if (ier /= 0) stop 'error allocating array request_recv_scalar'

       ! partition border copy into the buffer
       do iinterface = 1, nb_interfaces_dd
          do ipoin = 1, nibool_interfaces_ext_dd(iinterface)
             buffer_send_scalar(ipoin,iinterface) = array_val(ibool_interfaces_ext_dd(ipoin,iinterface))
          enddo
       enddo
    
       ! send messages
       do iinterface = 1, nb_interfaces_dd
          ! non-blocking synchronous send request
          call isend_cr(buffer_send_scalar(1:nibool_interfaces_ext_dd(iinterface),iinterface), &
               nibool_interfaces_ext_dd(iinterface), &
               my_neighbors_ext_dd(iinterface), &
               itag, &
               request_send_scalar(iinterface))

          ! receive request
          call irecv_cr(buffer_recv_scalar(1:nibool_interfaces_ext_dd(iinterface),iinterface), &
               nibool_interfaces_ext_dd(iinterface), &
               my_neighbors_ext_dd(iinterface), &
               itag, &
               request_recv_scalar(iinterface))
          
       enddo
       call synchronize_all()
       
       ! wait for communications completion (recv)
       do iinterface = 1, nb_interfaces_dd
          call wait_req(request_recv_scalar(iinterface))
       enddo
       
       ! adding contributions of neighbors
       do iinterface = 1, nb_interfaces_dd
          do ipoin = 1, nibool_interfaces_ext_dd(iinterface)
             array_val(ibool_interfaces_ext_dd(ipoin,iinterface)) = &
                  array_val(ibool_interfaces_ext_dd(ipoin,iinterface)) + buffer_recv_scalar(ipoin,iinterface)
          enddo
       enddo
       
       ! wait for communications completion (send)
       do iinterface = 1, nb_interfaces_dd
          call wait_req(request_send_scalar(iinterface))
       enddo
       
       deallocate(buffer_send_scalar)
       deallocate(buffer_recv_scalar)
       deallocate(request_send_scalar)
       deallocate(request_recv_scalar)

    endif
    
    call synchronize_all()

  end subroutine assemble_MPI_scalar_blocking

!
!
!----
!

  subroutine assemble_MPI_scalar_i_blocking(NPROC,NGLOB_AB,array_val, &
       num_interfaces_ext_mesh,max_nibool_interfaces_ext_mesh, &
       nibool_interfaces_ext_mesh,ibool_interfaces_ext_mesh, &
       my_neighbors_ext_mesh)
  
  
    implicit none
    
    integer :: NPROC
    integer :: NGLOB_AB

    ! array to assemble
    integer, dimension(NGLOB_AB) :: array_val
    
    integer :: num_interfaces_ext_mesh,max_nibool_interfaces_ext_mesh
    integer, dimension(num_interfaces_ext_mesh) :: nibool_interfaces_ext_mesh,my_neighbors_ext_mesh
    integer, dimension(max_nibool_interfaces_ext_mesh,num_interfaces_ext_mesh) :: ibool_interfaces_ext_mesh
    
    integer, dimension(:,:), allocatable :: buffer_send_scalar
    integer, dimension(:,:), allocatable :: buffer_recv_scalar
    integer, dimension(:), allocatable :: request_send_scalar
    integer, dimension(:), allocatable :: request_recv_scalar
    
    integer :: ipoin,iinterface,ier
    
    ! here we have to assemble all the contributions between partitions using MPI
    
    ! assemble only if more than one partition
    if (NPROC > 1) then
       
       allocate(buffer_send_scalar(max_nibool_interfaces_ext_mesh,num_interfaces_ext_mesh),stat=ier)
       if (ier /= 0) stop 'error allocating array buffer_send_scalar'
       allocate(buffer_recv_scalar(max_nibool_interfaces_ext_mesh,num_interfaces_ext_mesh),stat=ier)
       if (ier /= 0) stop 'error allocating array buffer_recv_scalar'
       allocate(request_send_scalar(num_interfaces_ext_mesh),stat=ier)
       if (ier /= 0) stop 'error allocating array request_send_scalar'
       allocate(request_recv_scalar(num_interfaces_ext_mesh),stat=ier)
       if (ier /= 0) stop 'error allocating array request_recv_scalar'

       ! partition border copy into the buffer
       do iinterface = 1, num_interfaces_ext_mesh
          do ipoin = 1, nibool_interfaces_ext_mesh(iinterface)
             buffer_send_scalar(ipoin,iinterface) = array_val(ibool_interfaces_ext_mesh(ipoin,iinterface))
          enddo
       enddo
       
       ! send messages
       do iinterface = 1, num_interfaces_ext_mesh
          ! non-blocking synchronous send request
          call isend_i(buffer_send_scalar(1:nibool_interfaces_ext_mesh(iinterface),iinterface), &
               nibool_interfaces_ext_mesh(iinterface), &
               my_neighbors_ext_mesh(iinterface), &
               itag, &
               request_send_scalar(iinterface))
          ! receive request
          call irecv_i(buffer_recv_scalar(1:nibool_interfaces_ext_mesh(iinterface),iinterface), &
               nibool_interfaces_ext_mesh(iinterface), &
               my_neighbors_ext_mesh(iinterface), &
               itag, &
               request_recv_scalar(iinterface))
       enddo
       
       ! wait for communications completion
       do iinterface = 1, num_interfaces_ext_mesh
          call wait_req(request_recv_scalar(iinterface))
       enddo
       
       ! adding contributions of neighbors
       do iinterface = 1, num_interfaces_ext_mesh
          do ipoin = 1, nibool_interfaces_ext_mesh(iinterface)
             array_val(ibool_interfaces_ext_mesh(ipoin,iinterface)) = &
                  array_val(ibool_interfaces_ext_mesh(ipoin,iinterface)) + buffer_recv_scalar(ipoin,iinterface)
          enddo
       enddo
       
       ! wait for communications completion (send)
       do iinterface = 1, num_interfaces_ext_mesh
          call wait_req(request_send_scalar(iinterface))
       enddo
       
       deallocate(buffer_send_scalar)
       deallocate(buffer_recv_scalar)
       deallocate(request_send_scalar)
       deallocate(request_recv_scalar)
       
    endif
    
  end subroutine assemble_MPI_scalar_i_blocking
!
!------------------------------------------------------------------------------------------------------------------------------
!
  
  subroutine assemble_MPI_vector_async_send(NGLOB_AB,array_val)
    
    integer, intent(in) :: NGLOB_AB
    real(kind=CUSTOM_REAL), dimension(NDIM, NGLOB_AB), intent(inout) :: array_val

    integer :: iinterface, ipoin
    
    if (nbproc_dd > 1) then
       
       ! partition border copy into the buffer
       do iinterface = 1, nb_interfaces_dd
          do ipoin = 1, nibool_interfaces_ext_dd(iinterface)
             buffer_send_vector_ext_dd(:,ipoin,iinterface) = &
                  array_val(:,ibool_interfaces_ext_dd(ipoin,iinterface))
          enddo
       enddo
       
       ! send messages
       do iinterface = 1, nb_interfaces_dd
          
          call isend_cr(buffer_send_vector_ext_dd(1,1,iinterface), &
               NDIM*nibool_interfaces_ext_dd(iinterface), &
               my_neighbors_ext_dd(iinterface), &
               itag, &
               request_send_vector_ext_dd(iinterface))
          
          call irecv_cr(buffer_recv_vector_ext_dd(1,1,iinterface), &
               NDIM*nibool_interfaces_ext_dd(iinterface), &
               my_neighbors_ext_dd(iinterface), &
               itag, &
               request_recv_vector_ext_dd(iinterface))
       enddo
       
    end if
    
    
  end subroutine assemble_MPI_vector_async_send
  
!
!------------------------------------------------------------------------------------------------------------------------------
!

  subroutine assemble_MPI_vector_async_w_ord(NGLOB_AB,array_val)

    integer, intent(in) :: NGLOB_AB
    real(kind=CUSTOM_REAL), dimension(NDIM, NGLOB_AB), intent(inout) :: array_val

    integer :: iglob, iinterface, ipoin
    logical :: need_add_my_contrib

    
    if (nbproc_dd > 1) then
       
       ! move interface values of array_val to local buffers
       do iinterface = 1, nb_interfaces_dd
          do ipoin = 1, nibool_interfaces_ext_dd(iinterface)
             iglob = ibool_interfaces_ext_dd(ipoin,iinterface)
             mybuffer(:,ipoin,iinterface) = array_val(:,iglob)
             ! set them to zero right away to avoid counting it more than once during assembly:
             ! buffers of higher rank get zeros on nodes shared with current buffer
             array_val(:,iglob) = 0._CUSTOM_REAL
          enddo
       enddo

       ! wait for communications completion (recv)
       do iinterface = 1, nb_interfaces_dd
          call wait_req(request_recv_vector_ext_dd(iinterface))
       enddo
       
       ! adding all contributions in order of processor rank
       need_add_my_contrib = .true.
       do iinterface = 1, nb_interfaces_dd
          if (need_add_my_contrib .and. myrank < my_neighbors_ext_dd(iinterface)) call add_my_contrib()
          do ipoin = 1, nibool_interfaces_ext_dd(iinterface)
             iglob = ibool_interfaces_ext_dd(ipoin,iinterface)
             array_val(:,iglob) = array_val(:,iglob) + buffer_recv_vector_ext_dd(:,ipoin,iinterface)
          enddo
       enddo
       
       if (need_add_my_contrib) call add_my_contrib()

       ! wait for communications completion (send)
       do iinterface = 1, nb_interfaces_dd
          call wait_req(request_send_vector_ext_dd(iinterface))
       enddo
       
       
    end if
    
  contains

    subroutine add_my_contrib()

      integer :: my_iinterface,my_ipoin

      do my_iinterface = 1, nb_interfaces_dd
         do my_ipoin = 1, nibool_interfaces_ext_dd(my_iinterface)
            iglob = ibool_interfaces_ext_dd(my_ipoin,my_iinterface)
            array_val(:,iglob) = array_val(:,iglob) + mybuffer(:,my_ipoin,my_iinterface)
         enddo
      enddo
      need_add_my_contrib = .false.
      
    end subroutine add_my_contrib
    
  end subroutine assemble_MPI_vector_async_w_ord



  
end module domain_decomp_mod
