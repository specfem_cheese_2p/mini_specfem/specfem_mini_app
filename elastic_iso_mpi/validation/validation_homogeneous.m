U = load('../displ_00001.txt');
Ux = load('CE.1.FXX.semd');
Uy = load('CE.1.FXY.semd');
Uz = load('CE.1.FXZ.semd');
figure; 
subplot(3,1,1)
plot(U(:,2))
hold on
plot(Ux(:,2),'--')

% xlim([0, 900])
subplot(3,1,2)
plot(U(:,3))
hold on
plot(Uy(:,2),'--')
% xlim([0, 900])

T=1:1:length(U(:,4));
subplot(3,1,3)
plot(U(:,4))
hold on
plot(Uz(:,2),'--')
% xlim([0, 900])
linkaxes
