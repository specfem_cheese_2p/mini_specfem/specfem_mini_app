module solver_mod

    !
    !
    !  module that manage the main computation 
    !
    !

   use specfem_mod

   implicit none 

   private  

    !! gather group size for cache blocking
   integer, parameter :: ngroup_size = 8
   integer :: custom_group_size
   real(kind=CUSTOM_REAL), dimension(ngroup_size, NGLLX, NGLLY, NGLLZ) :: lambdaplus2mul, lambdal
   !DIR$ ATTRIBUTES ALIGN: 32 :: lambdaplus2mul, lambdal

   public :: iterate_time

contains

!!---------------------------------------------------------------------------
subroutine iterate_time()

    integer :: it, iphase
    real(kind=CUSTOM_REAL) :: t
    character(len=CHAR_LEN) :: name_file
    real(kind=CUSTOM_REAL) :: max_displ
    real :: time_start, time_end
    
    !! initlialize fields 
    call initialize_solver()
    
    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'Starting time iteration loop...'
       write(*,*)
    end if

    !! prepare for volume visualization
    call io_setup_connectivity(xdof_specfem, ibool_specfem, NGLLX, NGLLY, NGLLZ,&
    nspec_specfem, nglob_specfem, myrank)
    !! complete 3D mesh visualization 
    call write_xdmf_mesh(myrank)

    call cpu_time(time_start)

    do it = 1, NSTEP
     
      t = (it-1)*deltat_config

       if ( mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. check_stability) then
          max_displ = maxval(displ_specfem(:,:))
          if (max_displ /= max_displ) then
             stop 'forward simulation became unstable in elastic domain and blew up'
          end if
       end if
       
       if (myrank == 0 .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0 .and. verbose) then
          write(*,*) " time step : ",  it, ' / ', NSTEP
       end if
       
       if (movie .and. mod(it,NTSTEP_BETWEEN_OUTPUT_INFO)==0) then

          write(name_file,"('output/veloc_x_it',i6.6,'.bin')") it
          call dump_binary_mpi(name_file, veloc_specfem, 1, nglob_specfem)
         
          write(name_file,"('output/veloc_y_it',i6.6,'.bin')") it
          call dump_binary_mpi(name_file, veloc_specfem, 2, nglob_specfem)
          
          write(name_file,"('output/veloc_z_it',i6.6,'.bin')")  it
          call dump_binary_mpi(name_file, veloc_specfem, 3, nglob_specfem)

       end if

       call newmark_predictor()

       !! compute rhs --------------------------------------------------------------
       do iphase = 1, 2
          
          call compute_forces_iso_elastic(iphase)
          
          if (iphase == 1) then
             
             call add_source_term(it)          !! source term
             call store_seismogram(it)         !! store seismograms 
             call compute_stacey()             !! boundary conditions

             !! send accelareation to neighbours mpi-slice
             call assemble_MPI_vector_async_send(nglob_specfem, accel_specfem)
          else
             !! receive accelareation from neighbours mpi-slice
             call assemble_MPI_vector_async_w_ord(nglob_specfem, accel_specfem)
          end if

       end do
       !! -------------------------------------------------------------------------
       
       call  newmark_corector()
       
    end do

    call cpu_time(time_end)
    
    call write_sismogram()

    if (movie) then
       call write_xdmf_movie(myrank, NSTEP)
    end if

    if (myrank == 0 .and. verbose) then
       write(*,*)
       write(*,*) 'End of time iteration loop...', time_end - time_start,' s'
       write(*,*)
    end if
    
     
end subroutine iterate_time
!!---------------------------------------------------------------------------
subroutine initialize_solver()
    displ_specfem(:,:) = 0._CUSTOM_REAL
    veloc_specfem(:,:) = 0._CUSTOM_REAL
    accel_specfem(:,:) = 0._CUSTOM_REAL
end subroutine initialize_solver
!!---------------------------------------------------------------------------
subroutine newmark_predictor()
    integer :: i
    do i=1, nglob_specfem
       displ_specfem(:,i) = displ_specfem(:,i) + deltat_config*veloc_specfem(:,i) + deltatsqover2*accel_specfem(:,i)
       veloc_specfem(:,i) = veloc_specfem(:,i) + deltatover2*accel_specfem(:,i)
       accel_specfem(:,i) = 0._CUSTOM_REAL
    end do
end subroutine newmark_predictor
!!---------------------------------------------------------------------------
subroutine newmark_corector()
    integer :: i
    do  i=1, nglob_specfem
       accel_specfem(1,i) = accel_specfem(1,i)*inv_massx_specfem(i)
       accel_specfem(2,i) = accel_specfem(2,i)*inv_massy_specfem(i)
       accel_specfem(3,i) = accel_specfem(3,i)*inv_massz_specfem(i)
       veloc_specfem(:,i) = veloc_specfem(:,i) + deltatover2*accel_specfem(:,i)
    end do
end subroutine newmark_corector
!!---------------------------------------------------------------------------
subroutine add_source_term(it)
    integer, intent(in) :: it
    integer :: ispec, isrc, i,j,k, iglob
    real(kind=CUSTOM_REAL) :: hl
    do isrc =1, nsource
       ispec = ispec_src(isrc)
       if (ispec > 0) then
          
          do k=1,NGLLZ
             do j=1,NGLLY
                do i=1,NGLLX
                   iglob = ibool_specfem(i,j,k,ispec)
                   hl = hxis(i,isrc)*hetas(j,isrc)*hgammas(k,isrc) * used_stf(it) 
                   accel_specfem(1,iglob) = accel_specfem(1,iglob) + Fx(isrc) * hl
                   accel_specfem(2,iglob) = accel_specfem(2,iglob) + Fy(isrc) * hl
                   accel_specfem(3,iglob) = accel_specfem(3,iglob) + Fz(isrc) * hl
                end do
             end do
          end do
                
       end if
    end do
end subroutine add_source_term
!!---------------------------------------------------------------------------
subroutine store_seismogram(it)
    integer, intent(in) :: it
    integer :: ispec, irec, i,j,k, iglob
    real(kind=CUSTOM_REAL) :: hl
    do irec =1, nrec
       ispec = ispec_rec(irec)
       if (ispec > 0) then
          do k=1,NGLLZ
             do j=1,NGLLY
                do i=1,NGLLX
                   iglob = ibool_specfem(i,j,k,ispec)
                   hl = hxir(i,irec)*hetar(j,irec)*hgammar(k,irec)
                   seismogram_d(it, 1, irec) = seismogram_d(it, 1, irec) + hl * displ_specfem(1,iglob)
                   seismogram_d(it, 2, irec) = seismogram_d(it, 2, irec) + hl * displ_specfem(2,iglob)
                   seismogram_d(it, 3, irec) = seismogram_d(it, 3, irec) + hl * displ_specfem(3,iglob)
                end do
             end do
          end do
       end if
    end do
 end subroutine store_seismogram
 !!---------------------------------------------------------------------------
subroutine compute_stacey()
    integer :: ib, iglob
    real(kind=CUSTOM_REAL) :: nx, ny, nz, vx, vy, vz, vn
    real(kind=CUSTOM_REAL) :: tx, ty, tz, weight
    do ib = 1, ngll_boundary
       nx = normal(1,ib)
       ny = normal(2,ib)
       nz = normal(3,ib)
       iglob = index_gll_boundary(ib)
       vx = veloc_specfem(1,iglob)
       vy = veloc_specfem(2,iglob)
       vz = veloc_specfem(3,iglob)
       vn = vx*nx + vy*ny + vz*nz
       tx = rho_vp(ib) * vn*nx + rho_vs(ib) * (vx-vn*nx)
       ty = rho_vp(ib) * vn*ny + rho_vs(ib) * (vy-vn*ny)
       tz = rho_vp(ib) * vn*nz + rho_vs(ib) * (vz-vn*nz)
       weight = wstacey(ib)
       accel_specfem(1,iglob) = accel_specfem(1,iglob) - tx*weight
       accel_specfem(2,iglob) = accel_specfem(2,iglob) - ty*weight
       accel_specfem(3,iglob) = accel_specfem(3,iglob) - tz*weight
    end do
end subroutine compute_stacey

!!---------------------------------------------------------------------------
subroutine gather_group(dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
   jacobian_specfem_local, &
   dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

      !! local shape function jacobian matrix
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
      dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
      deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
      dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local
      
      !! local shape function jacobian matrix determinant
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: jacobian_specfem_local

      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: dummyx_loc,dummyy_loc,dummyz_loc

      integer, intent(in) :: iphase
      integer, intent(in) :: ispec_begin

      integer :: i, j, k, ispec_p, ispec, iglob, iel_loc
      
      iel_loc = 0
      do ispec_p = ispec_begin, ispec_begin + ngroup_size - 1
         
         iel_loc = iel_loc + 1
         
         ! returns element id from stored element list
         ispec = phase_ispec_inner_elastic(ispec_p,iphase)

         ! store element displacement field in local array
         do k=1,NGLLZ
            do j=1,NGLLY
               do i=1,NGLLX
                  iglob = ibool_specfem(i,j,k,ispec)
                  dummyx_loc(iel_loc,i,j,k) = displ_specfem(1,iglob)
                  dummyy_loc(iel_loc,i,j,k) = displ_specfem(2,iglob)
                  dummyz_loc(iel_loc,i,j,k) = displ_specfem(3,iglob)
               enddo
            enddo
         enddo
        
         ! store element shape function jacobian matrix and model in local array
         do k=1,NGLLZ
            do j=1,NGLLY
               do i=1,NGLLX
                  dxi_dx_specfem_local(iel_loc,i,j,k) = dxi_dx_specfem(i,j,k,ispec)
                  dxi_dy_specfem_local(iel_loc,i,j,k) = dxi_dy_specfem(i,j,k,ispec)
                  dxi_dz_specfem_local(iel_loc,i,j,k) = dxi_dz_specfem(i,j,k,ispec)
                  deta_dx_specfem_local(iel_loc,i,j,k) = deta_dx_specfem(i,j,k,ispec)
                  deta_dy_specfem_local(iel_loc,i,j,k) = deta_dy_specfem(i,j,k,ispec)
                  deta_dz_specfem_local(iel_loc,i,j,k) = deta_dz_specfem(i,j,k,ispec)
                  dgamma_dx_specfem_local(iel_loc,i,j,k) = dgamma_dx_specfem(i,j,k,ispec)
                  dgamma_dy_specfem_local(iel_loc,i,j,k) = dgamma_dy_specfem(i,j,k,ispec)
                  dgamma_dz_specfem_local(iel_loc,i,j,k) = dgamma_dz_specfem(i,j,k,ispec)
                  jacobian_specfem_local(iel_loc,i,j,k) = jacobian_specfem(i,j,k,ispec)
                  lambdaplus2mul(iel_loc,i,j,k) = kappa_specfem(i,j,k,ispec) + FOUR_THIRDS * mu_specfem(i,j,k,ispec)
                  lambdal(iel_loc,i,j,k) = lambdaplus2mul(iel_loc,i,j,k) - 2._CUSTOM_REAL * mu_specfem(i,j,k,ispec)

               enddo
            enddo
         end do
      end do

end subroutine gather_group   

!!---------------------------------------------------------------------------
subroutine gather_group_v(dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
   jacobian_specfem_local, &
   dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

      !! local shape function jacobian matrix
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
      dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
       deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: &
      dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local
      
      !! local shape function jacobian matrix determinant
      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: jacobian_specfem_local

      real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: dummyx_loc,dummyy_loc,dummyz_loc

      integer, intent(in) :: iphase
      integer, intent(in) :: ispec_begin

      integer :: i, j, k, ispec_p, ispec, iglob, iel_loc
      
      iel_loc = 0
      do ispec_p = ispec_begin, ispec_begin + custom_group_size - 1
         
         iel_loc = iel_loc + 1
         
         ! returns element id from stored element list
         ispec = phase_ispec_inner_elastic(ispec_p,iphase)

         ! store element displacement field in local array
         do k=1,NGLLZ
            do j=1,NGLLY
               do i=1,NGLLX
                  iglob = ibool_specfem(i,j,k,ispec)
                  dummyx_loc(iel_loc,i,j,k) = displ_specfem(1,iglob)
                  dummyy_loc(iel_loc,i,j,k) = displ_specfem(2,iglob)
                  dummyz_loc(iel_loc,i,j,k) = displ_specfem(3,iglob)
               enddo
            enddo
         enddo
        
         ! store element shape function jacobian matrix and model in local array
         do k=1,NGLLZ
            do j=1,NGLLY
               do i=1,NGLLX
                  dxi_dx_specfem_local(iel_loc,i,j,k) = dxi_dx_specfem(i,j,k,ispec)
                  dxi_dy_specfem_local(iel_loc,i,j,k) = dxi_dy_specfem(i,j,k,ispec)
                  dxi_dz_specfem_local(iel_loc,i,j,k) = dxi_dz_specfem(i,j,k,ispec)
                  deta_dx_specfem_local(iel_loc,i,j,k) = deta_dx_specfem(i,j,k,ispec)
                  deta_dy_specfem_local(iel_loc,i,j,k) = deta_dy_specfem(i,j,k,ispec)
                  deta_dz_specfem_local(iel_loc,i,j,k) = deta_dz_specfem(i,j,k,ispec)
                  dgamma_dx_specfem_local(iel_loc,i,j,k) = dgamma_dx_specfem(i,j,k,ispec)
                  dgamma_dy_specfem_local(iel_loc,i,j,k) = dgamma_dy_specfem(i,j,k,ispec)
                  dgamma_dz_specfem_local(iel_loc,i,j,k) = dgamma_dz_specfem(i,j,k,ispec)
                  jacobian_specfem_local(iel_loc,i,j,k) = jacobian_specfem(i,j,k,ispec)
                  lambdaplus2mul(iel_loc,i,j,k) = kappa_specfem(i,j,k,ispec) + FOUR_THIRDS * mu_specfem(i,j,k,ispec)
                  lambdal(iel_loc,i,j,k) = lambdaplus2mul(iel_loc,i,j,k) - 2._CUSTOM_REAL * mu_specfem(i,j,k,ispec)

               enddo
            enddo
         end do
      end do

end subroutine gather_group_v  

!!---------------------------------------------------------------------------
subroutine scatter_group(accel_specfem_local, iphase, ispec_begin)

   integer, intent(in) :: iphase
   integer, intent(in) :: ispec_begin
   real(kind=CUSTOM_REAL), dimension(3,ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: accel_specfem_local

   integer :: i, j, k, ispec_p, ispec, iglob, iel_loc
   !integer :: nb_group, ispec_end

   iel_loc = 0
   do ispec_p = ispec_begin, ispec_begin + ngroup_size - 1
      iel_loc = iel_loc + 1
      ispec = phase_ispec_inner_elastic(ispec_p, iphase)
      do k=1,NGLLZ
         do j=1,NGLLY
            do i=1,NGLLX
               iglob = ibool_specfem(i,j,k,ispec)
               accel_specfem(1,iglob) = accel_specfem(1,iglob) - accel_specfem_local(1, iel_loc,i,j,k)
               accel_specfem(2,iglob) = accel_specfem(2,iglob) - accel_specfem_local(2, iel_loc,i,j,k)
               accel_specfem(3,iglob) = accel_specfem(3,iglob) - accel_specfem_local(3, iel_loc,i,j,k)
            enddo
         enddo
      enddo
   end do
end subroutine scatter_group 

!!---------------------------------------------------------------------------
subroutine scatter_group_v(accel_specfem_local, iphase, ispec_begin)

   integer, intent(in) :: iphase
   integer, intent(in) :: ispec_begin
   real(kind=CUSTOM_REAL), dimension(3,ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: accel_specfem_local

   integer :: i, j, k, ispec_p, ispec, iglob, iel_loc
   !integer :: nb_group, ispec_end

   iel_loc = 0
   do ispec_p = ispec_begin, ispec_begin + custom_group_size - 1
      iel_loc = iel_loc + 1
      ispec = phase_ispec_inner_elastic(ispec_p, iphase)
      do k=1,NGLLZ
         do j=1,NGLLY
            do i=1,NGLLX
               iglob = ibool_specfem(i,j,k,ispec)
               accel_specfem(1,iglob) = accel_specfem(1,iglob) - accel_specfem_local(1, iel_loc,i,j,k)
               accel_specfem(2,iglob) = accel_specfem(2,iglob) - accel_specfem_local(2, iel_loc,i,j,k)
               accel_specfem(3,iglob) = accel_specfem(3,iglob) - accel_specfem_local(3, iel_loc,i,j,k)
            enddo
         enddo
      enddo
   end do
end subroutine scatter_group_v

!!---------------------------------------------------------------------------
subroutine compute_forces_iso_elastic(iphase)

   integer, intent(in) :: iphase

   !integer :: i, j, k, l, ispec_p, ispec, iglob
   integer :: num_elements
   integer :: ispec_begin, ispec_end
  
   !! local shape function jacobian matrix
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: &
   dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local
   !DIR$ ATTRIBUTES ALIGN: 32 :: dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
   !DIR$ ATTRIBUTES ALIGN: 32 :: deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
   !DIR$ ATTRIBUTES ALIGN: 32 :: dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local
   
   !! local shape function jacobian matrix determinant
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: jacobian_specfem_local
   !DIR$ ATTRIBUTES ALIGN: 32 :: jacobian_specfem_local

   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: dummyx_loc,dummyy_loc,dummyz_loc
   !DIR$ ATTRIBUTES ALIGN: 32 :: dummyx_loc,dummyy_loc,dummyz_loc

   real(kind=CUSTOM_REAL), dimension(3,ngroup_size,NGLLX,NGLLY,NGLLZ) :: accel_specfem_local 
   !DIR$ ATTRIBUTES ALIGN: 32 :: accel_specfem_local

   ! choses inner/outer elements
   if (iphase == 1) then
      num_elements = nspec_outer_elastic
   else
      num_elements = nspec_inner_elastic
   endif

   do ispec_begin = 1, num_elements, ngroup_size
      
      ispec_end = min(ispec_begin + ngroup_size - 1, num_elements)
      custom_group_size = ispec_end - ispec_begin + 1

      if (custom_group_size == ngroup_size) then 
         
         !write(*,*) "group size = ", ngroup_size,iphase
         
         call gather_group(dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
         deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
         dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
         jacobian_specfem_local, &
         dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

         call compute_forces_iso_elastic_group(accel_specfem_local,&
         dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
         deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
         dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
         jacobian_specfem_local,&
         dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

         call scatter_group(accel_specfem_local, iphase, ispec_begin)

      else  
         
         !write(*,*) "custom group size = ", custom_group_size,iphase

         call gather_group_v(dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
         deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
         dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
         jacobian_specfem_local, &
         dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

         call compute_forces_iso_elastic_group_v(accel_specfem_local,&
         dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
         deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
         dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
         jacobian_specfem_local,&
         dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

         call scatter_group_v(accel_specfem_local, iphase, ispec_begin)

      end if

   end do   
 
end subroutine compute_forces_iso_elastic

!!---------------------------------------------------------------------------
subroutine compute_forces_iso_elastic_group(accel_specfem_local,&
   dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
   jacobian_specfem_local,&
   dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

   !! local shape function jacobian matrix
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local

   !! local shape function jacobian matrix determinant
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: jacobian_specfem_local

   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: dummyx_loc,dummyy_loc,dummyz_loc

   integer, intent(in) :: iphase
   integer, intent(in) :: ispec_begin

   real(kind=CUSTOM_REAL), dimension(3,ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: accel_specfem_local

   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: &
    tempx1,tempx2,tempx3, tempy1,tempy2,tempy3,tempz1,tempz2,tempz3
   !DIR$ ATTRIBUTES ALIGN: 32 :: tempx1,tempx2,tempx3,tempy1,tempy2,tempy3,tempz1,tempz2,tempz3
   
   ! local arrays for stress computation
   real(kind=CUSTOM_REAL), dimension(ngroup_size) :: tempx1l,tempx2l,tempx3l, tempy1l,tempy2l,tempy3l,tempz1l,tempz2l,tempz3l
   !DIR$ ATTRIBUTES ALIGN: 32 :: tempx1l,tempx2l,tempx3l,tempy1l,tempy2l,tempy3l,tempz1l,tempz2l,tempz3l

   ! local stress 
   real(kind=CUSTOM_REAL) :: sigma_xx, sigma_yy, sigma_zz, sigma_xy, sigma_xz, sigma_yz, sigma_yx, sigma_zx, sigma_zy

   ! local stain 
   real(kind=CUSTOM_REAL) :: duxdxl, duxdyl, duxdzl, duydxl, duydyl, duydzl, duzdxl, duzdyl, duzdzl
   real(kind=CUSTOM_REAL) :: duxdxl_plus_duydyl, duxdxl_plus_duzdzl, duydyl_plus_duzdzl, &
   duxdyl_plus_duydxl, duzdxl_plus_duxdzl, duzdyl_plus_duydzl

   ! local variables for stress computation
   real(kind=CUSTOM_REAL) :: hp1, hp2, hp3, mul
   ! local variables for integration
   real(kind=CUSTOM_REAL) :: fac1, fac2, fac3

   integer :: i, j, k, l, ie


   do k=1,NGLLZ
      do j=1,NGLLY
         do i=1,NGLLX
                
            tempx1l(:) = 0._CUSTOM_REAL
            tempy1l(:) = 0._CUSTOM_REAL
            tempz1l(:) = 0._CUSTOM_REAL
                
            tempx2l(:) = 0._CUSTOM_REAL
            tempy2l(:) = 0._CUSTOM_REAL
            tempz2l(:) = 0._CUSTOM_REAL
                
            tempx3l(:) = 0._CUSTOM_REAL
            tempy3l(:) = 0._CUSTOM_REAL
            tempz3l(:) = 0._CUSTOM_REAL
                
            ! we can merge these loops because NGLLX = NGLLY = NGLLZ
            do l=1,NGLLX
               do ie = 1, ngroup_size
                  hp1 = hprime_xxT(l,i)
                  tempx1l(ie) = tempx1l(ie) + dummyx_loc(ie,l,j,k) * hp1
                  tempy1l(ie) = tempy1l(ie) + dummyy_loc(ie,l,j,k) * hp1
                  tempz1l(ie) = tempz1l(ie) + dummyz_loc(ie,l,j,k) * hp1

                  hp2 = hprime_yyT(l,j)
                  tempx2l(ie) = tempx2l(ie) + dummyx_loc(ie,i,l,k) * hp2
                  tempy2l(ie) = tempy2l(ie) + dummyy_loc(ie,i,l,k) * hp2
                  tempz2l(ie) = tempz2l(ie) + dummyz_loc(ie,i,l,k) * hp2
                   
                  hp3 = hprime_zzT(l,k)
                  tempx3l(ie) = tempx3l(ie) + dummyx_loc(ie,i,j,l) * hp3
                  tempy3l(ie) = tempy3l(ie) + dummyy_loc(ie,i,j,l) * hp3
                  tempz3l(ie) = tempz3l(ie) + dummyz_loc(ie,i,j,l) * hp3
               enddo
            enddo
                
           
            do ie = 1, ngroup_size

               duxdxl = dxi_dx_specfem_local(ie, i,j,k)*tempx1l(ie) +deta_dx_specfem_local(ie, i,j,k)*tempx2l(ie) + &
               dgamma_dx_specfem_local(ie, i,j,k)*tempx3l(ie)
               duxdyl = dxi_dy_specfem_local(ie, i,j,k)*tempx1l(ie) +deta_dy_specfem_local(ie, i,j,k)*tempx2l(ie) + &
               dgamma_dy_specfem_local(ie, i,j,k)*tempx3l(ie)
               duxdzl = dxi_dz_specfem_local(ie, i,j,k)*tempx1l(ie) +deta_dz_specfem_local(ie, i,j,k)*tempx2l(ie) + &
               dgamma_dz_specfem_local(ie, i,j,k)*tempx3l(ie)

               duydxl = dxi_dx_specfem_local(ie, i,j,k)*tempy1l(ie) +deta_dx_specfem_local(ie, i,j,k)*tempy2l(ie) + &
               dgamma_dx_specfem_local(ie, i,j,k)*tempy3l(ie)
               duydyl = dxi_dy_specfem_local(ie, i,j,k)*tempy1l(ie) +deta_dy_specfem_local(ie, i,j,k)*tempy2l(ie) + &
               dgamma_dy_specfem_local(ie, i,j,k)*tempy3l(ie)
               duydzl = dxi_dz_specfem_local(ie, i,j,k)*tempy1l(ie) +deta_dz_specfem_local(ie, i,j,k)*tempy2l(ie) + &
               dgamma_dz_specfem_local(ie, i,j,k)*tempy3l(ie)

               duzdxl = dxi_dx_specfem_local(ie, i,j,k)*tempz1l(ie) +deta_dx_specfem_local(ie, i,j,k)*tempz2l(ie) + &
               dgamma_dx_specfem_local(ie, i,j,k)*tempz3l(ie)
               duzdyl = dxi_dy_specfem_local(ie, i,j,k)*tempz1l(ie) +deta_dy_specfem_local(ie, i,j,k)*tempz2l(ie) + &
               dgamma_dy_specfem_local(ie, i,j,k)*tempz3l(ie)
               duzdzl = dxi_dz_specfem_local(ie, i,j,k)*tempz1l(ie) +deta_dz_specfem_local(ie, i,j,k)*tempz2l(ie) + &
               dgamma_dz_specfem_local(ie, i,j,k)*tempz3l(ie)

               ! precompute some sums to save CPU time
               duxdxl_plus_duydyl = duxdxl + duydyl
               duxdxl_plus_duzdzl = duxdxl + duzdzl
               duydyl_plus_duzdzl = duydyl + duzdzl
               duxdyl_plus_duydxl = duxdyl + duydxl
               duzdxl_plus_duxdzl = duzdxl + duxdzl
               duzdyl_plus_duydzl = duzdyl + duydzl


                ! compute stress sigma
               sigma_xx = lambdaplus2mul(ie, i,j,k) * duxdxl + lambdal(ie, i, j, k) * duydyl_plus_duzdzl
               sigma_yy = lambdaplus2mul(ie, i,j,k) * duydyl + lambdal(ie, i, j, k) * duxdxl_plus_duzdzl
               sigma_zz = lambdaplus2mul(ie, i,j,k) * duzdzl + lambdal(ie, i, j, k) * duxdxl_plus_duydyl
               mul = 0.5 *(lambdaplus2mul(ie, i,j,k) - lambdal(ie, i, j, k)) 
               sigma_xy = mul * duxdyl_plus_duydxl
               sigma_xz = mul * duzdxl_plus_duxdzl
               sigma_yz = mul * duzdyl_plus_duydzl
                
                ! define symmetric components of sigma
               sigma_yx = sigma_xy
               sigma_zx = sigma_xz
               sigma_zy = sigma_yz


               !  sigma_yx = sigma_xy
               !  sigma_zx = sigma_xz
               !  sigma_zy = sigma_yz

               ! form dot product with test vector, non-symmetric form
               tempx1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * dxi_dy_specfem_local(ie,i,j,k) + sigma_zx * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * dxi_dy_specfem_local(ie,i,j,k) + sigma_zy * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * dxi_dy_specfem_local(ie,i,j,k) + sigma_zz * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

               tempx2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * deta_dy_specfem_local(ie,i,j,k) + sigma_zx * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * deta_dy_specfem_local(ie,i,j,k) + sigma_zy * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * deta_dy_specfem_local(ie,i,j,k) + sigma_zz * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

               tempx3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zx * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zy * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zz * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

            end do

         end do
      end do
   end do


   ! second double-loop over GLL to compute all the terms
   do k=1,NGLLZ
      do j=1,NGLLY
         do i=1,NGLLX
           
            tempx1l(:) = 0._CUSTOM_REAL
            tempy1l(:) = 0._CUSTOM_REAL
            tempz1l(:) = 0._CUSTOM_REAL
                
            tempx2l(:) = 0._CUSTOM_REAL
            tempy2l(:) = 0._CUSTOM_REAL
            tempz2l(:) = 0._CUSTOM_REAL

            tempx3l(:) = 0._CUSTOM_REAL
            tempy3l(:) = 0._CUSTOM_REAL
            tempz3l(:) = 0._CUSTOM_REAL

            ! we can merge these loops because NGLLX = NGLLY = NGLLZ
            do l=1,NGLLX
               do ie = 1, ngroup_size
                  
                  fac1 = hprimewgll_xx(l,i)

                  tempx1l(ie) = tempx1l(ie) + tempx1(ie,l,j,k) * fac1
                  tempy1l(ie) = tempy1l(ie) + tempy1(ie,l,j,k) * fac1
                  tempz1l(ie) = tempz1l(ie) + tempz1(ie,l,j,k) * fac1

                  fac2 = hprimewgll_yy(l,j)
                  tempx2l(ie) = tempx2l(ie) + tempx2(ie,i,l,k) * fac2
                  tempy2l(ie) = tempy2l(ie) + tempy2(ie,i,l,k) * fac2
                  tempz2l(ie) = tempz2l(ie) + tempz2(ie,i,l,k) * fac2

                  fac3 = hprimewgll_zz(l,k)
                  tempx3l(ie) = tempx3l(ie) + tempx3(ie,i,j,l) * fac3
                  tempy3l(ie) = tempy3l(ie) + tempy3(ie,i,j,l) * fac3
                  tempz3l(ie) = tempz3l(ie) + tempz3(ie,i,j,l) * fac3

               end do
            enddo
                
            fac1 = wgllwgll_yz(j,k)
            fac2 = wgllwgll_xz(i,k)
            fac3 = wgllwgll_xy(i,j)

                
            ! sum contributions from each element to the global mesh using indirect addressing
            do ie=1, ngroup_size
               accel_specfem_local(1, ie,i,j,k) = fac1 * tempx1l(ie) + fac2 * tempx2l(ie) + fac3 * tempx3l(ie)
               accel_specfem_local(2, ie,i,j,k) = fac1 * tempy1l(ie) + fac2 * tempy2l(ie) + fac3 * tempy3l(ie)
               accel_specfem_local(3, ie,i,j,k) = fac1 * tempz1l(ie) + fac2 * tempz2l(ie) + fac3 * tempz3l(ie)
            end do


         end do
      end do
   end do
       

  end subroutine compute_forces_iso_elastic_group

!!---------------------------------------------------------------------------
  subroutine compute_forces_iso_elastic_group_v(accel_specfem_local,&
   dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local, &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local, &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local, &
   jacobian_specfem_local,&
   dummyx_loc, dummyy_loc, dummyz_loc, iphase, ispec_begin)

   !! local shape function jacobian matrix
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   dxi_dx_specfem_local, dxi_dy_specfem_local, dxi_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   deta_dx_specfem_local, deta_dy_specfem_local, deta_dz_specfem_local
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: &
   dgamma_dx_specfem_local, dgamma_dy_specfem_local, dgamma_dz_specfem_local

   !! local shape function jacobian matrix determinant
   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: jacobian_specfem_local

   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ), intent(in) :: dummyx_loc,dummyy_loc,dummyz_loc

   integer, intent(in) :: iphase
   integer, intent(in) :: ispec_begin

   real(kind=CUSTOM_REAL), dimension(3,ngroup_size,NGLLX,NGLLY,NGLLZ), intent(inout) :: accel_specfem_local

   real(kind=CUSTOM_REAL), dimension(ngroup_size,NGLLX,NGLLY,NGLLZ) :: &
    tempx1,tempx2,tempx3, tempy1,tempy2,tempy3,tempz1,tempz2,tempz3
   !DIR$ ATTRIBUTES ALIGN: 32 :: tempx1,tempx2,tempx3,tempy1,tempy2,tempy3,tempz1,tempz2,tempz3
   
   ! local arrays for stress computation
   real(kind=CUSTOM_REAL), dimension(ngroup_size) :: tempx1l,tempx2l,tempx3l, tempy1l,tempy2l,tempy3l,tempz1l,tempz2l,tempz3l
   !DIR$ ATTRIBUTES ALIGN: 32 :: tempx1l,tempx2l,tempx3l,tempy1l,tempy2l,tempy3l,tempz1l,tempz2l,tempz3l

   ! local stress 
   real(kind=CUSTOM_REAL) :: sigma_xx, sigma_yy, sigma_zz, sigma_xy, sigma_xz, sigma_yz, sigma_yx, sigma_zx, sigma_zy

   ! local stain 
   real(kind=CUSTOM_REAL) :: duxdxl, duxdyl, duxdzl, duydxl, duydyl, duydzl, duzdxl, duzdyl, duzdzl
   real(kind=CUSTOM_REAL) :: duxdxl_plus_duydyl, duxdxl_plus_duzdzl, duydyl_plus_duzdzl, &
   duxdyl_plus_duydxl, duzdxl_plus_duxdzl, duzdyl_plus_duydzl

   ! local variables for stress computation
   real(kind=CUSTOM_REAL) :: hp1, hp2, hp3, mul
   ! local variables for integration
   real(kind=CUSTOM_REAL) :: fac1, fac2, fac3

   integer :: i, j, k, l, ie


   do k=1,NGLLZ
      do j=1,NGLLY
         do i=1,NGLLX
                
            tempx1l(:) = 0._CUSTOM_REAL
            tempy1l(:) = 0._CUSTOM_REAL
            tempz1l(:) = 0._CUSTOM_REAL
                
            tempx2l(:) = 0._CUSTOM_REAL
            tempy2l(:) = 0._CUSTOM_REAL
            tempz2l(:) = 0._CUSTOM_REAL
                
            tempx3l(:) = 0._CUSTOM_REAL
            tempy3l(:) = 0._CUSTOM_REAL
            tempz3l(:) = 0._CUSTOM_REAL
                
            ! we can merge these loops because NGLLX = NGLLY = NGLLZ
            do l=1,NGLLX
               do ie = 1, custom_group_size
                  hp1 = hprime_xxT(l,i)
                  tempx1l(ie) = tempx1l(ie) + dummyx_loc(ie,l,j,k) * hp1
                  tempy1l(ie) = tempy1l(ie) + dummyy_loc(ie,l,j,k) * hp1
                  tempz1l(ie) = tempz1l(ie) + dummyz_loc(ie,l,j,k) * hp1

                  hp2 = hprime_yyT(l,j)
                  tempx2l(ie) = tempx2l(ie) + dummyx_loc(ie,i,l,k) * hp2
                  tempy2l(ie) = tempy2l(ie) + dummyy_loc(ie,i,l,k) * hp2
                  tempz2l(ie) = tempz2l(ie) + dummyz_loc(ie,i,l,k) * hp2
                   
                  hp3 = hprime_zzT(l,k)
                  tempx3l(ie) = tempx3l(ie) + dummyx_loc(ie,i,j,l) * hp3
                  tempy3l(ie) = tempy3l(ie) + dummyy_loc(ie,i,j,l) * hp3
                  tempz3l(ie) = tempz3l(ie) + dummyz_loc(ie,i,j,l) * hp3
               enddo
            enddo
                
           
            do ie = 1, custom_group_size

               duxdxl = dxi_dx_specfem_local(ie, i,j,k)*tempx1l(ie) +&
               deta_dx_specfem_local(ie, i,j,k)*tempx2l(ie) + dgamma_dx_specfem_local(ie, i,j,k)*tempx3l(ie)
               duxdyl = dxi_dy_specfem_local(ie, i,j,k)*tempx1l(ie) +&
               deta_dy_specfem_local(ie, i,j,k)*tempx2l(ie) + dgamma_dy_specfem_local(ie, i,j,k)*tempx3l(ie)
               duxdzl = dxi_dz_specfem_local(ie, i,j,k)*tempx1l(ie) +&
               deta_dz_specfem_local(ie, i,j,k)*tempx2l(ie) + dgamma_dz_specfem_local(ie, i,j,k)*tempx3l(ie)

               duydxl = dxi_dx_specfem_local(ie, i,j,k)*tempy1l(ie) +&
               deta_dx_specfem_local(ie, i,j,k)*tempy2l(ie) + dgamma_dx_specfem_local(ie, i,j,k)*tempy3l(ie)
               duydyl = dxi_dy_specfem_local(ie, i,j,k)*tempy1l(ie) +&
               deta_dy_specfem_local(ie, i,j,k)*tempy2l(ie) + dgamma_dy_specfem_local(ie, i,j,k)*tempy3l(ie)
               duydzl = dxi_dz_specfem_local(ie, i,j,k)*tempy1l(ie) +&
               deta_dz_specfem_local(ie, i,j,k)*tempy2l(ie) + dgamma_dz_specfem_local(ie, i,j,k)*tempy3l(ie)

               duzdxl = dxi_dx_specfem_local(ie, i,j,k)*tempz1l(ie) +&
               deta_dx_specfem_local(ie, i,j,k)*tempz2l(ie) + dgamma_dx_specfem_local(ie, i,j,k)*tempz3l(ie)
               duzdyl = dxi_dy_specfem_local(ie, i,j,k)*tempz1l(ie) +&
               deta_dy_specfem_local(ie, i,j,k)*tempz2l(ie) + dgamma_dy_specfem_local(ie, i,j,k)*tempz3l(ie)
               duzdzl = dxi_dz_specfem_local(ie, i,j,k)*tempz1l(ie) +&
               deta_dz_specfem_local(ie, i,j,k)*tempz2l(ie) + dgamma_dz_specfem_local(ie, i,j,k)*tempz3l(ie)

               ! precompute some sums to save CPU time
               duxdxl_plus_duydyl = duxdxl + duydyl
               duxdxl_plus_duzdzl = duxdxl + duzdzl
               duydyl_plus_duzdzl = duydyl + duzdzl
               duxdyl_plus_duydxl = duxdyl + duydxl
               duzdxl_plus_duxdzl = duzdxl + duxdzl
               duzdyl_plus_duydzl = duzdyl + duydzl


                ! compute stress sigma
               sigma_xx = lambdaplus2mul(ie, i,j,k) * duxdxl + lambdal(ie, i, j, k) * duydyl_plus_duzdzl
               sigma_yy = lambdaplus2mul(ie, i,j,k) * duydyl + lambdal(ie, i, j, k) * duxdxl_plus_duzdzl
               sigma_zz = lambdaplus2mul(ie, i,j,k) * duzdzl + lambdal(ie, i, j, k) * duxdxl_plus_duydyl
               mul = 0.5 *(lambdaplus2mul(ie, i,j,k) - lambdal(ie, i, j, k)) 
               sigma_xy = mul * duxdyl_plus_duydxl
               sigma_xz = mul * duzdxl_plus_duxdzl
               sigma_yz = mul * duzdyl_plus_duydzl
                
                ! define symmetric components of sigma
               sigma_yx = sigma_xy
               sigma_zx = sigma_xz
               sigma_zy = sigma_yz


               !  sigma_yx = sigma_xy
               !  sigma_zx = sigma_xz
               !  sigma_zy = sigma_yz

               ! form dot product with test vector, non-symmetric form
               tempx1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * dxi_dy_specfem_local(ie,i,j,k) + sigma_zx * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * dxi_dy_specfem_local(ie,i,j,k) + sigma_zy * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz1(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * dxi_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * dxi_dy_specfem_local(ie,i,j,k) + sigma_zz * dxi_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

               tempx2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * deta_dy_specfem_local(ie,i,j,k) + sigma_zx * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * deta_dy_specfem_local(ie,i,j,k) + sigma_zy * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz2(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * deta_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * deta_dy_specfem_local(ie,i,j,k) + sigma_zz * deta_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

               tempx3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xx * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yx * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zx * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_x
               tempy3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xy * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yy * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zy * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_y
               tempz3(ie,i,j,k) = jacobian_specfem_local(ie,i,j,k) * (sigma_xz * dgamma_dx_specfem_local(ie,i,j,k) + &
               sigma_yz * dgamma_dy_specfem_local(ie,i,j,k) + sigma_zz * dgamma_dz_specfem_local(ie,i,j,k)) ! this goes to accel_z

            end do

         end do
      end do
   end do


   ! second double-loop over GLL to compute all the terms
   do k=1,NGLLZ
      do j=1,NGLLY
         do i=1,NGLLX
           
            tempx1l(:) = 0._CUSTOM_REAL
            tempy1l(:) = 0._CUSTOM_REAL
            tempz1l(:) = 0._CUSTOM_REAL
                
            tempx2l(:) = 0._CUSTOM_REAL
            tempy2l(:) = 0._CUSTOM_REAL
            tempz2l(:) = 0._CUSTOM_REAL

            tempx3l(:) = 0._CUSTOM_REAL
            tempy3l(:) = 0._CUSTOM_REAL
            tempz3l(:) = 0._CUSTOM_REAL

            ! we can merge these loops because NGLLX = NGLLY = NGLLZ
            do l=1,NGLLX
               do ie = 1, custom_group_size
                  
                  fac1 = hprimewgll_xx(l,i)

                  tempx1l(ie) = tempx1l(ie) + tempx1(ie,l,j,k) * fac1
                  tempy1l(ie) = tempy1l(ie) + tempy1(ie,l,j,k) * fac1
                  tempz1l(ie) = tempz1l(ie) + tempz1(ie,l,j,k) * fac1

                  fac2 = hprimewgll_yy(l,j)
                  tempx2l(ie) = tempx2l(ie) + tempx2(ie,i,l,k) * fac2
                  tempy2l(ie) = tempy2l(ie) + tempy2(ie,i,l,k) * fac2
                  tempz2l(ie) = tempz2l(ie) + tempz2(ie,i,l,k) * fac2

                  fac3 = hprimewgll_zz(l,k)
                  tempx3l(ie) = tempx3l(ie) + tempx3(ie,i,j,l) * fac3
                  tempy3l(ie) = tempy3l(ie) + tempy3(ie,i,j,l) * fac3
                  tempz3l(ie) = tempz3l(ie) + tempz3(ie,i,j,l) * fac3

               end do
            enddo
                
            fac1 = wgllwgll_yz(j,k)
            fac2 = wgllwgll_xz(i,k)
            fac3 = wgllwgll_xy(i,j)

                
            ! sum contributions from each element to the global mesh using indirect addressing
            do ie=1, custom_group_size
               accel_specfem_local(1, ie,i,j,k) = fac1 * tempx1l(ie) + fac2 * tempx2l(ie) + fac3 * tempx3l(ie)
               accel_specfem_local(2, ie,i,j,k) = fac1 * tempy1l(ie) + fac2 * tempy2l(ie) + fac3 * tempy3l(ie)
               accel_specfem_local(3, ie,i,j,k) = fac1 * tempz1l(ie) + fac2 * tempz2l(ie) + fac3 * tempz3l(ie)
            end do


         end do
      end do
   end do

  end subroutine compute_forces_iso_elastic_group_v

end module solver_mod