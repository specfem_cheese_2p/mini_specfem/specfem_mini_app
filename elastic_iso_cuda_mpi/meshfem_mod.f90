module meshfem_mod

  use config_mod
  use mpi_mod
  use addressing_mod
  
  implicit none

  !! boundary domain to mesh 
  double precision, public :: xmin_meshfem, xmax_meshfem, ymin_meshfem, ymax_meshfem, zmin_meshfem, zmax_meshfem

  !! size of mesh 
  integer, public :: nx_meshfem, ny_meshfem, nz_meshfem, nspec_meshfem, nglob_meshfem

  !! grid element
  double precision, public, dimension(:,:,:,:), allocatable :: xgrid_meshfem, ygrid_meshfem, zgrid_meshfem
  !! indirect adressing on grid elements 
  integer, public, dimension(:,:,:,:), allocatable :: ibool_meshfem

  logical, public, dimension(:,:), allocatable :: iboun_meshfem
  logical, public, dimension(:,:), allocatable :: mpi_x_meshfem, mpi_y_meshfem
  
  integer, private, parameter :: NMX=2, NMY=2, NMZ=2
 


  public  :: meshfem
  private :: get_boundary
  
contains

  subroutine meshfem(iproc_x_slice, iproc_y_slice)

    integer,  intent(in) :: iproc_x_slice, iproc_y_slice
    integer :: i, j, k, ispec
    double precision :: x0, x1, y0, y1, z0, z1
    double precision :: typical_mesh_size

    if (myrank_mpi==0 .and. verbose) then
       write(*,*)
       write(*,*) "                     Meshing Cartesian domain  "
       write(*,*)
       write(*,'(a33, 2f16.6)') '*** Xmin and Xmax of the model = ',xmin_config,xmax_config
       write(*,'(a33, 2f16.6)') '*** Ymin and Ymax of the model = ',ymin_config,ymax_config
       write(*,'(a33, 2f16.6)') '*** Zmin and Zmax of the model = ',zmin_config,zmax_config
       write(*,*)
       write(*,*) "      NGNOD = " , NGNOD
       write(*,*) "      NGLLX = " , NGLLX
       write(*,*) "      NGLLY = " , NGLLY
       write(*,*) "      NGLLZ = " , NGLLZ
       write(*,*)
    end if
    
    !! typical reference size   
    typical_mesh_size =  min(zmax_config-zmin_config, &
         min(xmax_config - xmin_config, ymax_config - ymin_config))
    
    !! compute boundary for the current  MPI slice domain   
    xmin_meshfem = xmin_config +  iproc_x_slice * dx_config * NX_config / NPX_config
    xmax_meshfem = xmin_config +  (iproc_x_slice + 1)* dx_config * NX_config / NPX_config
    ymin_meshfem = ymin_config +  iproc_y_slice * dy_config * NY_config / NPY_config
    ymax_meshfem = ymin_config +  (iproc_y_slice + 1)* dy_config * NY_config / NPY_config
    zmin_meshfem = zmin_config
    zmax_meshfem = zmax_config

    !! number of element in the current MPI slice domain
    nx_meshfem = (xmax_meshfem - xmin_meshfem) / dx_config
    ny_meshfem = (ymax_meshfem - ymin_meshfem) / dy_config
    nz_meshfem = (zmax_meshfem - zmin_meshfem) / dz_config
    nspec_meshfem = nx_meshfem * ny_meshfem * nz_meshfem

    !! element grid dof
    allocate( xgrid_meshfem(NMX,NMY,NMZ, nspec_meshfem), &
              ygrid_meshfem(NMX,NMY,NMZ, nspec_meshfem), &
              zgrid_meshfem(NMX,NMY,NMZ, nspec_meshfem))
    allocate(ibool_meshfem(NMX,NMY,NMZ, nspec_meshfem))
    allocate(iboun_meshfem(6, nspec_meshfem))
    allocate(mpi_x_meshfem(2, nspec_meshfem),&
             mpi_y_meshfem(2, nspec_meshfem))
    

    ispec = 0
    !! loop on cartesian grid in domain to mesh 
    do k=1, nz_meshfem

       z0 = zmin_meshfem + (k-1)*dz_config
       z1 = zmin_meshfem + (k)*dz_config

       do j=1, ny_meshfem

          y0 = ymin_meshfem + (j-1)*dy_config
          y1 = ymin_meshfem + (j)*dy_config
          
          do i=1, nx_meshfem

             x0 = xmin_meshfem + (i-1)*dx_config
             x1 = xmin_meshfem + (i)*dx_config

             !! define new index element
             ispec = ispec + 1

             !! define 8 corner of element 
             xgrid_meshfem(1,1,1,ispec) = x0
             ygrid_meshfem(1,1,1,ispec) = y0
             zgrid_meshfem(1,1,1,ispec) = z0
             
             xgrid_meshfem(2,1,1,ispec) = x1
             ygrid_meshfem(2,1,1,ispec) = y0
             zgrid_meshfem(2,1,1,ispec) = z0

             xgrid_meshfem(2,2,1,ispec) = x1
             ygrid_meshfem(2,2,1,ispec) = y1
             zgrid_meshfem(2,2,1,ispec) = z0
             
             xgrid_meshfem(1,2,1,ispec) = x0
             ygrid_meshfem(1,2,1,ispec) = y1
             zgrid_meshfem(1,2,1,ispec) = z0
             
             xgrid_meshfem(1,1,2,ispec) = x0
             ygrid_meshfem(1,1,2,ispec) = y0
             zgrid_meshfem(1,1,2,ispec) = z1
             
             xgrid_meshfem(2,1,2,ispec) = x1
             ygrid_meshfem(2,1,2,ispec) = y0
             zgrid_meshfem(2,1,2,ispec) = z1


             xgrid_meshfem(2,2,2,ispec) = x1
             ygrid_meshfem(2,2,2,ispec) = y1
             zgrid_meshfem(2,2,2,ispec) = z1
             
             xgrid_meshfem(1,2,2,ispec) = x0
             ygrid_meshfem(1,2,2,ispec) = y1
             zgrid_meshfem(1,2,2,ispec) = z1

             
          end do
       end do
    end do

    call get_boundary()
    call create_adressing(ibool_meshfem, xgrid_meshfem, ygrid_meshfem, zgrid_meshfem, &
         NMX, NMY, NMZ, nspec_meshfem, typical_mesh_size)
    nglob_meshfem = maxval(ibool_meshfem)
    

    if (myrank_mpi==0 .and. verbose) then 
       write(*,*) 
       write(*,*) "          mesher passed   "
       write(*,*) 
    end if
    
  end subroutine meshfem

  !!-------------------------------------------------------------------------------------------------------------------------------
  subroutine get_boundary()

    integer :: ispec
    double precision :: dx,dy
    double precision :: xelm(8),yelm(8),zelm(8)
    double precision :: target_val,TOLERANCE_METERS

    dx = dabs( xmax_meshfem - xmin_meshfem )
    dy = dabs( ymax_meshfem - ymin_meshfem )
    TOLERANCE_METERS = min( dx / 100000. , dy / 100000. )
    
    ! in case of very large meshes
    if (nx_meshfem * NMX > 100000) &
         TOLERANCE_METERS = min( TOLERANCE_METERS, dx / (nx_meshfem * NMX) )
    if (ny_meshfem * NMY > 100000) &
         TOLERANCE_METERS = min( TOLERANCE_METERS, dy / (ny_meshfem * NMY) )
    
    do ispec = 1, nspec_meshfem
       xelm(1)=xgrid_meshfem(1,1,1,ispec)
       yelm(1)=ygrid_meshfem(1,1,1,ispec)
       zelm(1)=zgrid_meshfem(1,1,1,ispec)
       xelm(2)=xgrid_meshfem(NMX,1,1,ispec)
       yelm(2)=ygrid_meshfem(NMX,1,1,ispec)
       zelm(2)=zgrid_meshfem(NMX,1,1,ispec)
       xelm(3)=xgrid_meshfem(NMX,NMY,1,ispec)
       yelm(3)=ygrid_meshfem(NMX,NMY,1,ispec)
       zelm(3)=zgrid_meshfem(NMX,NMY,1,ispec)
       xelm(4)=xgrid_meshfem(1,NMY,1,ispec)
       yelm(4)=ygrid_meshfem(1,NMY,1,ispec)
       zelm(4)=zgrid_meshfem(1,NMY,1,ispec)
       xelm(5)=xgrid_meshfem(1,1,NMZ,ispec)
       yelm(5)=ygrid_meshfem(1,1,NMZ,ispec)
       zelm(5)=zgrid_meshfem(1,1,NMZ,ispec)
       xelm(6)=xgrid_meshfem(NMX,1,NMZ,ispec)
       yelm(6)=ygrid_meshfem(NMX,1,NMZ,ispec)
       zelm(6)=zgrid_meshfem(NMX,1,NMZ,ispec)
       xelm(7)=xgrid_meshfem(NMX,NMY,NMZ,ispec)
       yelm(7)=ygrid_meshfem(NMX,NMY,NMZ,ispec)
       zelm(7)=zgrid_meshfem(NMX,NMY,NMZ,ispec)
       xelm(8)=xgrid_meshfem(1,NMY,NMZ,ispec)
       yelm(8)=ygrid_meshfem(1,NMY,NMZ,ispec)
       zelm(8)=zgrid_meshfem(1,NMY,NMZ,ispec)

       
       ! ****************************************************
       !     determine if the element falls on a boundary
       ! ****************************************************

    
       iboun_meshfem(:,ispec)=.false.
       
       ! on boundary 1: x=xmin
       target_val = xmin_config + TOLERANCE_METERS
       if (xelm(1) < target_val .and. xelm(4) < target_val .and. xelm(5) < target_val .and. xelm(8) < target_val) &
       iboun_meshfem(1,ispec)=.true.

       ! on boundary 2: xmax
       target_val = xmax_config - TOLERANCE_METERS
       if (xelm(2) > target_val .and. xelm(3) > target_val .and. xelm(6) > target_val .and. xelm(7) > target_val) &
       iboun_meshfem(2,ispec)=.true.
    
       ! on boundary 3: ymin
       target_val = ymin_config + TOLERANCE_METERS
       if (yelm(1) < target_val .and. yelm(2) < target_val .and. yelm(5) < target_val .and. yelm(6) < target_val) &
       iboun_meshfem(3,ispec)=.true.
    
       ! on boundary 4: ymax
       target_val = ymax_config - TOLERANCE_METERS
       if (yelm(3) > target_val .and. yelm(4) > target_val .and. yelm(7) > target_val .and. yelm(8) > target_val) &
       iboun_meshfem(4,ispec)=.true.
    
       ! on boundary 5: bottom
       target_val = zmin_config + TOLERANCE_METERS
       if (zelm(1) < target_val .and. zelm(2) < target_val .and. zelm(3) < target_val .and. zelm(4) < target_val) &
       iboun_meshfem(5,ispec)=.true.
       
       ! on boundary 6: top
       target_val = zmax_config - TOLERANCE_METERS
       if (zelm(5) > target_val .and. zelm(6) > target_val .and. zelm(7) > target_val .and. zelm(8) > target_val) &
       iboun_meshfem(6,ispec)=.true.

       ! *******************************************************************
       !     determine if the element falls on an MPI cut plane along xi
       ! *******************************************************************

       ! detect the MPI cut planes along xi in the cubed sphere
       
       mpi_x_meshfem(:,ispec)=.false.
       
       ! left cut-plane in the current slice along X = constant (Xmin of this slice)
       ! and add geometrical tolerance
       
       target_val = xmin_meshfem  + TOLERANCE_METERS
       if (xelm(1) < target_val .and. xelm(4) < target_val .and. xelm(5) < target_val .and. xelm(8) < target_val) &
            mpi_x_meshfem(1,ispec)=.true.

       ! right cut-plane in the current slice along X = constant (Xmax of this slice)
       ! and add geometrical tolerance

       target_val = xmax_meshfem - TOLERANCE_METERS
       if (xelm(2) > target_val .and. xelm(3) > target_val .and. xelm(6) > target_val .and. xelm(7) > target_val) &
            mpi_x_meshfem(2,ispec)=.true.
       
       ! ********************************************************************
       !     determine if the element falls on an MPI cut plane along eta
       ! ********************************************************************
       
       mpi_y_meshfem(:,ispec)=.false.
        
       ! left cut-plane in the current slice along Y = constant (Ymin of this slice)
       ! and add geometrical tolerance
       
       target_val = ymin_meshfem + TOLERANCE_METERS
       if (yelm(1) < target_val .and. yelm(2) < target_val .and. yelm(5) < target_val .and. yelm(6) < target_val) &
            mpi_y_meshfem(1,ispec)=.true.
       
       ! right cut-plane in the current slice along Y = constant (Ymax of this slice)
       ! and add geometrical tolerance
       
       target_val = ymax_meshfem - TOLERANCE_METERS
       if (yelm(3) > target_val .and. yelm(4) > target_val .and. yelm(7) > target_val .and. yelm(8) > target_val) &
            mpi_y_meshfem(2,ispec)=.true.
       
    end do
    
  end subroutine get_boundary


end module meshfem_mod
