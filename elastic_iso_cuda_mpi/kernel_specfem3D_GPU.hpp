#ifndef __KERNEL_SPECFEM3D__HPP
#define __KERNEL_SPECFEM3D__HPP

#include "cuda_constants.cuh"

void predictor_CUDA(TC* __restrict__ p_displ, 
TC* __restrict__ p_veloc, 
TC* __restrict__ p_accel,
const TC dt, const TC dt_2, 
const TC dts2, 
const int n,
cudaStream_t compute_stream);

void corrector_CUDA( TC* __restrict__ p_veloc, 
const TC* __restrict__ p_accel, 
const TC dt_2, 
const int n,
cudaStream_t compute_stream);

void corrector_1_CUDA( TC* __restrict__ p_veloc, 
TC* __restrict__ p_accel,
const TC* __restrict__ p_invmass, 
const TC dt_2, 
const int n,
cudaStream_t compute_stream);

void apply_invmass_CUDA(TC* __restrict__ p_accel, 
const TC* __restrict__ p_invmass, 
const int n,
cudaStream_t compute_stream);

void compute_internal_forces_elastic_iso_3D_CUDA(
    TC* __restrict__ p_accel,
    const TC* __restrict__ p_displ,
    const TC* __restrict__ p_dxi_dx,
    const TC* __restrict__ p_dxi_dy,
    const TC* __restrict__ p_dxi_dz,
    const TC* __restrict__ p_deta_dx,
    const TC* __restrict__ p_deta_dy,
    const TC* __restrict__ p_deta_dz,
    const TC* __restrict__ p_dgamma_dx,
    const TC* __restrict__ p_dgamma_dy,
    const TC* __restrict__ p_dgamma_dz,
    const TC* __restrict__ p_kappa,
    const TC* __restrict__ p_mu,
    const TC* __restrict__ p_wgllwgll_xy,
    const TC* __restrict__ p_hprime_wgll_xx,
    const TC* __restrict__ p_hprime_xxT,
    const int* __restrict__ p_ibool,
    const int* __restrict__ p_phase_ispec_inner,
    const int NSPEC, 
    const int NB_PHASE_ISPEC,
    const int NSPEC_INNER, 
    const int NSPEC_OUTER, 
    const int IPHASE,
    cudaStream_t compute_stream);
     
void add_source_force_CUDA(
  TC* __restrict__ p_accel,
  const TC* __restrict__ p_Fx,
  const TC* __restrict__ p_Fy,
  const TC* __restrict__ p_Fz,
  const TC* __restrict__ p_hxis,
  const TC* __restrict__ p_hetas,
  const TC* __restrict__ p_hgammas,
  const TC* __restrict__ p_stf,
  const int* __restrict__ p_ibool, 
  const int* __restrict__ p_ispec_src,
  const int NSOURCES,
  const int NSTEP, 
  const int IT,
  cudaStream_t compute_stream);

void stacey_elastic_boundary_condition3D_CUDA(
   TC* __restrict__ p_accel,
   const TC* __restrict__ p_veloc,
   const TC* __restrict__ p_rho_vp,
   const TC* __restrict__ p_rho_vs,
   const TC* __restrict__ p_wstacey,
   const TC* __restrict__ p_boundary_normal,
   const int* __restrict__  p_index_gll,
   const int n,
   cudaStream_t compute_stream);

void store_seismogram_CUDA(
  TC* __restrict__ p_seismogram_d,
  const TC* __restrict__ p_displ,
  const TC* __restrict__ p_hxir,
  const TC* __restrict__ p_hetar,
  const TC* __restrict__ p_hgammar,
  const int* __restrict__ p_ispec_rec,
  const int* __restrict__ p_ibool,
  const int it,
  const int NREC,
  cudaStream_t compute_stream);

// for MPI communitcation 
void prepare_MPI_buffer_CUDA(TC* __restrict__ d_mpi_send_buf,
TC* __restrict__ h_mpi_send_buf,
const TC* __restrict__ pt_accel, 
const int* __restrict__ p_nibool_interfaces_ext_mesh,
const int* __restrict__ p_ibool_interfaces_ext_mesh, 
const int offset_ibool_interf,
const int nb_mpi_interfaces,
const int max_nibool_interfaces, 
const int size_mpi_buffer, 
cudaStream_t compute_stream,
cudaStream_t copy_stream);

void assemble_MPI_vector_CUDA(TC* __restrict__ p_accel, 
const TC* __restrict__  d_mpi_recv_buf, 
const int* __restrict__ p_nibool_interfaces_ext_mesh, 
const int* __restrict__ p_ibool_interfaces_ext_mesh,
const int offset_ibool_interf,
const int  nb_mpi_interfaces, 
const int  max_nibool_interfaces,
const int  size_mpi_buffer,
cudaStream_t compute_stream, 
cudaStream_t copy_stream);
  
#endif
